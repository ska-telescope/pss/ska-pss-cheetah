/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHFACTORY_H
#define SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHFACTORY_H

#include "cheetah/pipelines/search_pipeline/PipelineHandler.h"
#include "cheetah/pipelines/search_pipeline/detail/AccelerationSearchImpl.h"
#include "cheetah/pipelines/search_pipeline/detail/RfimAccelerationSearchImpl.h"
namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {

template<typename, typename>
class AccelerationSearch;

template<typename>
class CheetahConfig;

template<typename>
class BeamConfig;

/**
 * @brief Generate a suitable AccelerationSearch implementaion based on the configuration parameters
 * @details
 */

template<typename NumericalT, typename AccelerationSearchTraitsT>
class AccelerationSearchFactory
{
    public:
        AccelerationSearchFactory(CheetahConfig<NumericalT> const& config);
        ~AccelerationSearchFactory();

        PipelineHandler<NumericalT>* create(BeamConfig<NumericalT> const& beam_config) const;

    protected:
        template<template<typename> class RfimPolicyTempl>
        PipelineHandler<NumericalT>* create_policy(BeamConfig<NumericalT> const& beam_config) const;

    private:
        CheetahConfig<NumericalT> const& _config;
};


} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
#include "cheetah/pipelines/search_pipeline/detail/AccelerationSearchFactory.cpp"

#endif // SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHFACTORY_H
