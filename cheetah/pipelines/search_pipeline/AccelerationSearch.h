/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCH_H
#define SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCH_H

#include "cheetah/pipelines/search_pipeline/PipelineHandler.h"
#include "cheetah/pipelines/search_pipeline/AccelerationSearchFactory.h"
#include "cheetah/pipelines/search_pipeline/SinglePulseFactory.h"
#include "cheetah/pipelines/search_pipeline/Dedispersion.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {

template<typename NumericalT>
class CheetahConfig;

template<typename NumericalT>
class BeamConfig;

/* TODO description/ascii art description of the pipeline
 *
 * Define an AccelerationSearchTraits class to customise the AccelerationPipeline.
 *
 * #Traits Requirements
 *
 * ##The Dedispersion Pipeline
 *   The traits class must provide the following static method in order to generate a suitable dedisperion pipeline.
 *   @code
 *   static
 *   Dedispersion<NumericalT>* create_dedispersion_pipeline( CheetahConfig<NumericalT> const& cheetah_config
 *                                                        , BeamConfig<NumericalT> const& beam_config
 *                                                        , DmHandlerT dm_handler
 *                                                        )
 *  @endcode
 *  A default method using the cheetah::SinglePulse pipeline is available by inheriting from the DefaultAccelerationSearchTraits class.
*/

template<typename NumericalT>
struct DefaultAccelerationSearchTraits
{
    /**
     * @brief Default dedispersion is the cheetah SinglePulse pipeline
     */
    template<typename DmHandlerT>
    static inline
    Dedispersion<NumericalT>* create_dedispersion_pipeline( CheetahConfig<NumericalT> const& cheetah_config
                                                          , BeamConfig<NumericalT> const& beam_config
                                                          , DmHandlerT dm_handler
                                                          )
    {
        SinglePulseFactory<NumericalT> sp_factory(cheetah_config);
        return sp_factory.create(beam_config, dm_handler);
    }
};

/**
 * @brief An Acceleration Search Pipeline
 * @details Uses config parameters to construct a suitably optimised Acceleration Search pipeline
 */

template<typename NumericalT, typename AccelerationSearchTraitsT>
class AccelerationSearch: public PipelineHandler<NumericalT>
{
	typedef PipelineHandler<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;

    public:
        AccelerationSearch(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const& beam_config);
        AccelerationSearch(AccelerationSearch&&);
        ~AccelerationSearch();

        void operator()(TimeFrequencyType&);

    private:
        std::unique_ptr<PipelineHandler<NumericalT>> _accimpl;
};


} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
#include "cheetah/pipelines/search_pipeline/detail/AccelerationSearch.cpp"

#endif // SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCH_H
