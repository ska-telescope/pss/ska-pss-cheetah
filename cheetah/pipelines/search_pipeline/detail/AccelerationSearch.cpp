/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipelines/search_pipeline/AccelerationSearchFactory.h"

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::AccelerationSearch(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const& beam_config)
    : BaseT(config, beam_config)
{
    AccelerationSearchFactory<NumericalT, AccelerationSearchTraitsT> factory(config);
    _accimpl.reset(factory.create(beam_config));
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::AccelerationSearch(AccelerationSearch&& acc_search)
    : _accimpl(acc_search._accimpl)
{
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::~AccelerationSearch()
{
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
inline void AccelerationSearch<NumericalT, AccelerationSearchTraitsT>::operator()(TimeFrequencyType& data)
{
    _accimpl->operator()(data);
}

} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
