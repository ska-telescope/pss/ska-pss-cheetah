/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipelines/search_pipeline/AccelerationSearchFactory.h"
#include "cheetah/pipelines/search_pipeline/detail/AccelerationSearchImpl.h"
#include "cheetah/pipelines/search_pipeline/detail/RfimAccelerationSearchImpl.h"
#include "cheetah/modules/rfim/policy/FlagPolicy.h"
#include "cheetah/modules/rfim/policy/LastUnflagged.h"


namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {


template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearchFactory<NumericalT, AccelerationSearchTraitsT>::AccelerationSearchFactory(CheetahConfig<NumericalT> const& config)
    : _config(config)
{
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
AccelerationSearchFactory<NumericalT, AccelerationSearchTraitsT>::~AccelerationSearchFactory()
{
}

template<typename NumericalT, typename  AccelerationSearchTraitsT>
template<template<typename> class RfimPolicyTempl>
PipelineHandler<NumericalT>* AccelerationSearchFactory<NumericalT, AccelerationSearchTraitsT>::create_policy(BeamConfig<NumericalT> const& beam_config) const
{
    if(!_config.rfim_config().algo_defined())
    {
        return new AccelerationSearchImpl<NumericalT, AccelerationSearchTraitsT>(_config, beam_config);
    }
    else
    {
        return new RfimAccelerationSearchImpl<NumericalT, AccelerationSearchTraitsT, RfimPolicyTempl>(_config, beam_config);
    }
}

template<typename NumericalT, typename AccelerationSearchTraitsT>
PipelineHandler<NumericalT>* AccelerationSearchFactory<NumericalT, AccelerationSearchTraitsT>::create(BeamConfig<NumericalT> const& beam_config) const
{
    if(!_config.rfim_config().algo_defined())
    {
        return new AccelerationSearchImpl<NumericalT, AccelerationSearchTraitsT>(_config, beam_config);
    }
    else
    {
        if(_config.rfim_config().flag_policy())
	{
            return create_policy<modules::rfim::FlagPolicy>(beam_config);
        }
        else
	{
            return create_policy<modules::rfim::LastUnflagged>(beam_config);
        }
    }
}

} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
