/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHIMPL_H
#define SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHIMPL_H
#include "cheetah/pipelines/search_pipeline/AccelerationSearchFactory.h"
#include "cheetah/pipelines/search_pipeline/SinglePulseFactory.h"
#include "cheetah/pipelines/search_pipeline/Dedispersion.h"
#include "cheetah/pipelines/search_pipeline/AccelerationSearchAlgoConfig.h"
#include "cheetah/pipelines/search_pipeline/detail/FldoHandler.h"
#include "cheetah/modules/psbc/Psbc.h"
#include "cheetah/modules/sift/Sift.h"
#include "cheetah/modules/fldo/Fldo.h"
#include <panda/DataSwitch.h>
#include <memory>

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {

/**
 * @class AccelerationSearchImpl
 * @brief
 *    The acceleration search pipeline
 *
 * @details
 * TODO description/ascii art description of the pipeline
 *
 * Define an AccelerationSearchImplTraits class to customise the AccelerationPipeline.
 *
 * ##The Acceleration Search Pipeline
 * The AccelerationSearchImplTraits class must provide a static method to create the acceleration search part of the pipeline (i.e post-Dedispersion).
 * This method will return a data type based on whether you are configuring it for a time-domain (Tdas) or frequency-domain (Fdas) acceleration search.
 * The AccelerationSearchImplAlgoConfig provides access to both the Fdas and Tdas configs.
 * The templated PostAccelerationSearchImplHandler is the handler for the Sift module, which is called upon completion of the acceleration search pipeline.
 * @code
 * template<typename PostAccelerationSearchImplHandler>
 * static
 * MyAlgoType* create_acceleration_search_algo(AccelerationSearchImplAlgoConfig const& configuration, PostAccelerationSearchImplHandler const& handler);
 * @endcode
 *
 * #Traits optional methods:
 *
 * ##The FLDO handler
 *    Post folding (fldo) processing. By default fldo output is passed to the pipelines data export module under the channel name "ocld".
 *    An alternative fldo handler can be specified by adding a static
 *    @code
 *         NewHandlerType* create_fldo_handler(DataExport<NumericT>&, CheetahConfig<NumericalT> const&, BeamConfig<NumericalT> const&);
 *    @endcode
 *    to the AccelerationSearchImplTraits class.
 */





template<typename NumericalT, typename AccelerationSearchTraitsT>
class AccelerationSearchImpl : public PipelineHandler<NumericalT>
{
    private:
        typedef PipelineHandler<NumericalT> BaseT;
        typedef typename panda::is_pointer_wrapper<typename Dedispersion<NumericalT>::DmTrialType>::type DmTrialsType;

    public:
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;
        typedef AccelerationSearchTraitsT Traits;
        typedef NumericalT value_type;

    public:
        AccelerationSearchImpl(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const&);
        AccelerationSearchImpl(AccelerationSearchImpl const&) = delete;
        virtual ~AccelerationSearchImpl();

        /**
         * @brief called whenever data is available for processing
         */
        void operator()(TimeFrequencyType&) override;

    public:
        /**
         * @brief  template for testing if type T has a create_fldo_handler function
         * @details  Use with panda::HasMethod. e.g.
         * @code
         *    static bool has_method = panda::HasMethod<T, has_create_fldo_handler_t>::value;
         * @endcode
         */
        template<typename T>
        using has_create_fldo_handler_t = decltype(std::declval<T&>().create_fldo_handler(std::declval<DataExport<value_type>&>()
                                                                                        , std::declval<CheetahConfig<NumericalT> const&>()
                                                                                        , std::declval<BeamConfig<NumericalT> const&>()));
    private:
        typedef detail::FldoHandler<AccelerationSearchImpl<NumericalT, AccelerationSearchTraitsT>, NumericalT> FldoHandler;
        friend FldoHandler;

        struct SiftHandler {
            public:
                SiftHandler(AccelerationSearchImpl&);
                void operator()(std::shared_ptr<data::Scl>) const;

            private:
                AccelerationSearchImpl& _pipeline;
        };

        typedef modules::sift::Sift<SiftHandler> SiftType;
        typedef typename std::remove_pointer<decltype(std::declval<AccelerationSearchTraitsT>().create_acceleration_search_algo(
                                                                                                    std::declval<AccelerationSearchAlgoConfig const&>()
                                                                                                  , std::declval<SiftType&>()
                                                                                                )
                                                     )>::type AccelerationSearchAlgoType;
    public:
        /**
         * @brief access to the dedispersion pipeline object
         */
        Dedispersion<NumericalT> const& dedispersion_pipeline() const;

        /**
         * @brief access the fldo handler object
         */
        typename FldoHandler::FldoHandlerType const& fldo_handler() const;

        /**
         * @brief access the acceleration_search pipeline object
         */
        AccelerationSearchAlgoType const& acceleration_search_pipeline() const;

	/**
	 * @brief access to the shared_ptr of tf data
	 */
	std::vector<std::shared_ptr<TimeFrequencyType>>& tf_data();

    private:
        CheetahConfig<NumericalT> const& _config;
        BeamConfig<NumericalT> const& _beam_config;
        std::vector<std::shared_ptr<TimeFrequencyType>> _tf_data; // required by fldo_handler destructor
        FldoHandler _fldo_handler;
        modules::fldo::Fldo<FldoHandler, NumericalT> _fldo;
        SiftHandler _sift_handler;
        SiftType _sift;
        std::unique_ptr<AccelerationSearchAlgoType> _acceleration_search;
        modules::psbc::Psbc<typename decltype(_acceleration_search)::element_type> _psbc;
        ska::panda::DataSwitch<DmTrialsType> _dm_switch; // need to be able to switch pipelines on/off at runtime
        std::unique_ptr<Dedispersion<NumericalT>> _dedisperser;
};

} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska

#include "cheetah/pipelines/search_pipeline/detail/AccelerationSearchImpl.cpp"

#endif // SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHIMPL_H
