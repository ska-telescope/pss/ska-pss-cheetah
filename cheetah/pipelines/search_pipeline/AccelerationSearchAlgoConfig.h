/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHALGOCONFIG_H
#define SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHALGOCONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/tdas/Config.h"

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {

/**
 * @brief
 *    Configuration parameters for the available acceleration searches
 */

class AccelerationSearchAlgoConfig : public utils::Config
{
        typedef utils::Config BaseT;
        typedef BaseT::PoolManagerType PoolManagerType;

    public:
        AccelerationSearchAlgoConfig(PoolManagerType&);
        ~AccelerationSearchAlgoConfig();

        /**
         * @brief return the fdas module-specific configuration parameters
         */
        modules::fdas::ConfigType const& fdas_config() const;
        modules::fdas::ConfigType& fdas_config();

        /**
         * @brief return the tdas module-specific configuration parameters
         */
        modules::tdas::ConfigType const& tdas_config() const;
        modules::tdas::ConfigType& tdas_config();

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        modules::fdas::ConfigType _fdas_config;
        modules::tdas::ConfigType _tdas_config;

};

} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_PIPELINES_SEARCH_PIPELINE_ACCELERATIONSEARCHALGOCONFIG_H
