/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipelines/search_pipeline/test/AccelerationSearchFactoryTest.h"
#include "cheetah/pipelines/search_pipeline/test/AccelerationSearchTest.h"
#include "cheetah/pipelines/search_pipeline/AccelerationSearchFactory.h"
#include "cheetah/pipelines/search_pipeline/CheetahConfig.h"
#include "cheetah/pipelines/search_pipeline/BeamConfig.h"
#include "cheetah/modules/rfim/iqrmcpu/Config.h"
#include "cheetah/modules/rfim/policy/LastUnflagged.h"
#include "cheetah/pipelines/search_pipeline/TdasAccelerationSearchTraits.h"
#include "panda/test/TestFile.h"

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {
namespace test {


AccelerationSearchFactoryTest::AccelerationSearchFactoryTest()
: ::testing::Test()
{
}

AccelerationSearchFactoryTest::~AccelerationSearchFactoryTest()
{
}

void AccelerationSearchFactoryTest::SetUp()
{
}

void AccelerationSearchFactoryTest::TearDown()
{
}

TEST_F(AccelerationSearchFactoryTest, test_factory_constructor)
{
    search_pipeline::CheetahConfig<uint8_t> config;
    search_pipeline::AccelerationSearchFactory<uint8_t, TestAccelerationSearchTraits<uint8_t>> factory(config);
}

/*TEST_F(AccelerationSearchFactoryTest, test_create)
{
    search_pipeline::CheetahConfig<uint8_t> config;
    BeamConfig<uint8_t> beam_config;
    search_pipeline::AccelerationSearchFactory<uint8_t, TestAccelerationSearchTraits<uint8_t>> factory(config);

    // Configure high-density low DMs
    modules::ddtr::DedispersionConfig dd_config_low;
    config.sps_config().ddtr_config().dedispersion_config(dd_config_low);
    dd_config_low.dm_start(modules::ddtr::DedispersionConfig::Dm(0.0 * data::parsecs_per_cube_cm));
    dd_config_low.dm_end(modules::ddtr::DedispersionConfig::Dm(100.0 * data::parsecs_per_cube_cm));
    dd_config_low.dm_step(modules::ddtr::DedispersionConfig::Dm(0.1 * data::parsecs_per_cube_cm));


    // Set sps priority
    config.sps_config().set_priority(0);

    // Set the size of the dedispersion buffer
    config.sps_config().ddtr_config().dedispersion_samples(1<<18);

    // Set the psbc buffer size via the dump_time in the psbc config, i.e. psbc will flush its buffer after this amount of time
    config.psbc_config().dump_time(0.0001 * data::seconds);

    // Select the sift algo
    config.sift_config().template config<modules::sift::simple_sift::Config>().active(true);

    // Select the fldo algo
    config.fldo_config().template config<modules::fldo::cuda::Config>().active(true);

    auto pipeline = static_cast<AccelerationSearchImpl<uint8_t, TestAccelerationSearchTraits<uint8_t>>*>(factory.create(beam_config));

    // check the return type
    bool val1 = std::is_same<AccelerationSearchImpl<uint8_t, TestAccelerationSearchTraits<uint8_t>>*,decltype(pipeline)>::value;
    ASSERT_EQ(val1, true);

    auto const& acceleration_search_pipeline_object = pipeline->acceleration_search_pipeline();
    //typename TestAccelerationSearchTraits<uint8_t>::TestHandler const& dedispersion_pipeline_object
		//= static_cast<typename TestAccelerationSearchTraits<uint8_t>::TestHandler const&>(pipeline->dedispersion_pipeline());
    //typename TestAccelerationSearchTraits<uint8_t>::FldoTestHandler const& fldo_pipeline_object = pipeline->fldo_handler();

    acceleration_search_pipeline_object.wait_sift_handler_called();
    //dedispersion_pipeline_object.wait_dedispersion_handler_called();
    //fldo_pipeline_object.wait_fldo_handler_called();



    // Now lets toggle some RFIM algorithm
    char cmd[] = "cheetah_test";
    char config_option[] = "--config";
    panda::test::TestFile config_file("%%%%-%%%%-%%%%-%%%%.xml");

    char config_filename[32];
    strcpy(config_filename, config_file.filename().c_str());
    char* argv[3] = { cmd, config_option, config_filename };

    // add some content
    config_file << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    config_file << "<cheetah>\n";
    config_file << "<pipeline>" << "test_b" << "</pipeline>\n";
    config_file << "<rfim>\n";
    config_file << "<rfim_iqrmcpu>\n";
    config_file << "<active>" << "true" << "</active>\n";
    config_file << "</rfim_iqrmcpu>\n";
    config_file << "</rfim>\n";
    config_file << "</cheetah>\n";
    config_file.flush();

    // recreate the cheetah config and check the returned factory type
    config.parse(3, argv);

    // Set the psbc buffer size via the dump_time in the psbc config, i.e. psbc will flush its buffer after this amount of time
    config.psbc_config().dump_time(0.0001 * data::seconds);

    // Select the sift algo
    config.sift_config().template config<modules::sift::simple_sift::Config>().active(true);

    // Select the fldo algo
    config.fldo_config().template config<modules::fldo::cuda::Config>().active(true);

    search_pipeline::AccelerationSearchFactory<uint8_t, TdasAccelerationSearchTraits<uint8_t>> factory2(config);
    auto pipeline2 = static_cast<RfimAccelerationSearchImpl<uint8_t, TdasAccelerationSearchTraits<uint8_t>, modules::rfim::LastUnflagged>*>(factory2.create(beam_config));
    bool val2 = std::is_same<RfimAccelerationSearchImpl<uint8_t, TdasAccelerationSearchTraits<uint8_t>, modules::rfim::LastUnflagged>*, decltype(pipeline2)>::value;
    ASSERT_EQ(val2, true);
}*/

} // namespace test
} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
