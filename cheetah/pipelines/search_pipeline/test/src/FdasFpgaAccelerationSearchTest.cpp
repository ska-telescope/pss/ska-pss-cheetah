/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipelines/search_pipeline/test/FdasFpgaAccelerationSearchTest.h"
#include "cheetah/pipelines/search_pipeline/AccelerationSearch.h"
#include "cheetah/pipelines/search_pipeline/FdasAccelerationSearchTraits.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/FrequencyTime.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/modules/ddtr/DedispersionConfig.h"
#include "cheetah/generators/SimplePulsar.h"
#include "cheetah/generators/SimplePulsarConfig.h"
#include "cheetah/generators/PulsarInjection.h"
#include "cheetah/generators/PulsarInjectionConfig.h"
#include "cheetah/generators/SimplePhaseModelConfig.h"
#include "panda/test/gtest.h"
#include "panda/test/TestFile.h"
#include <memory>
#include <chrono>
#include <thread>

namespace ska {
namespace cheetah {
namespace pipelines {
namespace search_pipeline {
namespace test {

FdasFpgaAccelerationSearchTest::FdasFpgaAccelerationSearchTest()
    : ::testing::Test()
{
}

FdasFpgaAccelerationSearchTest::~FdasFpgaAccelerationSearchTest()
{
}

void FdasFpgaAccelerationSearchTest::SetUp()
{
}

void FdasFpgaAccelerationSearchTest::TearDown()
{
}

template<typename NumericalT>
struct FdasFpgaAccelerationSearchTests
{
    static void run()
    {
        // Search ~60 seconds of data at standard resoluton
        float total_time = 81.0;
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef typename TimeFrequencyType::DataType DataType;

        // Preparation of pipeline configuration components
        CheetahConfig<NumericalT> config;

        char cmd[] = "cheetah_test";
        char config_option[] = "--config";
        panda::test::TestFile config_file("%%%%-%%%%-%%%%-%%%%.xml");
        char config_filename[32];
        strcpy(config_filename, config_file.filename().c_str());
        char* argv[3] = { cmd, config_option, config_filename };

        // Set up via xml intel_fpga implementation specific configuration arguments
        config_file << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        config_file << "<cheetah>\n";
        config_file << "<acceleration>\n";
        config_file << "<fdas>\n";
        config_file << "<intel_fpga>\n";
        config_file << "<active>" << "true" << "</active>\n";
        config_file << "<filters_filename>" << "/opt/pss-fdas-fpga/etc/filter.dat" << "</filters_filename>\n";
        config_file << "<hsum_filename>" << "/opt/pss-fdas-fpga/etc/config_60_sec.txt" << "</hsum_filename>\n";
        config_file << "<number_of_filters>" << "42" << "</number_of_filters>\n";
        config_file << "</intel_fpga>\n";
        config_file << "</fdas>\n";
        config_file << "</acceleration>\n";
        config_file << "</cheetah>\n";
        config_file.flush();
        // recreate the cheetah config and check the returned factory type
        config.parse(3, argv);


        // Configure DMs
        config.ddtr_config().add_dm_range(modules::ddtr::DedispersionConfig::Dm(100.0 * data::parsecs_per_cube_cm)
                                  , modules::ddtr::DedispersionConfig::Dm(300 * data::parsecs_per_cube_cm)
                                  , modules::ddtr::DedispersionConfig::Dm(10.0 * data::parsecs_per_cube_cm));

        config.ddtr_config().dedispersion_samples(1<<18);
        config.ddtr_config().cpu_algo_config().active(true);
        config.sps_config().cpu_config().activate();

        // Set sps priority
        config.sps_config().set_priority(0);

        // Set the size of the dedispersion buffer
        config.sps_config().ddtr_config().dedispersion_samples(1<<18);

        // Set the psbc config
        config.psbc_config().dump_time((total_time-20.0) * data::seconds);

        // Set fdas priority
        config.acceleration_search_config().fdas_config().set_priority(1);

        // select the sift algo
        config.sift_config().template config<modules::sift::simple_sift::Config>().active(true);

        // select the fldo algo
        config.fldo_config().template config<modules::fldo::cpu::Config>().active(true);

        BeamConfig<NumericalT> beam_config;
        typedef AccelerationSearch<NumericalT, FdasAccelerationSearchTraits<NumericalT>> AccelerationSearchType;
        AccelerationSearchType pipeline(config, beam_config);

        // Set up pulsar parameters for data to be passed through the pipeline
        generators::SimplePulsarConfig pulsar_config;
        pulsar_config.mean(96.0);
        pulsar_config.std_deviation(10.0);
        pulsar_config.dm(200.0*data::parsecs_per_cube_cm);
        pulsar_config.period(0.005*data::seconds);
        pulsar_config.width(0.001*data::seconds);
        pulsar_config.snr(0.5);
        generators::SimplePulsar<TimeFrequencyType> pulsar(pulsar_config);
        // Start epoch
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(50000.0));

        data::DimensionSize<data::Time> number_of_samples(1024);
        data::DimensionSize<data::Frequency> number_of_channels(16);
        double tsamp_us = 64.0;
        double f_low = 600.0;
        double f_step = 330.0/(float)number_of_channels;
        std::size_t total_nsamps = std::size_t(total_time / (tsamp_us * 1e-6));

        std::size_t loop_count = total_nsamps/number_of_samples + 10;

        for (std::size_t ii=0; ii<loop_count; ++ii)
        {
            auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
            auto f1 =  typename TimeFrequencyType::FrequencyType((f_low+f_step*number_of_channels) * boost::units::si::mega * boost::units::si::hertz);
            auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::mega * boost::units::si::hertz);
            auto delta = (f2 - f1)/ (double)number_of_channels;
            tf->set_channel_frequencies_const_width( f1, delta );
            tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
            tf->start_time(epoch);
            epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
            pulsar.next(*tf);
            pipeline(*tf);
        }

    }
};

TEST_F(FdasFpgaAccelerationSearchTest, run_fdas_fpga_uint8)
{
#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
    FdasFpgaAccelerationSearchTests<uint8_t>::run();
#endif
}



} // namespace test
} // namespace search_pipeline
} // namespace pipelines
} // namespace cheetah
} // namespace ska
