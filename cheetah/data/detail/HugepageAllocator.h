/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DATA_HUGEPAGEALLOCATOR_H
#define SKA_CHEETAH_DATA_HUGEPAGEALLOCATOR_H

#include "cheetah/data/detail/HugepageAllocatorBase.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace data {

/**
 * @brief      Specific implementation of Hugepage Allocator
 *
 * @tparam     Arch  The architecture
 * @tparam     T     The value type
 */
template <typename T,typename Arch>
class HugepageAllocator: public HugepageAllocatorBase<T,HugepageAllocator<T,Arch>>
{
};

/**
 * @brief      A CPU-based HugepageAllocator implementation
 *
 * @tparam     T     The value type
 */
template <typename T>
class HugepageAllocator<T,cheetah::Cpu>: public HugepageAllocatorBase<T,HugepageAllocator<T,cheetah::Cpu>>
{
    public:
        template<typename OtherT>
        //rebind alloctor to type OtherT
        struct rebind
        {
            typedef HugepageAllocator<OtherT, Cpu> other;
        };
        typedef Cpu Architecture;
        typedef T value_type;

    public:
        /**
        * @brief      Create a new instance
         */
        HugepageAllocator(){};

        /**
         * @brief      Delete existing instance
         */
        ~HugepageAllocator(){};
};

} // namespace data
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DATA_HUGEPAGEALLOCATOR_H
