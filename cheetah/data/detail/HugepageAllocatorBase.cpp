/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/detail/HugepageAllocatorBase.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace data {

template <typename T, typename Arch>
HugepageAllocatorBase<T,Arch>::HugepageAllocatorBase()
    : _hugepage(std::make_shared<intel_fpga::HugepageHandler>())
    , _current_allocated_size{0}
    , _base_hugepage_address{nullptr}
    , _current_hugepage_address{nullptr}
{
}

template <typename T, typename Arch>
HugepageAllocatorBase<T,Arch>::~HugepageAllocatorBase()
{
}

template <typename T, typename Arch>
T* HugepageAllocatorBase<T,Arch>::allocate(std::size_t num_elements)
{
    // std::lock_guard<std::mutex> lock(_mutex);

    // verify the hugepage area has been created
    // otherwise 1GB page will be created
    if (_hugepage->page_address() == nullptr)
    {
        // creating 1GB hugepage
        _base_hugepage_address = _hugepage->create_hugepage();
        if(_base_hugepage_address == nullptr)
            throw std::runtime_error("Unable to create hugepage");

        // initialise current pointer with base address
        _current_hugepage_address = _base_hugepage_address;
    }

    // Calculate required alignment
    std::size_t alignment = alignof(T);

    // size of the given type that will be allocated
    std::size_t _allocated_size_requested = num_elements * sizeof(T);

    // Check that the size of the new allocation
    // is not larger than the maximum size.
    if (_allocated_size_requested + _current_allocated_size > _default_hugepage_size )
    {
        // not sure if throwing an exception is ok to handle memory overflow
        throw std::bad_alloc();
    }

    // allocate space required
    char* aligned_ptr = reinterpret_cast<char*>(_base_hugepage_address) + _current_allocated_size;
    _current_hugepage_address = reinterpret_cast<char*>(reinterpret_cast<std::uintptr_t>(aligned_ptr) & ~(alignment - 1));

    // reserve space requested
    _current_allocated_size += _allocated_size_requested;

    return reinterpret_cast<T*>(_current_hugepage_address);

}

// We are using a huge 1GB pre-allocated page, so deallocating
// will only move the current pointer to a lower position given
// by the requested deallocation size.
template <typename T, typename Arch>
void HugepageAllocatorBase<T,Arch>::deallocate([[maybe_unused]]T *ptr, std::size_t n)
{
    // size to deallocate given the type
    std::size_t _deallocated_size_requested = n * sizeof(T);

    // Calculate required alignment
    std::size_t alignment = alignof(T);

    // we are using a preallocated huge page,
    // so we are just reducing the size requested from the total amount
    if (_current_allocated_size < _deallocated_size_requested)
    {
        _current_allocated_size = 0;
        _current_hugepage_address = _base_hugepage_address;
    }
    else
    {
        _current_allocated_size -= _deallocated_size_requested;

        // move the current pointer to a lower  position
        char* aligned_ptr = reinterpret_cast<char*>(_base_hugepage_address) + _current_allocated_size;
        _current_hugepage_address = reinterpret_cast<char*>(reinterpret_cast<std::uintptr_t>(aligned_ptr) & ~(alignment - 1));

    }
}

template <typename T, typename Arch>
void* HugepageAllocatorBase<T,Arch>::hugepage_address() const
{
    return _hugepage->page_address();
}

} // namespace data
} // namespace cheetah
} // namespace ska
