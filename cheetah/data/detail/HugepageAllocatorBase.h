/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DATA_HUGEPAGEALLOCATORBASE_H
#define SKA_CHEETAH_DATA_HUGEPAGEALLOCATORBASE_H

#include <mutex>
#include "cheetah/data/intel_fpga/HugepageHandler.h"

namespace ska {
namespace cheetah {
namespace data {

/**
 * @brief      Base class for Hugepage Allocator implementations.
 *
 * @tparam     DerivedType  { description }
 * @tparam     T            { description }
 */
template <typename T, typename DerivedType>
class HugepageAllocatorBase : public std::allocator<T>
{
    public:
        typedef T           value_type;
        typedef size_t      size_type;
        typedef ptrdiff_t   difference_type;
        typedef T*          pointer;
        typedef const T*    const_pointer;
        typedef T&          reference;
        typedef const T&    const_reference;
        // rebind allocator to type U
        template <class U>
        struct rebind
        {
           typedef HugepageAllocatorBase<U,DerivedType> other;
        };
    public:
        /**
         * @brief      Create a new instance
         */
        HugepageAllocatorBase();

        /**
         * @brief      Delete existing instance
         */
        ~HugepageAllocatorBase();

        /**
         * @brief      Allocate memory
         *
         * @param[in]  num_elements  The number elements to be allocate of type T
         *
         * @return     A pointer to the allocated memory
         */
        T* allocate(std::size_t num_elements);

        /**
         * @brief      Deallocate memory
         *
         * @param[in]  ptr   Pointer to the memory to be deallocated
         * @param[in]  n     The number of elements of type T to be deallocated
         */
        void deallocate(T *ptr, std::size_t /*n*/);

        /**
         * @brief virtual address of hugepage
        */
        void* hugepage_address() const;
    protected:

        /**
         * @brief smart pointer to a hugepage handler on the CPU architecture
         */
        std::shared_ptr<intel_fpga::HugepageHandler> _hugepage;

        /**
         * @brief holds the total amount of memory allocated
         */
        std::size_t _current_allocated_size;

        /**
         * @brief provides the starting memory address of the huge page
         */
        void* _base_hugepage_address;

        /**
         * @brief provides the current memory address of the huge page
         *        after multiple allocations
         */
        void* _current_hugepage_address;

        // std::mutex _mutex;
        static constexpr size_t _default_hugepage_size{1<<30};
};

} // namespace data
} // namespace cheetah
} // namespace ska

#include "cheetah/data/detail/HugepageAllocatorBase.cpp"

#endif // SKA_CHEETAH_DATA_HUGEPAGEALLOCATORBASE_H
