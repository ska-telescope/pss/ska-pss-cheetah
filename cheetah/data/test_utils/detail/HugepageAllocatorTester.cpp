/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test_utils/HugepageAllocatorTester.h"
#include "cheetah/data/detail/HugepageAllocator.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

template <typename HugepageAllocatorTestTraitsType>
HugepageAllocatorTest<HugepageAllocatorTestTraitsType>::HugepageAllocatorTest()
    : ::testing::Test()
{
}

template <typename HugepageAllocatorTestTraitsType>
HugepageAllocatorTest<HugepageAllocatorTestTraitsType>::~HugepageAllocatorTest()
{
}


template <typename HugepageAllocatorTestTraitsType>
void HugepageAllocatorTest<HugepageAllocatorTestTraitsType>::SetUp()
{
}

template <typename HugepageAllocatorTestTraitsType>
void HugepageAllocatorTest<HugepageAllocatorTestTraitsType>::TearDown()
{
}

template <typename TypeParam>
void AllocateDeallocateTest<TypeParam>::test()
{
    HugepageAllocator<typename TypeParam::ValueType, typename TypeParam::Architecture> hugepage;
    auto* a = hugepage.allocate(1000);
    auto* b = hugepage.allocate(20);
    hugepage.deallocate(a,1000);
    auto* c = hugepage.allocate(4444);
    hugepage.deallocate(b,20);
    hugepage.deallocate(c,4444);

}

template <typename TypeParam>
void AllocateVectorTest<TypeParam>::test()
{
    typedef typename TypeParam::ValueType ValueType;
    typedef typename TypeParam::Architecture Arch;
    typedef HugepageAllocator<ValueType, Arch> Alloc;

    std::vector<ValueType, Alloc> _vector;
    _vector.emplace_back((ValueType)1);
    _vector.emplace_back((ValueType)2);
    ASSERT_EQ(_vector.size(),2);

}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
