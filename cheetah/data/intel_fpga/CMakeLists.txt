set(MODULE_INTEL_FPGA_LIB_SRC_INTEL_FPGA PARENT_SCOPE)

set(MODULE_INTEL_FPGA_LIB_SRC_CPU
    src/HugepageHandler.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

add_subdirectory(test)
