
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/DataTypeTraits.h"
#include "panda/Copy.h"


namespace ska {
namespace cheetah {
namespace data {

template <typename ValueType, typename Alloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series()
    : BaseT()
{
}

template <typename ValueType, typename Alloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series(Allocator const& allocator)
    : BaseT(allocator)
{
}

template <typename ValueType, typename Alloc>
template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series(Series<OtherArch, OtherValueT, OtherAlloc> const& copy)
    : BaseT(copy.size(), ValueType(), Allocator())
{
    panda::copy(copy.begin(), copy.end(), this->begin());
}

template <typename ValueType, typename Alloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series(std::size_t size, Allocator const& allocator)
    : BaseT(size, ValueType(), allocator)
{
}

template <typename ValueType, typename Alloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series(Series const& copy)
    : BaseT(copy.size(), ValueType(), copy.allocator())
{
    panda::copy(copy.begin(), copy.end(), this->begin());
}

template <typename ValueType, typename Alloc>
template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
Series<cheetah::IntelFpga, ValueType, Alloc>::Series(Series<OtherArch, OtherValueT, OtherAlloc> const& copy, Allocator const& allocator)
    : BaseT(copy.size(), ValueType(), allocator)
{
    panda::copy(copy.begin(), copy.end(), this->begin());
}

template <typename ValueType, typename Alloc>
Series<cheetah::IntelFpga, ValueType, Alloc>& Series<cheetah::IntelFpga, ValueType, Alloc>::operator=(Series const& copy)
{
    this->resize(copy.size());
    panda::copy(copy.begin(), copy.end(), this->begin());
    return *this;
}

template <typename ValueType, typename Alloc>
template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
Series<cheetah::IntelFpga, ValueType, Alloc>& Series<cheetah::IntelFpga, ValueType, Alloc>::operator=(Series<OtherArch, OtherValueT, OtherAlloc> const& copy)
{
    this->resize(copy.size());
    panda::copy(copy.begin(), copy.end(), this->begin());
    return *this;
}

} //namespace data
} //namespace cheetah
} //namespace ska