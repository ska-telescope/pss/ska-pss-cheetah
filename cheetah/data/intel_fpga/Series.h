/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_DATA_INTEL_FPGA_SERIES_H
#define SKA_CHEETAH_DATA_INTEL_FPGA_SERIES_H

#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA

#include "cheetah/data/Series.h"
#include "cheetah/data/detail/HugepageAllocator.h"
#include "panda/arch/intel_agilex_fpga/DeviceMemory.h"
#include "panda/arch/intel_agilex_fpga/DeviceCopy.h"

namespace ska {
namespace cheetah {
namespace data {


/**
 * @brief specialisation of the Series class for IntelFpga
 */

template <typename ValueType, typename Alloc>
class Series<cheetah::IntelFpga, ValueType, Alloc>: public VectorLike<std::vector<ValueType, Alloc>>
{
        typedef VectorLike<std::vector<ValueType, Alloc>> BaseT;

    public:
        typedef cheetah::IntelFpga Architecture;
        typedef Alloc Allocator;
        typedef ValueType value_type;

    public:
        Series();
        Series(Allocator const&);
        Series(std::size_t size, Allocator const&);
        Series(Series const&);

        template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
        Series(Series<OtherArch, OtherValueT, OtherAlloc> const& copy);

        /**
         * @brief copy constructor. Series data transfered from Series data on a device
         */
        template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
        Series(Series<OtherArch, OtherValueT, OtherAlloc> const& copy, Allocator const& allocator);

        /**
         * @brief assignment operator
         */
        Series& operator=(Series const&);

        /**
         * @brief assignment operator
         */
        template<typename OtherArch, typename OtherValueT, typename OtherAlloc>
        Series& operator=(Series<OtherArch, OtherValueT, OtherAlloc> const&);
};


} // namespace data
} // namespace cheetah
} // namespace ska

#include "cheetah/data/intel_fpga/detail/Series.cpp"

#endif //SKA_CHEETAH_ENABLE_INTEL_FPGA

#endif // SKA_CHEETAH_DATA_INTEL_FPGA_SERIES_H
