/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "OcldFileStreamerTraits.h"
namespace ska {
namespace cheetah {
namespace io {
namespace exporters {

OcldFileStreamerTraits::OcldFileStreamerTraits()
{
}

OcldFileStreamerTraits::~OcldFileStreamerTraits()
{
}

void OcldFileStreamerTraits::write(std::ostream& os, data::Ocld const& data)
{
    for(typename data::Ocld::BaseT::ValueType const& cand : data) {
        os << cand.metadata().period()
           << "\t" << cand.metadata().pdot() << "\t"
           << cand.metadata().dm()
           << "\t" << cand.metadata().width() << "\t"
           << cand.metadata().sigma() << "\n";

        os << "START";
	for(std::size_t ii=0; ii < cand.number_of_subints(); ++ii)
        {
	    for(std::size_t jj = 0; jj < cand.number_of_subbands(); ++jj)
            {
	        auto subband =  cand.subint(ii).slice(data::DimensionSpan<data::Frequency>(
                                                   data::DimensionIndex<data::Frequency>(jj)
                                                   , data::DimensionSize<data::Frequency>(1)
                                                  )
                                              , data::DimensionSpan<pss::astrotypes::units::PhaseAngle>(
                                                   cand.subint(ii).template dimension<pss::astrotypes::units::PhaseAngle>()
                                                  )
                                             );
		auto begin = subband.begin();
		auto end = subband.end();
	        os.write(reinterpret_cast<const char*>(&(*begin)), std::distance(begin, end) * sizeof(float));
	    }
	}
        os << "END";
    }
}

} // namespace exporters
} // namespace io
} // namespace cheetah
} // namespace ska
