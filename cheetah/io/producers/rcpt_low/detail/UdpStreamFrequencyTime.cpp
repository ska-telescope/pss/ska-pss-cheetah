/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace io {
namespace producers {
namespace rcpt_low {

template<typename Producer>
UdpStreamFrequencyTimeTmpl<Producer>::UdpStreamFrequencyTimeTmpl(Config const& config)
    : BaseT(config.engine(), ConnectionTraits::SocketType(static_cast<panda::Engine&>(config.engine()), config.remote_end_point()))
    , _n_channels(PssLowTraits::number_of_channels)
    , _n_samples(config.spectra_per_chunk())
    , _fch1(0.0*ska::cheetah::data::megahertz)
    , _foff(0.0*ska::cheetah::data::megahertz)
    , _sampling_interval(0.0)
{
    boost::asio::socket_base::receive_buffer_size option(16*1024*1024);
    this->connection().socket().set_option(option);
}

template<typename Producer>
UdpStreamFrequencyTimeTmpl<Producer>::~UdpStreamFrequencyTimeTmpl()
{
}

template<typename Producer>
void UdpStreamFrequencyTimeTmpl<Producer>::init()
{
    this->BaseT::init();
    this->start();
}


template<typename Producer>
template<typename DataType>
std::shared_ptr<DataType> UdpStreamFrequencyTimeTmpl<Producer>::get_chunk(unsigned , PacketInspector const& packet)
{

    auto chunk = BaseT::template get_chunk<DataType>();
    if(chunk) {
        chunk->resize( _n_samples, _n_channels);
    }

    if(_sampling_interval == 0.0)
    {
        _sampling_interval = 1.0e-6*((double)packet.packet().period_numerator()/(double)packet.packet().period_denominator());
        _fch1 = (1.0e-6*(packet.packet().first_channel_frequency()+((double)(_n_channels-(packet.packet().first_channel_number()-PssLowTraits::channel_offset))/1000.0)*packet.packet().channel_separation())) * data::megahertz;
        _foff = (-1.0e-9*packet.packet().channel_separation())*ska::cheetah::data::megahertz;
    }
    chunk->sample_interval(TimeType(_sampling_interval*ska::cheetah::data::seconds));
    chunk->set_channel_frequencies_const_width(_fch1, _foff);
    const long double ska_epoch = 51544.0; // SKA epoch is set as 01/01/2000.
    long double temp_mjd = ((long double)packet.packet().samples_since_ska_epoch()/86400.0)*_sampling_interval;
    chunk->start_time(ska::cheetah::utils::ModifiedJulianClock::time_point(utils::julian_days(ska_epoch+temp_mjd)));

    return chunk;
}


} // namespace rcpt_low
} // namespace producers
} // namespace io
} // namespace cheetah
} // namespace ska
