/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/io/producers/rcpt_low/BeamFormerDataTraits.h"
#include <panda/Log.h>
#include <algorithm>


namespace ska {
namespace cheetah {
namespace io {
namespace producers {
namespace rcpt_low {

template<class PacketTraits>
BeamFormerDataTraits<PacketTraits>::BeamFormerDataTraits()
{
}

template<class PacketTraits>
BeamFormerDataTraits<PacketTraits>::~BeamFormerDataTraits()
{

}

template<class PacketTraits>
uint64_t BeamFormerDataTraits<PacketTraits>::sequence_number(PacketInspector const& packet)
{
    return packet.sequence_number();
}

template<class PacketTraits>
std::size_t BeamFormerDataTraits<PacketTraits>::chunk_size(DataType const& chunk)
{
    return chunk.number_of_spectra() * chunk.number_of_channels();
}

template<class PacketTraits>
std::size_t BeamFormerDataTraits<PacketTraits>::packet_size()
{
    return (PacketInspector::Packet::number_of_samples()*PacketInspector::Packet::number_of_channels());
}

template<class PacketTraits>
std::size_t BeamFormerDataTraits<PacketTraits>::data_size()
{
    return PacketInspector::Packet::data_size();
}

template<class PacketTraits>
std::size_t BeamFormerDataTraits<PacketTraits>::packet_alignment_offset(PacketInspector const& inspector)
{
    return ((inspector.packet().first_channel_number()-PssLowTraits::channel_offset)/(PssLowTraits::number_of_channels/inspector.packet().number_of_channels()));
}

template<class PacketTraits>
void BeamFormerDataTraits<PacketTraits>::packet_stats(uint64_t packets_missing, uint64_t packets_expected)
{
    if(packets_missing > 0)
    {
        PANDA_LOG_WARN << "missing packets: " << packets_missing << " in " << packets_expected;
    }
}

template<class PacketTraits>
template<typename ContextType>
void BeamFormerDataTraits<PacketTraits>::deserialise_packet(ContextType& context, PacketInspector const& packet)
{
    typedef typename DataType::value_type ValueType;
    auto packet_it=packet.packet().begin();
    auto it=context.chunk().begin();

    static constexpr uint32_t number_of_channels_per_packet = PssLowTraits::number_of_channels_per_packet;
    static constexpr uint32_t number_of_samples_per_packet = PssLowTraits::number_of_samples_per_packet;
    static constexpr uint32_t samples_size_per_channel = number_of_samples_per_packet*4;
    static constexpr uint32_t half_samples_size_per_channel = number_of_samples_per_packet*2;

    std::vector<ValueType> temp_final(number_of_channels_per_packet*number_of_samples_per_packet);
    double scale = 1.0/(packet.packet().header().scale(0)*packet.packet().header().scale(0));

    for(unsigned int channel=0; channel<number_of_channels_per_packet; ++channel)
    {
        for(unsigned int sample=0; sample<number_of_samples_per_packet; ++sample)
        {
            int8_t x0 = *(packet_it + channel*samples_size_per_channel+2*sample);
            int8_t x1 = *(packet_it + channel*samples_size_per_channel+2*sample+1);
            int8_t y0 = *(packet_it + channel*samples_size_per_channel+2*sample+half_samples_size_per_channel);
            int8_t y1 = *(packet_it + channel*samples_size_per_channel+2*sample+half_samples_size_per_channel+1);
            temp_final[(channel)*number_of_samples_per_packet+sample] = (ValueType)(10.0*scale*((float)((int)x0 * (int)x0 + (int)x1 * (int)x1 + (int)y0 * (int)y0 + (int)y1 * (int)y1)));
        }
    }
    panda::copy(temp_final.begin(), temp_final.begin()+context.size(), it+context.offset());
}

template<class PacketTraits>
template<typename ContextType>
void BeamFormerDataTraits<PacketTraits>::process_missing_slice(ContextType& context)
{
    // essentially replacing the data corresponding to the missing packet to 0. (Dont know what is the best approach here.)
    unsigned offset = context.offset();
    auto it=context.chunk().begin() + offset;
    PANDA_LOG_DEBUG << "processing missing packet: data=" << (void*)&*it << context;
    for(std::size_t i=0; i < context.size() ; ++i) {
        *it = 0;
        assert(it!=context.chunk().end());
        ++it;
    }
}

template<class PacketTraits>
template<typename ResizeContextType>
void BeamFormerDataTraits<PacketTraits>::resize_chunk(ResizeContextType& context)
{
    PANDA_LOG_DEBUG << "resizing data: " << context;
    // drops any incomplete spectra
    context.chunk().resize(data::DimensionSize<data::Time>(context.size()/context.chunk().number_of_channels()));
}

template<class PacketTraits>
constexpr typename BeamFormerDataTraits<PacketTraits>::PacketNumberType
BeamFormerDataTraits<PacketTraits>::max_sequence_number()
{
    return PacketInspector::Packet::max_sequence_number();
}

} // namespace rcpt_low
} // namespace producers
} // namespace io
} // namespace cheetah
} // namespace ska
