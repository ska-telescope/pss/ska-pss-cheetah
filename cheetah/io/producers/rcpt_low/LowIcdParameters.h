#ifndef SKA_CHEETAH_IO_PRODUCERS_RCPT_LOW_LOWICDPARAMETERS_H
#define SKA_CHEETAH_IO_PRODUCERS_RCPT_LOW_LOWICDPARAMETERS_H

namespace ska {
namespace cheetah {
namespace io {
namespace producers {
namespace rcpt_low {

// TODO Move to the cbf-pss repository
struct PssLowTraits
{
    static constexpr uint32_t number_of_channels = 432;
    static constexpr uint32_t number_of_channels_per_packet = 54;
    static constexpr uint32_t number_of_samples_per_packet = 16;
    static constexpr uint32_t size_of_packet = 3664; // just the payload + header doesnot include the UDP headers
    static constexpr uint32_t channel_offset = 8640;
    typedef int8_t PacketDataType;
};

} // namespace rcpt_low
} // namespace producers
} // namespace io
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_IO_PRODUCERS_RCPT_LOW_MIDICDPARAMETERS_H