#include "cheetah/modules/tdao/cuda/Tdao.cuh"
#include "cheetah/modules/tdao/test_utils/TdaoTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace modules {
namespace tdao {
namespace cuda {
namespace test {

struct CudaTraits : public tdao::test::TdaoTesterTraits<tdao::cuda::Tdao::Architecture,tdao::cuda::Tdao::ArchitectureCapability>
{
    typedef tdao::test::TdaoTesterTraits<tdao::cuda::Tdao::Architecture, typename tdao::cuda::Tdao::ArchitectureCapability> BaseT;
    typedef Tdao::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace tdao
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace tdao {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Cuda, TdaoTester, CudaTraitsTypes);

} // namespace test
} // namespace tdao
} // namespace modules
} // namespace cheetah
} // namespace ska