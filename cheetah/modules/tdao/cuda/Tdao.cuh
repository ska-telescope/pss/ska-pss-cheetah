#ifndef SKA_CHEETAH_MODULES_TDAO_CUDA_TDAO_CUH
#define SKA_CHEETAH_MODULES_TDAO_CUDA_TDAO_CUH

#include "cheetah/Configuration.h"
#include "cheetah/modules/tdao/Config.h"
#include "cheetah/modules/tdao/cuda/Config.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/CachingAllocator.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/cuda_utils/cuda_thrust.h"

#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdao {
namespace cuda {


/**
 * @brief      CUDA/Thrust implementation of the Tdao module
 */
class Tdao
    : public utils::AlgorithmBase<Config,tdao::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        /**
         * @brief      Construct a new Tdao object
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Tdao(Config const& config, tdao::Config const& algo_config);
        Tdao(Tdao const&) = delete;
        Tdao(Tdao&&) = default;
        ~Tdao();

        /**
         * @brief      Find peaks in a power series above a statistical threshold
         *
         * @param      gpu         The gpu to process on
         * @param      input       The input power series
         * @param      output      The output candidate list
         * @param[in]  dm          The dispersion measure of the power series
         * @param[in]  acc         The acceleration value of the power series
         * @param[in]  nharmonics  The number of harmonic sums performed to produce the power series
         *
         * @tparam     T           The value type of the input
         * @tparam     Alloc       The allocator type of the input
         */
        template <typename T, typename Alloc>
        void process(ResourceType& gpu,
            data::PowerSeries<Architecture,T,1,Alloc>const& input,
            data::Ccl& output,
            data::DedispersionMeasureType<float> dm,
            data::AccelerationType acc,
            std::size_t nharmonics);

    private:
        /**
         * @brief      Prep all internal buffers
         *
         * @param[in]  size  The size of the input power series
         */
        void _prepare(std::size_t size);

        /**
         * @brief      Step through returned peaks and isolate only those that are
         *             separated by one or more bins
         */
        template <typename PowerSeriesType>
        void _filter_unique(PowerSeriesType const& input,
            data::Ccl& output,
            std::size_t num_copied,
            data::DedispersionMeasureType<float> dm,
            data::AccelerationType acc,
            std::size_t nharmonics);

        /**
         * @brief      Generate a Candidate object to be pushed onto the candidate list
         */
        template <typename PowerSeriesType>
        data::Ccl::CandidateType _make_candidate(
            unsigned idx,
            float power,
            PowerSeriesType const& input,
            data::DedispersionMeasureType<float> dm,
            double accel_fact,
            std::size_t nharmonics);

        /**
         * @brief      Wrapper for execution of peak finding
         */
        template <typename T>
        std::size_t _execute(typename thrust::device_vector<T>::const_iterator in, std::size_t size,
            std::size_t offset, float threshold);

        data::CachingAllocator<cheetah::Cuda,char> _allocator;
        thrust::device_vector<unsigned> _idxs;
        thrust::device_vector<float> _powers;
        thrust::host_vector<unsigned> _h_idxs;
        thrust::host_vector<float> _h_powers;
};

} // namespace cuda
} // namespace tdao
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/tdao/cuda/detail/Tdao.cu"

#endif // SKA_CHEETAH_MODULES_TDAO_CUDA_TDAO_CUH
