/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/tdao/test_utils/TdaoTester.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "panda/Log.h"

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/codata_constants.hpp>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdao {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
TdaoTesterTraits<ArchitectureTag,ArchitectureCapability>::TdaoTesterTraits()
    :_api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
tdao::Tdao& TdaoTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
tdao::Config& TdaoTesterTraits<ArchitectureTag,ArchitectureCapability>::config()
{
    return _config;
}

template <typename DeviceType, typename DataType>
struct ExectionTest
{
    inline static void test(DeviceType& device, tdao::Tdao& api, tdao::Config& config)
    {
        struct Signal
        {
            std::size_t bin;
            float strength;
            Signal(std::size_t bin,float strength)
            :bin(bin),strength(strength){}
        };

        config.minimum_frequency(0.0 * data::hz);
        config.maximum_frequency(5000.0 * data::hz);

        std::vector<Signal> test_signals;
        test_signals.push_back(Signal(50,10.0));
        test_signals.push_back(Signal(123,100.0));
        test_signals.push_back(Signal(3333,1000.0));
        DataType input(0.001 * data::hz);
        input.resize(10000,0.0);
        auto power_threshold = input.equiv_sigma_to_power(config.significance_threshold());

        std::size_t expected_candidates = 0;
        for (auto& signal: test_signals)
        {
            input[signal.bin] = signal.strength;
            if (signal.strength > power_threshold)
                ++expected_candidates;
        }

        data::Ccl output;
        data::DedispersionMeasureType<float> dm = 0.0f * data::parsecs_per_cube_cm;
        data::AccelerationType acc = 30.0 * data::metres_per_second_squared;
        api.process(device,input,output,dm,acc,1);

        float accel_fact = (acc.value() / (input.frequency_step().value() *  boost::units::si::constants::codata::c)).value();
        ASSERT_EQ(output.size(),expected_candidates);

        std::size_t cand_idx = 0;
        for (auto& signal : test_signals)
        {
            if (signal.strength > power_threshold)
            {
                ASSERT_NEAR(output[cand_idx].period().value()/1000.0, input.bin_to_period(signal.bin).value(), 0.00001);
                ASSERT_NEAR(output[cand_idx].pdot().value(), input.bin_to_period(signal.bin).value() * accel_fact, 0.00001);
                ++cand_idx;
            }
        }
    }
};

template <typename TestTraits>
TdaoTester<TestTraits>::TdaoTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
TdaoTester<TestTraits>::~TdaoTester()
{
}

template<typename TestTraits>
void TdaoTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void TdaoTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(TdaoTester, test_execution)
{
    TypeParam traits;
    ExectionTest<typename TypeParam::DeviceType, data::PowerSeries<typename TypeParam::Arch,float>>::test(device,traits.api(),traits.config());
    ExectionTest<typename TypeParam::DeviceType, data::PowerSeries<typename TypeParam::Arch,double>>::test(device,traits.api(),traits.config());
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_SUITE_P(TdaoTester, test_execution);

} // namespace test
} // namespace tdao
} // namespace modules
} // namespace cheetah
} // namespace ska
