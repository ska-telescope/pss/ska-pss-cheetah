#include "cheetah/modules/brdz/cuda/Brdz.h"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {
namespace cuda {
namespace detail {

/**
 * @brief      A functor for setting complex data to 0+0j
 *
 * @tparam     T     The base type of the complex number
 */
template <typename T>
struct ZapFunctor
{
    typedef typename data::ComplexTypeTraits<cheetah::Cuda,T>::type ComplexType;
    ComplexType* _ptr;
    ZapFunctor(ComplexType* ptr);
    inline __host__ __device__ void operator()(unsigned bin);
};


template <typename T>
ZapFunctor<T>::ZapFunctor(ComplexType* ptr)
    : _ptr(ptr)
{
}

template <typename T>
inline __host__ __device__ void ZapFunctor<T>::operator()(unsigned bin)
{
    _ptr[bin] = ComplexType(0.0,0.0);
}

} // namespace detail

template <typename T, typename Alloc>
void Brdz::process(ResourceType& gpu,
	data::FrequencySeries<Architecture,typename data::ComplexTypeTraits<Architecture,T>::type,Alloc>& input)
{
    typedef typename data::ComplexTypeTraits<Architecture,T>::type ComplexType;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    thrust::device_vector<unsigned> bins;
    auto const& birdies = _algo_config.birdies();
    for (auto& birdie: birdies)
    {
        std::size_t upper_bin = input.frequency_to_bin(birdie.frequency()+birdie.width() / 2.0);
        std::size_t lower_bin = input.frequency_to_bin(birdie.frequency()-birdie.width() / 2.0);
        upper_bin = std::min(upper_bin, input.size());
        lower_bin = std::max(std::size_t(0), lower_bin);
        for (std::size_t bin=lower_bin; bin<upper_bin; ++bin)
            bins.push_back((unsigned)bin);
    }
    ComplexType* ptr = thrust::raw_pointer_cast(input.data());
    thrust::for_each(thrust::cuda::par, bins.begin(), bins.end(), detail::ZapFunctor<T>(ptr));
}

} // namespace cuda
} // namespace brdz
} // namespace modules
} // namespace cheetah
} // namespace ska
