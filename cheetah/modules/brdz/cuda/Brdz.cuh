#ifndef SKA_CHEETAH_MODULES_BRDZ_CUDA_BRDZ_H
#define SKA_CHEETAH_MODULES_BRDZ_CUDA_BRDZ_H

#include "cheetah/modules/brdz/cuda/Config.h"
#include "cheetah/modules/brdz/Config.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {
namespace cuda {

/**
 * @brief      CUDA/Thrust implementation of the Brdz algorithm
 */
class Brdz: utils::AlgorithmBase<Config,brdz::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        /**
         * @brief      Create a new Brdz instance
         *
         * @param      impl_config  The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Brdz(Config const& impl_config, brdz::Config const& algo_config);
        Brdz(Brdz const&) = delete;
        Brdz(Brdz&&) = default;
        ~Brdz();

        /**
         * @brief      Set to zero frequencies marked as birdies
         *
         * @param      gpu    The gpu to process on
         * @param      input  The input frequency series
         *
         * @tparam     T      The base value type of the complex frequency series
         * @tparam     Alloc  The allocator type of the frequency series
         */
        template <typename T, typename Alloc>
        void process(ResourceType& gpu, data::FrequencySeries<Architecture,
            typename data::ComplexTypeTraits<Architecture,T>::type,Alloc>& input);
};

} //cuda
} // namespace brdz
} // namespace modules
} //cheetah
} //ska

#include "cheetah/modules/brdz/cuda/detail/Brdz.cu"

#endif //SKA_CHEETAH_MODULES_BRDZ_CUDA_BRDZ_H