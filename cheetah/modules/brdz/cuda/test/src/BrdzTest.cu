#include "cheetah/modules/brdz/cuda/Brdz.cuh"
#include "cheetah/modules/brdz/test_utils/BrdzTester.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {
namespace cuda {
namespace test {

struct CudaTraits : public brdz::test::BrdzTesterTraits<brdz::cuda::Brdz::Architecture,brdz::cuda::Brdz::ArchitectureCapability>
{
    typedef brdz::test::BrdzTesterTraits<brdz::cuda::Brdz::Architecture, typename brdz::cuda::Brdz::ArchitectureCapability> BaseT;
    typedef Brdz::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace brdz
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Cuda, BrdzTester, CudaTraitsTypes);

} // namespace test
} // namespace brdz
} // namespace modules
} // namespace cheetah
} // namespace ska