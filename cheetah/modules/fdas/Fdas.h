/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FDAS_FDAS_H
#define SKA_CHEETAH_MODULES_FDAS_FDAS_H

#include "cheetah/modules/fdas/Config.h"

#ifdef SKA_CHEETAH_ENABLE_NASM
#include "cheetah/modules/fdas/labyrinth/Fdas.h"
#endif
#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
#include "cheetah/modules/fdas/intel_fpga/Fdas.h"
#endif
#include "cheetah/utils/Architectures.h"
#include "cheetah/modules/fdas/cpu/Fdas.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/detail/DmTimeSlice.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/DmTrials.h"
#include "panda/ConfigurableTask.h"
#include "panda/AlgorithmTuple.h"
#include "panda/Log.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {

typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
typedef data::DmTime<DmTrialsType> DmTimeType;
typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

/**
 * @brief      Top-level synchronous interface for the Fdas module
 *
 * @details     Fdas stands for frequency-domain acceleration search. Here we search
 *             for significant periodic signal at multiple accelerations using
 *             the technique of Fourier domian search.
 *
 * @tparam     T     The value type to use for processing (float or double)
 */
template <typename T>
class FdasBase
{
    public:

        /**
         * @brief      Construct a new FdasBase instance
         *
         * @param      config  The algorithm configuration
         */
        FdasBase(Config const& config);
        FdasBase(FdasBase const&) = delete;
        FdasBase(FdasBase&&) = default;
        ~FdasBase();

        /**
         * @brief      Process a DmTime object in search of significant periodic
         *             signals over a range of acceleration values.
         *
         * @details     This is a forwarding interface that will forward the call to the
         *             relevant implementation based on the provided arguments.
         *
         * @param      resource   The resource to process on
         * @param      data       The input DmTime object
         *
         * @tparam     Arch       The architecture to process on
         *
         * @return     A shared pointer to a list of candidate signals
         */
        template <typename Arch>
        std::shared_ptr<data::Ccl> process(panda::PoolResource<Arch>& resource, std::shared_ptr<DmTimeType> const& data);

        /**
         * @brief     Process a DmTime object with Intel fpga specialisation of PoolResource
         */
        std::shared_ptr<data::Ccl> process(panda::PoolResource<cheetah::IntelFpga>& resource, std::shared_ptr<DmTimeType> const& data);

    private:
        Config const& _config;
};

/**
 * @brief      Async + Sync mixed interface for fdas
 *
 * @tparam     T        The value type to use for processing (float or double)
 * @tparam     Handler  The type of the Handler that will be used for async calls
 */
template <typename T, typename Handler=void>
class Fdas : FdasBase<T>
{
    public:
        typedef typename panda::ConfigurableTask<typename Config::PoolType, Handler&, std::shared_ptr<DmTimeType>> TaskType;

    public:

        /**
         * @brief      Construct a new fdas instance
         *
         * @param      config  The algorithm configuration
         */
        Fdas(ConfigType const& config, Handler& handler);
        Fdas(Fdas const&) = delete;
        Fdas(Fdas&&) = default;
        ~Fdas();

        /**
         * @brief      Async call to fdas
         *
         * @param      data  A DmTime to be processed
         */
        void operator()(std::shared_ptr<DmTimeType> const& data);

    private:
        ConfigType const& _config;
        TaskType _task;
        template<typename Algo> using Select = typename TaskType::template Select<Algo>;
};

/**
 * @brief      Sync-only interface for Fdas
 *
 * @tparam     T     The value type to use for processing (float or double)
 */
template <typename T>
class Fdas<T,void>: public FdasBase<T>
{
    using FdasBase<T>::FdasBase;
};

} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fdas/detail/Fdas.cpp"

#endif // SKA_CHEETAH_MODULES_FDAS_FDAS_H
