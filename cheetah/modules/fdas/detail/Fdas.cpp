/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/Fdas.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {

namespace {

template<typename Algo>
struct FdasCreateHelper {
    template<typename ConfigT>
    static inline
    Algo create(ConfigT const& config)
    {
        return Algo(config.template config<typename Algo::Config>(), config);
    }
};

#ifdef SKA_CHEETAH_ENABLE_NASM
#ifndef SKA_CHEETAH_ENABLE_INTEL_FPGA
template<typename T>
struct FdasCreateHelper<labyrinth::Fdas<T>>
{
    template<typename ConfigT>
    static inline
    labyrinth::Fdas<T> create(ConfigT const& config)
    {
        PANDA_LOG << "fdas::labyrinth algorithm activated";
        return labyrinth::Fdas<T>(config.labyrinth_config(), config);
    }
};
#endif
#endif

#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
template<typename T>
struct FdasCreateHelper<intel_fpga::Fdas<T>>
{
    template<typename ConfigT>
    static inline
    intel_fpga::Fdas<T> create(ConfigT const& config)
    {
        PANDA_LOG << "fdas::intel_fpga algorithm activated";
        return intel_fpga::Fdas<T>(config.intel_fpga_config(), config);
    }
};
#endif

template<typename T>
struct FdasCreateHelper<cpu::Fdas<T>>
{
    template<typename ConfigT>
    static inline
    cpu::Fdas<T> create(ConfigT const& config)
    {
        PANDA_LOG << "fdas::cpu algorithm activated";
        return cpu::Fdas<T>(config.cpu_config(), config);
    }
};

template<typename ConfigType, typename T>
struct FdasAlgoFactory
{
    public:
        FdasAlgoFactory(ConfigType const& config)
            : _config(config)
        {}

        template<typename Algo>
        Algo create()
        {
            return FdasCreateHelper<Algo>::create(_config);
        }

        template<typename Algo>
        bool active() const {
            return _config.template config<typename Algo::Config>().active();
        }

    private:
        ConfigType const& _config;
};

} // namespace

template <typename T>
FdasBase<T>::FdasBase(Config const& config)
    : _config(config)
{
}

template <typename T>
FdasBase<T>::~FdasBase()
{
}

template <typename T>
template <typename Arch>
std::shared_ptr<data::Ccl> FdasBase<T>::process(panda::PoolResource<Arch>& resource, std::shared_ptr<DmTimeType> const& data)
{
    if(_config.cpu_config().active())
    {
        auto algo = cpu::Fdas<T>(_config.cpu_config(), _config);
        return algo.process(resource, data);
    }
    #ifdef  SKA_CHEETAH_ENABLE_NASM
    #ifndef SKA_CHEETAH_ENABLE_INTEL_FPGA
    if(_config.labyrinth_config().active())
    {
        auto algo = labyrinth::Fdas<T>(_config.labyrinth_config(), _config);
        return algo.process(resource, data);
    }
    #endif
    #endif
    else
    {
        auto candidate_list_ptr = std::make_shared<data::Ccl>();
        return candidate_list_ptr;
    }
}

template <typename T>
std::shared_ptr<data::Ccl> FdasBase<T>::process(panda::PoolResource<cheetah::IntelFpga>& resource, std::shared_ptr<DmTimeType> const& data)
{
    #ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
    if(_config.intel_fpga_config().active())
    {
        auto algo = intel_fpga::Fdas<T>(_config.intel_fpga_config(), _config);
        return algo.process(resource, data);
    }
    else
    {
        PANDA_LOG_WARN << "No Frequency Domain Accelerated Search algorithm has been specified";
        auto candidate_list_ptr = std::make_shared<data::Ccl>();
        return candidate_list_ptr;
    }
    #else
    PANDA_LOG_WARN << "Intel FPGA architecture not enabled, cannot call FPGA Fdas";
    auto candidate_list_ptr = std::make_shared<data::Ccl>();
    return candidate_list_ptr;
    #endif
}

template <typename T, typename Handler>
Fdas<T,Handler>::Fdas(ConfigType const& config, Handler& handler)
    : FdasBase<T>(config)
    , _config(config)
    , _task(_config.pool(), handler)
{
    FdasAlgoFactory<ConfigType, T> factory(_config);
    if(!_task.configure(factory
                      , Select<cpu::Fdas<T>>(_config.cpu_config().active())
                      #ifdef SKA_CHEETAH_ENABLE_NASM
                      #ifndef SKA_CHEETAH_ENABLE_INTEL_FPGA
                      , Select<labyrinth::Fdas<T>>(config.labyrinth_config().active())
                      #endif
                      #endif
                      #ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
                      , Select<intel_fpga::Fdas<T>>(_config.intel_fpga_config().active())
                      #endif
                   ))
    {
        PANDA_LOG_WARN << "No Frequency Domain Accelerated Search algorithm has been specified";
    }
}

template <typename T, typename Handler>
Fdas<T,Handler>::~Fdas()
{
}

template <typename T, typename Handler>
void Fdas<T,Handler>::operator()(std::shared_ptr<DmTimeType> const& data)
{
    _task.submit(data);
}



} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
