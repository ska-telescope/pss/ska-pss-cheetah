/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FDAS_CONFIG_H
#define SKA_CHEETAH_MODULES_FDAS_CONFIG_H


#include "cheetah/utils/Config.h"
#ifdef SKA_CHEETAH_ENABLE_OPENCL
#include "cheetah/modules/fdas/opencl/Config.h"
#endif
#include "cheetah/modules/fdas/cpu/Config.h"
#if defined(SKA_CHEETAH_ENABLE_NASM) && !defined(SKA_CHEETAH_ENABLE_INTEL_FPGA)
#include "cheetah/modules/fdas/labyrinth/Config.h"
#endif
#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
#include "cheetah/modules/fdas/intel_fpga/Config.h"
#endif
#include "cheetah/modules/cxft/Config.h"
#include "panda/MultipleConfigModule.h"
#include "panda/PoolSelector.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {

typedef panda::MultipleConfigModule< utils::Config
                                    , cpu::Config
                                    #ifdef SKA_CHEETAH_ENABLE_NASM
                                    #ifndef SKA_CHEETAH_ENABLE_INTEL_FPGA
                                    , labyrinth::Config
                                    #endif
                                    #endif
                                    #ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
                                    , intel_fpga::Config
                                    #endif
                                    #ifdef SKA_CHEETAH_ENABLE_OPENCL
                                    , opencl::Config
                                    #endif
                                    > FdasAlgoConfigs;

/**
 * @brief
 *    Configuration for the fdas module
 *
 * @details
 *
 */

class Config : public FdasAlgoConfigs
{
        typedef FdasAlgoConfigs BaseT;

    public:
        Config();
        ~Config();

        /**
         * @brief returns true if fdas task is to be performed
         */
        bool active() const;

        /**
         * @brief return the CPU algorithm configuration parameters
         */
        cpu::Config const& cpu_config() const;
        cpu::Config& cpu_config();

        #if defined(SKA_CHEETAH_ENABLE_NASM) && !defined(SKA_CHEETAH_ENABLE_INTEL_FPGA)
        /**
         * @brief return the Labyrinth algorithm configuration parameters
         */
        labyrinth::Config const& labyrinth_config() const;
        labyrinth::Config& labyrinth_config();
        #endif

        #ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
        /**
         * @brief return the Intel FPGA algorithm configuration parameters
         */
        intel_fpga::Config const& intel_fpga_config() const;
        intel_fpga::Config& intel_fpga_config();
        #endif

        #ifdef SKA_CHEETAH_ENABLE_OPENCL
        /**
         * @brief return the OpenCl algorithm configuration parameters
         */
        opencl::Config const& opencl_algo_config() const;
        #endif

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        cpu::Config _cpu_config;
        #if defined(SKA_CHEETAH_ENABLE_NASM) && !defined(SKA_CHEETAH_ENABLE_INTEL_FPGA)
        labyrinth::Config _labyrinth_config;
        #endif
        #ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
        intel_fpga::Config _intel_fpga_config;
        #endif
        #ifdef SKA_CHEETAH_ENABLE_OPENCL
        opencl::Config _opencl_config;
        #endif
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FDAS_CONFIG_H
