/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {


Config::Config()
    : BaseT("fdas")
    , _active(true)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active)->default_value(true), "turn on/off the frequency domain acceleration search");
}

bool Config::active() const {
    return _active;
}

cpu::Config const& Config::cpu_config() const
{
    return _cpu_config;
}

cpu::Config& Config::cpu_config()
{
    return _cpu_config;
}

#if defined(SKA_CHEETAH_ENABLE_NASM) && !defined(SKA_CHEETAH_ENABLE_INTEL_FPGA)
labyrinth::Config const& Config::labyrinth_config() const
{
    return _labyrinth_config;
}

labyrinth::Config& Config::labyrinth_config()
{
    return _labyrinth_config;
}
#endif

#ifdef SKA_CHEETAH_ENABLE_INTEL_FPGA
intel_fpga::Config const& Config::intel_fpga_config() const
{
    return _intel_fpga_config;
}

intel_fpga::Config& Config::intel_fpga_config()
{
    return _intel_fpga_config;
}
#endif

#ifdef SKA_CHEETAH_ENABLE_OPENCL
opencl::Config const& Config::opencl_algo_config() const
{
    return _opencl_config;
}
#endif

} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
