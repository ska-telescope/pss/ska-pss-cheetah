/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fdas/labyrinth/Fdas.h"
#include "cheetah/corner_turn/CornerTurn.h"
#include "panda/Log.h"
#include "panda/Resource.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {

extern "C" void nasm_labyrinth(const float* data, int* output, uint64_t* num_candidates, uint64_t num_bins, unsigned* thresold);

template <typename T>
Fdas<T>::Fdas(Config const& config, fdas::Config const& algo_config)
    : utils::AlgorithmBase<Config, fdas::Config>(config, algo_config)
    , _config(config)
    , _current_range(0)
    , _current_dm_index(0)
    , _count(0)
{
    PANDA_LOG<<"FDAS initialised";
}

template <typename T>
Fdas<T>::~Fdas()
{
}

template <typename T>
unsigned Fdas<T>::call_nasm_labyrinth(std::vector<float> const& fop,  unsigned int num_bins, float thresold)
{
    std::vector<unsigned> power_thresholds(16);
    for(unsigned int i=0; i<16; ++i)
    {
        _plan.power_series().degrees_of_freedom(2*(i+1)); // degrees of freedom 2,4,6,8,10,12....
        power_thresholds[i] = (unsigned)_plan.power_series().equiv_sigma_to_power(thresold);
    }
    uint64_t num_bins_t = num_bins;
    uint64_t num_cans =0;
    nasm_labyrinth(&*fop.begin(), &*_plan.out_cands().begin(), &num_cans, num_bins_t, &*power_thresholds.begin());
    PANDA_LOG<<"Dm range: "<<_plan.dms()[0]<<" to "<<_plan.dms()[15]<<" num_cands: "<<num_cans<<" max: "<<*std::max_element(fop.begin(), fop.end());
    return (unsigned) num_cans;
}

template <typename T>
void Fdas<T>::normalize_data(data::TimeSeries<cheetah::Cpu, float>& time_data)
{
    double sum = std::accumulate(time_data.begin(), time_data.end(), 0.0);
    double mean = sum / time_data.size();

    std::vector<double> diff(time_data.size());
    std::transform(time_data.begin(), time_data.end(), diff.begin(), [mean](double x) { return x - mean; });

    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / time_data.size());
    std::transform(time_data.begin(), time_data.end(), time_data.begin(), [mean,stdev](float x) { return (x - (float)mean)/(float)stdev; });
}

template <typename T>
data::Ccl::CandidateType Fdas<T>::make_candidate(int dm_index, int bin_number, int harmonic, int power_100)
{
    double sigma = power_100/100.0;
    auto period = data::Ccl::CandidateType::MsecTimeType((1.0/(bin_number * _plan.power_series().frequency_step().value())) * data::seconds);
    auto pdot = data::Ccl::CandidateType::SecPerSecType(0.0);
    auto width = data::Ccl::CandidateType::MsecTimeType((period.value()/harmonic)*data::milliseconds);
    PANDA_LOG_DEBUG << "Candidate -> Period = " << period
                    << ", Pdot = " << pdot
                    << ", Width = " << width
                    << ", Sigma =" << sigma;
    return data::Ccl::CandidateType(period, pdot, _plan.dms()[dm_index], harmonic, width, sigma);
}

template <typename T>
unsigned Fdas<T>::process(panda::PoolResource<Architecture>& device, DmTimeSliceType const& data)
{
    unsigned int power_series_stride = _plan.fft_size()[_current_range]/2;
    _plan.time_data()[_current_range].resize(_plan.fft_size()[_current_range]);
    _plan.freq_data()[_current_range].resize(_plan.fft_size()[_current_range]/2+1);
    unsigned int nbins = (_config.frequency_cutoff()/_plan.power_series().frequency_step())/std::pow(2,_current_range);
    auto it = data.cbegin();
    _plan.dms()[_count]= (*it).dm();
    (*it).copy_to(_plan.time_data()[_current_range]);
    normalize_data(_plan.time_data()[_current_range]);
    _plan.fft()[_current_range].process(device, _plan.time_data()[_current_range], _plan.freq_data()[_current_range]);
    *_plan.freq_data()[_current_range].begin() = 0.0;
    std::transform(_plan.freq_data()[_current_range].begin()
                      , _plan.freq_data()[_current_range].end()-1
                      , _plan.power_series().begin()+_count*power_series_stride
                      , [this](std::complex<float> x) { return (x.real()*x.real()+x.imag()*x.imag())/((double)_plan.fft_size()[_current_range]);}
                      );
    ++_count;
    ++_current_dm_index;

    if(_current_dm_index==_plan.ndms_per_range()[_current_range] || _count==16)
    {
        corner_turn::corner_turn( &*_plan.power_series().begin()
                        , &*_plan.power_series_shuffled().begin()
                        , power_series_stride
                        , 16
                        );
        std::fill(_plan.power_series().begin(), _plan.power_series().end(), 0.0);

        _plan.power_series().frequency_step(_plan.freq_data()[_current_range].frequency_step());
        if(_current_dm_index==_plan.ndms_per_range()[_current_range])
        {
            _current_dm_index = 0;
            ++_current_range;
        }
        _count = 0;

        return call_nasm_labyrinth(_plan.power_series_shuffled(), nbins, _config.threshold());
    }
    return 0;
}

template <typename T>
std::shared_ptr<data::Ccl> Fdas<T>::process(panda::PoolResource<Architecture>& device, std::shared_ptr<DmTimeType> const& data)
{
    PANDA_LOG<<"running FFT initialization";
    _plan(device, data);
    PANDA_LOG<<"done FFT initialization";

    data->wait();
    auto candidate_list_ptr = std::make_shared<data::Ccl>();

    PANDA_LOG<<"running FDAS ";

    auto fdas_start = std::chrono::high_resolution_clock::now();

    for(auto it=data->begin(1); it<data->end(); ++it)
    {
        auto dm_slice_iterator = *it;

        unsigned num_cands = process(device,*dm_slice_iterator);
        for(unsigned int candidate=0; candidate<num_cands; ++candidate)
        {
            candidate_list_ptr->push_back(make_candidate(_plan.out_cands()[4*candidate]
                                                        , _plan.out_cands()[4*candidate+1]
                                                        , _plan.out_cands()[4*candidate+2]
                                                        , _plan.out_cands()[4*candidate+3]
                                                        )
                                         );
        }
    }

    auto fdas_stop = std::chrono::high_resolution_clock::now();
    PANDA_LOG<<" Fdas time: "<<std::chrono::duration_cast<std::chrono::nanoseconds>(fdas_stop - fdas_start).count()/1000000.0<<" ms\n";
    return candidate_list_ptr;
}


template <typename T>
std::shared_ptr<data::Ccl> Fdas<T>::operator()(panda::PoolResource<Architecture>& device,
    std::shared_ptr<DmTimeType> const& data)
{
    return this->process(device, data);
}

} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
