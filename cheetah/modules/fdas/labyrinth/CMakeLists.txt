set(MODULE_LABYRINTH_LIB_SRC_CPU
    src/Config.cpp
    src/FdasPlan.cpp
    src/kernels/nasm_labyrinth.asm
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
