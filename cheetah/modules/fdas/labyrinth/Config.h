/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FDAS_LABYRINTH_CONFIG_H
#define SKA_CHEETAH_MODULES_FDAS_LABYRINTH_CONFIG_H


#include "cheetah/utils/Config.h"
#include "cheetah/data/Units.h"
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/make_scaled_unit.hpp>

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {

/**
 * @brief
 *    Configuration specific to the cpu algorithm
 *
 * @details
 *
 */

class Config : public cheetah::utils::Config
{
    public:
        typedef boost::units::quantity<data::Hertz, double> FrequencyType;
    public:
        Config();
        ~Config();

        /**
         * @brief return true if this algorithm is to be used
         */
        bool active() const;

         /**
         * @brief set the activation value
         */
        void active(bool);

        /**
         * @brief getter and setter for upper cutoff frequency to search for candidates
         */
        FrequencyType frequency_cutoff() const;
        void frequency_cutoff(FrequencyType);

        /**
         * @brief getter and setter for the gaussian thresold set in the config
         */
        float threshold() const;
        void threshold(float);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        FrequencyType _frequency_cutoff;
        float _threshold;
};


} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FDAS_LABYRINTH_CONFIG_H