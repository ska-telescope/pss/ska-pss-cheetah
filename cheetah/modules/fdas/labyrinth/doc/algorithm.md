@page labyrinth_algorithm_guide algorithm guide

***Documentation for Labyrinth module***

**Author:** *Arun Naidu*

# Introduction
FDAS is computationally intensive, and the SKA Pulsar Search Subsystem (SKA-PSS) leverages Field-Programmable Gate
Arrays (FPGAs) for accelerated periodic signal searches. Due to resource constraints, expected 500 of the total 6000 
Dispersion Measures (DMs) are selected for acceleration searches, with the remaining DM trials processed for 
non-accelerated periodic signals using incoherent harmonic summing. Labyrinth is a CPU-based frequency domain 
search (FDS) pipeline for SKA-PSS.

Incoherent harmonic summing is a technique used in Fourier-based pulsar searches to improve the detection of pulsars 
whose signals appear at multiple harmonics in the frequency spectrum. pulsars exhibit pulse profiles that are not purely 
sinusoidal, meaning their power is distributed across harmonics (integer multiples of the fundamental frequency). 
Instead of just looking at the fundamental frequency alone, incoherent summing adds the power from these harmonics to 
increase detection sensitivity. The input time series (radio observations) is converted to the frequency domain using 
a Fast Fourier Transform (FFT) this produces a power spectrum where pulsar signals appear as peaks at their rotation frequency 
and its harmonics. Instead of analyzing only the fundamental frequency, power from higher harmonics (e.g., f,2f,3f,4f, etc.) 
is also considered. In incoherent summing, the power values at these harmonics are summed without phase alignment.  Since 
many pulsars exhibit significant power at multiple harmonics, summing the power from these frequencies increases the overall 
signal strength. This improves the chance of detecting weak pulsars that might otherwise be buried in noise. Due to the 
Finite Frequency Resolution, Spectral Leakage and Intrinsic Period Variability, instead of summing power at a precise 
harmonic frequency (nf), incoherent harmonic summing considers power contributions from a small window around each harmonic 
to capture potential shifts.

  
# Algorithm Overview
![HSUM](hsum.png)

The typical hamonic sum tree for SKA case is shown in the figure above. Each bubble in the figure represents a partial sum and each 
row in the figure represents a harmonic stage and the bubble on top of the pyramid represents the fundamental and every buble along the 
pine of the tree are corresponding harmonics and all oher bubble are the samples representing the uncertainity window. The 
figure only repsents 8 harmonics and Labyrinth is programmed to search upto 11 harmonics resulting in around ~360 partial sums per 
fundamental.    


## Main Components

DMTime Object: This object represents the input data with varying Dispersion Measure (DM) values over time.
FFT Plan Initialization: The algorithm begins by initializing an FFT (Fast Fourier Transform) plan based on the input DMTime object.

DM Loop: The algorithm then loops over the DMTime object, normalizing the data and performing FFTs for each DM.
NASM HSUM Function: After FFT, the data is processed in batches of 16 DMs using the NASM HSUM function, which implements the harmonic summing algorithm.

Candidate Conversion: The output candidates from the harmonic summing step are converted to a specific data type called Ccl.
Sift Handler: Finally, these candidates are passed on to a sift handler for further processing, such as candidate selection or rejection.

## Process Flow
Initialize FFT Plan based on the DMTime object.

Loop Through DMTime:
Normalize the data.
Perform FFT on the normalized data.

Batch Processing:
Pass the FFT results in batches of 16 DMs to the NASM HSUM function.

Convert to Ccl Data Type:
Transform the output candidates to the Ccl data type.
Pass to Sift Handler for further processing.

#Performace
The Labyrinth algorithm is very efficient and all expected 6000 DMs from a 10 minute can be processed in less 5 minutes on a single thread 
of AMD EYPC 9454 processor.