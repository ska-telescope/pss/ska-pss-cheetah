/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDASPLAN_H
#define SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDASPLAN_H

#include "cheetah/modules/fdas/labyrinth/Config.h"
#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/fft/Fft.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/detail/DmTimeSlice.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {

/**
 * @brief    Cpu implementation of the fdas module
 *
 * @details  The fdas module supports frequency domain acceleration search.
 *
 * @tparam     T     The internal value type to use for processing (float or double)
 */
class FdasPlan
{
    public:
        typedef data::DmTrials<cheetah::Cpu, float> DmTrialsType;
        typedef data::DmTime<DmTrialsType> DmTimeType;
        typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

    private:
        typedef data::TimeSeries<cheetah::Cpu, float> TimeSeriesType;
        typedef data::FrequencySeries<cheetah::Cpu, std::complex<float>> FrequencySeriesType;
        typedef data::PowerSeries<cheetah::Cpu, float, 16> PowerSeriesType;

    public:
        typedef cheetah::Cpu Architecture;
        typedef typename fft::fftw::Fft<float> FftType;

    public:


        /**
         * @brief      Construct a new fdas instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        FdasPlan();
        FdasPlan(FdasPlan const&) = delete;
        FdasPlan(FdasPlan&&) = default;
        ~FdasPlan();


        /**
         * @brief  operator used for initialzing plan when the device is available.
        */
        void operator()(panda::PoolResource<Architecture>& device, std::shared_ptr<DmTimeType> const& data);

        /**
         * @brief  return the fftw fft object.
        */
        std::vector<FftType>& fft();

        /**
         * @brief  return the time data object.
        */
        std::vector<TimeSeriesType>& time_data();

        /**
         * @brief  return the frequency data object.
        */
        std::vector<FrequencySeriesType>& freq_data();

        /**
         * @brief  returns the powerseries object.
        */
        PowerSeriesType& power_series();

        /**
         * @brief  returns the shuffled power series data object.
        */
        std::vector<float>& power_series_shuffled();

        /**
         * @brief  returns the out candidates vector.
        */
        std::vector<int>& out_cands();

        /**
         * @brief  returns vector of the current DMs being processed.
        */
        std::vector<typename data::DedispersionMeasureType<float>>& dms();

        /**
         * @brief  returns vector containg number of dms per range.
        */
        std::vector<unsigned>& ndms_per_range();

        /**
         * @brief  returns the vector containg the fft_sizes per range
        */
        std::vector<unsigned>& fft_size();

    private:
        std::vector<FftType> _fft;
        std::vector<TimeSeriesType> _time_data;
        std::vector<FrequencySeriesType> _freq_data;
        PowerSeriesType _power_series;
        std::vector<float> _power_series_shuffled;
        std::vector<int> _out_cands;
        std::vector<typename data::DedispersionMeasureType<float>> _dms;
        std::vector<unsigned> _ndms_per_range;
        std::vector<unsigned int> _fft_size;
};


} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDASPLAN_H