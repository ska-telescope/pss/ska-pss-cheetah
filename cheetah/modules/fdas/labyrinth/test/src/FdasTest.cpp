/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/labyrinth/Fdas.h"
#include "cheetah/modules/fdas/test_utils/FdasTester.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {
namespace test {

template <typename T>
struct LabyrinthTraits
    : public fdas::test::FdasTesterTraits<typename fdas::labyrinth::Fdas<T>::Architecture, typename fdas::labyrinth::Fdas<T>::Architecture, T>
{
    typedef fdas::labyrinth::Fdas<T> ApiType;
    typedef typename ApiType::Architecture Arch;
    typedef typename fdas::test::FdasTesterTraits< Arch, Arch, T> BaseT;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace test {

typedef ::testing::Types<labyrinth::test::LabyrinthTraits<float>> LabyrinthTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Labyrinth, FdasTester, LabyrinthTraitsTypes);

} // namespace test
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska