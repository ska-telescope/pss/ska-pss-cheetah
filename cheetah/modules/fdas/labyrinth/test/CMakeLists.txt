include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fdas_labyrinth_src
    src/gtest_fdas_labyrinth.cpp
    src/FdasTest.cpp
   )

add_executable(gtest_fdas_labyrinth ${gtest_fdas_labyrinth_src})
target_link_libraries(gtest_fdas_labyrinth ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fdas_labyrinth gtest_fdas_labyrinth --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
