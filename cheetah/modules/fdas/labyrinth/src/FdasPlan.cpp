/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/labyrinth/Fdas.h"
#include "cheetah/corner_turn/CornerTurn.h"
#include "panda/Log.h"
#include "panda/Resource.h"
#include <cmath>

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {

FdasPlan::FdasPlan()
    : _fft(1)
    , _time_data(1)
    , _freq_data(1)
    , _power_series(0)
    , _power_series_shuffled(0)
    , _out_cands(4*1024*1024)
    , _dms(16)
    , _ndms_per_range(0)
    , _fft_size(1)
{
    PANDA_LOG<<"FDASPLAN called";
}

FdasPlan::~FdasPlan()
{
}

void FdasPlan::operator()(panda::PoolResource<Architecture>& device, std::shared_ptr<DmTimeType> const& data)
{
    auto& dmtrials = *(data->blocks().begin());
    unsigned current_size = (*dmtrials)[0].size();
    unsigned int count=0;

    for(unsigned int i=0; i<dmtrials->size(); ++i)
    {
        ++count;
        if((*dmtrials)[i].size()!=current_size)
        {
            _ndms_per_range.push_back(count-1);
            current_size = (*dmtrials)[i].size();
            count=1;
        }
    }
    _ndms_per_range.push_back(count);
    _fft_size.resize(_ndms_per_range.size());
    _time_data.resize(_ndms_per_range.size());
    _freq_data.resize(_ndms_per_range.size());
    _fft.resize(_ndms_per_range.size());

    _fft_size[0] = (unsigned)(data->dump_time().value()/(*dmtrials)[0].sampling_interval().value());


    unsigned power = (unsigned)(std::log2((double)_fft_size[0]));

    unsigned dm_index = 0;
    for(unsigned range=0; range<_ndms_per_range.size(); ++range)
    {
        _fft_size[range] = std::pow(2,power-range);
        _time_data[range].resize(_fft_size[range]);
        _time_data[range].sampling_interval((*dmtrials)[dm_index].sampling_interval());
        _freq_data[range].resize(_fft_size[range]/2+1);
        _fft[range].process(device, _time_data[range], _freq_data[range]);
        dm_index += _ndms_per_range[range];
    }

    _power_series.resize(16*_fft_size[0]/2);
    _power_series_shuffled.resize(_power_series.size());

    _power_series.frequency_step(_freq_data[0].frequency_step());
}

std::vector<FdasPlan::TimeSeriesType>& FdasPlan::time_data()
{
    return _time_data;
}

std::vector<FdasPlan::FrequencySeriesType>& FdasPlan::freq_data()
{
    return _freq_data;
}

FdasPlan::PowerSeriesType& FdasPlan::power_series()
{
    return _power_series;
}

std::vector<float>& FdasPlan::power_series_shuffled()
{
    return _power_series_shuffled;
}

std::vector<int>& FdasPlan::out_cands()
{
    return _out_cands;
}

std::vector<typename data::DedispersionMeasureType<float>>& FdasPlan::dms()
{
    return _dms;
}

std::vector<unsigned>& FdasPlan::ndms_per_range()
{
    return _ndms_per_range;
}

std::vector<unsigned>& FdasPlan::fft_size()
{
    return _fft_size;
}

std::vector<FdasPlan::FftType>& FdasPlan::fft()
{
    return _fft;
}

} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska