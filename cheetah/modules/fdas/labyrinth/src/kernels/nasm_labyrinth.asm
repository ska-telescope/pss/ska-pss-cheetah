;These not the exact values but the address of the location
;where these are stored in stack
;extern "C" void nasm_labyrinth(float* data, int* output, uint64_t* num_candidates, uint64_t num_bins, uint64_t thresold)

; Basic compare predicates
CMP_EQ equ 00h
CMP_LT equ 01h
CMP_LE equ 02h
CMP_UNORD equ 03h
CMP_NEQ equ 04h
CMP_NLT equ 05h
CMP_NLE equ 06h
CMP_ORD equ 07h
DATA_SIZE equ 0
COUNTER equ 10240
RSP_OFFSET equ 10480
RDX_REG equ 10488
NUM_BINS equ 10496
THRESHOLD equ 10504 ; 16 values (64bytes)
TEMP equ 10568
STACK_SIZE equ 11264
SCRATCH equ 5120


; actual data location
GLOBAL nasm_labyrinth
nasm_labyrinth:
        ;rdi: VariablesArray
        ;rsi: total_size
        ;zmm0: power/threshold
        ;zmm3: harmonic

        ;jmp print
;----------allign the stack pointer to 64byte cache line-----------------
        mov r11, rdx
        push rdx
        push rbx
        push rsi
        push rdi
        push r12
        push r13
        push r14
        push r15
        push rbp
        mov r12, rcx
        mov rdx, 0
        mov rax, rsp
        mov r10, 1024
        div r10
        sub rsp, rdx
        sub rsp, STACK_SIZE
        mov qword[rsp+RSP_OFFSET], rdx
;-------------------------------------------------------------------------
        mov qword[rsp+RDX_REG], r11
        mov r10, 0
        mov qword[rsp+COUNTER], r10

        mov qword[rsp+NUM_BINS], r12

        ;copy thersholds
        vmovdqu16 zmm0, zword [r8]
        vmovdqu16 zword[rsp+THRESHOLD], zmm0


        xor r15, r15
        vpxord zmm3, zmm3
scratch_loop:
        vmovdqu16 zword [rsp+r15], zmm3
        vmovdqu16 zword [rsp+r15+SCRATCH], zmm3
        add r15, 64
        cmp r15, SCRATCH
        jl scratch_loop

        vpxord zmm3, zmm3
        xor r15, r15
        mov r15, 320
        mov r13, 6
sample_loop:

        ;---------------fundamental---------------------------------

        mov r14d, dword[rsp+THRESHOLD]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0



        mov r14, 1
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        vmovdqu16 zmm15, zword [rdi+r15]

        vcmpps k1, zmm0, zmm15, CMP_LT
        vmovaps zmm0{k1}, zmm15
        vmovaps zmm3{k1}, zmm4

        mov r14d, dword[rsp+THRESHOLD]
        mov r11, rsp
        call copy_candidates
        ;---------------2f0 harmonic---------------------------------

        mov r14d, dword[rsp+THRESHOLD+4]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 2
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 128
        mul r13
        sub rax, 64

        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]

        mov r9, 0
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+4]
        mov r11, rsp
        call copy_candidates
        ;---------------3f0 harmonic---------------------------------

        mov r14d, dword[rsp+THRESHOLD+8]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 3
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 192
        mul r13
        sub rax, 64

        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]


        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 0
        mov r9, 0
        add r9, SCRATCH
        mov r10, 1
        mov r11, rsp
        call sum_branch

        mov r9, 128
        add r9, SCRATCH
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+8]
        mov r11, rsp
        call copy_candidates
        ;---------------4f0 harmonic---------------------------------

        mov r14d, dword[rsp+THRESHOLD+12]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 4
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 256
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-128]
        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]
        vmovdqu16 zmm10, zword [rdi+rax+128]



        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r8, SCRATCH
        mov r10, 1
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 0
        mov r9, 128
        add r8, SCRATCH
        mov r10, 2
        mov r11, rsp
        call sum_branch

        mov r9, 384
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+12]
        mov r11, rsp
        call copy_candidates
        ;---------------5f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+16]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 5
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 320
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-128]
        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]
        vmovdqu16 zmm10, zword [rdi+rax+128]


        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r9, SCRATCH
        mov r10, 2
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 128
        mov r9, 256
        add r9, SCRATCH
        mov r10, 3
        mov r11, rsp
        call sum_branch

        mov r9, 640
        add r9, SCRATCH
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+16]
        mov r11, rsp
        call copy_candidates
       ;---------------6f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+20]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 6
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 384
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-192]
        vmovdqu16 zmm7, zword [rdi+rax-128]

        vmovdqu16 zmm9, zword [rdi+rax+128]
        vmovdqu16 zmm10, zword [rdi+rax+192]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r8, SCRATCH
        mov r10, 1
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 0
        mov r9, 128
        add r8, SCRATCH
        mov r10, 4
        mov r11, rsp
        call sum_branch

       vmovdqu16 zmm7, zword [rdi+rax-64]
       vmovdqu16 zmm8, zword [rdi+rax]
       vmovdqu16 zmm9, zword [rdi+rax+64]

       vmovaps zmm2, zmm7
       vmovaps zmm13, zmm9
       mov r8, 384
       mov r9, 640
       add r8, SCRATCH
       mov r10, 3
       mov r11, rsp
       call sum_branch
       mov r9, 1024
       mov r11, rsp
       call sum_spine

       mov r14d, dword[rsp+THRESHOLD+20]
       mov r11, rsp
       call copy_candidates
       ;---------------7f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+24]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 7
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 448
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-192]
        vmovdqu16 zmm7, zword [rdi+rax-128]

        vmovdqu16 zmm9, zword [rdi+rax+128]
        vmovdqu16 zmm10, zword [rdi+rax+192]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r9, SCRATCH
        mov r10, 3
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 256
        mov r9, 384
        add r9, SCRATCH
        mov r10, 4
        mov r11, rsp
        call sum_branch

        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 640
        mov r9, 896
        add r9, SCRATCH
        mov r10, 4
        mov r11, rsp
        call sum_branch

        mov r9, 1280
        add r9, SCRATCH
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+24]
        mov r11, rsp
        call copy_candidates
       ;---------------8f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+28]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 8
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 512
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-256]
        vmovdqu16 zmm7, zword [rdi+rax-192]

        vmovdqu16 zmm9, zword [rdi+rax+192]
        vmovdqu16 zmm10, zword [rdi+rax+256]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r8, SCRATCH
        mov r10, 1
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 0
        mov r9, 128
        add r8, SCRATCH
        mov r10, 4
        mov r11, rsp
        call sum_branch

        vmovdqu16 zmm6, zword [rdi+rax-128]
        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]
        vmovdqu16 zmm10, zword [rdi+rax+128]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 384
        mov r9, 640
        add r8, SCRATCH
        mov r10, 5
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 896
        mov r9, 1152
        add r8, SCRATCH
        mov r10, 5
        mov r11, rsp
        call sum_branch

        mov r9, 1920
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+28]
        mov r11, rsp
        call copy_candidates
       ;---------------9f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+32]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 9
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 576
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-256]
        vmovdqu16 zmm7, zword [rdi+rax-192]

        vmovdqu16 zmm9, zword [rdi+rax+192]
        vmovdqu16 zmm10, zword [rdi+rax+256]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r9, SCRATCH
        mov r10, 2
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 128
        mov r9, 256
        add r9, SCRATCH
        mov r10, 5
        mov r11, rsp
        call sum_branch

        vmovdqu16 zmm6, zword [rdi+rax-128]
        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]
        vmovdqu16 zmm10, zword [rdi+rax+128]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 640
        mov r9, 896
        add r9, SCRATCH
        mov r10, 6
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 1280
        mov r9, 1664
        add r9, SCRATCH
        mov r10, 6
        mov r11, rsp
        call sum_branch

        mov r9, 2432
        add r9, SCRATCH
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+32]
        mov r11, rsp
        call copy_candidates
        ;---------------10f0---------------------------------

        mov r14d, dword[rsp+THRESHOLD+36]
        movd xmm0, r14d
        vpbroadcastd zmm0, xmm0
        vcvtdq2ps zmm0, zmm0

        mov r14, 10
        movd xmm4, r14d
        vpbroadcastd zmm4, xmm4

        mov rax, 640
        mul r13
        sub rax, 64

        vmovdqu16 zmm6, zword [rdi+rax-320]
        vmovdqu16 zmm7, zword [rdi+rax-256]

        vmovdqu16 zmm9, zword [rdi+rax+256]
        vmovdqu16 zmm10, zword [rdi+rax+320]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 0
        mov r9, 0
        add r8, SCRATCH
        mov r10, 1
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 0
        mov r9, 128
        add r8, SCRATCH
        mov r10, 3
        mov r11, rsp
        call sum_branch

        vmovdqu16 zmm6, zword [rdi+rax-192]
        vmovdqu16 zmm7, zword [rdi+rax-128]

        vmovdqu16 zmm9, zword [rdi+rax+128]
        vmovdqu16 zmm10, zword [rdi+rax+192]

        vmovaps zmm2, zmm6
        vmovaps zmm13, zmm10
        mov r8, 512
        mov r9, 768
        add r8, SCRATCH
        mov r10, 6
        mov r11, rsp
        call sum_branch

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 896
        mov r9, 1280
        add r8, SCRATCH
        mov r10, 7
        mov r11, rsp
        call sum_branch

        vmovdqu16 zmm7, zword [rdi+rax-64]
        vmovdqu16 zmm8, zword [rdi+rax]
        vmovdqu16 zmm9, zword [rdi+rax+64]

        vmovaps zmm2, zmm7
        vmovaps zmm13, zmm9
        mov r8, 1664
        mov r9, 2176
        add r8, SCRATCH
        mov r10, 7
        mov r11, rsp
        call sum_branch

        mov r9, 3072
        mov r11, rsp
        call sum_spine

        mov r14d, dword[rsp+THRESHOLD+36]
        mov r11, rsp
        call copy_candidates
;;---------------11f0---------------------------------
;
;        mov r14d, dword[rsp+THRESHOLD+40]
;        movd xmm0, r14d
;        vpbroadcastd zmm0, xmm0
;        vcvtdq2ps zmm0, zmm0
;
;        mov r14, 11
;        movd xmm4, r14d
;        vpbroadcastd zmm4, xmm4
;
;        mov rax, 704
;        mul r13
;        sub rax, 64
;
;        vmovdqu16 zmm6, zword [rdi+rax-320]
;        vmovdqu16 zmm7, zword [rdi+rax-256]
;
;        vmovdqu16 zmm9, zword [rdi+rax+256]
;        vmovdqu16 zmm10, zword [rdi+rax+320]
;
;        vmovaps zmm2, zmm6
;        vmovaps zmm13, zmm10
;        mov r8, 0
;        mov r9, 0
;        add r9, SCRATCH
;        mov r10, 2
;        mov r11, rsp
;        call sum_branch
;
;        vmovaps zmm2, zmm7
;        vmovaps zmm13, zmm9
;        mov r8, 128
;        mov r9, 256
;        add r9, SCRATCH
;        mov r10, 4
;        mov r11, rsp
;        call sum_branch
;
;        vmovdqu16 zmm6, zword [rdi+rax-192]
;        vmovdqu16 zmm7, zword [rdi+rax-128]
;        vmovdqu16 zmm9, zword [rdi+rax+128]
;        vmovdqu16 zmm10, zword [rdi+rax+192]
;
;        vmovaps zmm2, zmm6
;        vmovaps zmm13, zmm10
;        mov r8, 512
;        mov r9, 768
;        add r9, SCRATCH
;        mov r10, 7
;        mov r11, rsp
;        call sum_branch
;
;        vmovaps zmm2, zmm7
;        vmovaps zmm13, zmm9
;        mov r8, 2176
;        mov r9, 2688
;        add r9, SCRATCH
;        mov r10, 8
;        mov r11, rsp
;        call sum_branch
;
;        vmovdqu16 zmm7, zword [rdi+rax-64]
;        vmovdqu16 zmm8, zword [rdi+rax]
;        vmovdqu16 zmm9, zword [rdi+rax+64]
;
;        vmovaps zmm2, zmm7
;        vmovaps zmm13, zmm9
;        mov r8, 2176
;        mov r9, 2688
;        add r9, SCRATCH
;        mov r10, 8
;        mov r11, rsp
;        call sum_branch
;
;        mov r9, 3712
;        add r9, SCRATCH
;        mov r11, rsp
;        call sum_spine
;
;        mov r14d, dword[rsp+THRESHOLD+40]
;        mov r11, rsp
;        call copy_candidates
;
;;---------------12f0---------------------------------
;
;        mov r14, 12
;        movd xmm4, r14d
;        vpbroadcastd zmm4, xmm4
;
;;        mov rax, 768
;;        mul r13
;;        sub rax, 64
;;
;;        vmovdqu16 zmm6, zword [rdi+rax-386]
;;        vmovdqu16 zmm7, zword [rdi+rax-320]
;;
;;        vmovdqu16 zmm9, zword [rdi+rax+320]
;;        vmovdqu16 zmm10, zword [rdi+rax+386]
;;
;;        vmovaps zmm2, zmm6
;;        vmovaps zmm13, zmm10
;;        mov r8, 0
;;        mov r9, 0
;;        add r8, SCRATCH
;;        mov r10, 1
;;        mov r11, rsp
;;        call sum_branch
;;
;;        vmovaps zmm2, zmm7
;;        vmovaps zmm13, zmm9
;;        mov r8, 0
;;        mov r9, 128
;;        add r8, SCRATCH
;;        mov r10, 3
;;        mov r11, rsp
;;        call sum_branch
;;
;;        vmovdqu16 zmm6, zword [rdi+rax-256]
;;        vmovdqu16 zmm7, zword [rdi+rax-192]
;;        vmovdqu16 zmm9, zword [rdi+rax+192]
;;        vmovdqu16 zmm10, zword [rdi+rax+256]
;;
;;        vmovaps zmm2, zmm6
;;        vmovaps zmm13, zmm10
;;        mov r8, 256
;;        mov r9, 640
;;        add r8, SCRATCH
;;        mov r10, 5
;;        mov r11, rsp
;;        call sum_branch
;;
;;        vmovaps zmm2, zmm7
;;        vmovaps zmm13, zmm9
;;        mov r8, 768
;;        mov r9, 1280
;;        add r8, SCRATCH
;;        mov r10, 8
;;        mov r11, rsp
;;        call sum_branch
;;
;;        vmovdqu16 zmm6, zword [rdi+rax-128]
;;        vmovdqu16 zmm7, zword [rdi+rax-64]
;;        vmovdqu16 zmm8, zword [rdi+rax]
;;        vmovdqu16 zmm9, zword [rdi+rax+64]
;;        vmovdqu16 zmm10, zword [rdi+rax-128]
;;
;;        vmovaps zmm2, zmm6
;;        vmovaps zmm13, zmm10
;;        mov r8, 1664
;;        mov r9, 2304
;;        add r8, SCRATCH
;;        mov r10, 9
;;        mov r11, rsp
;;        call sum_branch
;;
;;        vmovaps zmm2, zmm7
;;        vmovaps zmm13, zmm9
;;        mov r8, 2688
;;        mov r9, 3456
;;        add r8, SCRATCH
;;        mov r10, 9
;;        mov r11, rsp
;;        call sum_branch
;;
;;        mov r9, 4608
;;        mov r11, rsp
;;        call sum_spine
;;
;;
;------------check the candidates-----------------------
done_copy:

        inc r13
        add r15, 64
        mov rcx, qword[rsp+NUM_BINS]
        cmp r13, rcx
        jl sample_loop

done:
        mov r8, qword[rsp+COUNTER]
        mov rdx, qword[rsp+RDX_REG]
        mov qword [rdx], r8
        mov r8, qword[rsp+RSP_OFFSET]
        add r8, STACK_SIZE
        add rsp, r8
        pop rbp
        pop r15
        pop r14
        pop r13
        pop r12
        pop rdi
        pop rsi
        pop rbx
        pop rdx
        ret

print:
        mov r10, 11111
        mov rdx, qword[rsp+RDX_REG]
        mov qword[rdx+8], r10
        vmovdqu16 zword [rsi], zmm0
        jmp done

sum_branch:
    xor r12, r12
loop1:
    vmovaps zmm14, zword [r11+r8]
    vaddps zmm1, zmm14, zmm2
    vmovaps zword [r11+r9], zmm1
    vcmpps k1, zmm0, zmm1, CMP_LT
    vmovaps zmm0{k1}, zmm1
    vmovaps zmm3{k1}, zmm4

    vmovaps zmm14, zword [r11+r8+64]
    vaddps zmm1, zmm14, zmm13
    vmovaps zword [r11+r9+64], zmm1
    vcmpps k1, zmm0, zmm1, CMP_LT
    vmovaps zmm0{k1}, zmm1
    vmovaps zmm3{k1}, zmm4

    add r8, 128
    add r9, 128
    inc r12
    cmp r10, r12
    jl loop1
    ret

sum_spine:

    vaddps zmm1, zmm15, zmm7
    vmovaps zword [r11+r9], zmm1
    vcmpps k1, zmm0, zmm1, CMP_LT
    vmovaps zmm0{k1}, zmm1
    vmovaps zmm3{k1}, zmm4


    vaddps zmm1, zmm15, zmm9
    vmovaps zword [r11+r9+64], zmm1
    vcmpps k1, zmm0, zmm1, CMP_LT
    vmovaps zmm0{k1}, zmm1
    vmovaps zmm3{k1}, zmm4


    vaddps zmm15, zmm8, zmm15
    vcmpps k1, zmm0, zmm15, CMP_LT
    vmovaps zmm0{k1}, zmm15
    vmovaps zmm3{k1}, zmm4

    ret

copy_candidates:
    ; dm_index
    ; snr
    ; summer index
    ; bin number
    ;r14: thresold
    ;r11: rsp

    ;mov r14, qword[rsp+THRESHOLD]
    movd xmm1, r14d
    vpbroadcastd zmm1, xmm1
    vcvtdq2ps zmm1, zmm1
    vcmpps k1, zmm1, zmm0, CMP_LT
    kmovw eax,k1
    cmp rax, 0
    je donot_copy

    mov r14d, 100 ;
    movd xmm4, r14d
    vpbroadcastd zmm7, xmm4
    vcvtdq2ps zmm7, zmm7
    vmulps zmm0, zmm0, zmm7
    vcvtps2dq zmm0, zmm0

    vmovdqu16 zword [r11+TEMP], zmm0
    vmovdqu16 zword [r11+64+TEMP], zmm3


    mov r8, qword[r11+COUNTER]
    xor r14, r14
    xor r9, r9
index_loop:
    mov r10d, dword[r11+r14+64+TEMP]
    cmp r10d, 0
    je continue
    mov rax, 16
    mul r8
    inc r8
    mov dword [rsi+rax], r9d
    mov dword [rsi+rax+4], r13d
    mov dword [rsi+rax+8], r10d
    mov r10, 0
    mov r10d, dword[r11+r14+TEMP]
    mov dword [rsi+rax+12], r10d
    ;cmp r10, 100000
    ;jg print
continue:
    inc r9
    add r14, 4
    cmp r9, 16
    jl index_loop
    mov qword[r11+COUNTER], r8
    ;vmovdqu16 zword [rsi], zmm0
    ;jmp done
donot_copy:
    ret