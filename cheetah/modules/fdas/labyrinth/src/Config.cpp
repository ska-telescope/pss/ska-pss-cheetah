/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/labyrinth/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {


Config::Config()
    : cheetah::utils::Config("labyrinth")
    , _active(true)
    , _frequency_cutoff(500.0*data::hz)
    , _threshold(8.0)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active)->default_value(_active), "activate the labyrinth zero acceleration search algorithm")
    ("frequency_cutoff", boost::program_options::value<double>()->default_value(_frequency_cutoff.value())->notifier([this](double v) { _frequency_cutoff = v*data::hz; }), "The highest cutoff frequency to search")
    ("threshold", boost::program_options::value<float>(&_threshold)->default_value(_threshold), "The highest cutoff frequency to search");

}

bool Config::active() const
{
    return _active;
}

void Config::active(bool value)
{
    _active = value;
}

typename Config::FrequencyType Config::frequency_cutoff() const
{
    return _frequency_cutoff;
}

void Config::frequency_cutoff(typename Config::FrequencyType val)
{
    _frequency_cutoff = val;
}

float Config::threshold() const
{
    return _threshold;
}

void Config::threshold(float val)
{
    _threshold = val;
}

} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
