/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDAS_H
#define SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDAS_H

#include "cheetah/modules/fdas/labyrinth/Config.h"
#include "cheetah/modules/fdas/labyrinth/FdasPlan.h"
#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/fft/Fft.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/detail/DmTimeSlice.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace labyrinth {

/**
 * @brief    labyrinth is a cpu implementation of the zero acceleration search module
 * @tparam     T     The internal value type to use for processing (float or double)
 */
template <typename T>
class Fdas
    : public utils::AlgorithmBase<Config, fdas::Config>
{
    private:
        typedef data::DmTrials<cheetah::Cpu, float> DmTrialsType;
        typedef data::DmTime<DmTrialsType> DmTimeType;
        typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;


    public:
        typedef cheetah::Cpu Architecture;
        typedef typename fft::fftw::Fft<float> FftType;
    public:

        /**
         * @brief      Construct a new fdas instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Fdas(Config const& config, fdas::Config const& algo_config);
        Fdas(Fdas const&) = delete;
        Fdas(Fdas&&) = default;
        ~Fdas();

        /**
         * @brief      Search a DmTimeSlice for significant periodic signals
         *             at a range of accelerations
         *
         * @param      cpu   The cpu to process on
         * @param      data  the DmTimeSlice to search
         *
         * @return     number of candidates stored in _plan->out_cands() vector returned by nasm kernel.
         */
        unsigned process(panda::PoolResource<Architecture>& cpu,
            DmTimeSliceType const& data);

        /**
         * @brief  method to normalize the time data before FFT.
         *
        */
        void normalize_data(data::TimeSeries<Architecture, float>& time_data);

        /**
         * @brief  method to call the labyrinth nasm code.
         *
        */
        unsigned call_nasm_labyrinth(std::vector<float> const& fop,  unsigned int num_bins, float thresold);

        /**
         * @brief  covert the nasm output to Ccl format.
        */
        data::Ccl::CandidateType make_candidate(int dm_index, int bin_number, int harmonic, int power_100);


        /**
         * @brief  version of process method for async calls
         */
        std::shared_ptr<data::Ccl> operator()(panda::PoolResource<Architecture>& cpu, std::shared_ptr<DmTimeType> const& data);

        std::shared_ptr<data::Ccl> process(panda::PoolResource<Architecture>& cpu, std::shared_ptr<DmTimeType> const& data);

    private:
        Config const& _config;
        FdasPlan _plan;
        unsigned _current_range;
        unsigned _current_dm_index;
        unsigned _count;
};


} // namespace labyrinth
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fdas/labyrinth/detail/Fdas.cpp"

#endif // SKA_CHEETAH_MODULES_FDAS_LABYRINTH_FDAS_H