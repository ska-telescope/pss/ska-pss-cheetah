include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fdas_src src/gtest_fdas.cpp)

add_executable(gtest_fdas ${gtest_fdas_src})
target_link_libraries(gtest_fdas ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fdas gtest_fdas --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
