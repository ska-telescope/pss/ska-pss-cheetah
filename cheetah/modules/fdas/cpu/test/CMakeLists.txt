include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fdas_cpu_src src/gtest_fdas_cpu.cpp)

add_executable(gtest_fdas_cpu ${gtest_fdas_cpu_src})
target_link_libraries(gtest_fdas_cpu ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fdas_cpu gtest_fdas_cpu --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
