/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FDAS_CPU_FDAS_H
#define SKA_CHEETAH_MODULES_FDAS_CPU_FDAS_H

#include "cheetah/modules/fdas/cpu/Config.h"
#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/tdrt/Tdrt.h"
#include "cheetah/modules/tdao/Tdao.h"
#include "cheetah/modules/pwft/Pwft.h"
#include "cheetah/modules/hrms/Hrms.h"
#include "cheetah/modules/brdz/Brdz.h"
#include "cheetah/modules/fft/Fft.h"
#include "cheetah/modules/dred/Dred.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/detail/DmTimeSlice.h"
#include "panda/arch/nvidia/DeviceCapability.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace cpu {

/**
 * @brief    Cpu implementation of the fdas module
 *
 * @details  The fdas module supports frequency domain acceleration search.
 *
 * @tparam     T     The internal value type to use for processing (float or double)
 */
template <typename T>
class Fdas
    : public utils::AlgorithmBase<Config, fdas::Config>
{
    private:
        typedef data::DmTrials<cheetah::Cpu, float> DmTrialsType;
        typedef data::DmTime<DmTrialsType> DmTimeType;
        typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

    public:
        typedef cheetah::Cpu Architecture;

    public:

        /**
         * @brief      Construct a new fdas instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Fdas(Config const& config, fdas::Config const& algo_config);
        Fdas(Fdas const&) = delete;
        Fdas(Fdas&&) = default;
        ~Fdas();

        /**
         * @brief      Search a DmTimeSlice for significant periodic signals
         *             at a range of accelerations
         *
         * @param      cpu   The cpu to process on
         * @param      data  The DmTimeSlice to search
         *
         * @return     A shared pointer to a list of candidate signals
         */
        std::shared_ptr<data::Ccl> process(panda::PoolResource<Architecture>& cpu,
            std::shared_ptr<DmTimeType> const& data);

        /**
         * @brief      Version of process method for async calls
         */
        std::shared_ptr<data::Ccl> operator()(panda::PoolResource<Architecture>& cpu,
            std::shared_ptr<DmTimeType> const& data);
};


} // namespace cpu
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fdas/cpu/detail/Fdas.cpp"

#endif // SKA_CHEETAH_MODULES_FDAS_CPU_FDAS_H