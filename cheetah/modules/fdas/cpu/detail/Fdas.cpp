/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fdas/cpu/Fdas.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace cpu {

template <typename T>
Fdas<T>::Fdas(Config const& config, fdas::Config const& algo_config)
    : utils::AlgorithmBase<Config, fdas::Config>(config, algo_config)
{
    PANDA_LOG<<"FDAS initiated";
}

template <typename T>
Fdas<T>::~Fdas()
{
}

template <typename T>
std::shared_ptr<data::Ccl> Fdas<T>::process(panda::PoolResource<Architecture>& /*device*/,
    std::shared_ptr<DmTimeType> const& /*data*/)
{
    auto candidate_list_ptr = std::make_shared<data::Ccl>();

    return candidate_list_ptr;
}

template <typename T>
std::shared_ptr<data::Ccl> Fdas<T>::operator()(panda::PoolResource<Architecture>& device,
    std::shared_ptr<DmTimeType> const& data)
{
    PANDA_LOG<<"running FDAS ";
    return process(device,data);
}

} // namespace cpu
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska