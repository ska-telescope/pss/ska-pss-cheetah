set(MODULE_OPENCL_LIB_SRC_CPU
    src/Config.cpp
    src/Fdas.cpp
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
