/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fdas/intel_fpga/Fdas.h"
#include "cheetah/data/Candidate.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {

template<typename T>
Fdas<T>::Fdas(Config const& config, fdas::Config const& algo_config)
    : utils::AlgorithmBase<Config, fdas::Config>(config, algo_config)
    , _intel_fpga_specific_config{algo_config.config<FpgaConfigType>()}
    , _config{config}
    , _current_range{0}
    , _number_dms_per_slice{_intel_fpga_specific_config.number_dms_per_slice()}
    , _current_dm_index{0}
{
    device_configuration(_intel_fpga_specific_config);
    PANDA_LOG << "FPGA FDAS initiated";
}

template<typename T>
Fdas<T>::~Fdas()
{
}

template <typename T>
void Fdas<T>::normalize_data(data::TimeSeries<cheetah::Cpu, float>& time_data)
{
    double sum = std::accumulate(time_data.begin(), time_data.end(), 0.0);
    double mean = sum / time_data.size();

    // Mean subtracted
    std::vector<double> diff(time_data.size());
    std::transform(time_data.begin(), time_data.end(), diff.begin(), [mean](double x) { return x - mean; });

    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);// sum of products
    double stdev = std::sqrt(sq_sum / time_data.size());

    std::transform(time_data.begin(), time_data.end(), time_data.begin(), [mean,stdev](float x) { return (x - (float)mean)/(float)stdev; });
}

template <typename T>
unsigned Fdas<T>::process(panda::PoolResource<Architecture>& device, DmTimeSliceType const& data)
{
    _plan.time_data()[_current_range].resize(_plan.fft_size()[_current_range]);
    _plan.freq_data()[_current_range].resize(_plan.fft_size()[_current_range]/2+1);

    // DmTimeSliceIterator of the first DM trial in the slice
    auto it = data.cbegin();

    //Copy single trial from DmTimeSlice into time series vector
    (*it).copy_to(_plan.time_data()[_current_range]);

    normalize_data(_plan.time_data()[_current_range]);

    // running the fft
    _plan.fft()[_current_range].process(device, _plan.time_data()[_current_range], _plan.freq_data()[_current_range]);

    // scaling factor related to the number of samples in the time series.
    double _constant_factor = std::sqrt(2./((double)_plan.fft_size()[_current_range]));
    std::transform(_plan.freq_data()[_current_range].begin(),
                   _plan.freq_data()[_current_range].end(),
                   _plan.freq_data()[_current_range].begin(),
                   [&_constant_factor](std::complex<double> x)
                   {
                        return std::complex<float>(x.real()*_constant_factor, x.imag()*_constant_factor);
                   });

    // DC bin to zero
    *_plan.freq_data()[_current_range].begin() = 0.0;

    // set hugepage address where DM-f was located
    auto _addr = _plan.freq_data()[_current_range].allocator().hugepage_address();
    device.hugepage(_addr);

    ++_current_dm_index;

    // verify to change range index of the DMs (different number of samples)
    if(_current_dm_index==_plan.ndms_per_range()[_current_range])
    {
        PANDA_LOG_DEBUG << " Changing DM index: " << _current_dm_index << " Range: " <<  _current_range;
        _current_dm_index = 0;
        ++_current_range;
    }

    return 0;
}



template<typename T>
std::shared_ptr<data::Ccl> Fdas<T>::process(panda::PoolResource<Architecture>& device
                                            , std::shared_ptr<DmTimeType> const& data
                                           )
{

    auto candidate_list_ptr = std::make_shared<data::Ccl>();
    CandidateType candidate;

    // It checks that the DmTime structure contains
    // at least one DM to be processed by CXFT.
    if (data->blocks().size() > 0)
    {

        // initilisation of the fft computing of fftw-wisdom (which it takes some time to be computed)
        PANDA_LOG<< "starting FFT initialization";
        _plan(device, data);
        PANDA_LOG<< "done FFT initialization";

        // waiting for Psbc to store DmTrials into DmTime container
        data->wait();

        // observation time
        auto _duration = data->dump_time();

        PANDA_LOG << " running FPGA FDAS for an observation duration: " << _duration;

        auto _start = std::chrono::high_resolution_clock::now();

        // it is DmTimeIterator that gives a pointer to an slice (DmTimeSlice) composed by a _number_dms_per_slice DMs
        for(auto it=data->begin(_number_dms_per_slice); it<data->end(); ++it)
        {
            auto dm_time_slice = *it; // operator*() returns shared_ptr of DmTimeSlice

            // perform CFXT
            if (process(device,*dm_time_slice) < 0)
            {
                PANDA_LOG_ERROR << " CFXT on FPGA FDAS failed!";
            }

            // fpga configuration parameters
            device.configure(_device_config);

            if(device.run_fdas())
            {
                PANDA_LOG_ERROR << "run_fdas() failed!";
            }

            //time duration of the DmTrials (number_of_samples * sampling_interval)
            auto& dmtrials = *(data->blocks().front());

            PANDA_LOG_DEBUG
                    << " DM Trials duration: " << dmtrials.duration()
                    << " DM: " << (*(*dm_time_slice).cbegin()).dm()
                    << " number of samples: " << (*(*dm_time_slice).cbegin()).number_of_samples()
                    << " sampling interval: " << (*(*dm_time_slice).cbegin()).sampling_interval()
                    << " duration: " << (*(*dm_time_slice).cbegin()).number_of_samples() * (*(*dm_time_slice).cbegin()).sampling_interval()
                    ;

            CandidateContainerType const& cand_list = device.candidates();

            for(auto candidate_vec : cand_list)
            {

                // checks rows are not zero
                if (std::all_of(candidate_vec.begin()+1, candidate_vec.end(), [](std::int32_t x){ return x == 0; }))
                {
                    continue; // Found a zero row!
                }

                std::size_t fop_column = candidate_vec[2];

                double period = _duration.value()/(double) fop_column;
                typename CandidateType::MsecTimeType period_val(period * boost::units::si::seconds);

                // FOP ROW difference from the centre
                auto offset = candidate_vec[3] - int(candidate_vec[3]/16)*2 ;

                // positive acceleration goes from 0-42 in the FOP plane
                auto sign = offset%2 ? -1 : 1 ;

                auto foprow_diff = sign * offset/2;

                // acceleration, the equation is given at AT4-1136
                auto filter_tap = std::abs(foprow_diff) * 10 + 1;
                auto sig_num = (foprow_diff > 0) ? 1 : ((foprow_diff < 0) ? -1 : 0); // signum function
                auto acceleration = sig_num * foprow_diff * (filter_tap - 1) * 515 * period ;

                typename CandidateType::SecPerSecType pdot_val(acceleration);

                typename CandidateType::Dm dm_val((*(*dm_time_slice).cbegin()).dm().value() * data::parsecs_per_cube_cm);

                candidate.period(period_val);
                candidate.pdot(pdot_val);
                candidate.dm(dm_val);
                candidate.harmonic(candidate_vec[1]);
                candidate.sigma(candidate_vec[4]);

                candidate_list_ptr->add(candidate);
                PANDA_LOG_DEBUG
			            << "Candidate list period: " << period_val
                        << " pdot: " << pdot_val.value()
                        << " dm: " << dm_val.value()
                        << " harmonic: " << candidate_vec[1] ;
            }

        }
        auto _stop = std::chrono::high_resolution_clock::now();
        PANDA_LOG   << " FPGA Fdas processing time: "
                    << std::chrono::duration_cast<std::chrono::nanoseconds>(_stop - _start).count()/1000000.0
                    << " ms\n";
    }

    return candidate_list_ptr;
}

template<typename T>
std::shared_ptr<data::Ccl> Fdas<T>::operator()(panda::PoolResource<Architecture>& device
                                               , std::shared_ptr<DmTimeType> const& data
                                              )
{
    std::stringstream dmtime_log_debug_;
    dmtime_log_debug_ << " Fdas<IntelFpga>::operator() invoked\n";
    for(auto const & dm_trials : data->blocks())
    {
        dmtime_log_debug_
        << "Number of DmTrials object : " << dm_trials->size() <<"\n"
        << "Number of samples from each DM contained in the DMTrials object: " << dm_trials->metadata().number_of_samples() << "\n"
        << "Total number of samples in DmTrials object: " << dm_trials->metadata().total_data_size()
        ;
    }

    PANDA_LOG_DEBUG << dmtime_log_debug_.str();


    return process(device, data);
}

template <typename T>
void Fdas<T>::device_configuration(FpgaConfigType const& fdas_config)
{
    _device_config.buffer_transfer_size(fdas_config.buffer_transfer_size());
    _device_config.filters_filename(fdas_config.filters_filename());
    _device_config.hsum_filename(fdas_config.hsum_filename());
    _device_config.number_of_filters(fdas_config.number_of_filters());

}


} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
