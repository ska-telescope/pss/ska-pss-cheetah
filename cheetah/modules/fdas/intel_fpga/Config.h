/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_CONFIG_H
#define SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_CONFIG_H

#include "cheetah/utils/Config.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {

/**
 * @brief
 *    Configuration specific to the cpu algorithm
 *
 * @details
 *
 */
class Config : public cheetah::utils::Config
{
    public:
        Config();
        Config(std::string const&, std::string const&);
        ~Config();

        /**
         * @brief return true if this algorithm is to be used
         */
        bool active() const;

         /**
         * @brief set the activation value
         */
        void active(bool);

        /**
         * @brief set the filename that contains configuration of fdas filter
         * coefficients
         */
        void filters_filename(std::string const&);

        /**
         * @brief gets the configured filename with fdas filter information
         */
        std::string const& filters_filename() const;

        /**
         * @brief sets filename with the hsum configuration
         */
        void hsum_filename(std::string const&);

        /**
         * @brief gets filename with the fdas fpga hsum configuration
         */
        std::string const& hsum_filename() const;

        /**
         * @brief sets the number of filters into the fdas fpga
         */
        void number_of_filters(std::uint32_t const&);

        /**
         * @brief gets the number of filter in the fdas fpga
         */
        std::uint32_t const& number_of_filters() const;

        /**
         * @brief Gets the value of the variable that represents the buffer
         *        size in bytes used in the configuration of the host memory
         *        space to be used in host DMA transfer to the fpga.
         */
        std::uint32_t const& buffer_transfer_size() const;

        /**
         * @brief Sets a variable with the size of the buffer in bytes that will be used
         *        to configure the space in host memory that will be required
         *        for DMA transfer into the fpga.
         */
        void buffer_transfer_size(std::uint32_t const&);

        /**
         * @brief The number DM trials per slice
         */
        unsigned const& number_dms_per_slice() const;
        void number_dms_per_slice(unsigned const&);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        /// @brief number of filters in filter file
        std::uint32_t _number_of_filters;
        /// @brief file with filters used by fdas algorithm
        std::string _filters_filename;
        /// @brief file with particular configuration of fdas algorithm
        std::string _hsum_filename;
        /// @brief size in bytes of the input data to be DMA transferred to FPGA
        std::uint32_t _buffer_transfer_size;
        /// @brief number of DMs in a slice
        unsigned _number_dms_per_slice;
};

// default location of the fdas configuration files
static const std::string default_hsum_configuration_file{"/opt/pss-fdas-fpga/etc/config.txt"};
static const std::string default_filters_configuration_file{"/opt/pss-fdas-fpga/etc/filter.dat"};

} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_CONFIG_H
