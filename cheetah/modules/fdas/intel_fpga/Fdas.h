/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_FDAS_H
#define SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_FDAS_H

#include "cheetah/modules/fdas/intel_fpga/Config.h"
#include "cheetah/modules/fdas/intel_fpga/FdasPlan.h"
#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/fft/Fft.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/detail/DmTimeSlice.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {

/**
 * @brief    FPGA implementation of the fdas module
 *
 * @details  The fdas module supports frequency domain acceleration search.
 *
 * @tparam     T     The internal value type to use for processing (float or double)
 */
template <typename T>
class Fdas
    : public utils::AlgorithmBase<Config, fdas::Config>
{
    private:
        typedef data::DmTrials<cheetah::Cpu, float> DmTrialsType;
        typedef data::DmTime<DmTrialsType> DmTimeType;
        typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

    public:
        typedef panda::intel_fpga::Rtl Architecture;
        typedef data::Candidate<Cpu, float> CandidateType;
        typedef panda::DeviceConfig<Architecture> DeviceConfigType;
        typedef std::vector<std::vector<std::int32_t>> CandidateContainerType;

    private:
        typedef intel_fpga::Config FpgaConfigType;

    public:

        /**
         * @brief      Construct a new fdas instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Fdas(Config const& config, fdas::Config const& algo_config);
        Fdas(Fdas const&) = delete;
        Fdas(Fdas&&) = default;
        ~Fdas();

        /**
         * @brief  method to perform FFT of a DM slice.
         *
        */
        unsigned process(panda::PoolResource<Architecture>& cpu,
            DmTimeSliceType const& data);

        /**
         * @brief  method to normalize the time data before FFT.
         *
        */
        void normalize_data(data::TimeSeries<cheetah::Cpu, float>& time_data);

        /**
         * @brief      Search a DmTimeSlice for significant periodic signals
         *             at a range of accelerations
         *
         * @param      fpga  The FPGA on which to process
         * @param      data  The DmTimeSlice to search
         *
         * @return     A shared pointer to a list of pulsar candidates
         */
        std::shared_ptr<data::Ccl> process(panda::PoolResource<Architecture>& fpga
                                           , std::shared_ptr<DmTimeType> const& data
                                          );

        /**
         * @brief      Version of the process() method for async calls
         */
        std::shared_ptr<data::Ccl> operator()(panda::PoolResource<Architecture>& fpga
                                              , std::shared_ptr<DmTimeType> const& data
                                             );
    private:

        /**
         * @brief Collects part of the Config settings and applies them to the
         * configuration of the fpga device.
         */
        void device_configuration(FpgaConfigType const&);

    private:
        /// @brief reference to configuration parameters of the fpga and fdas.
        FpgaConfigType const& _intel_fpga_specific_config;
        /// @brief configuration of the panda device
        Config const& _config;
        DeviceConfigType _device_config;
        unsigned _current_range;
        unsigned _number_dms_per_slice;
        /// @brief index of the DM in the DM_Trial currently being processed
        unsigned _current_dm_index;
        FdasPlan _plan;

};

} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fdas/intel_fpga/detail/Fdas.cpp"

#endif // SKA_CHEETAH_MODULES_FDAS_INTEL_FPGA_FDAS_H
