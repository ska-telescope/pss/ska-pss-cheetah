/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/intel_fpga/Config.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {

Config::Config()
    : cheetah::utils::Config("intel_fpga")
    , _active(true)
    , _number_of_filters(42) // default is set to 42 filters
    , _filters_filename(default_filters_configuration_file)
    , _hsum_filename(default_hsum_configuration_file)
    , _buffer_transfer_size(33554432) // default value 4*2^23
    , _number_dms_per_slice(1)
{
}

Config::Config(std::string const& filters, std::string const& hsum)
    : cheetah::utils::Config("intel_fpga")
    , _active(true)
    , _number_of_filters(42) // default is set to 42 filters
    , _filters_filename(filters)
    , _hsum_filename(hsum)
    , _buffer_transfer_size(33554432) // default value 4*2^23
    , _number_dms_per_slice(1)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    (
     "active", boost::program_options::value<bool>(&_active)->default_value(true)
     , "activate the cpu frequency domain acceleration search algorithm"
    )
    (
     "filters_filename"
     , boost::program_options::value<std::string>()->notifier(
        [&](std::string const& v)
        {
             _filters_filename = v;
        })
     , "file with the FDAS algorithm filter convolution coefficients"
    )
    (
     "hsum_filename"
     , boost::program_options::value<std::string>()->notifier(
        [&](std::string const& v)
        {
             _hsum_filename = v;
        })
     , "file including address of the registers on the FPGA card to perform harmonic summation"
    )
    (
     "number_of_filters"
     , boost::program_options::value<unsigned int>(&_number_of_filters)->default_value(_number_of_filters)->notifier(
        [&](std::uint32_t v)
        {
            _number_of_filters = v;
        })
     , "number of the FDAS filters programmed on the FPGA card"
    )
    (
     "buffer_transfer_size"
     , boost::program_options::value<std::uint32_t>(&_buffer_transfer_size)->default_value(_buffer_transfer_size)->notifier(
        [&](std::uint32_t v)
        {
            _buffer_transfer_size = v;
        })
     , "size in bytes of the data to be transferred between Host to Device"
    )
    (
     "dms_per_slice"
     , boost::program_options::value<unsigned>(&_number_dms_per_slice)->default_value(_number_dms_per_slice)->notifier(
        [&](std::uint32_t v)
        {
            _number_dms_per_slice = v;
        })
     , "the number DM of trials will taken in a slice"
    )
    ;
}

bool Config::active() const
{
    return _active;
}

void Config::active(bool value)
{
    _active = value;
}

std::string const& Config::filters_filename() const
{
    return _filters_filename;
}

void Config::filters_filename(std::string const& name)
{
    _filters_filename = name;
}

std::string const& Config::hsum_filename() const
{
    return _hsum_filename;
}

void Config::hsum_filename(std::string const& name)
{
    _hsum_filename = name;
}

std::uint32_t const& Config::number_of_filters() const
{
    return _number_of_filters;
}

void Config::number_of_filters(std::uint32_t const& value)
{
    _number_of_filters = value;
}

std::uint32_t const& Config::buffer_transfer_size() const
{
    return _buffer_transfer_size;
}

void Config::buffer_transfer_size(std::uint32_t const& value)
{
    _buffer_transfer_size = value;
}

unsigned const& Config::number_dms_per_slice() const
{
    return _number_dms_per_slice;
}

void Config::number_dms_per_slice(unsigned const& value)
{
    _number_dms_per_slice = value;
}

} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
