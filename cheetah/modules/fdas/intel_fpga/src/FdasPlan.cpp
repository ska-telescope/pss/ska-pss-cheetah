/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/intel_fpga/Fdas.h"
#include "panda/Log.h"
#include "panda/Resource.h"
#include <cmath>

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {

FdasPlan::FdasPlan()
    : _fft(1)
    , _time_data(1)
    , _freq_data(1)
    , _dms(16)
    , _ndms_per_range(0)
    , _fft_size(1)
    , _hp_addr()
{
    PANDA_LOG<<" Intel Fpga FDASPLAN called";
}

FdasPlan::~FdasPlan()
{
}

void FdasPlan::operator()(panda::PoolResource<cheetah::IntelFpga>& device, std::shared_ptr<DmTimeType> const& data)
{
    // gets the first DmTrials block from DmTime object.
    // the set of blocks are held as a list of smart pointers: {*DmTrials_1, *DmTrials_2, ...}
    auto& dmtrials = *(data->blocks().front());

    // get the number of samples of the first Dm
    unsigned current_size = dmtrials[0].size();

    // count shall have the total number of Dms
    unsigned int count=0;

    // determines the total of the Dms in the DmTrials object
    // independently of the size of the number of samples.
    for(unsigned int i=0; i<dmtrials.size(); ++i)
    {
        ++count;
        // size() is shrunk by the downsampling factor of the trial
        if(dmtrials[i].size()!=current_size)
        {
            _ndms_per_range.push_back(count-1);
            current_size = dmtrials[i].size();
            count=1;
        }
    }

    // keeps the number of Dms of similar size (range), where
    // size is the number of samples
    _ndms_per_range.push_back(count);

    // resize vectors for every range
    _fft_size.resize(_ndms_per_range.size());
    _time_data.resize(_ndms_per_range.size());
    _freq_data.resize(_ndms_per_range.size());
    _fft.resize(_ndms_per_range.size());
    _hp_addr.resize(_ndms_per_range.size());

    // total number of samples given the dump time and the sampling interval
    _fft_size[0] = (unsigned)(data->dump_time().value()/dmtrials[0].sampling_interval().value());

    // number of bits
    // todo: we need to to round up to 2^23 because of FDAS fpga constraint
    unsigned power = (unsigned)(std::log2((double)_fft_size[0]));

    unsigned dm_index = 0;
    for(unsigned range=0; range<_ndms_per_range.size(); ++range)
    {
        // set to the number of samples for the given range
        _fft_size[range] = std::pow(2,power-range);
        _time_data[range].resize(_fft_size[range]);
        _time_data[range].sampling_interval(dmtrials[dm_index].sampling_interval());

        // allocates (creates) hugepage
        // Note: resize created a new hugepage (1GB) every time is called.
        // we have to optimise the resize method to ensure that a 1GB
        // huge-page allocates the maximum amount of fft data.
        _freq_data[range].resize(_fft_size[range]/2+1);

        // save hugepage address per every range
        _hp_addr[range] =  _freq_data[range].allocator().hugepage_address();

        // initialise FFT, generation of the FFTW wisdom files (optimisation of Fourier transform computation)
        _fft[range].process(device, _time_data[range], _freq_data[range]);
        dm_index += _ndms_per_range[range];
    }

    // get hugepage memory address where DM-f will be stored
    auto _addr = _freq_data[0].allocator().hugepage_address(); // note create vector to save hp addresses
    device.hugepage(_addr);

}



std::vector<FdasPlan::TimeSeriesType>& FdasPlan::time_data()
{
    return _time_data;
}

std::vector<FdasPlan::FrequencySeriesType>& FdasPlan::freq_data()
{
    return _freq_data;
}

std::vector<typename data::DedispersionMeasureType<float>>& FdasPlan::dms()
{
    return _dms;
}

std::vector<unsigned>& FdasPlan::ndms_per_range()
{
    return _ndms_per_range;
}

std::vector<unsigned>& FdasPlan::fft_size()
{
    return _fft_size;
}

std::vector<FdasPlan::FftType>& FdasPlan::fft()
{
    return _fft;
}

std::vector<void*>& FdasPlan::hugepage_address()
{
    return _hp_addr;
}

} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
