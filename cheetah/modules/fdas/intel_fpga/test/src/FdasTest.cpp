/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fdas/Fdas.h"
#include "cheetah/modules/fdas/intel_fpga/Fdas.h"
#include "cheetah/modules/fdas/intel_fpga/Config.h"
#include "cheetah/modules/fdas/intel_fpga/test/FdasTest.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/SimplePulsar.h"
#include <fstream>
#include "panda/test/gtest.h"
#include "panda/test/TestFile.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace intel_fpga {
namespace test {

FdasTest::FdasTest()
    : ::testing::Test()
{
}

FdasTest::~FdasTest()
{
}

void FdasTest::SetUp()
{
}

void FdasTest::TearDown()
{
}

/// We will use for now testing defined in the FdasTest class,
/// which is very specific to the Intel FPGA module. But in
/// the near future we are going to use the FdasTester which
/// is a more general test and is based on Trait types.

TEST_F(FdasTest, test_with_candidates)
{
    typedef typename utils::ModifiedJulianClock::time_point TimePoint;
    typedef typename data::SecondsType<double> Seconds;
    typedef typename fdas::DmTrialsType::DmType Dm;
    typedef data::TimeFrequency<Cpu, float> TimeFrequencyType;

    // Create an Fdas object
    fdas::Config config;
    intel_fpga::Config& fpga_config = config.intel_fpga_config();
    fpga_config.filters_filename("/opt/pss-fdas-fpga/etc/filter.dat");
    fpga_config.hsum_filename("/opt/pss-fdas-fpga/etc/config_60_sec.txt");
    fpga_config.number_of_filters(42);
    intel_fpga::Fdas<float> fdas(fpga_config, config);

    // Create an FpgaAdapter object
    std::shared_ptr<panda::intel_fpga::FpgaAdapter> fpga_adapter =
    std::make_shared<panda::intel_fpga::FpgaAdapter>();

    // Create a pool resource from this FpgaAdapter
    panda::PoolResource<panda::intel_fpga::Rtl> device(fpga_adapter, fpga_adapter->device_id());

    // DM preparation
    auto buffer = fdas::DmTimeType::make_shared();
    buffer->dump_time(61.0*data::seconds);
    typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(40587.0));

    generators::SimplePulsarConfig pulsar_config;
    pulsar_config.mean(60.0);
    pulsar_config.std_deviation(10.0);
    pulsar_config.dm(200.0*data::parsecs_per_cube_cm);
    pulsar_config.period(0.005*data::seconds);
    pulsar_config.width(0.001*data::seconds);
    pulsar_config.snr(1.0);
    generators::SimplePulsar<TimeFrequencyType> pulsar(pulsar_config);
    data::DimensionSize<data::Time> number_of_samples(1<<18);
    data::DimensionSize<data::Frequency> number_of_channels(1);
    double tsamp_us = 64.0;
    double f_low = 600.0;
    double f_step = 330.0/1024.0;

    auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
    auto f1 =  typename TimeFrequencyType::FrequencyType((f_low+f_step*number_of_channels) * boost::units::si::mega * boost::units::si::hertz);
    auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::mega * boost::units::si::hertz);
    auto delta = (f2 - f1)/ (double)number_of_channels;
    tf->set_channel_frequencies_const_width( f1, delta );
    tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
    unsigned total_samples = (unsigned)(buffer->dump_time().value()/tf->sample_interval().value());
    std::size_t number_of_blocks = (size_t)(total_samples/number_of_samples)+1;
    for (std::size_t block=0; block<number_of_blocks; ++block)
    {
        auto metadata = data::DmTrialsMetadata::make_shared(Seconds(tsamp_us * boost::units::si::micro * data::seconds),number_of_samples);
        for(unsigned int i=0; i<10; ++i)
        {
            metadata->emplace_back(Dm((190.0+i*2)*data::parsecs_per_cube_cm),1);
        }
        auto trials = fdas::DmTrialsType::make_shared(metadata,epoch);
        tf->start_time(epoch);
        epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
        pulsar.next(*tf);
        std::copy(tf->begin(), tf->end(), (*trials)[0].begin());
        buffer->add(trials);
    }
    buffer->ready();

    // Call Fdas() and capture the Ccl pointer it retuns
    std::shared_ptr<data::Ccl> cand_list = fdas(device, buffer);
    ASSERT_NE(cand_list->size(),0);

}

} // namespace test
} // namespace intel_fpga
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
