/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fdas/test_utils/FdasTester.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/test_utils/DmTimeTest.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/SimplePulsar.h"
#include <iostream>
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
FdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::FdasTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
fdas::Fdas<T, void>& FdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::api()
{
    return _api;
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
fdas::Config& FdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::config()
{
    return _config;
}

template <typename DeviceType, typename Arch, typename T>
struct ExecTest
{
    /**
    * @brief Simple test which generates DmTime object with zeros and passes it on to Fdas algos
    */
    inline static void test_nocands(DeviceType& device, fdas::Fdas<T>& api)
    {
        typedef typename utils::ModifiedJulianClock::time_point TimePoint;
        typedef typename data::SecondsType<double> Seconds;
        typedef typename fdas::DmTrialsType::DmType Dm;
        std::size_t number_of_blocks = 10L;
        auto buffer = fdas::DmTimeType::make_shared();
        buffer->dump_time(10.0*data::seconds);
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(40587.0));
        for (std::size_t block=0; block<number_of_blocks; ++block)
        {
            auto metadata = data::DmTrialsMetadata::make_shared(Seconds(0.0001*data::seconds),1<<14);
            for(unsigned int i=0; i<32; ++i)
            {
                metadata->emplace_back(Dm((0.0+i*0.1)*data::parsecs_per_cube_cm),1);
            }
            auto trials = fdas::DmTrialsType::make_shared(metadata,epoch);
            buffer->add(trials);
        }
        buffer->ready();
        std::shared_ptr<data::Ccl> candidate_list = api.process(device,buffer);
        ASSERT_EQ(candidate_list->size(),0);
    }

    /**
    * @brief Generates DmTime object with gaussian and passes it on to Fdas algos
    */
   inline static void test_with_noise(DeviceType& device, fdas::Fdas<T>& api)
    {
        typedef typename utils::ModifiedJulianClock::time_point TimePoint;
        typedef typename data::SecondsType<double> Seconds;
        typedef typename fdas::DmTrialsType::DmType Dm;
        typedef data::TimeFrequency<Cpu, float> TimeFrequencyType;


        auto buffer = fdas::DmTimeType::make_shared();
        buffer->dump_time(10.0*data::seconds);
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(40587.0));

        generators::GaussianNoiseConfig noise_config;
        noise_config.mean(60.0);
        noise_config.std_deviation(10.0);
        generators::GaussianNoise<TimeFrequencyType> noise(noise_config);
        data::DimensionSize<data::Time> number_of_samples(1<<14);
        data::DimensionSize<data::Frequency> number_of_channels(1);
        double tsamp_us = 64.0;
        double f_low = 600.0;
        double f_step = 330.0/1024.0;

        auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
        auto f1 =  typename TimeFrequencyType::FrequencyType((f_low+f_step*number_of_channels) * boost::units::si::mega * boost::units::si::hertz);
        auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::mega * boost::units::si::hertz);
        auto delta = (f2 - f1)/ (double)number_of_channels;
        tf->set_channel_frequencies_const_width( f1, delta );
        tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
        unsigned total_samples = (unsigned)(buffer->dump_time().value()/tf->sample_interval().value());
        std::size_t number_of_blocks = (size_t)(total_samples/number_of_samples)+1;
        for (std::size_t block=0; block<number_of_blocks; ++block)
        {
            auto metadata = data::DmTrialsMetadata::make_shared(Seconds(tsamp_us * boost::units::si::micro * data::seconds),number_of_samples);
            for(unsigned int i=0; i<32; ++i)
            {
                metadata->emplace_back(Dm((0.0+i*0.1)*data::parsecs_per_cube_cm),1);
            }
            auto trials = fdas::DmTrialsType::make_shared(metadata,epoch);
            tf->start_time(epoch);
            epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
            noise.next(*tf);
            std::copy(tf->begin(), tf->end(), (*trials)[0].begin());
            buffer->add(trials);
        }
        buffer->ready();
        std::shared_ptr<data::Ccl> candidate_list = api.process(device,buffer);
        ASSERT_EQ(candidate_list->size(),0);
    }

    /**
    * @brief Generates DmTime object with pulsar signal and passes it on to Fdas algos
    */
    inline static void test_with_cands(DeviceType& device, fdas::Fdas<T>& api)
    {
        typedef typename utils::ModifiedJulianClock::time_point TimePoint;
        typedef typename data::SecondsType<double> Seconds;
        typedef typename fdas::DmTrialsType::DmType Dm;
        typedef data::TimeFrequency<Cpu, float> TimeFrequencyType;


        auto buffer = fdas::DmTimeType::make_shared();
        buffer->dump_time(10.0*data::seconds);
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(40587.0));

        generators::SimplePulsarConfig pulsar_config;
        pulsar_config.mean(60.0);
        pulsar_config.std_deviation(10.0);
        pulsar_config.dm(200.0*data::parsecs_per_cube_cm);
        pulsar_config.period(0.005*data::seconds);
        pulsar_config.width(0.001*data::seconds);
        pulsar_config.snr(1.0);
        generators::SimplePulsar<TimeFrequencyType> pulsar(pulsar_config);
        data::DimensionSize<data::Time> number_of_samples(1<<14);
        data::DimensionSize<data::Frequency> number_of_channels(1);
        double tsamp_us = 64.0;
        double f_low = 600.0;
        double f_step = 330.0/1024.0;

        auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
        auto f1 =  typename TimeFrequencyType::FrequencyType((f_low+f_step*number_of_channels) * boost::units::si::mega * boost::units::si::hertz);
        auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::mega * boost::units::si::hertz);
        auto delta = (f2 - f1)/ (double)number_of_channels;
        tf->set_channel_frequencies_const_width( f1, delta );
        tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
        unsigned total_samples = (unsigned)(buffer->dump_time().value()/tf->sample_interval().value());
        std::size_t number_of_blocks = (size_t)(total_samples/number_of_samples)+1;
        for (std::size_t block=0; block<number_of_blocks; ++block)
        {
            auto metadata = data::DmTrialsMetadata::make_shared(Seconds(tsamp_us * boost::units::si::micro * data::seconds),number_of_samples);
            for(unsigned int i=0; i<32; ++i)
            {
                metadata->emplace_back(Dm((0.0+i*0.1)*data::parsecs_per_cube_cm),1);
            }
            auto trials = fdas::DmTrialsType::make_shared(metadata,epoch);
            tf->start_time(epoch);
            epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
            pulsar.next(*tf);
            std::copy(tf->begin(), tf->end(), (*trials)[0].begin());
            buffer->add(trials);
        }
        buffer->ready();
        std::shared_ptr<data::Ccl> candidate_list = api.process(device,buffer);
        ASSERT_NE(candidate_list->size(),0);
    }

};

template <typename TestTraits>
FdasTester<TestTraits>::FdasTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
FdasTester<TestTraits>::~FdasTester()
{
}

template<typename TestTraits>
void FdasTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void FdasTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(FdasTester, test_no_cands)
{
    TypeParam traits;
    auto& api = traits.api();
    ExecTest<typename TypeParam::DeviceType,typename TypeParam::Arch, typename TypeParam::ValueType>::test_nocands(device,api);
}

ALGORITHM_TYPED_TEST_P(FdasTester, test_with_cands)
{
    TypeParam traits;
    auto& api = traits.api();
    ExecTest<typename TypeParam::DeviceType,typename TypeParam::Arch, typename TypeParam::ValueType>::test_with_cands(device,api);
}

ALGORITHM_TYPED_TEST_P(FdasTester, test_with_noise)
{
    TypeParam traits;
    auto& api = traits.api();
    ExecTest<typename TypeParam::DeviceType,typename TypeParam::Arch, typename TypeParam::ValueType>::test_with_noise(device,api);
}

REGISTER_TYPED_TEST_SUITE_P(FdasTester, test_no_cands, test_with_cands, test_with_noise);

} // namespace test
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska
