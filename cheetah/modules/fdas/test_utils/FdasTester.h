/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FDAS_TEST_FDASTESTER_H
#define SKA_CHEETAH_MODULES_FDAS_TEST_FDASTESTER_H

#include "cheetah/modules/fdas/Config.h"
#include "cheetah/modules/fdas/Fdas.h"
#include "cheetah/utils/NullHandler.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/Units.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace fdas {
namespace test {

/**
 * @brief
 * Generic functional test for the FdasTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requiremnst it needs to run.
 */


template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
struct FdasTesterTraits : public utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability>
{
    public:
        typedef utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::DeviceType DeviceType;
        typedef T ValueType;

    public:
        FdasTesterTraits();
        fdas::Fdas<T, void>& api();
        fdas::Config& config();

    private:
        fdas::Config _config;
        fdas::Fdas<T, void> _api;
};


template <typename TestTraits>
class FdasTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        FdasTester();
        ~FdasTester();

    private:
};

TYPED_TEST_SUITE_P(FdasTester);

} // namespace test
} // namespace fdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fdas/test_utils/detail/FdasTester.cpp"


#endif // SKA_CHEETAH_MODULES_FDAS_TEST_FDASTESTER_H
