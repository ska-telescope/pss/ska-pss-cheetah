/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_FFT_H
#define SKA_CHEETAH_MODULES_FFT_FFT_H

#include "cheetah/modules/fft/Config.h"
#include "cheetah/modules/fft/detail/FftModule.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief defines the algorithms to be included in the module
 * @details each algorithm here must have a corresponding create() method in the AlgoFactory
 */
template<class FftTraitsT>
using FftBase = FftModule<FftTraitsT, FftAlgos<FftTraitsT>>;

/**
 * @brief      Fourier Transform Module Top level API
 *
 * @details    The Fft class acts as a forwarding class for architecture
 *             dependent FFT implementations. Both sync and async methods are
 *             available.
 *
 *             The algorithms activated in the Config object passed to the constructor are the only ones that
 *             are instantiated and made available for use.
 *
 *             In order to get consistent results between the different active algorithms, a common fft plan
 *             is established (based on the input data type & size). This means that your outputs will be
 *             consistent for any set of activated algorithms but may differ when different combinations of algorithms are enabled.
 *
 *             ### Sequence Of Operations
 *                Consider the case where the Fft module has two different fft algorithms, AlgoA, and AlgoB
 *                and that these had both been activated for use in the configuration. The async calls would
 *                follow the general sequence of operations shown in this sequence diagram:
 *
 * @verbatim
 *                Determine A common plan by polling each active algorithm for inforamtion of any modifications required to the input data
 *                in order for that algorithm to be able to process it (the Plan). These individual
 *                plans are combined into the common_plan object that sets the parameters to values compatible with all active algos.
 *                Once we have a common plan we can submit a job to the device pool to get a suitable resource to perform the FT.
 *
 *                                       +-----+                     +-----------+               +-------+  +-------+   +--------------+
 *                                       | Fft |                     | FftModule |               | AlgoA |  | AlgoB |   | ResourcePool |
 *                                       +-----+                     +-----------+               +-------+  +-------+   +--------------+
 *                fft_real(input_data) ---> |                              |                         |          |              |
 *                                          | ---- plan(input_data)------->"                         |          |              |
 *                                          |                              "------plan(input_data)-->|          |              |
 *                                          |                              "<---- Plan --------------|          |              |
 *                                          |                              "------plan(input_data)---+--------->|              |
 *                                          |                              "<---- Plan --------------+----------|              |
 *                                          |                              "                                                   |
 *                                          |<---- common_plan ------------"                                                   |
 *                                          |                                                                                  |
 *                                          |---------------------- submit(input_data, common_plan) -------------------------->|
 *
 * @endverbatim
 *                Once a resource in the pool becomes available the appropriate algorithm will be called. Internally there is a wrapper
 *                layer that will automatically transfer data to the resource allocated in the most efficient manner, resizing if necessary
 *                according to the plan.
 *                Finally the Handler will be called with the results of the operation.
 *
 *                Note that sync calls also determine a common_plan although there is not submit required.
 *
 *             ## Developer Notes ##
 *                If you wish to work on the interface for this class, or add a new algorithm that supports this Fft module
 *                Then please see the \subpage Fft_developer_guide "Fft Developer Guide".
 */
template<typename Handler, typename NumericalT>
class Fft : protected FftBase<FftTraits<NumericalT, Handler>>
{
    private:
        typedef FftBase<FftTraits<NumericalT, Handler>> BaseT;

    public:

        /**
         * @brief      Construct and Fft instance
         *
         * @param[in]  config    A configuration object for the Fft instance
         * @details An exception may be thrown if more than one algorithm associated with the same
         *          architecture is selected as active by the Config at the same time.
         */
        Fft(fft::ConfigType const& config, Handler& handler);

        //Copy constructors removed.
        Fft(Fft const&) = delete;
        Fft(Fft&&) = default;

        /**
         * @brief      Perform an FFT (sync)
         *
         * @details     The type of FFT performed is deduced from the types of the inputs such that:
         *             Input                  Output                FftType
         *             TimeSeries (Real)      FrequencySeries       R2C
         *             FrequencySeries        TimeSeries (Real)     C2R
         *             TimeSeries (Complex)   FrequencySeries       C2C Forward
         *             FrequencySeries        TimeSeries (Complex)  C2C Inverse
         *
         *             This acts as a forwarding method, forwarding the relevant parameters to
         *             implementations that are determined by the resource type and the input and
         *             output types.
         *
         * @param[in]  resource   A panda::PoolResource instance specifying the resource to process on
         * @param[in]  input      A TimeSeries or FrequencySeries instance
         * @param[out] output     A TimeSeries or FrequencySeries instance
         *
         * @tparam     Arch        The cheetah architecture type
         * @tparam     InputType   The type of the input parameter
         * @tparam     OutputType  The type of the output parameter
         * @tparam     Args        The types of any additional parameters to be forwarded to the implementation
         */
        template <typename Arch, typename InputType, typename OutputType, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            InputType const& input,
            OutputType& output,
            Args&&... args);

        /**
         * @brief perform a complex fft transform asyncronously
         * @tparam InputType can be either a data::TimeSeries or data::FrequencySeries
         *         of real or complex value_type
         * @details
         *        The Handler will be called with a std::shared_ptr<data::FrequencySeries<Cpu,T>> or
         *        std::shared_ptr<data::TimeSeries<Cpu,T>> complex types respectively.
         */
        template<class InputArch, typename InputType>
        std::shared_ptr<panda::ResourceJob> fft_to_complex(std::shared_ptr<data::TimeSeries<InputArch, InputType>> const&) const;

        template<class InputArch, typename InputType>
        std::shared_ptr<panda::ResourceJob> fft_to_complex(std::shared_ptr<data::FrequencySeries<InputArch, InputType>> const&) const;

        /**
         * @brief perform a real fft transform asyncronously
         * @tparam InputType can be either a data::TimeSeries or data::FrequencySeries
         *         of real or complex value_type
         * @details
         *        The Handler will be called with a std::shared_ptr<data::FrequencySeries<Cpu,T>> or
         *        std::shared_ptr<data::TimeSeries><Cpu,T> real types respectively.
         */
        template<typename InputType>
        std::shared_ptr<panda::ResourceJob> fft_to_real(std::shared_ptr<InputType> const&) const;

        /*
         * @brief      Normalize FFT input data series about zero
         *
         * @details     This normalization makes the data normally distributed about zero.
         * @tparam     InputTypeHost        Series data type, input to FFT
         *             InputTypeHost type should have begin() to end() methods defined.
         *
         */
        template <typename InputTypeHost>
        static void normalize_about_zero(InputTypeHost&);

        /**
         * @brief      Normalize FFT input data about zero, using iterators
         *
         * @details     This normalization makes the data normally distributed to Mean = 0
         *             and standard deviation = 1. Boost library is used to get Mean and variance values.
         *             As it distributed to zero Mean, half of the values are negative,
         *             so the unsigned data types are not supported. This method calls partially specialized
         *             template function normalize_about_zero_implementation() of this interface based on type.
         * @tparam     InputIt          Input series iterators, specify the range of normalization.
         *                              e.g it can normalize from begin() to end() of a buffer
         *
         */
        template <typename InputIt>
        static void normalize_about_zero(InputIt&&, InputIt const&);

        /**
         * @brief      Normalize FFT output data by sqrt(2/fft_size)
         *
         * @details     FFT output is normalized by a factor of square root of ( 2 / FFT length).
         * @tparam     OutputTypeHost        Series data type, complex output from FFT
         *             OutputTypeHost type should have begin() to end() methods defined.
         *
         */
        template <typename OutputTypeHost>
        static void normalize_sqrt_two_by_length(OutputTypeHost&, std::size_t);

        /**
         * @brief      Normalize FFT output data by sqrt(2/fft_size), using iterators.
         *
         * @details     FFT output is normalized by a factor of square root of ( 2 / FFT length).
         *             Chi-square data (with 2 degrees of freedom) of de-dispered FFT output is well interpreted
         *             when normalized input is Fourier transformed followed by this output normalization.
         *             The FFT length may be different than host_output buffer size in case of
         *             real to complex transform and this normalization expects the input/FFT size.
         * @tparam     OutputIt         Output series iterators, specify the range of normalization.
         *                              e.g it can normalize from begin() to end() of a buffer
         * @param      fft_size         Input/FFT size dpends on FFT type
         *
         */
        template <typename OutputIt>
        static void normalize_sqrt_two_by_length(OutputIt, OutputIt const&, std::size_t fft_size);

    private:
        /**
         * @brief      Normalize FFT input data about zero, using iterators
         *
         * @details     This is partial specialization of normalize_about_zero_implementation, for unsigned type.
         * @tparam     T                                            Input data type
         * @tparam     InputIt                                      Input series iterator
         * @tparam     std::enable_if<std::is_unsigned<T>::value
         *                           , bool>::type = true           Compiles only for unsigned data type
         *
         */
        template <typename T
                 , typename InputIt
                 , typename std::enable_if<std::is_unsigned<T>::value, bool>::type = true>
        static void normalize_about_zero_implementation(InputIt, InputIt const&);

        /**
         * @brief      Normalize FFT input data about zero, using iterators
         *
         * @details     This is partial specialization of normalize_about_zero_implementation, for signed type.
         * @tparam     T                                            Input data type
         * @tparam     InputIt                                      Input series iterator
         * @tparam     std::enable_if<!std::is_unsigned<T>::value
         *                           , bool>::type = true           Compiles only for signed data type
         *
         */
        template <typename T
                 , typename InputIt
                 , typename std::enable_if<!std::is_unsigned<T>::value, bool>::type = true>
        static void normalize_about_zero_implementation(InputIt, InputIt const&);

    private:
        Config const& _config;
};

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/detail/Fft.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_FFT_H
