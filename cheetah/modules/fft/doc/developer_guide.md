@page Fft_developer_guide Fft Developer Guide
# Notes for Developers
## The Fft Algorithm Interface

Each algorithm should be wrapped in a single Fft class with a process method for each supported fft operation.
The supported operations are:
- Real Time -> Complex Frequency
- Complex Time -> Complex Frequency
- Complex Frequency -> Complex Time
- Complex Frequency -> Real Time

Note that an algorithm sub-module is not required to implement all of these interfaces as the
Fft framework will adapt automatically to the interfaces that are made available.

### Extending the fft interface
If you wish to extend the above interface then you will need to understand the contents of the
@ref FftModule.h and @ref FftTraits.h classes in the fft/detail directory.

## Adding a new algorithm
### Where to put your new algorithm
The new module should live in a sub directory of the fft module directory. The structure of this directory
should conform to that specified in the developer guide.

### Required classes for your new algorithm
Each algorithm must be in its own uinique namespace (within the fft namespace) and must provide at least two classes:
  - Config
       This should, at a minimum, provide the "active" keyword as an option in order to determine if this algorithm is to be used.
       It must default to "false". The easiest way to achieve this is to inherit from panda::ConfigActive class.
  - Fft
     This is the entry point for users of your Algo. All functionality should be made available through this API.
    + It must have a constructor that takes the Config object described above.
    + It must provide a typedef for Architecture and (optionally) one for ArchitectureCapability in order to interface with panda resource management.
    + It must provide a public typedef Config (e.g. '''typedef my_algo::Config Config;''') for your Config class.

An example declaration for the example_algo is shown
~~~~{.cpp}
namespace cheetah {
namespace modules {
namespace fft {
namespace example_algo {

/**
 * @brief This is an example of the Fft algorithm specific interface.
 * @details A real algo should document the details of the algo here, or if more involved, in seperate
 *          markup documents inside the doc folder of this sub-directory and referenced here.
 *          using the doxgen \@ref command
 */
class Fft
{
    public:
        typedef Cpu Architecture;  // should specifiy the type of hardware required for the panda scheduler

        typedef example_algo::Config Config; // This typdef must exist and point to a suitbale Config object
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        Fft(Config const&);
        Fft(Fft const&) = delete;
        Fft(Fft&&);        // you need to define the move constructor explicitly

        /**
         * @brief Real time -> complex frequency F
         * @param input TimeSeries data to be Fourier Transformed.
         *              The Arch flag of the TimeSeries must match the Architecture typedef above
         * @param output FrequencySeries containing the result of the fourier transform.
         *               The Arch flag of the FrequencySeries must match the Architecture typedef above
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& cpu_core,
            data::TimeSeries<Architecture, T, InputAlloc> const& input,
            data::FrequencySeries<Architecture, std::complex<T>, OutputAlloc>& output) const;
};

} // namespace example_algo
} // namespace fft
} // namespace modules
} // namespace cheetah
~~~~

### Integrate the new algorithm into the top level Fft API
To incorporate our new algo into the top level Fft API, please add it in @ref FftAlgos.h "detail/FftAlgos.h" according to the instructions there.
Note that this will automatically incorporate the Configuration object specified by your typedef Config into the overall configuration tree.
There will be a seperate Config node generated for each process method supported in the interface.

### Testing your algorithm
There is a generic Fft tester (#FftTester) available in test_utils directory. You should create a suitable traits class
in your unit testing suite for use with this tester and ensure it is compiled into the unit testing executable for this sub module.
