/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/altera/detail/FftWorker.h"
#include "cheetah/modules/fft/altera/Fft.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include "panda/Copy.h"
#include "panda/arch/altera/DevicePointer.h"
#include "panda/arch/altera/DeviceCopy.h"

#include <cmath>
#include <boost/math/constants/constants.hpp>


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {

template <typename T, typename InputAlloc, typename OutputAlloc>
void FftWorker::operator()(data::TimeSeries<cheetah::Fpga, T, InputAlloc> const& input
                          , data::FrequencySeries<cheetah::Fpga, FftWorker::Complex<T>
                          , OutputAlloc>& output) const
{
    std::size_t input_power_two = round(log2((T)input.size()));

    // check if the transform is defined for required FFT size
    if (input_power_two % 2 == 0 ) {
        panda::Error e("Altera real-to-complex transform defined for odd power of 2. Got ");
        e << input.size();
        throw e;
    }

    // calculate FFT size based on input data length. TBD: will be replaced by top interface plan
    std::size_t fft_size = pow (2, input_power_two);

    // throw an error for FFT size beyond the range of Rabbit library compiled
    if(fft_size < _cxft.eight_million_point().min_size() || fft_size > _cxft.eight_million_point().max_size()){
        panda::Error e("Invalid FFT size!");
        e << fft_size;
        throw e;
    }

    // create a valid plan for the given data size
    if (!_plan.valid(input.size()))
        _plan = _cxft.eight_million_point().plan(input.size());

    // get last stage twiddle factors for the plan object
    auto const& twiddles = _plan.twiddles();

    PANDA_LOG << "Input data size:" << input.size() << ". R2C FFT size:" << input.size();

    // calculate half FFT size based on actual FFT size chosen
    std::size_t half_fft_size = fft_size/2;
    std::size_t sqrt_half_fft = int(sqrt(half_fft_size));

    // define intermediate device buffers
    panda::altera::DevicePointer<Complex<T>> dev_in_ev(_device, half_fft_size, *_first_queue);
    panda::altera::DevicePointer<Complex<T>> dev_in_od(_device, half_fft_size, *_first_queue);
    panda::altera::DevicePointer<Complex<T>> dev_temp(_device
                                                     , half_fft_size,*_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_sig_ev(_device
                                                       , half_fft_size, *_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_sig_od(_device
                                                       , half_fft_size, *_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_twd(_device
                                                    , twiddles.size(), *_last_queue);

    // copy last stage twiddle factors from host to device buffer
    panda::copy(twiddles.cbegin(), twiddles.cend(), dev_twd.begin());

    /*
     * @details:
     * First stage separates even & odd samples of data,
     * Explicit finishing of the _first_queue makes even & odd input buffers available to the FFT kernels
     */
    (*_first_kernel)(*_first_queue, 1, 1, static_cast<cl_mem>(input.begin())
                    , (&*dev_in_ev), (&*dev_in_od), input.size(), twiddles.size());
    (*_first_queue).finish();

    /*
     * @details:
     * FFT kernels for 4M samples called for four times (2 loops)
     * Each cxft_kernels() call computes complex to complex transform for row or colum of even or odd data
     * Multiwire archietecture makes this FFT faster
     */
    cl_int mangle_int = 0;
    cl_int twidle_int = 1;
    cl_int inverse_int = 0;
    cl_uint rows_arg = sqrt_half_fft;
    cl_int columns_arg = sqrt_half_fft;
    cl_int log_rows_arg = log2((T)sqrt_half_fft);
    cl_int log_columns_arg = log2((T)sqrt_half_fft);
    int columns = (1 << log_columns_arg);
    int rows = (1 << log_rows_arg);
    float delta_const = -2.0f * (boost::math::constants::pi<T>()) / (columns * rows);

    // For row FFT, i=0 and for column FFT i=1. For even samples j=0 and for odd samples j=1
    for (int j = 0; j < 2; ++j){
        for (int i = 0; i < 2; ++i){
            twidle_int = !i;
            cxft_kernels(i == 0 ? (j == 0 ?(&*dev_in_ev):(&*dev_in_od)) : (&*dev_temp)
                        , i == 0 ? (&*dev_temp) : (j == 0 ? (&*dev_sig_ev) :(&*dev_sig_od))
                        , mangle_int, twidle_int, log_rows_arg, log_columns_arg, rows_arg, columns_arg
                        , inverse_int, delta_const);
        }
    }

    // explicit finishing of the _transpose_queue makes even & odd output buffers available to the last stage
    (*_transpose_mwt_queue).finish();
    (*_transpose_queue).finish();

    /*
     * @details:
     * Last stage implements twiddle multiplications for 8M data.
     * It doesn't compute one side of the spectrum for real to complex transform, saving resources
     */
    (*_last_kernel)(*&(_device.default_queue()), 1, 1
                   , (&*dev_sig_ev), (&*dev_sig_od), (&*dev_twd)
                   , static_cast<cl_mem>(output.begin()), half_fft_size);

}

template <typename T, typename InputAlloc, typename OutputAlloc>
void FftWorker::operator()(data::TimeSeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input
                          , data::FrequencySeries<cheetah::Fpga, FftWorker::Complex<T>, OutputAlloc>& output)
{
    cl_int inverse_int = 0;
    c2c_transform<T>(input, output, inverse_int);
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void FftWorker::operator()(data::FrequencySeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input
                          , data::TimeSeries<cheetah::Fpga, FftWorker::Complex<T>, OutputAlloc>& output)
{
    cl_int inverse_int = 1;
    c2c_transform<T>(input, output, inverse_int);
}

template <typename T, typename InputType, typename OutputType>
void FftWorker::c2c_transform(InputType const& input, OutputType& output, cl_int inverse_int)
{
    std::size_t input_power_two = round(log2((T)input.size()));

    // check if the transform is defined for required FFT size
    if (input_power_two % 2 != 0 ) {
        panda::Error e("Altera complex-to-complex transform work for even power of 2");
        throw e;
    }

    // calculate FFT size based on input data length. TBD: will be replaced by top interface plan
    std::size_t fft_size = pow (2, input_power_two);
    std::size_t sqrt_fft_size = int(sqrt(fft_size));

    // throw an error for FFT size beyond the range of Rabbit library compiled
    if(fft_size < _cxft.eight_million_point().min_size() || fft_size > _cxft.eight_million_point().max_size()){
        panda::Error e("Invalid FFT size!");
        e << fft_size;
        throw e;
    }

    // create a valid plan for the given data size
    if (!_plan.valid(input.size()))
    {
        _plan = _cxft.eight_million_point().plan(input.size());
    }

    PANDA_LOG << "Input data size:" << input.size() << ". C2C FFT size:" << fft_size;

    // define temporary device buffer
    panda::altera::DevicePointer<Complex<T>> dev_temp(_device, fft_size, *_data_queue);

    /*
     * @details:
     * Complex to complex FFT for even power of 2 samples, computes row operation & column operations.
     * Multiwire archietecture makes this FFT faster
     */
    cl_int mangle_int = 0;
    cl_int twidle_int = 1;
    cl_int rows_arg = sqrt_fft_size;
    cl_int columns_arg = sqrt_fft_size;
    cl_int log_rows_arg = log2((T)sqrt_fft_size);
    cl_int log_columns_arg = log2((T)sqrt_fft_size);
    int columns = (1 << log_columns_arg);
    int rows = (1 << log_rows_arg);
    float delta_const = -2.0f * (boost::math::constants::pi<T>()) / (columns * rows);

    // For row FFT, i=0 and for column FFT i=1. For even samples j=0 and for odd samples j=1
    for (int i = 0; i < 2; ++i){
        twidle_int = !i;
        cxft_kernels(i == 0 ? static_cast<cl_mem>(input.begin()) : (&*dev_temp)
                    , i == 0 ? (&*dev_temp) : static_cast<cl_mem>(output.begin())
                    , mangle_int, twidle_int, log_rows_arg, log_columns_arg, rows_arg, columns_arg
                    , inverse_int, delta_const);
    }

    // explicit finishing of the _transpose_queue makes even & odd output buffers available to the last stage
    (*_transpose_mwt_queue).finish();
    (*_transpose_queue).finish();
}


} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
