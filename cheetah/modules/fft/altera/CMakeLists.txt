if(ENABLE_OPENCL)
    set(altera_lib_src
        src/Fft.cpp
        src/FftWorker.cpp
    )
endif()

set(MODULE_ALTERA_LIB_SRC_CPU
    src/Config.cpp
    ${altera_lib_src}
    PARENT_SCOPE
)

if(ENABLE_OPENCL)
    add_subdirectory(test)
endif()
