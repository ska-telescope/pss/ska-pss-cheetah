/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FFT_FFTW_FFT_H
#define SKA_CHEETAH_MODULES_FFT_FFTW_FFT_H

#include "cheetah/Configuration.h"
#include "cheetah/modules/fft/Plan.h"
#include "cheetah/modules/fft/fftw/FftPlan.h"
#include "cheetah/modules/fft/fftw/Config.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/DmTime.h"

#include "panda/TypeTag.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace fftw {

/**
 * @brief      A implementation of the fftw module
 *
 * @details    This class provides and interface to Cpu's FFTW library
 *             wrapping plan generation and execution and providing a single
 *             overloaded method to simplify the process of performing FFTs.
 *
 *             It should be noted that for performance reasons it is best to
 *             perform repetitions of the same transform size and type as this
 *             will not result in reallocation of the FftPlan object and regeneration
 *             of a FFTW plan.
 *
 *             It is therefore recommended that separate instantiations should be
 *             created for forward and back transforms and for repeated transforms
 *             of the same size the object should persist between transforms.
 *
 *             Note that FFTW rescales such that iFFT(FFT(A)) = size(A)*A for complex
 *             transforms and  iFFT(FFT(A)) = sqrt(size(A))*A for a real to complex followed
 *             by complex to real transform.
 *             ##Algorithm Please refer to algorithm.md
 */
template<typename NumericalT>
class Fft
{

    public:
        typedef cheetah::Cpu Architecture;
        typedef panda::PoolResource<Architecture> ResourceType;
        typedef fftw::Config Config;

    public:
        /**
         * @brief      Construct and Fft instance
         *
         * @param[in]  config    A configuration object for the Fft instance
         */
        Fft();
        Fft(Config const& algo_config);
        Fft(Fft const&) = delete;
        Fft(Fft&&);

        /**
         * @brief      Perform a real-to-complex 1D FFT
         *
         * @details    The output object will be resized appropriately to match the expected
         *             output size of the transform. For this reason it is most performant to
         *             reuse an output object of the correct size for each call to this method.
         *             The method also updates meta data associated with its output based on
         *             metadata associated with its input.
         *
         * @param[in]      cpu          A PoolResource object of architecture type Cpu.
         * @param[in]      input        A real TimeSeries instance to be transformed
         * @param[out]     output       A complex FrequencySeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& cpu,
            data::TimeSeries<cheetah::Cpu, T, InputAlloc> const& input,
            data::FrequencySeries<cheetah::Cpu, std::complex<T>, OutputAlloc>& output) const;

        /**
         * @brief      Perform a real-to-complex 1D FFT using a Intel fpga specialisation of FrequencySeries
         *
         * @param[in]      IntelFpga    A PoolResource object of architecture type IntelFpga.
         * @param[in]      input        A real TimeSeries instance to be transformed
         * @param[out]     output       A complex FrequencySeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
          */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(panda::PoolResource<cheetah::IntelFpga>& ,
            data::TimeSeries<cheetah::Cpu, T, InputAlloc> const& input,
            data::FrequencySeries<cheetah::IntelFpga, std::complex<T>, OutputAlloc>& output) const;

        /**
         * @brief      Perform a complex-to-real 1D FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      cpu          A PoolResource object of architecture type Cpu.
         * @param[in]      input        A complex FrequencySeries instance to be transformed
         * @param[out]     output       A real TimeSeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& cpu,
                    data::FrequencySeries<cheetah::Cpu
                                        , std::complex<T>
                                        , InputAlloc> const& input
                    , data::TimeSeries<cheetah::Cpu,T,OutputAlloc>& output
                    ) const;

        /**
         * @brief      Perform a complex-to-complex 1D forward FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      cpu          A PoolResource object of architecture type Cpu.
         * @param[in]      input        A complex TimeSeries instance to be transformed
         * @param[out]     output       A complex FrequencySeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& cpu,
                    data::TimeSeries<cheetah::Cpu, std::complex<T>, InputAlloc> const& input,
                    data::FrequencySeries<cheetah::Cpu
                                         ,std::complex<T>
                                         ,OutputAlloc>& output
                    ) const;

        /**
         * @brief      Perform a complex-to-complex 1D inverse FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      cpu          A PoolResource object of architecture type Cpu.
         * @param[in]      input        A complex FrequencySeries instance to be transformed
         * @param[out]     output       A complex TimeSeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& cpu,
            data::FrequencySeries<cheetah::Cpu, std::complex<T>, InputAlloc> const& input,
            data::TimeSeries<cheetah::Cpu, std::complex<T>, OutputAlloc>& output) const;

        /**
         * @brief      Create FFT Plan
         *
         * @details
         * @param[in]       input       A complex TimeSeries or FrequencySeries instance to be transformed
         * @tparam          InputT      Input data type
         * @tparam          OutputT     Output data type
         */
        template <typename InputT, typename OutputT>
        fft::Plan plan(InputT const& input, panda::TypeTag<OutputT> const&) const;

    private:
        mutable std::unique_ptr<FftPlan<NumericalT>> _plan;
};

} // namespace fftw
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/fftw/detail/Fft.cpp"

#endif //SKA_CHEETAH_MODULES_FFT_FFTW_FFT_H
