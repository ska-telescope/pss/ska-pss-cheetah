/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fft/fftw/FftPlan.h"
#include <type_traits>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace fftw {

namespace {

template <typename PlanType, typename ValueType, typename Enable=void>
struct ExecutePlanHelper
{
    void execute_fftw(PlanType const&)
    {

    }
};

template <typename PlanType, typename ValueType>
struct ExecutePlanHelper<PlanType, ValueType, typename std::enable_if<std::is_same<float, ValueType>::value>::type>
{
    void execute_fftw(PlanType const& plan)
    {
        fftwf_execute(plan);
    }

};

template <typename PlanType, typename ValueType>
struct ExecutePlanHelper<PlanType, ValueType, typename std::enable_if<std::is_same<double, ValueType>::value>::type>
{
    void execute_fftw(PlanType const& plan)
    {
        fftw_execute(plan);
    }
};

template <typename PlanType, typename ValueType, typename Enable=void>
struct DestroyPlanHelper
{
    void destroy_fftw(PlanType&)
    {
    }
};

template <typename PlanType, typename ValueType>
struct DestroyPlanHelper<PlanType, ValueType, typename std::enable_if<std::is_same<float, ValueType>::value>::type>
{
    void destroy_fftw(PlanType& plan)
    {
        fftwf_destroy_plan(plan);
    }
};

template <typename PlanType, typename ValueType>
struct DestroyPlanHelper<PlanType, ValueType, typename std::enable_if<std::is_same<double, ValueType>::value>::type>
{
    void destroy_fftw(PlanType& plan)
    {
        fftw_destroy_plan(plan);
    }
};

template <typename PlanType, typename ValueType, typename Enable=void>
struct CreatePlanHelperC2C
{
    template<typename... Args>
    void  create_plan(PlanType& plan, Args... args)
    {
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperC2C<PlanType, ValueType, typename std::enable_if<std::is_same<float, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftwf_plan_dft_1d(args...);
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperC2C<PlanType, ValueType, typename std::enable_if<std::is_same<double, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftw_plan_dft_1d(args...);
    }
};

template <typename PlanType, typename ValueType, typename Enable=void>
struct CreatePlanHelperR2C
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperR2C<PlanType, ValueType, typename std::enable_if<std::is_same<float, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftwf_plan_dft_r2c_1d(args...);
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperR2C<PlanType, ValueType, typename std::enable_if<std::is_same<double, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftw_plan_dft_r2c_1d(args...);
    }
};

template <typename PlanType, typename ValueType, typename Enable=void>
struct CreatePlanHelperC2R
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperC2R<PlanType, ValueType, typename std::enable_if<std::is_same<float, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftwf_plan_dft_c2r_1d(args...);
    }
};

template <typename PlanType, typename ValueType>
struct CreatePlanHelperC2R<PlanType, ValueType, typename std::enable_if<std::is_same<double, ValueType>::value>::type>
{
    template<typename... Args>
    void create_plan(PlanType& plan, Args... args)
    {
        plan = fftw_plan_dft_c2r_1d(args...);
    }
};

} //namespace

template <typename NumericalRep>
FftPlan<NumericalRep>::FftPlan()
    : _plan(0)
    , _input_location(nullptr)
    , _output_location(nullptr)
    , _size(0)
{
}

template <typename NumericalRep>
FftPlan<NumericalRep>::~FftPlan()
{
    if(_plan!=0) destroy_plan();
    _plan = 0;
    _size = 0;
    _input_location = nullptr;
    _output_location = nullptr;
}


template <typename NumericalRep>
template<typename InputT, typename OutputT>
bool FftPlan<NumericalRep>::valid(InputT const& input, OutputT& output, size_t size)
{
    return (size == _size || _input_location == reinterpret_cast<const NumericalRep*>(&*input.begin()) || _output_location == reinterpret_cast<NumericalRep*>(&*output.begin()));
}


template <typename NumericalRep>
void FftPlan<NumericalRep>::destroy_plan()
{
    DestroyPlanHelper<PlanType,NumericalRep> destroy_plan_helper;
    destroy_plan_helper.destroy_fftw(_plan);
}

template <typename NumericalRep>
void FftPlan<NumericalRep>::execute_plan(PlanType const& plan)
{
    ExecutePlanHelper<PlanType,NumericalRep> execute_plan_helper;
    execute_plan_helper.execute_fftw(plan);
}

template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::TimeSeries<cheetah::Cpu, NumericalRep> const& input, data::FrequencySeries<cheetah::Cpu, std::complex<NumericalRep>>& output, size_t size)
{
    if (!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperR2C<PlanType,NumericalRep> plan_helper;
        plan_helper.create_plan( _plan, size, const_cast<NumericalRep*>(&*(input.begin())), reinterpret_cast<FftwComplexType*>(&*(output.begin())), FFTW_MEASURE);
    }
    return this->_plan;
}

template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::TimeSeries<cheetah::Cpu, std::complex<NumericalRep>> const& input, data::FrequencySeries<cheetah::Cpu, std::complex<NumericalRep>>& output, size_t size)
{
    if(!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperC2C<PlanType,NumericalRep> plan_helper;
        //not ideal but fftw_plan seems to erase the input data. This hopely occurs only once during initialization. (CXFT module should make sure of that)
        data::TimeSeries<cheetah::Cpu, std::complex<NumericalRep>> input_temp(input);
        plan_helper.create_plan(_plan, size, const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(output.begin())), FFTW_FORWARD, FFTW_MEASURE);
        std::memcpy(const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(input_temp.begin())), input.size()*sizeof(FftwComplexType));
    }
    return this->_plan;
}

template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::TimeSeries<cheetah::Cpu, NumericalRep> const& input, data::FrequencySeries<cheetah::IntelFpga, std::complex<NumericalRep>>& output, size_t size)
{
    if (!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperR2C<PlanType,NumericalRep> plan_helper;
        plan_helper.create_plan( _plan, size, const_cast<NumericalRep*>(&*(input.begin())), reinterpret_cast<FftwComplexType*>(&*(output.begin())), FFTW_MEASURE);
    }
    return this->_plan;
}

template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::TimeSeries<cheetah::Cpu, std::complex<NumericalRep>> const& input, data::FrequencySeries<cheetah::IntelFpga, std::complex<NumericalRep>>& output, size_t size)
{
    if(!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperC2C<PlanType,NumericalRep> plan_helper;
        //not ideal but fftw_plan seems to erase the input data. This hopely occurs only once during initialization. (CXFT module should make sure of that)
        data::TimeSeries<cheetah::Cpu, std::complex<NumericalRep>> input_temp(input);
        plan_helper.create_plan(_plan, size, const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(output.begin())), FFTW_FORWARD, FFTW_MEASURE);
        std::memcpy(const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(input_temp.begin())), input.size()*sizeof(FftwComplexType));
    }
    return this->_plan;
}


template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::FrequencySeries<cheetah::Cpu, std::complex<NumericalRep>> const& input, data::TimeSeries<cheetah::Cpu, NumericalRep>& output, size_t size)
{
    if (!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperC2R<PlanType,NumericalRep> plan_helper;
        plan_helper.create_plan(_plan, size, const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), (&*(output.begin())), FFTW_MEASURE);
    }

    return this->_plan;
}

template<typename NumericalRep>
typename FftPlan<NumericalRep>::PlanType const& FftPlan<NumericalRep>::plan(data::FrequencySeries<cheetah::Cpu, std::complex<NumericalRep>> const& input, data::TimeSeries<cheetah::Cpu, std::complex<NumericalRep>>& output, size_t size)
{
    if (!valid(input,output,size))
    {
        destroy_plan();
        _size = size;
        _input_location = reinterpret_cast<const NumericalRep*>(&*input.begin());
        _output_location = reinterpret_cast<NumericalRep*>(&*output.begin());
        CreatePlanHelperC2C<PlanType,NumericalRep> plan_helper;
        //not ideal but fftw_plan seems to erase the input data. This hopely occurs only once during initialization. (CXFT module should make sure of that)
        data::FrequencySeries<cheetah::Cpu, std::complex<NumericalRep>> input_temp(input);
        plan_helper.create_plan(_plan, size, const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(output.begin())), FFTW_BACKWARD, FFTW_MEASURE);
        std::memcpy(const_cast<FftwComplexType*>(reinterpret_cast<const FftwComplexType*>(&*(input.begin()))), reinterpret_cast<FftwComplexType*>(&*(input_temp.begin())), input.size()*sizeof(FftwComplexType));
    }
    return this->_plan;
}

} // namespace fftw
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
