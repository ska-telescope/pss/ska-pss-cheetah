/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fft/fftw/Fft.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace fftw {

template <typename NumericalRep>
Fft<NumericalRep>::Fft()
    : _plan(new FftPlan<NumericalRep>())
{
}

template <typename NumericalRep>
Fft<NumericalRep>::Fft(Config const&)
    : Fft()
{
}

template <typename NumericalRep>
Fft<NumericalRep>::Fft(Fft&& other)
    : _plan(std::move(other._plan))
{
}

template <typename NumericalRep>
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft<NumericalRep>::process(ResourceType& /*cpu*/,
            data::TimeSeries<cheetah::Cpu, T, InputAlloc> const& input,
            data::FrequencySeries<cheetah::Cpu, std::complex<T>, OutputAlloc>& output) const
{
    output.resize(input.size()/2+1);
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
    _plan->execute_plan(_plan->plan(input, output, input.size()));
}

template <typename NumericalRep>
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft<NumericalRep>::process(panda::PoolResource<cheetah::IntelFpga>& /*fpga*/,
            data::TimeSeries<cheetah::Cpu, T, InputAlloc> const& input,
            data::FrequencySeries<cheetah::IntelFpga, std::complex<T>, OutputAlloc>& output) const
{
    output.resize(input.size()/2+1);
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
   _plan->execute_plan(_plan->plan(input, output, input.size()));
}

template <typename NumericalRep>
template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft<NumericalRep>::process(ResourceType& /*cpu*/,
            data::FrequencySeries<cheetah::Cpu
            , std::complex<T>
            , InputAlloc> const& input
            , data::TimeSeries<cheetah::Cpu,T,OutputAlloc>& output
            ) const
{
    output.resize(2*(input.size()-1));
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
    _plan->execute_plan(_plan->plan(input, output, 2*(input.size()-1)));
}

template <typename NumericalRep>
template <typename T, typename InputAlloc, typename OutputAlloc>
        void Fft<NumericalRep>::process(ResourceType& /*cpu*/,
            data::TimeSeries<cheetah::Cpu, std::complex<T>, InputAlloc> const& input,
            data::FrequencySeries<cheetah::Cpu
            ,std::complex<T>
            ,OutputAlloc>& output
            ) const
{
    output.resize(input.size());
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
    _plan->execute_plan(_plan->plan(input, output, input.size()));
}

template <typename NumericalRep>
template <typename T, typename InputAlloc, typename OutputAlloc>
        void Fft<NumericalRep>::process(ResourceType& /*cpu*/,
            data::FrequencySeries<cheetah::Cpu, std::complex<T>, InputAlloc> const& input,
            data::TimeSeries<cheetah::Cpu, std::complex<T>, OutputAlloc>& output) const
{
    output.resize(input.size());
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
    _plan->execute_plan(_plan->plan(input, output, input.size()));
}

// Common plan method all FFT types as FFTW can handle all configurations internally
template <typename NumericalRep>
template <typename InputT, typename OutputT>
fft::Plan Fft<NumericalRep>::plan(InputT const& input, panda::TypeTag<OutputT> const&) const
{
    return Plan(input.size());
}

} // namespace fftw
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
