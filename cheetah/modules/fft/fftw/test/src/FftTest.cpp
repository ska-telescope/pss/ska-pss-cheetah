/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fft/fftw/Fft.h"
#include "cheetah/modules/fft/fftw/test/FftTest.h"
#include "cheetah/modules/fft/test_utils/FftTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace fftw {
namespace test {

/**
 * @details Unit tests for FFT Plan generations for different combinations of FFT type and data length
 */

template<typename NumericalT>
struct FftwTraitsBase
    : public fft::test::FftTesterTraits<fft::fftw::Fft<NumericalT>, NumericalT>
{
        typedef fft::test::FftTesterTraits<fft::fftw::Fft<NumericalT>, NumericalT> BaseT;
        typedef typename fft::fftw::Fft<NumericalT>::Architecture Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        template<typename DataType>
        typename DataType::Allocator allocator(panda::PoolResource<Arch>&) {
            return typename DataType::Allocator();
        }
};

template<typename NumericalT>
struct FftwTraits : FftwTraitsBase<NumericalT>
{
};

template<>
struct FftwTraits<float> : FftwTraitsBase<float>
{
    static double accuracy() { return 5.0e-5; }
};

} // namespace test
} // namespace fftw
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace test {

typedef ::testing::Types<fftw::test::FftwTraits<float>, fftw::test::FftwTraits<double>> FftwTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Fftw, FftTester, FftwTraitsTypes);

} // namespace test
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
