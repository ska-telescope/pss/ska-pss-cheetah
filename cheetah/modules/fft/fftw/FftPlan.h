/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FFT_FFTW_FFTPLAN_H
#define SKA_CHEETAH_MODULES_FFT_FFTW_FFTPLAN_H

#include "cheetah/utils/Architectures.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/TimeSeries.h"
#include <fftw3.h>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace fftw {

/**
 * FftwPlan typetraits class specializations for fftw_plan and fftwf_plan
 */
template <typename T>
struct FftwPlanTypeTraits
{
};

template <>
struct FftwPlanTypeTraits<float>
{
    typedef fftwf_plan type;
};

template <>
struct FftwPlanTypeTraits<double>
{
    typedef fftw_plan type;
};

/**
 * FftwComplex typetraits class specializations for fftw_complex and fftwf_complex
 */
template <typename T>
struct FftwTypeTraits
{
};

template <>
struct FftwTypeTraits<float>
{
    typedef fftwf_complex type;
};

template <>
struct FftwTypeTraits<double>
{
    typedef fftw_complex type;
};


/**
 * Implementation of FftPlan for Fftw plans
 */
template<typename NumericalT>
class FftPlan
{
    public:
        typedef typename FftwPlanTypeTraits<NumericalT>::type PlanType;
        typedef typename FftwTypeTraits<NumericalT>::type FftwComplexType;

    public:
        /**
         * @brief      Construct an uninitialised fftw plan.
         */
        FftPlan();
        FftPlan(FftPlan const&) = delete;

        /**
         * @brief      Destroys the undetlying fftw plan if allocated.
         */
        ~FftPlan();

        /**
         * @brief      Get (or create) a fftw_plan .
         *
         * @details    The plan method will first check if its underlying plan matches
         *             the provided size. If it matches, a reference to the existing
         *             plan is returned. If the arguments do not match the configuration of
         *             the exiting plan, the existing plan is destroyed and a new one is created.
         *
         * @param[in]  input  could be either TimeSeries or FrequencySeries
         * @param[in]  output  could be either TimeSeries or FrequencySeries
         * @param[in]  size  length of the fft to be performed
         *
         * @return     A PlanType representing the current plan
         */
        PlanType const& plan(data::TimeSeries<cheetah::Cpu, NumericalT> const& input, data::FrequencySeries<cheetah::Cpu, std::complex<NumericalT>>& output, size_t size);
        PlanType const& plan(data::TimeSeries<cheetah::Cpu, std::complex<NumericalT>> const& input, data::FrequencySeries<cheetah::Cpu, std::complex<NumericalT>>& output, size_t size);
        PlanType const& plan(data::FrequencySeries<cheetah::Cpu, std::complex<NumericalT>> const& input, data::TimeSeries<cheetah::Cpu, NumericalT>& output, size_t size);
        PlanType const& plan(data::FrequencySeries<cheetah::Cpu, std::complex<NumericalT>> const& input, data::TimeSeries<cheetah::Cpu, std::complex<NumericalT>>& output, size_t size);

         /**
         * @brief      FrequencySeries specialisation of the CPU fftw_plan.
         *
         * @details    The plan method uses FrequencySeries based on hugepages.
         *
         * @param[in]  input  TimeSeries
         * @param[in]  output  FrequencySeries
         * @param[in]  size  length of the fft to be performed
         *
         * @return     A PlanType representing the current plan
         */
        PlanType const& plan(data::TimeSeries<cheetah::Cpu, NumericalT> const& input, data::FrequencySeries<cheetah::IntelFpga, std::complex<NumericalT>>& output, size_t size);
        PlanType const& plan(data::TimeSeries<cheetah::Cpu, std::complex<NumericalT>> const& input, data::FrequencySeries<cheetah::IntelFpga, std::complex<NumericalT>>& output, size_t size);

        /**
         * @brief   check if the current plan is valid or not.
         * @param[in]  input  could be either TimeSeries or FrequencySeries
         * @param[in]  output  could be either TimeSeries or FrequencySeries
         * @param[in]  size  length of the fft to be performed
         */
        template<typename InputT, typename OutputT>
        bool valid(InputT const& input, OutputT& output, size_t size);

        /**
         * @brief  execute the plan
         */
        void execute_plan(PlanType const& plan);

    private:
        /**
         * @brief  Destroy the current plan and set all parameters to their defaults
         */
        void destroy_plan();

    private:
        PlanType _plan;
        const NumericalT* _input_location; // looks ugly but this is the simple way to keep track of memory reference
        NumericalT* _output_location;
        size_t _size;
};

} // namespace fftw
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/fftw/detail/FftPlan.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_FFTW_FFTPLAN_H
