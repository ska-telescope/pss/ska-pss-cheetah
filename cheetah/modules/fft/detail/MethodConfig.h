/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_METHODCONFIG_H
#define SKA_CHEETAH_MODULES_FFT_METHODCONFIG_H

#include "cheetah/modules/fft/detail/HasFftProcessMethod.h"
#include "cheetah/utils/Config.h"
#include "panda/MultipleConfigModule.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief Method level configuration
 * @details
 *   As it is optional for any algo to implement the methods available, we need a seperate
 *   configuration file for each method to ensure the end user is only presented with
 *   options that are available.
 */
template<class... FftAlgoConfigs>
class MethodConfig : public panda::MultipleConfigModule<utils::Config, FftAlgoConfigs...>
{
        typedef panda::MultipleConfigModule<utils::Config, FftAlgoConfigs...> BaseT;

    public:
        MethodConfig(std::string const& tag);

        /**
         * @brief set the configuration for the specified algorithm to active
         */
        template<class AlgoT>
        void activate();


    protected:
        void add_options(panda::ConfigModule::OptionsDescriptionEasyInit& add_options) override;
};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "MethodConfig.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_METHODCONFIG_H
