/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/SeriesSlice.h"
#include <panda/Error.h>
#include <panda/Fill.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


template<typename WrappedType>
template<typename... WrappedTypeConstructorArgs>
AlgoWrapper<WrappedType>::AlgoWrapper(WrappedTypeConstructorArgs&&...args)
    : BaseT(std::forward<WrappedTypeConstructorArgs>(args)...)
{
}

template<typename WrappedType>
template<class ArchT, typename InputDataT, typename OutputDataT>
void AlgoWrapper<WrappedType>::operator()(panda::PoolResource<ArchT>& device
                                        , CommonPlan const& plan
                                        , std::shared_ptr<InputDataT> const& input_data
                                        , OutputDataT& output_data) const
{
    (*this)(device, plan, *input_data, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::launch_algo(panda::PoolResource<ArchT>& device
                                         , InputDataT const& input_data
                                         , OutputDataT& output_data) const
{
    static_cast<BaseT const&>(*this).process(device, input_data, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfNotHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::launch_algo(panda::PoolResource<ArchT>& device
                                         , InputDataT const& input_data
                                         , OutputDataT& output_data) const
{
    // copy data to device
    typedef FftReplaceArch_t<ArchT, InputDataT> InputT;
    std::unique_ptr<InputT> input(create_fft_data<InputT>(device, input_data));
    launch_algo(device, *input, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::shrink_and_launch_algo(panda::PoolResource<ArchT>& device
                                                    , InputDataT const& input_data
                                                    , OutputDataT& output_data
                                                    , std::size_t new_size) const
{
    // This works but very inefficent
    std::unique_ptr<InputDataT> input(create_fft_data<InputDataT>(device, new_size));
    panda::copy(input_data.begin(), input_data.begin() + new_size, input->begin());
    launch_algo(device, *input, output_data);

    // TODO: below is much more efficient - need to change the algo process interface to accept SeriesSlice objects
    //data::SeriesSlice<InputDataT> slice(input_data.begin(), input_data.begin() + new_size);
    //static_cast<BaseT const&>(*this).process(device, slice, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfNotHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::shrink_and_launch_algo(panda::PoolResource<ArchT>& device
                                                    , InputDataT const& input_data
                                                    , OutputDataT& output_data
                                                    , std::size_t new_size) const
{
    // copy data to device
    typedef FftReplaceArch_t<ArchT, InputDataT> InputT;
    std::unique_ptr<InputT> input(create_fft_data<InputT>(device, new_size));
    panda::copy(input_data.begin(), input_data.begin() + new_size, input->begin());
    launch_algo(device, *input, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::pad_and_launch_algo(panda::PoolResource<ArchT>& device
                                                 , InputDataT const& input_data
                                                 , OutputDataT& output_data
                                                 , std::size_t new_size) const

{
    // TODO inefficient and largely redundant copy
    //  Can be removed if we have an iterator based interface for the algorithms
    std::unique_ptr<InputDataT> input(create_fft_data<InputDataT>(device, new_size));
    typedef typename InputDataT::value_type ValueType;
    panda::copy(input_data.begin(), input_data.end(), input->begin());

    auto offset = input->begin() + input_data.size();
    panda::fill(offset, input->end(), ValueType(0));
    launch_algo(device, *input, output_data);
}

template<typename WrappedType>
template<class ArchT, typename InputDataT
       , typename OutputDataT
       , EnableIfNotHasArch<ArchT, InputDataT, bool>
       >
void AlgoWrapper<WrappedType>::pad_and_launch_algo(panda::PoolResource<ArchT>& device
                                                 , InputDataT const& input_data
                                                 , OutputDataT& output_data
                                                 , std::size_t new_size) const
{
    // copy data to device
    typedef FftReplaceArch_t<ArchT, InputDataT> InputT;
    typedef typename InputT::value_type ValueType;
    std::unique_ptr<InputT> input(create_fft_data<InputT>(device, new_size));
    panda::copy(input_data.begin(), input_data.end(), input->begin());

    auto offset = input->begin() + input_data.size();
    panda::fill(offset, input->end(), ValueType(0));
    launch_algo(device, *input, output_data);
}

template<typename WrappedType>
template<class ArchT
        , typename InputDataT, typename OutputDataT
        , EnableIfHasArch<ArchT, OutputDataT, bool>
       >
void AlgoWrapper<WrappedType>::operator()(panda::PoolResource<ArchT>& device
                                        , CommonPlan const& plan
                                        , InputDataT const& input_data
                                        , OutputDataT& output_data) const
{
    if(plan.fft_size() == input_data.size())
    {
        // input data does not need reshaping
        launch_algo(device, input_data, output_data);
    }
    else if(plan.fft_size() > input_data.size())
    {
        // input data needs padding
        pad_and_launch_algo(device, input_data, output_data, plan.fft_size());
    }
    else
    {
        // input data needs truncation
        shrink_and_launch_algo(device, input_data, output_data, plan.fft_size());
    }
}

template<typename WrappedType>
template<class ArchT
       , typename InputDataT
       , typename OutputDataT
       , EnableIfNotHasArch<ArchT, OutputDataT, bool>
       >
void AlgoWrapper<WrappedType>::operator()(panda::PoolResource<ArchT>& device
                                        , CommonPlan const& plan
                                        , InputDataT const& input
                                        , OutputDataT& output) const
{
    auto out = this->exec(device, plan, input, panda::TypeTag<OutputDataT>());
    output = *out;
}

template<typename WrappedType>
template<class ArchT
       , typename InputDataT
       , typename OutputT
       , EnableIfHasArch<ArchT, OutputT, bool>
       >
inline
std::shared_ptr<OutputT> AlgoWrapper<WrappedType>::exec(panda::PoolResource<ArchT>& device
                                                      , CommonPlan plan
                                                      , InputDataT const& input
                                                      , panda::TypeTag<OutputT>) const
{
    std::shared_ptr<OutputT> output(create_fft_data<OutputT>(device));
    this->template operator()<ArchT, InputDataT, OutputT>(device, plan, input, *output);
    return output;
}

template<typename WrappedType>
template<class ArchT
       , typename InputDataT
       , typename OutputT
       , EnableIfNotHasArch<ArchT, OutputT, bool>
       >
inline
std::shared_ptr<FftReplaceArch_t<ArchT, OutputT>> AlgoWrapper<WrappedType>::exec(panda::PoolResource<ArchT>& device
                                                                               , CommonPlan plan
                                                                               , InputDataT const& input
                                                                               , panda::TypeTag<OutputT>) const
{
    typedef FftReplaceArch_t<ArchT, OutputT> ArchOutputT;
    std::shared_ptr<ArchOutputT> output(create_fft_data<ArchOutputT>(device));
    this->template operator()<ArchT, InputDataT, ArchOutputT>(device, plan, input, *output);
    return output;
}

template<typename WrappedType>
template<class ArchT
       , typename InputDataT
       , typename OutputT
       >
std::shared_ptr<FftReplaceArch_t<ArchT, OutputT>> AlgoWrapper<WrappedType>::operator()(panda::PoolResource<ArchT>& device
                                                            , CommonPlan plan
                                                            , std::shared_ptr<InputDataT> const& input
                                                            , panda::TypeTag<OutputT> tag) const
{
    return this->exec(device, plan, *input, tag);
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
