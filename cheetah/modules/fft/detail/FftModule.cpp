/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


template<class FftTraitsT, class... FftAlgoTs>
FftModule<FftTraitsT, FftAlgoTs...>::FftModule(Config const& config, Handler& handler)
    : _time2complex(config.pool(), config.time_real_to_complex(), handler)
    , _time_complex2complex(config.pool(), config.time_complex_to_complex(), handler)
    , _freq_complex2complex(config.pool(), config.freq_complex_to_complex(), handler)
    , _freq_complex2real(config.pool(), config.freq_complex_to_real(), handler)
{
}

template<class FftTraitsT, class... FftAlgoTs>
template<class OutputArch
       , class InputArch
       , typename ValueT
       , data::EnableIfIsNotComplexNumber<ValueT, bool>>
std::shared_ptr<panda::ResourceJob> FftModule<FftTraitsT, FftAlgoTs...>::submit(std::shared_ptr<data::TimeSeries<InputArch, ValueT>> const& input) const
{
    return this->_time2complex.template submit<data::TimeSeries<InputArch, ValueT>, OutputArch>(input);
}

template<class FftTraitsT, class... FftAlgoTs>
template<class OutputArch
       , class InputArch
       , typename ValueT
       , data::EnableIfIsComplexNumber<ValueT, bool>>
std::shared_ptr<panda::ResourceJob> FftModule<FftTraitsT, FftAlgoTs...>::submit(std::shared_ptr<data::TimeSeries<InputArch, ValueT>> const& input) const
{
    return this->_time_complex2complex.template submit<data::TimeSeries<InputArch, ValueT>, OutputArch>(input);
}

template<class FftTraitsT, class... FftAlgoTs>
template<class OutputArch
       , class InputArch
       , typename ValueT
       , data::EnableIfIsComplexNumber<ValueT, bool>>
std::shared_ptr<panda::ResourceJob> FftModule<FftTraitsT, FftAlgoTs...>::submit(std::shared_ptr<data::FrequencySeries<InputArch, ValueT>> const& input) const
{
    return this->_freq_complex2complex.template submit<data::FrequencySeries<InputArch, ValueT>, OutputArch>(input);
}

template<class FftTraitsT, class... FftAlgoTs>
template<class OutputArch
       , class InputArch
       , typename ValueT
       , data::EnableIfIsComplexNumber<ValueT, bool>>
std::shared_ptr<panda::ResourceJob> FftModule<FftTraitsT, FftAlgoTs...>::submit_to_real(std::shared_ptr<data::FrequencySeries<InputArch, ValueT>> const& input) const
{
    return this->_freq_complex2real.template submit<data::FrequencySeries<InputArch, ValueT>, OutputArch>(input);
}

template<class FftTraitsT, class... FftAlgoTs>
template<typename Arch
       , typename InputArch , typename InputValT
       , class OutputArch, typename OutputValT
       , data::EnableIfIsNotComplexNumber<InputValT, bool>
       , data::EnableIfIsComplexNumber<OutputValT, bool>
       >
void FftModule<FftTraitsT, FftAlgoTs...>::exec(panda::PoolResource<Arch>& resource
                                             , data::TimeSeries<InputArch, InputValT> const& input
                                             , data::FrequencySeries<OutputArch, OutputValT>& output) const
{
    this->_time2complex.exec(resource, input, output);
}

template<class FftTraitsT, class... FftAlgoTs>
template<typename Arch
       , typename InputArch , typename InputValT
       , class OutputArch
       , data::EnableIfIsComplexNumber<InputValT, bool>
       >
void FftModule<FftTraitsT, FftAlgoTs...>::exec(panda::PoolResource<Arch>& resource
                                             , data::TimeSeries<InputArch, InputValT> const& input
                                             , data::FrequencySeries<OutputArch, InputValT>& output) const
{
    this->_time_complex2complex.exec(resource, input, output);
}

template<class FftTraitsT, class... FftAlgoTs>
template<typename Arch
       , typename InputArch , typename InputValT
       , class OutputArch
       , data::EnableIfIsComplexNumber<InputValT, bool>
       >
void FftModule<FftTraitsT, FftAlgoTs...>::exec(panda::PoolResource<Arch>& resource
                                             , data::FrequencySeries<InputArch, InputValT> const& input
                                             , data::TimeSeries<OutputArch, InputValT>& output) const
{
    this->_freq_complex2complex.exec(resource, input, output);
}

template<class FftTraitsT, class... FftAlgoTs>
template<typename Arch
       , typename InputArch , typename InputValT
       , class OutputArch, typename OutputValT
       , data::EnableIfIsComplexNumber<InputValT, bool>
       , data::EnableIfIsNotComplexNumber<OutputValT, bool>
       >
void FftModule<FftTraitsT, FftAlgoTs...>::exec(panda::PoolResource<Arch>& resource
                                             , data::FrequencySeries<InputArch, InputValT> const& input
                                             , data::TimeSeries<OutputArch, OutputValT>& output) const
{
    this->_freq_complex2real.exec(resource, input, output);
}

// ------------------------------------
// -- modules for a single algo
// ------------------------------------

template<class FftTraitsT>
FftSingleMethodModule<FftTraitsT>::FftSingleMethodModule(PoolType pool, Config const& config, Handler& handler)
    : BaseT(pool, AlgoFactory(config), handler, _reshaper)
{
}

template<class FftTraitsT>
template<class ArchT, typename InputDataT, typename OutputDataT>
void FftSingleMethodModule<FftTraitsT>::exec(panda::PoolResource<ArchT>& resource, InputDataT const& input, OutputDataT& output) const
{
    return BaseT::operator()(resource, this->plan(input, panda::TypeTag<OutputDataT>()), input, output);
}

template<class FftTraitsT>
template<typename InputDataT, typename... DataTs>
inline
CommonPlan FftSingleMethodModule<FftTraitsT>::plan(std::shared_ptr<InputDataT> const& data, DataTs&&... args) const
{
    return this->plan(*data, std::forward<DataTs>(args)...);
}

template<class FftTraitsT>
template<typename InputDataT, typename... DataTs>
CommonPlan FftSingleMethodModule<FftTraitsT>::plan(InputDataT const& data, DataTs&&... args) const
{
    CommonPlan common_plan(data.size());

    // define number of times we attempt to find a common plan
    // input data reshaping might work differently for each activated algorithm
    // loop iterates 5 (count value set arbitrarily) times and throws an error in case it fails to calculate
    int count = 5;
    do {
        if (count == 0) {
            panda::Error e("Unable to determine common plan for data size = ");
            e << data.size();
            throw e;
        }

        // interrogate each activated algorithm to update the common plan (by calling the InputDataReshaper)
        // This call should match the Signature defined by ShaperType alias templates in FftModule.h
        this->configured_task()(common_plan, data, std::forward<DataTs>(args)...);

        if(common_plan.valid()) break;

        common_plan.calculate();

        --count;

    } while(true);

    return common_plan;
}

template<class FftTraitsT>
template<typename InputT, class Arch>
std::shared_ptr<panda::ResourceJob> FftSingleMethodModule<FftTraitsT>::submit(std::shared_ptr<InputT> const& input) const
{
    typedef panda::TypeTag<OutputT<Arch>> OutputDataTag;
    return BaseT::submit(this->plan(input, OutputDataTag()), input, OutputDataTag());
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
