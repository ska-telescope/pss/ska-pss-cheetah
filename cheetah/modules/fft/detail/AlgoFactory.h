/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_ALGOFACTORY_H
#define SKA_CHEETAH_MODULES_FFT_ALGOFACTORY_H

#include "cheetah/modules/fft/detail/FftAlgos.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief Factory for Fft algorithms
 * @details  Creates fft algorithm objects. The default factory assumes that the
 *           algorithm takes its own config object (as defined by the public typedef Config of the Algos Fft interface class)
 *           in the constructor. If this is not the case, this can be specialised for a specific algo as required.
 */

template<class ConfigT>
class AlgoFactory
{
    public:
        AlgoFactory(ConfigT const& config)
            : _config(config)
        {}

        template<typename Algo>
        Algo create() const
        {
            return Algo(_config.template config<typename Algo::Config>());
        }

        template<typename Algo>
        bool active() const
        {
            return _config.template config<typename Algo::Config>().active();
        }

    private:
        ConfigT const& _config;
};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_ALGOFACTORY_H
