/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


template<typename FftTraitsT>
HandlerWrapper<FftTraitsT>::HandlerWrapper(Handler& handler)
    : _handler(handler)
{
}

template<typename FftTraitsT>
template<typename DataT>
inline
void HandlerWrapper<FftTraitsT>::operator()(std::shared_ptr<DataT> data) const
{
    this->exec_handler(std::move(data));
}

template<typename FftTraitsT>
template<typename DataT
        , EnableIfHasArch<panda::Cpu, DataT, bool>
        >
void HandlerWrapper<FftTraitsT>::exec_handler(std::shared_ptr<DataT> data) const
{
    _handler(std::move(data));
}

template<typename FftTraitsT>
template<typename DataT
        , EnableIfNotHasArch<panda::Cpu, DataT, bool>
        >
void HandlerWrapper<FftTraitsT>::exec_handler(std::shared_ptr<DataT> data) const
{
    _handler(std::make_shared<FftReplaceArch_t<panda::Cpu, DataT>>(*data));
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
