/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TupleUtilities.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


template<class... FftAlgoConfigs>
MethodConfig<FftAlgoConfigs...>::MethodConfig(std::string const& tag)
    : BaseT(tag)
{
}

template<class... FftAlgoConfigs>
void MethodConfig<FftAlgoConfigs...>::add_options(panda::ConfigModule::OptionsDescriptionEasyInit&)
{
}

namespace {

    template<typename ConfigT, typename ConfigTuple, typename Enable=void>
    struct ActivateHelper {
        template<typename MethodConfigT>
        static inline
        void exec(MethodConfigT&) {
        }
    };

    template<typename ConfigT, typename ConfigTuple>
    struct ActivateHelper<ConfigT
                        , ConfigTuple
                        , typename std::enable_if<panda::HasType<ConfigT, ConfigTuple>::value>::type
                        >
    {
        template<typename MethodConfigT>
        static inline
        void exec(MethodConfigT& mc) {
            mc.template config<ConfigT>().active(true);
        }
    };

} // namespace

template<class... FftAlgoConfigs>
template<class AlgoT>
void MethodConfig<FftAlgoConfigs...>::activate()
{
    typedef typename AlgoT::Config Config;
    ActivateHelper<Config, std::tuple<FftAlgoConfigs...>>::exec(*this);
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
