/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_RFIM_ZDOT_RFIM_H
#define SKA_CHEETAH_MODULES_RFIM_ZDOT_RFIM_H

#include "cheetah/data/TimeFrequency.h"
#include "cheetah/modules/rfim/RfimBase.h"
#include "cheetah/modules/rfim/PolicyInfo.h"
#include "cheetah/modules/rfim/policy/Policy.h"
#include "cheetah/modules/rfim/zdot/Config.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/RfimFlaggedData.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace rfim {
namespace zdot {

/**
 * @brief
 *    An implementation of zero-DM matched filter (also known as Zdot),
 *    which is explained in detail in https://doi.org/10.1093/mnras/stz1931
 */
template <typename DataType>
class Zdot
{
    public:
        typedef typename DataType::NumericalRep NumericalRep;

    public:
        Zdot();
        ~Zdot();

        void operator()(DataType& data);

    public:
        float _offset;  // Used to save the offset to rescale the data
};

} // namespace zdot
} // namespace rfim
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "detail/Zdot.cpp"

#endif // SKA_CHEETAH_MODULES_RFIM_ZDOT_RFIM_H
