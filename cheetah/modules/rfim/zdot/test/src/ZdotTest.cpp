/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/rfim/zdot/test/ZdotTest.h"
#include "cheetah/modules/rfim/zdot/Zdot.h"
#include "cheetah/modules/rfim/zdot/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include <algorithm>

namespace ska {
namespace cheetah {
namespace modules{
namespace rfim {
namespace zdot {
namespace test {

ZdotTest::ZdotTest() : ::testing::Test()
{
}

ZdotTest::~ZdotTest()
{
}

void ZdotTest::SetUp()
{
}

void ZdotTest::TearDown()
{
}

TEST_F(ZdotTest, test_object_creation)
{
    /**
    * Testing if the object gets created without any error
    */

    Config zdot_config;         // Empty config
    zdot_config.active(true);   // Setting zdot active
    Zdot<data::TimeFrequency<Cpu, uint8_t>> zdot_object;     //Checking if any errors are thrown during creation of Zdot object
}

TEST_F(ZdotTest, test_cleaning_noise)
{
    /**
    * Testing stats of the noise after cleaning
    */

    Config zdot_config;
    zdot_config.active(true);
    Zdot<data::TimeFrequency<Cpu, uint8_t>> zdot_obj;

    // Creating a TimeFrequency object with 64 timesamples and 32 frequency channels filled with gaussian noise
    float _mean = 128;
    float _rms = 10;
    data::TimeFrequency<Cpu, uint8_t> data_ptr(data::TimeFrequency<Cpu, uint8_t>(
                                                data::DimensionSize<data::Time>(64)
                                                , data::DimensionSize<data::Frequency>(32))
                                                );
    static std::random_device rd;
    std::seed_seq seed {  rd(), rd(), rd(), rd(), rd() };
    auto e2 = std::mt19937( seed );
    auto distribution = std::normal_distribution<float>(_mean,_rms);
    std::generate(data_ptr.begin(),data_ptr.end(), std::bind(distribution, e2));

    // Applying Zdot filter
    ASSERT_NO_THROW(zdot_obj(data_ptr));

    // Testing stats
    auto post_filter_mean = std::accumulate(data_ptr.begin(), data_ptr.end(), 0.0) / (data_ptr.number_of_channels()*data_ptr.number_of_spectra());

    float sqrdiff_sum = 0.0;
    std::for_each(
            data_ptr.begin(), data_ptr.end(), [&post_filter_mean, &sqrdiff_sum](float x) {
                sqrdiff_sum += std::pow( x-post_filter_mean , 2);
            }
        );

    ASSERT_NEAR(_mean, post_filter_mean, 0.1*_mean);
    ASSERT_NEAR(_rms, std::sqrt(sqrdiff_sum/(data_ptr.number_of_channels()*data_ptr.number_of_spectra())), 0.1*_rms);
}

TEST_F(ZdotTest, test_residual_rfi)
{
    /**
    * Test for checking RFI removal if 5 timesamples are affected by broadband RFI
    */

    Config zdot_config;
    zdot_config.active(true);
    Zdot<data::TimeFrequency<Cpu, uint8_t>> zdot_obj;

    // Creating a TimeFrequency object with 64 timesamples and 32 frequency channels filled with gaussian noise
    float _mean = 128;
    float _rms = 10;
    data::TimeFrequency<Cpu, uint8_t> data_ptr(data::TimeFrequency<Cpu, uint8_t>(
                                                data::DimensionSize<data::Time>(64)
                                                , data::DimensionSize<data::Frequency>(32))
                                                );
    static std::random_device rd;
    std::seed_seq seed {  rd(), rd(), rd(), rd(), rd() };
    auto e2 = std::mt19937( seed );
    auto distribution = std::normal_distribution<float>(_mean,_rms);
    std::generate(data_ptr.begin(),data_ptr.end(), std::bind(distribution, e2));

    auto rfi_it = data_ptr.begin() + data_ptr.number_of_channels()*16;
    for (auto it = rfi_it; it!=rfi_it+data_ptr.number_of_channels()*5; ++it)
    {
        *it = *it + _rms*2;
    }

    auto _rfi_mean = std::accumulate(rfi_it-data_ptr.number_of_channels(), rfi_it+data_ptr.number_of_channels()*6, 0.0)/(data_ptr.number_of_channels()*7);
    ASSERT_NEAR((2*_rms + _mean), _rfi_mean, 0.1*(2*_rms + _mean)); // Checking RFI injection

    // Applying Zdot filter
    ASSERT_NO_THROW(zdot_obj(data_ptr));

    // Testing stats of 7 affected time-samples
    auto post_filter_mean = std::accumulate(rfi_it-data_ptr.number_of_channels(), rfi_it+data_ptr.number_of_channels()*6, 0.0)/(data_ptr.number_of_channels()*7);
    ASSERT_NEAR(_mean, post_filter_mean, 0.1*_mean); // Checking RFI mitigation
}

TEST_F(ZdotTest, test_zdot_operation)
{
    /**
     * Testing every operation performed by the filter on the code copied from the implementation
     */

    float init_mean = 128;
    float init_rms = 10;
    data::TimeFrequency<Cpu, uint8_t> data(data::TimeFrequency<Cpu, uint8_t>(
                                                data::DimensionSize<data::Time>(64)
                                                , data::DimensionSize<data::Frequency>(32))
                                                );
    static std::random_device rd;
    std::seed_seq seed {  rd(), rd(), rd(), rd(), rd() };
    auto e2 = std::mt19937( seed );
    auto distribution = std::normal_distribution<float>(init_mean,init_rms);
    std::generate(data.begin(),data.end(), std::bind(distribution, e2));

    float vmax = std::numeric_limits<uint8_t>::max();
    float vmin = std::numeric_limits<uint8_t>::lowest();
    float _offset = 0.5* ( vmax+vmin );
    ASSERT_FLOAT_EQ(_offset,127.5);

    size_t nchan = data.number_of_channels();
    size_t nsamp = data.number_of_spectra();

    // Calulating zero-DM time-series
    auto it = data.begin();
    float const _mean = std::accumulate(data.begin(), data.end(), 0.0)/(nchan*nsamp);
    float sqrdiff_sum = 0.0;
    std::for_each(
            data.begin(), data.end(), [&_mean, &sqrdiff_sum](float val) {
                sqrdiff_sum += std::pow( val -_mean , 2);
            }
        );
    float const _stddev = std::sqrt(sqrdiff_sum/(nchan*nsamp));
    ASSERT_NEAR(_mean, init_mean, 0.1*init_mean);
    ASSERT_NEAR(_stddev, init_rms, 0.15*init_rms);

    std::vector<float> zts(nsamp, 0.0); // sum of the data along the frequency axis
    std::vector<float> mean_ztd(nsamp, 0.0);
    std::vector<float> scalar_products(nchan, 0.0); // scalar_products[i] = dot(channel_i, zts)

    for (size_t isamp = 0; isamp < nsamp; ++isamp, it += nchan)
    {
        const float sum = std::accumulate( it, it + nchan, 0.0f, [&](float acc, float val){ return acc + (val-_mean)/_stddev;
        });
        zts[isamp] = sum;
        std::transform( std::execution::par,
            it, it + nchan, scalar_products.begin(), scalar_products.begin(),
            [&](float val, float sp) {
                return sp + sum * (val-_mean)/_stddev;
            }
        );
    }
    ASSERT_EQ(it, data.end());
    ASSERT_EQ(scalar_products.size(),nchan);

    // Calculating weights
    const float zts_normsq = std::accumulate(
        zts.begin(), zts.end(), 0.0f,
        [](float acc, float val) {
            return acc + val * val;
        }
    );

    const float zts_normsq_inverse = zts_normsq > 0 ? 1.0f / zts_normsq : 0.0f;

    std::vector<float> weights(nchan, 0.0f);
    std::transform( std::execution::par,
        scalar_products.begin(), scalar_products.end(), weights.begin(),
        [&zts_normsq_inverse](float val) {
            return val * zts_normsq_inverse;
        }
    );
    ASSERT_EQ(weights.size(),nchan);

    // Applying the filter
    it = data.begin();
    for (size_t isamp = 0; isamp < nsamp; ++isamp, it += nchan)
    {
        const float z = zts[isamp];
        std::transform( std::execution::par,
            it, it + nchan, weights.begin(), it,
            [&](float x, float w) {
                return ((x-_mean) - (w * z)*_stddev) + _offset;
            }
        );
    }
    ASSERT_EQ(it,data.end());       // Checking if the iteration has reached the end of the data block

}

} // namespace test
} // namespace zdot
} // namespace rfim
} // namespace modules
} // namespace cheetah
} // namespace ska
