/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/rfim/zdot/Zdot.h"
#include "cheetah/data/FrequencyTime.h"
#include "cheetah/modules/rfim/iqrmcpu/detail/IqrmImpl.h"
#include "panda/Log.h"
#include <algorithm>
#include <random>
#include <execution>
#ifdef NDEBUG
#include <chrono>
#endif

namespace ska{
namespace cheetah{
namespace modules{
namespace rfim{
namespace zdot{

template <typename DataType>
Zdot<DataType>::Zdot()
{
    // finding the offset based on the NumericalRep of the DataType
    float vmax = std::numeric_limits<NumericalRep>::max();
    float vmin = std::numeric_limits<NumericalRep>::lowest();
    _offset = 0.5* ( vmax+vmin );
}

template <typename DataType>
Zdot<DataType>::~Zdot()
{
}

template<typename DataType>
void Zdot<DataType>::operator()(DataType& data)
{
    if (data.number_of_channels()*data.number_of_spectra() == 0)
        return;

    #ifdef NDEBUG
    PANDA_LOG_DEBUG << "Applying z-dot filter";
    #endif
    size_t nchan = data.number_of_channels();
    size_t nsamp = data.number_of_spectra();

    // Calulating zero-DM time-series
    auto it = data.begin();
    float const _mean = std::accumulate(data.begin(), data.end(), 0.0)/(nchan*nsamp);
    float sqrdiff_sum = 0.0;
    std::for_each(
            data.begin(), data.end(), [&_mean, &sqrdiff_sum](float val) {
                sqrdiff_sum += std::pow( val -_mean , 2);
            }
        );
    float const _stddev = std::sqrt(sqrdiff_sum/(nchan*nsamp));

    #ifdef NDEBUG
    auto z_dm_time_start = std::chrono::high_resolution_clock::now();
    #endif

    std::vector<float> zts(nsamp, 0.0); // sum of the data along the frequency axis
    std::vector<float> mean_ztd(nsamp, 0.0);
    std::vector<float> scalar_products(nchan, 0.0); // scalar_products[i] = dot(channel_i, zts)

    for (size_t isamp = 0; isamp < nsamp; ++isamp, it += nchan)
    {
        // Accumulate the standardised value to obtain standardised time-series.
        // Standardisation is done by subtracting mean from the time-frequency data and dividing it by the standard deviation
        const float sum = std::accumulate( it, it + nchan, 0.0f,
                                            [&](float acc, float val){ return acc + (val-_mean)/_stddev;});
        zts[isamp] = sum;
        std::transform( std::execution::par,
                        it, it + nchan, scalar_products.begin(), scalar_products.begin(),
                        [&](float val, float sp) { return sp + sum * (val-_mean)/_stddev; }
                        );
    }

    #ifdef NDEBUG
    auto z_dm_time_end = std::chrono::high_resolution_clock::now();
    double zdm_time = std::chrono::duration<double, std::milli>(z_dm_time_end - z_dm_time_start).count();
    PANDA_LOG_DEBUG << "Time taken to calculate ZDM Timeseries = " << zdm_time << " ms";
    #endif

    #ifdef NDEBUG
    auto alpha_factor_time_start = std::chrono::high_resolution_clock::now();
    #endif

    // Calculating weights
    const float zts_normsq = std::accumulate( zts.begin(), zts.end(), 0.0f,
                                                [](float acc, float val) {return acc + val * val; }
                                            );

    const float zts_normsq_inverse = zts_normsq > 0 ? 1.0f / zts_normsq : 0.0f;

    std::vector<float> weights(nchan, 0.0f);
    std::transform( std::execution::par,
                    scalar_products.begin(), scalar_products.end(), weights.begin(),
                    [&zts_normsq_inverse](float val) { return val * zts_normsq_inverse; }
                    );

    #ifdef NDEBUG
    auto alpha_factor_time_end = std::chrono::high_resolution_clock::now();
    double alpha_time = std::chrono::duration<double, std::milli>(alpha_factor_time_end - alpha_factor_time_start).count();
    PANDA_LOG_DEBUG << "Time taken to calculate alpha = " << alpha_time << " ms";
    auto filter_apply_time_start = std::chrono::high_resolution_clock::now();
    #endif

    // Applying the filter
    it = data.begin();
    for (size_t isamp = 0; isamp < nsamp; ++isamp, it += nchan)
    {
        const float z = zts[isamp];
        std::transform( std::execution::par,
                        it, it + nchan, weights.begin(), it,
                        [&](float x, float w) {
                        return ((x-_mean) - (w * z)*_stddev) + _offset;
                        });
    }

    #ifdef NDEBUG
    auto filter_apply_time_end = std::chrono::high_resolution_clock::now();
    double filter_time = std::chrono::duration<double, std::milli>(filter_apply_time_end-filter_apply_time_start).count();
    PANDA_LOG_DEBUG << "Time taken to apply filter = " << filter_time << " ms";
    #endif

}


} // namespace zdot
} // namespace rfim
} // namespace modules
} // namespace cheetah
} // namespace ska
