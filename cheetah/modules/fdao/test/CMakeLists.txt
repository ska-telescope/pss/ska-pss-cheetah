include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fdao_src src/gtest_fdao.cpp)

add_executable(gtest_fdao ${gtest_fdao_src})
target_link_libraries(gtest_fdao ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fdao gtest_fdao --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
