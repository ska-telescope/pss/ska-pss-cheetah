/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_SPS_CLUSTERING_HDBSCAN_CONFIG_H
#define SKA_CHEETAH_MODULES_SPS_CLUSTERING_HDBSCAN_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/data/Units.h"
#include "pss/astrotypes/units/DispersionMeasure.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {

/**
 * @brief Configuration for the SpsClustering module. See the documentation
 * of SpsClustering for an explanation of the parameters.
 */

class Config : public utils::Config
{
    public:
        typedef boost::units::quantity<data::MilliSeconds, double> MsecTimeType;
	typedef pss::astrotypes::units::DispersionMeasure<float> Dm;
    public:
        Config();
        ~Config();
         /* @brief: Size of the smallest allowed cluster, any cluster smaller thasn this will be treated as noise
         */
	std::size_t minimum_cluster_size() const;
        void minimum_cluster_size(std::size_t const& core);

        bool active() const;


    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
	std::size_t _minimum_cluster_size;
        bool _active;
};


} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_SPS_CLUSTERING_CONFIG_H
