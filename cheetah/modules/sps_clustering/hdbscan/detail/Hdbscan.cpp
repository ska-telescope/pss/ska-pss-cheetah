/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <boost/geometry/index/rtree.hpp>
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/for_each.hpp>
#include <chrono>

namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {

namespace bmpl = boost::mpl;
namespace bgi = boost::geometry::index;


template<typename NumRepType>
std::vector< std::vector<size_t>> Hdbscan::operator()(data::SpCcl<NumRepType> const& cands)
{
    std::vector<std::vector<size_t>> groups;
    float const DM_constant = 1/241.0;
    float start_frequency = cands.tf_blocks()[0]->low_high_frequencies().first.value()/1000.0; // converting MHz to GHz
    float end_frequency = cands.tf_blocks()[0]->low_high_frequencies().second.value()/1000.0;
    float const DM_to_time_factor = DM_constant*(1/(start_frequency*start_frequency) - 1/(end_frequency*end_frequency))*1000.0;
    typename Config::Dm dm_unit(1.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    typename Config::MsecTimeType time_unit(0.001 * boost::units::si::seconds);
    std::vector<float> Time_axis;
    std::vector<float> Dm_axis;
    std::vector<float> Widths;
    std::vector<float> SNRs;
    if(cands.tf_blocks().size() == 0 || cands.size() < _config.minimum_cluster_size()+1) return groups;

    for(size_t i = 0 ; i < cands.size() ; ++i)
    {
        Time_axis.push_back(static_cast<float>(cands[i].tstart().value()));  ///(time_unit)));
        Dm_axis.push_back(static_cast<float>(cands[i].dm().value())*DM_to_time_factor);
        Widths.push_back(static_cast<float>(cands[i].width().value()));
        SNRs.push_back(static_cast<float>(cands[i].sigma()));
    }

    SpanningTreeCartesian Tree(Time_axis, Dm_axis);
    Tree.construct_tree();
    MakeCartesianClusters clustering(Tree.parent(), Tree.weights(), Time_axis.size(), _config.minimum_cluster_size());
    clustering.make_clusters();
    WidthClustering width_clstr(Time_axis, Dm_axis, SNRs, Widths, clustering.good_clusters());
    width_clstr.do_clustering();
    groups = width_clstr.final_clusters();

    return groups;

}

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
