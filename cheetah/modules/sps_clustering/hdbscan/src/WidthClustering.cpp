/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "cheetah/modules/sps_clustering/hdbscan/WidthClustering.h"
namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan{


WidthClustering::WidthClustering(std::vector<float> X_locations, std::vector<float> Y_locations, std::vector<float> snr, std::vector<float> widths, std::vector<MakeCartesianClusters::cluster> input_clusters)
    :_widths(widths),
    _x(X_locations),
    _y(Y_locations),
    _snr(snr),
    _small_clusters(input_clusters)
{
}


void WidthClustering::get_representative()
{
    std::size_t max_idx;
    for(std::size_t ii = 0; ii<_small_clusters.size(); ++ii)
    {
        std::vector<size_t> snrs;
        for(std::size_t jj=0; jj<_small_clusters[ii].elements.size(); ++jj)
        {
              snrs.push_back(_snr[_small_clusters[ii].elements[jj]]);
        }
	std::size_t idx;
        idx = max_element(snrs.begin(), snrs.end()) - snrs.begin();
        max_idx = _small_clusters[ii].elements[idx];
        _representative_elements.push_back(max_idx);
    }
}

float WidthClustering::distance(std::size_t idx1, std::size_t idx2)
{
    float x = _x[idx1] - _x[idx2];
    float y = _y[idx1] - _y[idx2];
    return sqrt(x*x + y*y);
}



void WidthClustering::connect()
{
    std::vector<int> _status(_representative_elements.size(), 0);
    for(std::size_t ii=0; ii<_representative_elements.size(); ++ii)
    {
        if(_status[ii] == 0)
        {
            std::vector<std::size_t> list_elements;
            for(std::size_t jj=0; jj<_small_clusters[ii].elements.size(); ++jj)
            {
                list_elements.push_back(_small_clusters[ii].elements[jj]);
            }
            float current_W = _widths[_representative_elements[ii]];
            for(std::size_t jj=0; jj<_representative_elements.size(); ++jj)
            {
                if(_status[jj] == 0)
                {
                    float d12 = distance(_representative_elements[ii], _representative_elements[jj]);
                    if(d12 < current_W)
                    {
                        for(std::size_t kk=0; kk<_small_clusters[jj].elements.size(); ++kk)
                        {
                            list_elements.push_back(_small_clusters[jj].elements[kk]);
                        }
                        _status[jj] = 1;
                    }
                }
            }
            _status[ii] = 1;
            _final_clusters.push_back(list_elements);
        }
    }
}

void WidthClustering::connect_recursive(std::size_t idx)
{
// Get the neighbours for idx first,

    _connections.push_back(idx);
    float current_W =  _widths[_representative_elements[idx]];
    std::vector<std::size_t> neighbours;                             // neighbours stores the indices of neighbouring clusters in current small_clusters array
    for(std::size_t jj=0; jj<_representative_elements.size(); ++jj)
    {
        if(_status[jj] == 0)
        {
            float d12 = distance(_representative_elements[idx], _representative_elements[jj]);
            if(d12 < current_W)
            {
                neighbours.push_back(jj);
                _status[jj] = 1;           // Status of the connected neighbours is updated
            }
        }
    }

// Got the neighbours for idx
// Calling the recursive functions for these neighbours
    if(neighbours.size() > 0)
    {
        for(std::size_t ii=0; ii<neighbours.size(); ++ii)
        {
            connect_recursive(neighbours[ii]);
        }
    }
}

void WidthClustering::do_clustering()
{
    get_representative();
    while(_representative_elements.size() > 0)
    {
        std::vector<std::size_t> cluster_elements;
        _status.clear();
        for(std::size_t ii=0; ii<_representative_elements.size(); ++ii)
        {
            _status.push_back(0);
        }
        _status[0] = 1;
        _connections.clear();                        // connections store the indices of connected elements
        connect_recursive(0);
        for(std::size_t ii=0; ii<_connections.size(); ++ii)
        {
            for(std::size_t jj=0; jj<_small_clusters[_connections[ii]].elements.size(); ++jj)
            {
                cluster_elements.push_back(_small_clusters[_connections[ii]].elements[jj]);
            }
        }
	std::vector<std::size_t> rps = _representative_elements;
	std::vector<MakeCartesianClusters::cluster> cls = _small_clusters;
        _representative_elements.clear();
        _small_clusters.clear();
        _final_clusters.push_back(cluster_elements);
//		}
        for(std::size_t ii=0; ii<_status.size(); ++ii)
        {
            if(_status[ii] == 0)
            {
                _representative_elements.push_back(rps[ii]);
                _small_clusters.push_back(cls[ii]);
            }
        }
    }

}

std::vector<std::vector<std::size_t>> WidthClustering::final_clusters()
{
    return _final_clusters;
}

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
