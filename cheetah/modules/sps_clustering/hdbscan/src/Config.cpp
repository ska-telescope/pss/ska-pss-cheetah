/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/sps_clustering/hdbscan/Config.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {


Config::Config()
    : utils::Config("sps_clustering/hdbscan_clustering")
    , _minimum_cluster_size(15)
    , _active(true)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options )
{
    add_options
    ("active", boost::program_options::value<bool>(&_active)->default_value(_active),
        "perform SpsClustering with HDBSCAN if true"
    )
    ("minimum_cluster_size", boost::program_options::value<std::size_t>(&_minimum_cluster_size)->default_value(15),
        "Minimum cluster size in HDBSCAN clustering. If a cluster has less points than the minimum cluster size, it will be rejected as noise."
    );

}


std::size_t Config::minimum_cluster_size() const
{
    return _minimum_cluster_size;
}

void Config::minimum_cluster_size(std::size_t const& core)
{
    _minimum_cluster_size = core;
}

bool Config::active() const
{
    return _active;
}

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
