/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "cheetah/modules/sps_clustering/hdbscan/MakeCartesianClusters.h"
namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {



// Constructor
MakeCartesianClusters::MakeCartesianClusters(std::vector<int> Parent, std::vector<float> Weights, std::size_t data_size, std::size_t Core)
    :_weights(Weights),
    _parent(Parent),
    _core(Core),
    _size(data_size)
{
    _labeling_status.resize(_size);
}



void MakeCartesianClusters::break_clusters(float threshold)
{

    int lbl=1, max_label=1;     // The zeroth lebel to assign, then it will be increased by 1 for each new cluster. The cluster labels will be (1,2,3,4....)
    std::size_t pre;
    _uniq_labels.clear();
    fill(_labeling_status.begin(), _labeling_status.end(), 0);
    _indices.clear();
// Looping over elements to detect breaks and relebel the resultant child clusters
    for(std::size_t jj=0; jj<_size; ++jj)
    {
        if(_weights[jj] > threshold)
        {             // Break the links
            _parent[jj] = -1;
        }
        if(_labels[jj] != 0)
        {                     // Store non-zero labels to indices
            _indices.push_back(jj);
        }
        else
        {
            _parent[jj] = -1;                // Parents of all zero labels are -1
        }
    }

    for(std::size_t ii=0; ii<_indices.size(); ++ii)
    {
	    std::size_t jj = _indices[ii];
        if(_labeling_status[jj] == 0)
        {           // Enter the loop only if the element is not noise
                 _branch.clear();    // Branch stores the unlabeled connected elements from the current element to the first node until a break is found
                 _branch.push_back(jj);  // First member of branch is current elelment
                 pre=jj;        // pre is the index of the current element
                 while(_parent[pre] != -1)
                 {   // go backwards towards the first node until we find a
                     if(_labeling_status[_parent[pre]] == 1)
                     {
                         lbl = _labels[_parent[pre]];
                         break;
                     }
                     pre=_parent[pre];
                     _branch.push_back(pre);
                 }
                 if(_parent[pre] == -1)
                 {
                    max_label = max_label + 1;
                    lbl = max_label;
                    _uniq_labels.push_back(lbl);
                 }
                 for(std::size_t kk=0; kk<_branch.size(); ++kk)
                 {        // update the labels and the labeling status of the branch
                     _labels[_branch[kk]] = lbl;
                     _labeling_status[_branch[kk]] = 1;
                 }
             }
    }
//    int lsize;
    _sorted_labels.clear();
    _sorted_labels = _labels;
    sort(_sorted_labels.begin(), _sorted_labels.end());            // Store the sorted array of labels that will be used to decide clusters with size less than core
    _small_labels.clear();                                        // Stores the label of clusters that have size less than core size
    for(std::size_t jj=0; jj< _uniq_labels.size(); ++jj)
    {                     // Loop over all the uniq labels
        auto id1 = find(_sorted_labels.begin(), _sorted_labels.end(),_uniq_labels[jj]);              // Find the first occurance of the uniq_label in the sorted array
        int first_occ = id1 - _sorted_labels.begin();                                              // index of first occurance
        if(_sorted_labels[first_occ + _core-1] != _uniq_labels[jj])
        {                                 // If the element at core indices away from the first occurance is changed, then size is less than co
            _small_labels.push_back(_uniq_labels[jj]);                                          // Store the label of sort cluster
        }
    }
    for(std::size_t jj=0; jj<_small_labels.size(); ++jj)
    {
        replace(_labels.begin(), _labels.end(), _small_labels[jj], 0);                              // Replace labels corresponding to the small clusters with zero
    }
    _full_labels.push_back(_labels);
}
// condense_cluslist_labelters uses the 2D array of labels_full and weights_sorted to remove the noise falling off the clusters (small clusters with size less than core_size) and label them according to the inheritence


void MakeCartesianClusters::traceback_stability()
{
    int ia;
    std::vector<cluster> final_clusters;
    for(std::size_t ii=0; ii<_clusters_stable.size()-1; ++ii)
    {
        std::vector<cluster> clusters_stage, clusters_stage1;
        ia = _clusters_stable.size() - ii -1;
        clusters_stage = _clusters_stable[ia];
        clusters_stage1 = _clusters_stable[ia-1];
        for(std::size_t jj = 0; jj<clusters_stage.size(); ++jj)
        {
            cluster clstr, clstr1;
            int current_label = clusters_stage[jj].label;
	    std::vector<int> label_list;
            for(std::size_t kk=0; kk<_clusters_stable[ia-1].size(); ++kk)
            {
                label_list.push_back(_clusters_stable[ia-1][kk].label);
            }
            auto id0 = find(label_list.begin(), label_list.end(), current_label);
            if(id0 != label_list.end())
            {                            // If the current label was found in the previous stage
                int idx = id0 - label_list.begin();
                _clusters_stable[ia-1][idx].stability = clusters_stage[jj].stability;
            }
            else
            {                                                  // If the current label is not found in the previous stage (i.e. it's child of some label or a new cluster appearing
                std::vector<cluster> parent_clusters;
		std::vector<int> parent_idx;
                for(std::size_t kk=0; kk<clusters_stage1.size(); ++kk)
                {
                    if(clusters_stage1[kk].child.size() > 1)
                    {
                        parent_clusters.push_back(clusters_stage1[kk]);
                        parent_idx.push_back(kk);
                    }
                }
                int parent_exist = 0, pidx=0;
                for(std::size_t kk=0; kk<parent_clusters.size(); ++kk)
                {
                    for(std::size_t ll=0; ll<parent_clusters[kk].child.size(); ++ll)
                    {
                        if(parent_clusters[kk].child[ll] == current_label)
                        {
                            parent_exist = 1;
                            pidx = parent_idx[kk];
                        }
                    }
                }
                if(parent_exist == 1)
                {
                    float children_stability = 0.0;
                    for(std::size_t kk=0; kk<clusters_stage1[pidx].child.size(); ++kk)
                    {
                        for(std::size_t ll=0; ll<clusters_stage.size(); ++ll)
                        {
                            if(clusters_stage1[pidx].child[kk] == clusters_stage[ll].label)
                            {
                                children_stability = children_stability + clusters_stage[ll].stability;
                            }
                        }
                    }
                    if(children_stability > clusters_stage1[pidx].stability)
                    {
                        _clusters_stable[ia-1][pidx].stability = children_stability;
                    }
                }
            }
        }
    }
}

void MakeCartesianClusters::get_stable_clusters()
{
//    int stage;                 // stage of the breaking, number of labels including noise
    std::size_t num_labels;
    int max_lbl=1;                           // maximum label at current stage
    std::vector<int> stage_lbls, stage1_lbls, uniq_lbls;     // All labels at this stage
    for(std::size_t ii=0; ii<_full_labels.size()-1; ++ii)
    {   // Loop over all braking stages
        stage_lbls = _full_labels[ii];       // Stores labels of current stage
        stage1_lbls = _full_labels[ii+1];    // Sotres labels of the next stage
        uniq_lbls = stage_lbls;             // Will eventually store the number of uniq_labels at current stage including noise (0 labels)
	std::vector<cluster> clusters_array;
        sort(uniq_lbls.begin(), uniq_lbls.end());
        auto id = unique(uniq_lbls.begin(), uniq_lbls.end());
        uniq_lbls.resize(distance(uniq_lbls.begin(), id));
        num_labels = uniq_lbls.size();
        int max1;
        max1 = *max_element(stage1_lbls.begin(), stage1_lbls.end());               // maximum label at the current stage
        max_lbl = std::max(max1, max_lbl);
	std::vector<int> clstr_labels;                                                  // Store labels of previous stage clusters
        if(_clusters_stable.size() != 0)
        {
            for(std::size_t kk=0; kk<_clusters_stable[_clusters_stable.size()-1].size(); ++kk)
            {
                clstr_labels.push_back(_clusters_stable[_clusters_stable.size()-1][kk].label);
            }
        }
        for(std::size_t jj=0; jj<num_labels; ++jj)
        {
        if(uniq_lbls[jj] != 0)
        {
            std::vector<int> child_labels, child_idx;        // Stores the index of the current label and child labels for the current label
	    std::vector<std::size_t> elements;
            for(std::size_t kk=0; kk < stage_lbls.size(); ++kk)
            {
                if(stage_lbls[kk] == uniq_lbls[jj])
                {
                    elements.push_back(kk);
                    if(stage1_lbls[kk] != 0)
                    {
                        child_labels.push_back(stage1_lbls[kk]);  // Only stores no n noise child labels
                        child_idx.push_back(kk);
                    }
                }
            }
            if(child_labels.size() > 0)
            {                                    // If the current label has not disappeared in noise
                std::vector<int> uniq_lbls1;
                uniq_lbls1 = child_labels;                               // Will get the non-zero uniq child labels for current label in the stage
                sort(uniq_lbls1.begin(), uniq_lbls1.end());
                auto id1 = unique(uniq_lbls1.begin(), uniq_lbls1.end());
                uniq_lbls1.resize(distance(uniq_lbls1.begin(), id1));
                if(uniq_lbls1.size() == 1)
                {                    // If there is only one child of the current label (i.e. cluster will survive next breaking
                    if(_clusters_stable.size() > 0)
                    {       // If there are already layers of clusters from previous stages
                        auto id2 = find(clstr_labels.begin(), clstr_labels.end(), uniq_lbls[jj]);
                        if(id2 != clstr_labels.end())
                        {
                            cluster clstr;
                            int idx = id2 - clstr_labels.begin();
                            clstr = _clusters_stable[_clusters_stable.size()-1][idx];
                            clstr.child = uniq_lbls1;     // Update the child labels (though only single child at this stage)
                            clstr.stability = clstr.stability + (clstr.elements.size() - elements.size())*(1/_braking_threshold[ii] - 1/_braking_threshold[clstr.birth_stage]);
                            clstr.elements = elements;    // Update the elements at this stage
                            clusters_array.push_back(clstr);  // Store the cluster in the cluster array of the current stage
                        }
                        else
                        {
                            cluster clstr;
                            clstr.elements = elements;
                            clstr.child = uniq_lbls1;
                            clstr.parent = uniq_lbls[jj];
                            clstr.label = uniq_lbls[jj];
                            clstr.stability = 0;
                            clstr.birth_stage = ii;
                            clstr.death_stage = 0;
                            clusters_array.push_back(clstr);
                        }


                    }
                    else
                    {                // If the vector of cluster arrays is empty, mabe its first step or there have been no stable label yet
                        cluster clstr;               // Initialize a cluster now as this label is surviving
                        clstr.elements = elements;
                        clstr.child = uniq_lbls1;
                        clstr.parent = uniq_lbls[jj];    // Parent will be this label and should not be updated in the following stages
                        clstr.label = uniq_lbls[jj];     // Current label is the label, this will also be same up to the point the cluster is surviving as we will be updating it
                        clstr.stability = 0;             // initialize stability as 0
                        clstr.birth_stage = ii;          // Birth stage will rmain same
                        clstr.death_stage = 0;           // Death stage is not known yet
                        clusters_array.push_back(clstr); // Push back this into the cluster array of current stage
                    }
                    for(std::size_t kk=0; kk<child_idx.size(); ++kk)
                    {
                        _full_labels[ii+1][child_idx[kk]] = uniq_lbls[jj];
                    }
                                                                   // all the child labels by the current label
                }


                if(uniq_lbls1.size() > 1)
                {      // If there are more than one children (i.e. cluster is splitting)
                    cluster clstr;
                    if(_clusters_stable.size() > 0)
                    {    // If there already are parent clusters stored for this label
                        auto id2 = find(clstr_labels.begin(), clstr_labels.end(), uniq_lbls[jj]);
                        if(id2 != clstr_labels.end())
                        {
                            int idx = id2 - clstr_labels.begin();
                            clstr = _clusters_stable[_clusters_stable.size()-1][idx];
                            clstr.stability = clstr.stability + (clstr.elements.size())*(1/_braking_threshold[ii] - 1/_braking_threshold[clstr.birth_stage]);
                            clstr.elements = elements;     // Update the elements
                            clstr.death_stage = ii;        // Now we know the death stage of the cluster
                        }
                        else
                        {
                            clstr.elements = elements;
                            clstr.parent = uniq_lbls[jj];
                            clstr.label = uniq_lbls[jj];
                            clstr.stability = 0;
                            clstr.birth_stage = ii;
                            clstr.death_stage = ii;
                        }
                    }
                    else
                    {
                        clstr.elements = elements;
                        clstr.parent = uniq_lbls[jj];
                        clstr.label = uniq_lbls[jj];
                        clstr.stability = 0;
                        clstr.birth_stage = ii;
                        clstr.death_stage = ii;
                    }
		    std::vector<int> next_children;
                    for(std::size_t kk=0; kk<uniq_lbls1.size(); ++kk)
                    {
			std::vector<size_t> child_idx1;
                        for(std::size_t ll=0; ll<child_idx.size(); ++ll)
                        {
                            if(_full_labels[ii+1][child_idx[ll]] == uniq_lbls1[kk])
                            {
                                child_idx1.push_back(child_idx[ll]);
                            }
                        }
                        next_children.push_back(max_lbl+1+kk);

                        for(std::size_t ll=0; ll<child_idx1.size(); ++ll)
                        {
                            _full_labels[ii+1][child_idx1[ll]] = max_lbl+1+kk;
                        }
                    }
                    clstr.child = next_children;
                    clusters_array.push_back(clstr);

                    max_lbl = max_lbl+uniq_lbls1.size();  // update the max label

                }

            }
            else
            {                 // If there is no non zero child of the current cluster (i.e. cluster has become noise now)
                if(_clusters_stable.size() != 0)
                { // If there is any previous cluster for this label, store it's final stage
                    auto id2 = find(clstr_labels.begin(), clstr_labels.end(), uniq_lbls[jj]);
                    if(id2 != clstr_labels.end())
                    {
                        cluster clstr;
                        int idx = id2 - clstr_labels.begin();
                        clstr = _clusters_stable[_clusters_stable.size()-1][idx];
                        clstr.stability = clstr.stability + (clstr.elements.size())*(1/_braking_threshold[ii] - 1/_braking_threshold[clstr.birth_stage]);
                        clstr.elements = elements;
                        clstr.death_stage = ii;
                        clusters_array.push_back(clstr);
                    }
                }   // If there is no previous cluster for this label and its breaking at this stage, no point in storing this
            }

        }   // if statement for uniq_label[jj] != 0 ends here
        }  // Loop over jj (i.e. index for uniq labels in current stage) ends here
        _clusters_stable.push_back(clusters_array);   // Store the current stage clusters for all jj values in the clusters_stable for each ii
    }          // Loop over ii (stages) ends here
}

void MakeCartesianClusters::extract_clusters(size_t stage, size_t index)
{
    cluster current_cluster = _clusters_stable[stage][index];
    std::vector<cluster> next_stage = _clusters_stable[stage+1];
    std::size_t num_child = current_cluster.child.size();
    if(num_child == 1)
    {
//        // Try to find the current label in next_stage
        std::vector<int> list_labels;
        for(std::size_t ii = 0; ii<next_stage.size(); ++ii)
        {
            list_labels.push_back(next_stage[ii].label);
        }
        auto id0 = find(list_labels.begin(), list_labels.end(), current_cluster.label);
	std::size_t idx;
        if(id0 != list_labels.end())
        {
                idx = id0 - list_labels.begin();
                if(int(stage+1) < int(_clusters_stable.size()-1))
                {
                    extract_clusters(stage+1, idx);
                }
        }
//
        else
        {
            _good_clusters.push_back(current_cluster);
        }
        // if same label is found, recall the next stage
        // if the same label is not found in the next_stage, sotre the cluster in the good_clusters array
    }
    else
    {
        // calculate the stability of children clusters
        float children_stability = 0.0;
	std::vector<std::size_t> indexes;
        for(std::size_t ii=0; ii<num_child; ii++)
        {
            for(std::size_t jj=0; jj<next_stage.size(); ++jj)
            {
                if(next_stage[jj].label == current_cluster.child[ii])
                {
                    children_stability = children_stability + next_stage[jj].stability;
                    indexes.push_back(jj);
                }
            }
        }
        if(current_cluster.stability > 1.0*children_stability)
        {
            _good_clusters.push_back(current_cluster);
        }
        else
        {
            if(int(stage+1) < int(_clusters_stable.size()-1))
            {
                for(std::size_t kk=0; kk<indexes.size(); ++kk)
                {
                    extract_clusters(stage+1, indexes[kk]);
                }
            }
        }
    }
    if(int(stage+1) >= int(_clusters_stable.size()-1))
    {          // Store all the clusters present in the last stage
        _good_clusters.push_back(current_cluster);
    }


}




void MakeCartesianClusters::make_clusters()
{
    int brk_unit = 1;                     // Minimum step size in threshold (ms) used to break the tree
// Initiate the vector of labels with 1
    for(i=0; i<_size; ++i)
    {
        _labels.push_back(1);
    }
// Sorting the vector of weights
    std::vector<float> thresholds;
    for(i=0; i<_weights.size(); ++i)
    {
        thresholds.push_back(float(brk_unit*int(_weights[i]/brk_unit)));
    }
    sort(thresholds.begin(), thresholds.end());
    auto id = unique(thresholds.begin(), thresholds.end());
    thresholds.resize(distance(thresholds.begin(), id));
    reverse(thresholds.begin(), thresholds.end());
    _braking_threshold = thresholds;
    if(thresholds.size() > 2)
    {       // Use the standard process of hdbscan only if more than two breaking thresholds are available
// Calling relevant functions with decreasing weights
        for(i=0; i < thresholds.size(); ++i)
        {
// Break the cluster usign the decreased weight
            break_clusters(thresholds[i]);

        }

        get_stable_clusters();

        if(_clusters_stable.size() > 2)
        {          // If more than two stages of stability calculation was done, only then go for extracting clusters
            traceback_stability();

            for(std::size_t ii=0; ii<_clusters_stable[0].size(); ++ii)
            {
                extract_clusters(0, ii);
            }
        }

        else
        {                                  // If less than two stages, just use the result of first breaking to form good clusters
		std::vector<int> end_labels = _full_labels[0];
		std::vector<int> uniq_lbls = end_labels;
                sort(uniq_lbls.begin(), uniq_lbls.end());
                auto id = unique (uniq_lbls.begin(), uniq_lbls.end());
                uniq_lbls.resize(distance(uniq_lbls.begin(), id));
                for(std::size_t ii=0; ii < uniq_lbls.size(); ++ii)
                {
                    if(uniq_lbls[ii] != 0)
                    {
                        std::vector<std::size_t> elements;
                        for(i=0; i<end_labels.size(); ++i)
                        {
                            if(end_labels[i] == uniq_lbls[ii])
                            {
                                elements.push_back(i);
                            }
                        }
                        if(elements.size() >= _core)
                        {
                            cluster clstr;
                            clstr.elements = elements;
                            clstr.parent = uniq_lbls[ii];
                            clstr.label = uniq_lbls[ii];
                            clstr.stability = 0;
                            clstr.birth_stage = 0;
                            clstr.death_stage = 0;
                            _good_clusters.push_back(clstr);
                        }
                    }
                }
        }
    }
    else
    {                                 // If less than 2 breaking thresholds are available, return a single cluster with all the elements
        std::vector<std::size_t> elements;
        for(std::size_t ii=0; ii<_size; ++ii)
        {
            elements.push_back(ii);
        }
        cluster clstr;
        clstr.elements = elements;
        clstr.parent = 0;
        clstr.label = 1;
        clstr.stability = 0;
        clstr.birth_stage = 0;
        clstr.death_stage = 0;
        _good_clusters.push_back(clstr);
    }

}

std::vector<MakeCartesianClusters::cluster> MakeCartesianClusters::good_clusters()
{
    return _good_clusters;
}

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
