/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "cheetah/modules/sps_clustering/hdbscan/SpanningTreeCartesian.h"
namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan{

// This class takes two co-ordinates of the data points and computes the Minimum spanning tree

SpanningTreeCartesian::SpanningTreeCartesian(std::vector<float> X, std::vector<float> Y)
// Constructor
    :_x_locations(X),
    _y_locations(Y),
    _size(X.size())
{
    _parent.resize(_size),   // resizing parent vector
    _parent[0] = -1;        // First element of the tree is the tree node and does not have a parent
    _weights.resize(_size);  // Weights will store the weigth of a link, i.e. distance between the two linked points
}

void SpanningTreeCartesian::Cartesian(std::size_t idx1, std::size_t idx2)
{
// Utility function to compute distance between two given points
    _xdist=_x_locations[idx1]-_x_locations[idx2];
    _ydist=_y_locations[idx1]-_y_locations[idx2];
    _distance = sqrt(_xdist*_xdist + _ydist*_ydist);
}




void SpanningTreeCartesian::construct_tree()
{
    std::vector<float> key(_size, FLT_MAX);              // Initialised key. Key stores the distances of all the points from current tree
    std::vector<size_t> indices(_size);                   // A array of indices ranging from 0 to size-1. We need this array to keep track of indices because we plan to remove elements from the key
    iota(indices.begin(), indices.end(),0);     // Fill indices using iota funciton
    key[0] = 0.0;                               // The first element will be part of the tree with no parent
    while(indices.size() > 1)
    {                  //lopping over untill all elements are connected
        std::size_t u, index;
        u = min_element(key.begin(), key.end()) - key.begin();     // Get the index of smallest element in the key
        index = indices[u];                                             // Real index of the element that has minimum value of key (i.e. this element is nearest to the current tree)
        indices.erase(indices.begin()+u);                               // Remove the element
        key.erase(key.begin()+u);                                       // Remove the key
        for(std::size_t v=0; v<indices.size(); ++v)
        {                                // Loop over remaining elements that are not part of the tree
            Cartesian(index, indices[v]);                           // Compute the distance from the nearest element to the tree
            if(_distance < key[v])
            {                                  // If the distance between the new tree element and the index of v is less than the earlier distance of v from the tree
                _parent[indices[v]] = int(index);                       // Update the parent of each remaining element
                key[v] = _distance;                              // Update the key of v because the tree has a new element and distance to the tree from the v is now less than before
                _weights[indices[v]] = _distance;                 // Update the Weights
           }
        }                                   // After completing these two loops, all elements will have correct parents and weights
    }
}

std::vector<int>  SpanningTreeCartesian::parent()
{
    return _parent;
}

std::vector<float>  SpanningTreeCartesian::weights()
{
    return _weights;
}



} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
