include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_hdbscan_clustering_src
	src/HdbscanTest.cpp
    src/SpsClusteringTest.cpp
    src/gtest_hdbscan_clustering.cpp
)

add_executable(gtest_hdbscan_clustering ${gtest_hdbscan_clustering_src})
target_link_libraries(gtest_hdbscan_clustering ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_hdbscan_clustering gtest_hdbscan_clustering --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
