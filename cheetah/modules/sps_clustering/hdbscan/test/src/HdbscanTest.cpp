/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/sps_clustering/hdbscan/test/HdbscanTest.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {
namespace test {


HdbscanTest::HdbscanTest()
    : ::testing::Test()
{
}

HdbscanTest::~HdbscanTest()
{
}

void HdbscanTest::SetUp()
{
}

void HdbscanTest::TearDown()
{
}

TEST_F(HdbscanTest, test_constructor)
{
    // Generate SpSift Config and construct SpSift object
    Config config;
    config.minimum_cluster_size(10);
    ASSERT_NO_THROW(Hdbscan another_clusterer(config));
}

TEST_F(HdbscanTest, test_group_similar_to_core)
{
//  This test provides a cluster with number of candidates similar to the core size, performs the hdbscsan clustering, and checks the results. This tests the algorithm with small number of candidates.
    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));

    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(12.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.005 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 10.0;

    for (std::size_t idx=0; idx<15; ++idx)
    {
        data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);
        tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType((0.001+0.0001*idx)*boost::units::si::seconds);
        dm += typename Config::Dm((1.0+0.01*idx) * pss::astrotypes::units::parsecs_per_cube_cm);
    }

    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),1U);
    ASSERT_GE(groups[0].size(),10U);
    ASSERT_LE(groups[0].size(),15U);
}

TEST_F(HdbscanTest, test_group_empty)
{
//  This test provides a cluster with zero candidates, performs the hdbscsan clustering, and checks the results
    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));

    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(12.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.005 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),0U);
}



TEST_F(HdbscanTest, test_group_less_than_core)
{
//  This test provides a cluster with number of candidates less than the core size, performs the hdbscsan clustering, and checks the results. Since there are not enough candidates to form a stable cluster, we expect zero clusters in the result.
    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));

    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(12.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.005 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 10.0;

    for (std::size_t idx=0; idx<8; ++idx)
    {
        data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);
        tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType((0.001+0.0001*idx)*boost::units::si::seconds);
        dm += typename Config::Dm((1.0+0.01*idx) * pss::astrotypes::units::parsecs_per_cube_cm);
    }

    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),0U);
}

TEST_F(HdbscanTest, test_group_thousand)
{
//  This test provides a thousand candidates, performs the hdbscsan clustering, and checks the results. The candidates are in a single group and distance between the candidates increases with increasing time and DM. We expect only a single cluster from this set of candidates. This tests the Minimum spanning tree (MST) formation and breaking steps.
    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));

    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(12.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.005 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 10.0;

    for (std::size_t idx=0; idx<1000; ++idx)
    {
        data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);
        tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType((0.001+0.0001*idx)*boost::units::si::seconds);
        dm += typename Config::Dm((1.0+0.01*idx) * pss::astrotypes::units::parsecs_per_cube_cm);
    }

    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),1U);
    ASSERT_GE(groups[0].size(),10U);
}

TEST_F(HdbscanTest, test_group_thousand_equal_distance)
{
//  This test provides a cluster with thousand candidates separeted by constant distance (only one breaking length), performs the hdbscsan clustering, and checks the results. Here seperation between candidates is constant that means all the candidates will have same associated linking length. This means that the minimum spanning tree (MST) will be broken to individual candidates in a single step of breaking. Here we are testing the robustness of the MST breaking step.

    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));

    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(12.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.005 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 10.0;

    for (std::size_t idx=0; idx<1000; ++idx)
    {
        data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);
        tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType((0.001)*boost::units::si::seconds);
        dm += typename Config::Dm((1.0) * pss::astrotypes::units::parsecs_per_cube_cm);
    }
    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),1U);
    ASSERT_GE(groups[0].size(),10U);
}

TEST_F(HdbscanTest, test_multiple_groups)
{
//  This test provides ten clusters with 50 candidates each, performs the hdbscsan clustering, and checks the results. Here we are testing the robustness of implementation against merging independent clusters.

    data::SpCcl<NumericalRep>::BlocksType tf_v;
    data::TimeFrequency<Cpu, NumericalRep> tf1(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(data::TimeFrequency<Cpu, NumericalRep>::TimeType(1 * boost::units::si::milli * boost::units::si::seconds));
    tf1.set_channel_frequencies_const_width(data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(1350.0 * boost::units::si::mega * boost::units::si::hertz), data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(20.0 * boost::units::si::mega * boost::units::si::hertz));
    tf_v.push_back(std::make_shared<data::TimeFrequency<Cpu, NumericalRep>>(tf1));


    // Generate SpCcl instance
    data::SpCcl<NumericalRep> cand_list(tf_v, data::DimensionIndex<data::Time>(0));

    //set single pulse candidate dispersion measure
    typename Config::Dm dm(50.0 * pss::astrotypes::units::parsecs_per_cube_cm);
    //set the single pulse candidate width to 5.0 ms
    typename Config::MsecTimeType width(0.1 * boost::units::si::seconds);
    //set the single pulse start time to 2.0 seconds
    data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 10.0;

    for (std::size_t idx=0; idx<10; ++idx)
    {
        for (std::size_t ind=0; ind < 50; ++ind)
        {
            data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, tstart, width, sigma, idx);
            cand_list.push_back(candidate);
            tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType((0.002+0.0002*ind)*boost::units::si::seconds);
//            width += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType(0.002*boost::units::si::seconds);
            dm +=  (1.0+ind*0.01) * pss::astrotypes::units::parsecs_per_cube_cm;
        }
        tstart += data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType(0.5*boost::units::si::seconds);
    }
    Config config;
    config.minimum_cluster_size(10);
    Hdbscan clusterer(config);
    auto groups = clusterer(cand_list);
    ASSERT_EQ(groups.size(),10U);
    ASSERT_GE(groups[0].size(),10U);
    ASSERT_LE(groups[0].size(),50U);
}



} // namespace test
} // fof
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
