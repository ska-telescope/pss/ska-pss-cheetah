/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef WIDTH_CLUSTERING_H
#define WIDTH_CLUSTERING_H
#include "cheetah/modules/sps_clustering/hdbscan/MakeCartesianClusters.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cfloat>
#include <bits/stdc++.h>
#include <algorithm>

namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {

class WidthClustering {
    public:
        WidthClustering(std::vector<float> X_locations, std::vector<float> Y_locations, std::vector<float> SNR, std::vector<float> Widths, std::vector<MakeCartesianClusters::cluster> input_clusters);

        void do_clustering();

        std::vector<std::vector<size_t>> final_clusters();

    private:
        std::vector<float> _widths, _x, _y, _snr;
        std::vector<MakeCartesianClusters::cluster> _small_clusters;
        std::vector<std::vector<size_t>> _final_clusters;
        std::vector<size_t> _representative_elements;

        void get_representative();

        void connect_recursive(size_t idx);

        float distance(size_t idx1, size_t idx2);
        std::vector<size_t> _connections;
        std::vector<size_t> _status;

        void connect();
};

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
#endif
