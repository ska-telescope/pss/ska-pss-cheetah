/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef SPANNING_TREE_CARTESIAN_H
#define SPANNING_TREE_CARTESIAN_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <math.h>
#include <cfloat>
namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {

class SpanningTreeCartesian {
    public:
        SpanningTreeCartesian(std::vector<float> X, std::vector<float> Y);

        void construct_tree();

        std::vector<int> parent();

        std::vector<float> weights();

    private:
        std::vector<float> _x_locations, _y_locations;
        std::vector<float> _weights;
        std::vector<int> _parent;
        float _xdist, _ydist, _distance;
        size_t _size;

        void Cartesian(size_t idx1, size_t idx2);

};

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
#endif
