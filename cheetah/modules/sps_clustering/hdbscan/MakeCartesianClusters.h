/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef MAKE_CARTESIAN_CLUSTERS_H
#define MAKE_CARTESIAN_CLUSTERS_H

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cfloat>
#include <bits/stdc++.h>
#include <algorithm>
#include <chrono>
namespace ska {
namespace cheetah {
namespace modules {
namespace sps_clustering {
namespace hdbscan {

class MakeCartesianClusters
{
    public:
        struct cluster
        {
            std::vector<size_t> elements;
            std::vector<int> child;
            int parent;
            int label;
            float stability;
            size_t birth_stage;
            size_t death_stage;
        };

// do_clustering() executes all the functions sequentially
        MakeCartesianClusters(std::vector<int> Parent, std::vector<float> Weights, size_t data_size, size_t Core);

// make_clusters() sorts the weights and calls the relevant functions with decreasing order of threshold
        void make_clusters();

	std::vector<cluster> good_clusters();
// 2D Vectors to store the dendogram of clustering, tree_labels to store the labels of clusters, tree_connections to store the labels of previous stage,
// tree sizes to store the size of the clusters

    private:
	std::vector<std::vector<int>> _full_labels;
        std::vector<cluster> _good_clusters;
        std::vector<std::vector<cluster>> _clusters_stable;
        std::vector<float> _weights;
        std::vector<int> _parent;
        std::vector<int> _labels;
// make_clusters() sorts the weights and calls the relevant functions with decreasing order of threshold
// break_clusters() breaks the Minimum spanning tree into clusters and relabels them according to the splits.
// This also stores the connections of new clusters to older set of cluster in previous step

	void break_clusters(float threshold);

        void extract_clusters(size_t stage, size_t index);

        void get_stable_clusters();

        void traceback_stability();

        size_t _core;
        size_t i, j, k, _size;
        std::vector<float> _braking_threshold;
        std::vector<size_t> _branch, _labeling_status, _indices;
        std::vector<int> _uniq_labels, _sorted_labels, _small_labels;
        float var;
};

} // namespace hdbscan
} // namespace sps_clustering
} // namespace modules
} // namespace cheetah
} // namespace ska
#endif
