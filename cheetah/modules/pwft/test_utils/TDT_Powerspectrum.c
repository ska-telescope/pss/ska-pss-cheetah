#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>

/*cmodel for the TDT PWFT block.
This function makes use of FFTW.
Note that because I included complex.h, the output data time from the
FFTW plan/execute is a C complex time, not a double[2].
I also used the float versions of the functions,
which must be linked with -lfftw3f (not -lfftw3)
e.g.:
gcc TDT_Powerspectrum.c -o powerspectrum -lfftw3f -lm
*/

void PowerSpectrum(float *tsIn, float *tsOut, int length)
{
    int i;
    float bprev, bnext, b0;
    fftwf_plan p;
    fftwf_complex *dout;
    float *tstmp;
    tstmp = (float *) malloc(sizeof(float)*length);
    dout = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*(length/2+1));

    printf("Making fftw plan.\n");
    p=fftwf_plan_dft_r2c_1d(length, tstmp, dout, FFTW_MEASURE);

    printf("Initialize input array.\n");
    for(i=0; i<length; i++)
    {
        tstmp[i]=tsIn[i];
    }

    printf("Executing fftw plan.\n");
    fftwf_execute(p);

    printf("Calculating the absolute value.\n");
    for(i=0; i<length/2+1; i++) {
        tstmp[i] = cabsf(dout[i]);   //NOTE: cabsf defined in complex.h
    }

    printf("Interpoloating spectrum\n");
    for(i=0; i<length/2+1; i++)
    {
        if(i==0) {
            bnext=0.5*powf(fabsf(tstmp[i]+tstmp[i+1]), 2);
            tsOut[i]=fmaxf(tstmp[i]*tstmp[i], bnext);
        }
        else if(i==length/2) {
            bprev=0.5*powf(fabsf(tstmp[i]+tstmp[i-1]), 2);
            tsOut[i]=fmaxf(tstmp[i]*tstmp[i], bprev);
        }
        else {
	    bprev=0.5*powf(fabsf(tstmp[i]+tstmp[i-1]), 2);
            bnext=0.5*powf(fabsf(tstmp[i]+tstmp[i+1]), 2);
            b0=fmaxf(bprev, bnext);
            tsOut[i]=fmax(b0, tstmp[i]*tstmp[i]);
        }
    }

    fftwf_destroy_plan(p);
    fftwf_free(dout);
    free(tstmp);
}

void delta_train(float *tsDelta, int length, int period)
{
    int i;
    for(i=0; i<length; i++)
    {
        if(i%period==0) {
            tsDelta[i]=1.0;
        } else {
            tsDelta[i]=0.0;
        }
    }
}

int main(int argc, char **argv)
{
    int length=32768;
    int period=256;    //In samples

    float *tsIn = (float *) malloc(length*sizeof(float));
    float *tsOut = (float *) malloc(length*sizeof(float));
    FILE *fout;

    printf("Making train of delta functions.\n");
    delta_train(tsIn, length, period);
    fout=fopen("input_vector.dat", "w");
    fwrite(tsIn, sizeof(float), length, fout);
    fclose(fout);

    printf("Calculating power spectrum.\n");
    PowerSpectrum(tsIn, tsOut, length);

    printf("Writing data to disk.\n");

    fout=fopen("output_vector.dat", "w");
    //fwrite(tsOut, sizeof(float), length/2+1, fout);

    int i;
    for(i=0; i< length/2+1; ++i)
        fprintf(fout, "%f\n", tsOut[i]);

    fclose(fout);

    free(tsIn);
    free(tsOut);

}