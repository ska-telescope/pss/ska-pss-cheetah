/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_PWFT_TEST_SRC_PWFT-TDT_REFERENCE_INTERFACE_H_
#define SKA_CHEETAH_MODULES_PWFT_TEST_SRC_PWFT-TDT_REFERENCE_INTERFACE_H_

#include "cheetah/modules/tdrt/TimeSeries.h"
#include "cheetah/modules/tdrt/FrequencySeries.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace pwft {
namespace test {

class PwftTDTReference;  // forward declaration to implementation

 /**
 * @brief
 * Power Spectrum Fourier Transform Reference Model
 * I N T E R F A C E    (using pimpl idiom)
 * @details
 * avoid having to include fftw3.h in the cuda unittest.
 * We hide the implementation (with private members like e.g.
 * fftwf_plan _plan). NVCC makes serious problems otherwise.
 * E.g. the "__float128" is undefined error. It is better
 * practice to decouple non cuda modules and compile them
 * with the host compiler directly
 */
class PwftTDTReferenceInterface
{
    typedef class PwftTDTReference Implementation;

    public:
        PwftTDTReferenceInterface(size_t length);

        void process(tdrt::TimeSeries<float>& h_input,
                     tdrt::FrequencySeries<float>& h_output);

        // Test data generator

        void sine_signal(float period,
                         tdrt::TimeSeries<float>& h_output);

        // Checker function for the Pwft. It calculates the cross correlation
        // of the two output vectors normalized by the auto correlation of Vector A.
        // If the vectors are identical, it returns 1.0. If they're completely
        // different it returns 0.0. This test is highly sensitive to differences
        // in bin but given that FFTs are library functions, I think a stringent
        // test is good.

        float pwft_correlation(modules::tdrt::FrequencySeries<float>& h_fsA, modules::tdrt::FrequencySeries<float>& h_fsB);

    private:
        std::shared_ptr<Implementation> _pimpl;
};

} // namespace test
} // namespace pwft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif /* SKA_CHEETAH_MODULES_PWFT_TEST_SRC_PWFT-TDT_REFERENCE_INTERFACE_H_ */