/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "Pwft-TDT-Reference.h"
#include <iostream>
// uncomment to disable assert()
// #define NDEBUG
#include <cassert>
#include <cstring>

namespace ska {
namespace cheetah {
namespace modules {
namespace pwft {
namespace test {

PwftTDTReference::PwftTDTReference(size_t length)
    : _length(length)
{
    _fft_in  = (float*) malloc(sizeof(float) * _length);
    _fft_out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * (_length/2+1));
    _plan = fftwf_plan_dft_r2c_1d(_length, _fft_in, _fft_out, FFTW_MEASURE);
}

PwftTDTReference::~PwftTDTReference()
{
    fftwf_destroy_plan(_plan);
    fftwf_free(_fft_in);
    free(_fft_out);
}

void PwftTDTReference::process(modules::tdrt::TimeSeries<float>& h_input,
                               modules::tdrt::FrequencySeries<float>& h_output)
{
    assert(h_input.get_length() == _length);
    assert(h_output.get_length() == _length);
    assert( 1.0 / h_input.get_delta_t() == h_output.get_delta_f());

    std::memcpy(_fft_in, h_input.get_data(), sizeof(float) * _length);

    fftwf_execute(_plan);

    float* tmp_fs= _fft_in;
    for(size_t i=0; i < _length/2 + 1; ++i)
    {
        __complex__ float tmp;
        __real__ tmp= _fft_out[i][0];
        __imag__ tmp= _fft_out[i][1];
        tmp_fs[i] = cabsf(tmp);
    }

    float* tsOut = h_output.get_data();
    float bprev, bnext, b0;

    for(size_t i=0; i < _length/2 + 1; ++i)
    {
        if(i==0)
        {
            bnext=0.5*powf(fabsf(tmp_fs[i]+tmp_fs[i+1]), 2);
            tsOut[i]=fmaxf(tmp_fs[i]*tmp_fs[i], bnext);
        }
        else if(i==_length/2)
        {
            bprev=0.5*powf(fabsf(tmp_fs[i]+tmp_fs[i-1]), 2);
            tsOut[i]=fmaxf(tmp_fs[i]*tmp_fs[i], bprev);
        }
        else
        {
            bprev=0.5*powf(fabsf(tmp_fs[i]+tmp_fs[i-1]), 2);
            bnext=0.5*powf(fabsf(tmp_fs[i]+tmp_fs[i+1]), 2);
            b0=fmaxf(bprev, bnext);
            tsOut[i]=fmax(b0, tmp_fs[i]*tmp_fs[i]);
        }
    }
}

void PwftTDTReference::sine_signal(float period, modules::tdrt::TimeSeries<float>& h_output)
{
    const float tsamp = h_output.get_delta_t();
    const float pi =    3.14159265;
    float* tsout =      h_output.get_data();

    for(size_t i= 0; i < h_output.get_length(); i++)
    {
        tsout[i]= sin( 2.0*pi*i * tsamp / period  );
    }
}

float PwftTDTReference::pwft_correlation(modules::tdrt::FrequencySeries<float>& h_fsA, tdrt::FrequencySeries<float>& h_fsB)
{
    assert(h_fsA.get_length() == h_fsB.get_length());
    assert(h_fsA.get_delta_f() == h_fsB.get_delta_f());

    size_t length = h_fsA.get_length();
    float* psA = h_fsA.get_data();
    float* psB = h_fsB.get_data();

    double correlation=0.0;
    double sumA=0.0;

    for(size_t i=0; i<length; ++i)
    {
        correlation+=psA[i]*psB[i];
        sumA+=(psA[i]*psA[i]);
    }
    //std::cout << "Correlation: " << correlation << std::endl;
    //std::cout << "SumA: " << sumA << std::endl;
    //std::cout << "Normalized correlation: " << correlation/sumA << std::endl;

    if(sumA != 0.0)
    {
        correlation/=sumA;
    }
    else
    {
        correlation=0.0;
    }
    return correlation;
}

} // namespace test
} // namespace pwft
} // namespace modules
} // namespace cheetah
} // namespace ska