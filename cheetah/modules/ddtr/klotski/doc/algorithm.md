@page klotski_algorithm_guide Klotski algorithm guide

***Documentation for module***

**Author:** *Arun Naidu*


# Introduction
The Klotski_Bruteforce algorithm is our first attempt to use CPUs for DDTR. The performance of the
algorithm is encouraging and to further improve the performance we have developed Klotski-tree algorithm
which uses tree-dedipsersion technique. Similar to klotski_bruteforce, klotski is a CPU based DDTR
algorithm where all the time-critical functions (hotspots) are written in assembly. This document
will give an brief overview of the algorithm and developer notes.


![Klotski](Klotski_ilustration.png)

# Alogrithm concept and implementation details

The tree dedispersion algorithm (Taylor 1974) reduces computational load by regularizing the quadratic
delay pattern into a linear function, easing computation across dispersion measures. This simplification
minimizes redundancy and optimizes memory access. However, the linear approximation can reduce signal
alignment accuracy, and it restricts computed dispersion measures to specific values. Additionally,
it requires number of frequency channels to be in powers of two, necessitating blank channel padding
for non-standard configurations. Techniques such as piecewise linear segmentation \citep{piecewise}
and frequency padding \citep{padding} can offset these limitations to improve adaptability.

The concept of the Klotski-tree algorithm aligns with piecewise dedispersion by dividing FT data into small
tiny subbands and calculating only unique dispersion trails per subband, thereby reducing computation while
accounting for quadratic smearing.

Unlike in the Klotski_bruteforce, where FT (Frequency-Time) data is divided into Nb bands and each band is assigned
a klotski, in Klotski-tree FT data from each band are further divided into tiny subbands and passed onto klotskis.
Hence, The number of channels per klotski is fewer than a typical klotski of Klotski-bruteforce algorithm. Klotski's 
with fewer channels will minimize number of unique DMs. The resulting DT plane data from each klotski is shifted and 
added to get the final DDTR output as shown in the Figure.

## Implementation details

Similar to Klotski_bruteforce the core engine of Klotski is written in assembly, while the higher-level routines
are implemented in C++. Shifts required for dedispersion, along with memory addresses of variables,
are pre-calculated to optimize performance, and synchronization in multi-threaded contexts is managed in C++.
All the Nb bands are processed synchronosly and klotskis with in the band are processed in series.

### Determine the maximum number of channels per Klotski
Idealy to get close to O(NlogN) complexity the number of channels klotski should be 2. However, this will increase number of
write to RAM which will impact the performace. So, the selection of maximum number of channels per klotski is a critical
to the performance of the algorithm. In my test I have established for Sharabha (AMD EPYC 9445 machine in Oxford) 
the number is 30 channels per klotski.   

Please note: You can provide any value as the max K_chans in the config the algorithm will work.

### Determine the size of the L1 cache

The user should also provide the size of the L1 cache in the config file by default; it is set to 1 MB.

### simple workflow of the algorithm.

buffering -> cornerturn -> threaded dedispersion -> integrate -> downsample

![benchmarks_mid](benchmarks_mid.png)
## Performance
The expected performance of Klotski for various SKA-MID scenarios. All the measurements are done on a single
processing thread of AMD EPYC 9454 processor. In addition to Klotski’s performance (red), the plot also has performances of
the scalar version of CPU (blue), the same algorithm using the STL (standard c++ library functions) libraries (orange) and the
brute force version of Klotski (green). The black dashed line represents the maximum scalar performance of a single thread of
AMD CPU. It is clear from the plot that Klotski (Klotski-tree) needs only four threads to run in real-time for SKA-Mid senarios.
