/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRWORKER_H
#define SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRWORKER_H
#ifdef SKA_CHEETAH_ENABLE_CUDA

#include "cheetah/modules/ddtr/astroaccelerate/Config.h"
#include "cheetah/modules/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/modules/ddtr/astroaccelerate/DedispersionPlan.h"
#include "cheetah/modules/ddtr/DedispersionConfig.h"
#include "cheetah/utils/Architectures.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {

/**
 * @brief Manages the running of the dedispersion kernel on a specific GPU device
 */

template<typename DdtrTraits>
class DdtrWorker
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef typename DdtrTraits::NumericalRep NumericalRep;

    public:
        DdtrWorker(Config const&);

        /**
         * @brief call the dedispersion algorithm using the provided device
         */
        template<typename BufferType, typename CallBackT>
        std::shared_ptr<DmTrialsType> operator()(panda::PoolResource<cheetah::Cuda>&
                                               , BufferType const&
                                               , std::shared_ptr<DedispersionPlan<DdtrTraits>>
                                               , CallBackT const&);

    private:
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        Config const& _config;
        size_t _current_time_slice;
        size_t _current_dm_range;
};


} // namespace astroaccelerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "DdtrWorker.cpp"

#endif // SKA_CHEETAH_ENABLE_CUDA
#endif // SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRWORKER_H
