/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021  The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DedispersionStrategy.cuh"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/cuda/Series.h"
#include "cheetah/data/TimeSeries.h"
#include <cmath>

namespace astroaccelerate {

    extern
    void set_device_constants_dedispersion_kernel(const int &nchans, const int &length, const int &t_processed, const float *const dmshifts);

    extern
    void set_device_constants_dedispersion_kernel(const long int &length, const int &t_processed);

} // namespace astroaccelerate

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {

//__device__ __constant__ int i_nsamp, i_nchans, i_t_processed_s;
//__device__ __constant__ float dm_shifts[8192];

template<typename NumericalRep, typename OptimizationParameterT>
DedispersionStrategy<Cuda, NumericalRep , OptimizationParameterT>::DedispersionStrategy(DedispersionStrategy<Cpu, NumericalRep, OptimizationParameterT> const& st)
    : _d_out(new data::TimeSeries<cheetah::Cuda, float>((st._t_processed[0][0] + st.maxshift()) * (size_t)std::max(st.max_ndms(), st.nchans())))
{
    ::astroaccelerate::set_device_constants_dedispersion_kernel(st._nchans, 0, 0, &st._dmshifts[0]);
}

template<typename NumericalRep, typename OptimizationParameterT>
void DedispersionStrategy<Cuda, NumericalRep , OptimizationParameterT>::set_iteration(int t_processed, int maxshift)
{
    const int length = ( t_processed + maxshift );
    ::astroaccelerate::set_device_constants_dedispersion_kernel(length, t_processed);
}

template<typename NumericalRep, typename OptimizationParameterT>
data::TimeSeries<cheetah::Cuda, float>& DedispersionStrategy<Cuda, NumericalRep , OptimizationParameterT>::ddtr_output()
{
    return *_d_out;
}

} // namespace astroacclerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
