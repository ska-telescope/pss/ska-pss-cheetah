/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/ddtr/astroaccelerate/DedispersionStrategy.h"

namespace astroaccelerate {

extern
void call_kernel_cache_dedisperse_kernel(const dim3 &block_size, const dim3 &grid_size, const int &bin, unsigned short *const d_input, float *const d_output, const float &mstartdm, const float &mdmstep);

} // namespace astroaccelerate


namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {


template<typename NumericalT, typename OptimisationParamsT>
void Kernel<NumericalT, OptimisationParamsT>::exec(int dm_range
                                                , DedispersionStrategy<Cpu, NumericalT, OptimisationParamsT> const& strategy
                                                , data::FrequencyTime<Cuda, uint16_t> const& d_input
                                                , float* d_output
                                                , int t_processed
                                                , float tsamp
                                                )
{

    // failsafe (unoptimised) kernel
    float startdm = strategy.dm_low()[dm_range];

    static const int divisions_in_t = OptimisationParamsT::SDIVINT;
    static const int divisions_in_dm = OptimisationParamsT::SDIVINDM;
    static const dim3 threads_per_block(divisions_in_t, divisions_in_dm);

    int num_blocks_t = t_processed / divisions_in_t;
    int num_blocks_dm = strategy.ndms()[dm_range] / divisions_in_dm;

    dim3 num_blocks(num_blocks_t, num_blocks_dm);

    ::astroaccelerate::call_kernel_cache_dedisperse_kernel(num_blocks
                                      , threads_per_block
                                      , strategy.in_bin()[dm_range]
                                      , const_cast<uint16_t*>(&*d_input.begin())
                                      //, const_cast<uint16_t*>(thrust::raw_pointer_cast(&*d_input.cbegin()))
                                      , d_output
                                      , (float) ( startdm / tsamp )
                                      , (float) ( strategy.dm_step()[dm_range] / tsamp )
                                      );

}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
