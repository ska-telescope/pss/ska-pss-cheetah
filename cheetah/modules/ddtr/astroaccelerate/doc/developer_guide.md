@page astro-accelerate_developer_guide Astro-accelerate developerguide
# Notes for Developers
## GPU kernels

Astro-accelerate module in CHEETAH uses the shared_dedisperse_kernel_16() in astro-accelerate/src/device_dedispersion_kernel.cu. The input to the kernel is a 16-bit FT data.

## Constants Used
All the constants used for this module are collected the DedispersionOptimizationParameters class, which can be found in the file DedispersionStrategy.h. It is critical that all the constants used should have the same values as in astro-accelerate/include/params.hpp. We have used the exact values present the Meertrap version (astro-accelerate dependency used to compile CHEETAH). Please check if all the constants are same in both repos.

## Instantiating Kernel and DdtrStrategy objects
Various CUDA template codes need to be explicitly instantiated with the value_type of the incoming data in order to be included in the cheetah library.
This allows them to be used in c++ code.

Instead of having a single place to define these types the developer needs to ensure
the following types are updated and consistent with the required set of value_types:

- Kernel objects are instantiated in src/Kernel.cu
- DdtrStrategy objects are instantiated in src/DedispersionStrategy.cu
- Shared kernel objects in src/SharedKernel.cu
- Unit tests are set in
    - test/src/DdtrTest.cpp
    - test/src/DedispersionStrategyTest