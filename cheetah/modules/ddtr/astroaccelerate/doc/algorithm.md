@page astroaccelerate_algorithm_guide Astroaccelerate algorithm guide
# Astro-accelerate Algorithm
Astro-accelerate is a brute-force de-dispersion transform algorithm that utilizes NVIDIA GPUs. This module uses kernels from third-party astro-accelerate libraries (https://gitlab.com/MeerTRAP/astro-accelerate/)(paper: https://iopscience.iop.org/article/10.3847/1538-4365/ab7994). The algorithm is straightforward: it works by adding bins along a predicted DM curve (cure representing predicted delays vs frequency). To improve speed, the de-dispersion transform (DDTR) kernel uses shared memory as an intermediate storage.

There are three main steps that are followed after the buffering stage (where the time-frequency chunks are aggregated to form a single buffer that can be transferred to the GPU):

- Cornerturn (GPU and CPU use the CHEETAH's FrequencyTime object's constructor)
- Binning (GPU kernel)
- DDTR (GPU kernel)

The Cornerturn step rearranges the data in the buffer so that it is ordered by frequency and time, rather than by time and frequency. This improves the performance of the Binning and DDTR steps.

The Binning step simply adds the adjecent samples to increase the sampling time by factor of 2. Astro-accelerate reduces the time reslution by factor of 2 after processing every dedispersion range.

The DDTR kernel performs dedispersion transform on the each DM range in steps specified in the config file (well this is not always true, astro-accelerate has a tendency of makeing changes to user defined search parameters to improve the performance and contraints of the kernel)

Overall, the Astro-accelerate algorithm is a fast and efficient way to perform de-dispersion on large amounts of data using the power of NVIDIA GPUs. The use of shared memory and GPU kernels allows the algorithm to run in parallel, which greatly improves its performance.

# Pitfalls of using Astro-accelerate

1. The Astro-accelerate library (https://gitlab.com/MeerTRAP/astro-accelerate/) is not well-documented and the documentation that is available is not very comprehensive. This can make it difficult for new users to understand how to use the library and can lead to a steep learning curve.

2. The specific Astro-accelerate library in use is the Meertrap version (https://gitlab.com/MeerTRAP/astro-accelerate/), which means that users do not have access to the source code and cannot make modifications to the library as needed. This can be a major limitation for users who require custom functionality.

3. Even though this is a very efficient code it only works for cases when the channels are in powers of 2.

4. The constants used in the DedispersionOptimizationParameters class in DedispersionStrategy.h must be same as in the params.hpp of asroaccelerate library. This makes the code very fragile and easy to break.

5. DedispersionStrategy object a member of the DedispersionPlan modifies the user define search parameters according to the GPU in use and the performance requirements.