include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_ddtr_astroaccelerate_src
    src/DdtrTest.cpp
    src/gtest_ddtr_astroaccelerate.cpp
    src/DedispersionStrategyTest.cpp
)

if(ENABLE_CUDA)
    if(ENABLE_ASTROACCELERATE)
        add_executable(gtest_ddtr_astroaccelerate ${gtest_ddtr_astroaccelerate_src})
        target_link_libraries(gtest_ddtr_astroaccelerate ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
        add_test(gtest_ddtr_astroaccelerate gtest_ddtr_astroaccelerate --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
    endif()
endif()
