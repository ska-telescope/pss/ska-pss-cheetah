/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRPROCESSOR_H
#define SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRPROCESSOR_H

#include "cheetah/modules/ddtr/astroaccelerate/detail/DedispersionStrategy.cuh"
#include "cheetah/modules/ddtr/astroaccelerate/DedispersionPlan.h"
#include "cheetah/data/cuda/FrequencyTime.h"
#include "cheetah/data/DmTrials.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {

/**
 * @brief
 * @details
 */

template<typename DdtrTraits>
class DdtrProcessor
{
        typedef typename DdtrTraits::value_type NumericalRep;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef DedispersionStrategy<Cpu, NumericalRep> DedispersionStrategyType;
        typedef DedispersionStrategy<cheetah::Cuda, typename DedispersionStrategyType::NumericalRep> GpuStrategyType;

    public:
        DdtrProcessor( DedispersionStrategyType const&
                     , data::FrequencyTime<cheetah::Cuda, uint16_t>&
                     , std::shared_ptr<DmTrialsType>
                     , bool copy_flag
                     , size_t& current_dm_range
                     , size_t& current_time_slice
                     );

        /**
         * @brief process the next step of the DedispersionPlan
         */
        DdtrProcessor& operator++();

        /**
         * @return true if there is no more processing to do
         */
        bool finished() const;

        /**
         * @return Gpu DedispersionStrategy object
         *
         */
        GpuStrategyType& gpu_strategy();

        /**
         * @return DedispersionStrategy object
         *
         */
        DedispersionStrategyType const& dedispersion_strategy() const;

        /**
         * @return current_dm_range
         *
         */
        std::size_t const& current_dm_range() const;

        /**
         * @return current_time_slice
         *
         */
        std::size_t const& current_time_slice() const;

        /**
         * @return processed_samples
         *
         */
        data::DimensionIndex<data::Time> const& processed_samples() const;

    private:
        void update_dm_range();

    private:
        GpuStrategyType _gpu_strategy;
        data::FrequencyTime<cheetah::Cuda, uint16_t>& _device_ft_data;
        DedispersionStrategyType const&  _strategy;
        std::shared_ptr<DmTrialsType> _dm_trials_ptr;
        bool _copy_dm_trials;
        std::vector<int> const* _t_processed_range;
        data::DimensionIndex<data::Time> _processed_samples;
        std::size_t& _current_dm_range;
        std::size_t& _current_time_slice;
        std::size_t _local_maxshift;
        float _local_tsamp;
        float *_temp;
};


} // namespace astroaccelerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "detail/DdtrProcessor.cpp"

#endif // SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DDTRPROCESSOR_H
