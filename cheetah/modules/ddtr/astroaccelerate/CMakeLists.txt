set(MODULE_ASTROACCELERATE_LIB_SRC_CPU
    src/Config.cpp
    #src/Ddtr.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

set(MODULE_ASTROACCELERATE_LIB_SRC_CUDA
    src/DedispersionStrategy.cu
    src/Kernel.cu
    src/SharedKernel.cu
    PARENT_SCOPE
)

add_subdirectory(test)
