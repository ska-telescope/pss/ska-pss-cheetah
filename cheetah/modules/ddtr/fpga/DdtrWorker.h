/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_FPGA_DDTRWORKER_H
#define SKA_CHEETAH_MODULES_DDTR_FPGA_DDTRWORKER_H

#include "cheetah/modules/ddtr/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#ifdef ENABLE_SKA_RABBIT
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/CommandQueue.h"
#endif // ENABLE_SKA_RABBIT
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace fpga {

/**
 * @brief Execute the ddtr kernel on a specific device
 * @details
 */

template<typename BufferType, typename DmTrialsType>
class DdtrWorker
{
        typedef typename BufferType::Rep NumericalRep;

    public:
        DdtrWorker(ddtr::Config const& config, panda::PoolResource<Fpga> const& device);
        DdtrWorker(DdtrWorker const&)=delete;
        ~DdtrWorker();

        std::shared_ptr<DmTrialsType> operator()(BufferType const& data, std::vector<double> dm_factors, std::size_t max_delay);

    private:
#ifdef ENABLE_SKA_RABBIT
        ddtr::Config const& _config;
        panda::PoolResource<panda::altera::OpenCl> const& _device;
        std::unique_ptr<panda::altera::Kernel> _kernel;
        std::unique_ptr<panda::altera::Kernel> _kernel_2;
        std::unique_ptr<panda::altera::CommandQueue> _command_queue;
        std::unique_ptr<panda::altera::CommandQueue> _command_queue_2;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
#endif // ENABLE_SKA_RABBIT
};


} // namespace fpga
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "detail/DdtrWorker.cpp"

#endif // SKA_CHEETAH_MODULES_DDTR_FPGA_DDTRWORKER_H
