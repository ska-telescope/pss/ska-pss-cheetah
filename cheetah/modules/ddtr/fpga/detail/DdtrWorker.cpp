/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/ddtr/fpga/DdtrWorker.h"
#include "cheetah/modules/ddtr/fpga/Ddtr.h"
#include "panda/Error.h"
#include "panda/Log.h"
#ifdef ENABLE_SKA_RABBIT
#include "panda/arch/altera/DevicePointer.h"
#include "panda/arch/altera/Aocx.h"
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/DeviceCopy.h"
#include "rabbit/utils/ImageManager.h"
#include "rabbit/ddtr/Ddtr.h"
#include "rabbit/ddtr/opencl/input_params.h"
#include "rabbit/ddtr/opencl/kernel_params.h"
#endif // ENABLE_SKA_RABBIT

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace fpga {


#ifdef ENABLE_SKA_RABBIT
template<typename BufferType, typename DmTrialsType>
DdtrWorker<BufferType, DmTrialsType>::DdtrWorker(ddtr::Config const& config, panda::PoolResource<Fpga> const& device)
    : _config(config)
    , _device(device)
{
    ska::rabbit::ddtr::Ddtr ddtr;
    std::string str=_device.device_name();
    str.resize(str.find(':')-1);
    std::string filename=ddtr.image_manager().image_path(str);
    PANDA_LOG <<"\nDevice: "<<_device.device_name();
    PANDA_LOG<<"\nCompiled binary: "<<filename<<"\n";
    ska::panda::altera::Aocx aocx(filename);
    ska::panda::altera::Program program(aocx,_device);
    _command_queue.reset(new panda::altera::CommandQueue(_device));
    _command_queue_2.reset(new panda::altera::CommandQueue(_device));
    _kernel.reset(new panda::altera::Kernel("dedisperse", program));
    _kernel_2.reset(new panda::altera::Kernel("dedisperse", program));

    PANDA_LOG << "DdtrWorker constructed";
}
#else  // ENABLE_SKA_RABBIT
template<typename BufferType, typename DmTrialsType>
DdtrWorker<BufferType, DmTrialsType>::DdtrWorker(ddtr::Config const&, panda::PoolResource<Fpga> const& )
{
}
#endif // ENABLE_SKA_RABBIT

template<typename BufferType, typename DmTrialsType>
DdtrWorker<BufferType, DmTrialsType>::~DdtrWorker()
{
}

#ifdef ENABLE_SKA_RABBIT
template<typename BufferType, typename DmTrialsType>
std::shared_ptr<DmTrialsType> DdtrWorker<BufferType, DmTrialsType>::operator()(BufferType const& data, std::vector<double> _dm_factors, std::size_t _max_delay)
{
    auto const& tf_obj = *(data.composition().front());
    std::size_t nchans = tf_obj.number_of_channels();
    std::size_t nsamples = data.data_size() / nchans;
    data::DimensionIndex<data::Time> offset_samples(data.offset_first_block()/(nchans * sizeof(NumericalRep)));
    auto const& start_time = tf_obj.start_time(offset_samples);

    //genrate _dm_trial_metadata based on samples available
    _dm_trial_metadata.reset(new data::DmTrialsMetadata(tf_obj.sample_interval(), nsamples - _max_delay));
    for (auto dm: this->_config.dm_trials())
    {
        _dm_trial_metadata->emplace_back(dm, 1);
    }

    //get DM details from config object
    std::size_t dm_start=this->_config.dm_trials()[0].value();
    std::size_t dm_step=this->_config.dm_trials()[1].value()-this->_config.dm_trials()[0].value();
    std::size_t dm_size=this->_config.dm_trials().size();

    //create host buffers
    panda::Buffer<float> dm_shift_Buffer_1(nchans);
    panda::Buffer<float> dm_shift_Buffer_2(nchans);
    size_t output_total = 3*nsamples * TDMS * sizeof(unsigned int);	//size more than two kernel outputs
    panda::Buffer<unsigned char> output_buffer(output_total);
    std::fill(output_buffer.begin(),output_buffer.end(),0); //Zeroed ouput buffers

    //create device pointers with device memory buffers, used to copy host to device
    panda::altera::DevicePointer<NumericalRep> buff_in(_device, nsamples*nchans*sizeof(unsigned char),*_command_queue);
    panda::copy(data.cbegin(), data.cend(), buff_in.begin());
    panda::altera::DevicePointer<NumericalRep> buff_1(_device, sizeof(float)*nchans,*_command_queue);
    panda::altera::DevicePointer<NumericalRep> buff_2(_device, sizeof(float)*nchans,*_command_queue);

    //queue used to copy data from device to host
    panda::altera::DevicePointer<NumericalRep> buff_out(_device, nsamples*TDMS*sizeof(unsigned int),*_command_queue_2);
    panda::altera::DevicePointer<NumericalRep> buff_out_2(_device, nsamples*TDMS*sizeof(unsigned int),*_command_queue_2);

    for(int k=0;k<2;++k)    //two kernels called one by one
    {
        int dsamp=1;float dm1=dm_start, dm2=(TDMS/2)*dm_step;
        for (int c = 0; c < nchans; ++c) {
            float shift = _dm_factors[c];//calculated based on freq information
            *(dm_shift_Buffer_1.begin()) = shift * ((float) dm_step * (float) (1/TSAMP) / (float) dsamp);
            *(dm_shift_Buffer_2.begin()) = shift * ((float) dm_step * (float) (1/TSAMP) / (float) dsamp);
        }

        //copy host buffer data to device huffers
        panda::copy(dm_shift_Buffer_1.begin(), dm_shift_Buffer_1.end(), buff_1.begin());
        panda::copy(dm_shift_Buffer_2.begin(), dm_shift_Buffer_2.end(), buff_2.begin());

        if (k==0){
            //single work-item/thread, two arrays of 500 DMs, (TDMS/2)
            (*_kernel)(*_command_queue,1, 1, (&*buff_1), (&*buff_2),(&*buff_in),(&*buff_out), dm1, dm2, dsamp);
            //copy device buffer data to host, starts with begin
            panda::copy(buff_out.begin(), buff_out.end(), output_buffer.begin());
        }
        else{
            //single work-item/thread, two arrays of 500 DMs, (TDMS/2)
            (*_kernel_2)(*_command_queue,1, 1, (&*buff_1), (&*buff_2),(&*buff_in),(&*buff_out_2), dm1, dm2, dsamp);
            //copy subsequent device buffer data to host, starts with end of previous copy operation
            panda::copy(buff_out_2.begin(), buff_out.end(), output_buffer.end());   //subsequent output data
        }
    }
    PANDA_LOG << "DM trials completed";

    //create DmTrialsType instance
    std::shared_ptr<DmTrialsType> dmtime  = DmTrialsType::make_shared(_dm_trial_metadata, start_time);
    DmTrialsType& dmtrials = *(dmtime);
    NumericalRep const* tf_data_fpga = static_cast<NumericalRep const*>(output_buffer.data());//fpga output

    //save host buffer data into dmtrials object
    int start_count=(int)dm_start, end_count=(int)dm_size, samp_idx=0;
    for(int i = 0; i < 2; ++i)     //two kernels called
    for (int dm_count = 0; dm_count < TDMS; ++dm_count) {
	    samp_idx=0;
		for(int j = 0; j < (nsamples - _max_delay)/NUM_REGS; ++j) {
			for (int r = 0; r < NUM_REGS; r++) {
			    std::size_t input_idx = i*nsamples*TDMS + (j * (TDMS * NUM_REGS)) + (dm_count*NUM_REGS) + r;
			    float acc=(float) tf_data_fpga[input_idx] / (float) nchans;
				if( ((dm_count+i*TDMS) >= start_count) && ((dm_count+i*TDMS) < end_count) )
				{
                    dmtrials[dm_count][samp_idx] = acc;
                    samp_idx=samp_idx+1;
				}
			}
		}
	}
    PANDA_LOG << "DM trials copied";
    return dmtime;
}
#else // ENABLE_SKA_RABBIT
template<typename BufferType, typename DmTrialsType>
std::shared_ptr<DmTrialsType> DdtrWorker<BufferType, DmTrialsType>::operator()(BufferType const&, std::vector<double>, std::size_t)
{
    PANDA_LOG_ERROR << "call to Ddtr::fpga module without compiling in the fpga modules";
    std::shared_ptr<DmTrialsType> dmtime;
    return dmtime;
}
#endif // ENABLE_SKA_RABBIT

} // namespace fpga
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
