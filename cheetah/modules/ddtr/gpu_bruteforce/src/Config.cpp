/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/ddtr/gpu_bruteforce/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {


Config::Config()
    : utils::Config("gpu_bruteforce")
    , _active(false)
    , _copy_dmtrials_to_host(true)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active)->default_value(_active), "use this algorithm for dedispersion")
    ("copy_dmtrials_to_host", boost::program_options::value<bool>(&_copy_dmtrials_to_host)->default_value(_copy_dmtrials_to_host), "copy the Dmtrials object to host (cpu)");
}

bool Config::active() const
{
    return _active;
}

void Config::active(bool state)
{
    _active = state;
}

bool Config::copy_dmtrials_to_host() const
{
    return _copy_dmtrials_to_host;
}

void Config::copy_dmtrials_to_host(bool state)
{
    _copy_dmtrials_to_host = state;
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
