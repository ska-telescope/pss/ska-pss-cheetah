/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_GPUCONSTRAINTS_H
#define SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_GPUCONSTRAINTS_H


namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {

/**
 * @brief Specifies algorithm constants based on GPU constraints
 * @details Based on an NVIDIA Gpu with Compute Capability of 3.0
 */
struct GpuConstraints
{
    public:
        /**
         * @brief max threads per block supported on 3.0 cards
         */
        static constexpr std::size_t threads_per_block {1024};

        /**
         * @brief max available constant memory
         * @details It is the maximum available constant memory on GPUs of
         * compute capablility 3.0.
         */
        static constexpr std::size_t max_constant_size {8192};
};


} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_GPUCONSTRAINTS_H
