/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {

template<typename NumericalT>
__global__ void batched_gpu_bin( NumericalT *d_input, NumericalT *d_temp, int block, int threads_per_block, int nelements)
{
    NumericalT temp=0;
    int t = threadIdx.x;
    int location = block*threads_per_block;
    if(location+t<nelements)
    {
        temp = (NumericalT)(((float)d_input[2*location+2*t] + (float)d_input[2*location+2*t+1])/2.0);
        d_temp[t] = temp;
        __syncthreads();
        d_input[location+t] = d_temp[t];
    }
    __syncthreads();
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska