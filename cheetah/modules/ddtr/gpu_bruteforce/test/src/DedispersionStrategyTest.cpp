/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/ddtr/gpu_bruteforce/test/DedispersionStrategyTest.h"
#include "cheetah/modules/ddtr/gpu_bruteforce/DedispersionStrategy.h"
#include "cheetah/modules/ddtr/DedispersionTrialPlan.h"
#include <pss/astrotypes/units/Units.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {
namespace test {

template<typename T>
DedispersionStrategyTest<T>::DedispersionStrategyTest()
    : ::testing::Test()
{
}

template<typename T>
DedispersionStrategyTest<T>::~DedispersionStrategyTest()
{
}

template<typename T>
void DedispersionStrategyTest<T>::SetUp()
{
}

template<typename T>
void DedispersionStrategyTest<T>::TearDown()
{
}

typedef ::testing::Types<unsigned char, unsigned short, float> NumericTypes;
TYPED_TEST_SUITE(DedispersionStrategyTest, NumericTypes);

/**
* @test test_DedispersionStrategy_constructor
* @given TF object on the host, plan and memory on gpu
* @when when passed to the DedispersionStrategy constructor
* @then then test simply checks if the objects is constructed as expected
*/
TYPED_TEST(DedispersionStrategyTest, test_DedispersionStrategy_constructor)
{
    typedef TypeParam NumericRep;

    data::DimensionSize<data::Time> number_of_spectra(128*1024U);
    data::DimensionSize<data::Frequency> number_of_channels(512U);

    data::TimeFrequency<Cpu,NumericRep> tf(number_of_spectra, number_of_channels);
    auto f1 =  data::FrequencyType(1000.0 * pss::astrotypes::units::megahertz);
    auto f2 =  data::FrequencyType(1500.0 * pss::astrotypes::units::megahertz);
    auto delta = (f2 - f1)/ (double)number_of_channels;
    tf.set_channel_frequencies_const_width( f1, delta );
    tf.sample_interval(0.001*data::second);
    ddtr::Config plan;
    plan.dedispersion_samples(1<<14);
    ASSERT_EQ(1<<14, plan.dedispersion_samples());

    ASSERT_DOUBLE_EQ(0.0, plan.max_dm().value());

    plan.add_dm_range(40 * data::parsecs_per_cube_cm, 100 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm);
    ASSERT_DOUBLE_EQ(80.0, plan.max_dm().value());

    plan.add_dm_range(0 * data::parsecs_per_cube_cm, 40 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm); // should not affect the max dm
    ASSERT_DOUBLE_EQ(80.0, plan.max_dm().value());

    DedispersionStrategy<Cpu, NumericRep> ds(tf, plan,128*1024*1024);
    ASSERT_EQ(ds.nchans(), 512);
    ASSERT_EQ(ds.nsamp(), plan.dedispersion_samples());
    ASSERT_EQ(ds.tsamp(), 0.001*data::seconds);
}


} // namespace test
} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
