/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/ddtr/detail/TFDataCopyIterator.h"
#include <algorithm>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {

template<typename TimeFrequencyType, typename RfiFlagDataType>
TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::TFDataCopyIterator( RfiFlagDataType& data, size_t offset)
    : _flag_it(data.rfi_flags().begin() + offset)
    , _tf_it(data.tf_data().begin() + offset)
    , _tf_end_it(data.tf_data().cend())
{
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::~TFDataCopyIterator()
{
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
bool TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::operator!=( TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType> const& iterator) const
{
    return this->_tf_it != iterator._tf_it;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
bool TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::operator==( TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType> const& iterator) const
{
    return this->_tf_it == iterator._tf_it;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>& TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::operator++()
{
    ++_tf_it;
   _value = *(_tf_it);

    return *this;
}

template<typename TimeFrequencyType, typename RfiFlagDataType>
typename TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::const_reference TFDataCopyIterator<TimeFrequencyType, RfiFlagDataType>::operator*() const
{
    return _value;
}

} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
