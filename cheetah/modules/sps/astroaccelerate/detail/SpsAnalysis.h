#ifndef SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_SPSANALYSIS_H
#define SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_SPSANALYSIS_H

#include "cheetah/modules/sps/astroaccelerate/detail/MsdEstimator.h"
#include "cheetah/modules/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/modules/ddtr/astroaccelerate/DdtrProcessor.h"
#include "cheetah/modules/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sps {
namespace astroaccelerate {

/**
 * @brief Single pulse Analysis includes MSD estimation and search
 *
 */
template <typename SpsTraits, typename OptimizationParameterT=SpsOptimizationParameters<typename SpsTraits::value_type>>
class SpsAnalysis
{
    private:
        typedef typename SpsTraits::value_type DataType;

    public:
        typedef data::TimeFrequency<Cpu, DataType> TimeFrequencyType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        SpsAnalysis(ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor
                    , sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy
                    , std::vector<int>& dm_list
                    , std::vector<float>& candidate_list
                    , float threshold
                    , size_t number_of_candidates
                    , size_t max_peak_size
                    );
        ~SpsAnalysis();

        /**
         * @brief Number of the candidates detected above the threshold
         */
        size_t number_of_candidates() const;

        /**
         * @brief Mean Standard Deviation search on the dmtrials data
         */
        int perform_msd_search(int dm_list_index);

        /**
         * @brief go through the SNR array and perform peak search
         */
        void peak_find_sps(float threshold, int dm_list_index, int max_peak_size);

        /**
         * @brief go through the SNR array and perform search threshold based search basic group in performed.
         */
        int threshold_based_sps(float threshold, int dm_list_index);

    private:
        ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& _ddtr_processor;
        sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& _sps_strategy;
        sps::astroaccelerate::MsdEstimator<SpsTraits> _msd_estimator; // object which estimates MSD when initialized
        std::vector<int>& _dm_list;
        size_t _number_of_samples;
        panda::nvidia::CudaDevicePointer<float> _d_peak_list; // array which finally hold the candidates
        panda::nvidia::CudaDevicePointer<float> _d_decimated; // decimated time series is stored here
        panda::nvidia::CudaDevicePointer<float> _d_boxcar_values; // boxcar values for each DM range
        panda::nvidia::CudaDevicePointer<float> _d_output_snr; // SNR witch it essentially have snr values for each sample
        panda::nvidia::CudaDevicePointer<ushort> _d_output_taps; // both input and output taps are stored temporally
        panda::nvidia::CudaDevicePointer<int> _gmem_pos; // size of int
        std::vector<float>& _candidate_list; // candidate list on host
        size_t _number_of_candidates;
};

} // namespace astroaccelerate
} // namespace sps
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/sps/astroaccelerate/detail/SpsAnalysis.cpp"
#endif // SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_ANALYSIS_H
