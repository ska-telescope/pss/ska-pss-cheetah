/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "SpsCuda.cuh"
#include "MsdEstimator.h"
#include "cheetah/modules/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/modules/sps/astroaccelerate/detail/SpsCuda.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sps {
namespace astroaccelerate {

template<typename SpsTraits, typename OptimizationParameterT>
SpsCuda<SpsTraits, OptimizationParameterT>::SpsCuda(sps::Config const& config)
    : BaseT(config.astroaccelerate_config(), config)
    , _number_of_candidates(0)
    , _previous_candidates(0)
{
    // Get the detection threshold
    _threshold = config.threshold();
    _samples = config.ddtr_config().dedispersion_samples();
}

template<typename SpsTraits, typename OptimizationParameterT>
SpsCuda<SpsTraits, OptimizationParameterT>::~SpsCuda()
{
}

template<typename SpsTraits, typename OptimizationParameterT>
void SpsCuda<SpsTraits, OptimizationParameterT>::create_dm_list(sps::astroaccelerate::SpsStrategy<Cpu, uint8_t> const& sps_strategy, int current_range)
{
    size_t number_of_samples = sps_strategy.current_time_samples();
    size_t ndms = sps_strategy.strategy().ndms()[current_range];

    size_t free_mem, total_mem;
    cudaMemGetInfo(&free_mem, &total_mem);
    // Logic behind the equation: taking into account 95% free memory as available we estimate the maximum samples
    // we can fit in the GPU.
    unsigned long int max_number_of_samples=(free_mem*0.95)/(5.5*sizeof(float) + 2*sizeof(ushort));
    size_t dms_per_iteration = max_number_of_samples/number_of_samples;

    int temp = (int) (dms_per_iteration/OptimizationParameterT::THR_WARPS_PER_BLOCK);
    dms_per_iteration = temp*OptimizationParameterT::THR_WARPS_PER_BLOCK;

    int number_of_iterations = ndms/dms_per_iteration;
    int remaining_ndms = ndms - number_of_iterations*dms_per_iteration;

    for (int it = 0; it < number_of_iterations; ++it) {
        _dm_list.push_back(dms_per_iteration);
    }

    if (remaining_ndms > 0) {
        _dm_list.push_back(remaining_ndms);
    }
}

template<typename SpsTraits, typename OptimizationParameterT>
void SpsCuda<SpsTraits, OptimizationParameterT>::process_block(modules::ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor, std::vector<float>& sps_cands)
{
    sps::astroaccelerate::SpsStrategy<Cpu, uint8_t> sps_strategy(ddtr_processor.dedispersion_strategy());
    // subtract 1 from both current_dm_range and current_time_slice as these are incremeted in the DdtrProcessor
    sps_strategy.generate_processing_details((int)ddtr_processor.current_dm_range(), 0);

    std::size_t max_peak_size = 0;

    for (unsigned t = 0; t < ddtr_processor.dedispersion_strategy().num_tchunks(); ++t)
    {
        for (unsigned dm_range = 0; dm_range < ddtr_processor.dedispersion_strategy().range(); ++dm_range)
        {
            max_peak_size += (size_t) ( ddtr_processor.dedispersion_strategy().ndms()[dm_range]*ddtr_processor.dedispersion_strategy().t_processed()[dm_range][t] / 2 );
        }
    }

    sps_cands.resize(max_peak_size * 4);
    _candidate_list.resize(max_peak_size * 4);
    std::fill( _candidate_list.begin(), _candidate_list.end(), 0);

    create_dm_list(sps_strategy, (int)ddtr_processor.current_dm_range());
    SpsAnalysis<SpsTraits, OptimizationParameterT> sps_analysis(ddtr_processor, sps_strategy, _dm_list, _candidate_list, _threshold, 0, max_peak_size);
    _number_of_candidates = sps_analysis.number_of_candidates();

    convert_candidates(sps_cands
                      , (int)(ddtr_processor.current_dm_range())
                      , sps_strategy
                      , ddtr_processor.dedispersion_strategy().tsamp().value());

    _dm_list.clear();

}

template<typename SpsTraits, typename OptimizationParameterT>
template<typename BufferType, typename SpHandler>
std::shared_ptr<data::DmTrials<Cpu,float>> SpsCuda<SpsTraits, OptimizationParameterT>::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , SpHandler& sp_handler
                    , modules::ddtr::astroaccelerate::Ddtr<SpsTraits>& ddtr
                    )
{

    PUSH_NVTX_RANGE("sps_astroaccelerate_SpsCuda_operator",0);
    PANDA_LOG << "astroaccelerate::SpsCuda::operator() invoked (on device "<< gpu.device_id() << ")";

    std::vector<float> sps_cands;
    std::shared_ptr<data::DmTrials<Cpu,float>> dm_trials = ddtr(gpu, agg_buf
              , [this, &sps_cands](modules::ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor)
                {
                    this->process_block(ddtr_processor, sps_cands);
                });

    PUSH_NVTX_RANGE("astroaccelerate_cands_to_cheetah_cands",2);

    auto sp_candidate_list = std::make_shared<data::SpCcl<uint8_t>>(agg_buf.composition()
                , data::DimensionIndex<data::Time>(agg_buf.offset_first_block()/agg_buf.composition()[0]->number_of_channels()));
    sp_candidate_list->reserve(sps_cands.size()/4);

    for (std::size_t idx=0; idx<sps_cands.size(); idx+=4)
    {
        if(sps_cands[idx] == 0 && sps_cands[idx + 1 ]==0 && sps_cands[idx+2] ==0) break;
        sp_candidate_list->emplace(
                          sps_cands[idx] * data::parsecs_per_cube_cm
                        , data::SpCcl<uint8_t>::SpCandidateType::MsecTimeType(sps_cands[idx+1] * boost::units::si::seconds) // tstart
                        , data::SpCcl<uint8_t>::SpCandidateType::MsecTimeType(sps_cands[idx+3] * boost::units::si::seconds) // width
                        , sps_cands[idx+2] // sigma
                        );
    }

    POP_NVTX_RANGE;

    sp_handler(sp_candidate_list);
    return dm_trials;
}

template<typename SpsTraits, typename OptimizationParameterT>
void SpsCuda<SpsTraits, OptimizationParameterT>::convert_candidates(std::vector<float> &output_candidates, int const &current_range, sps::astroaccelerate::SpsStrategy<Cpu, uint8_t> const& sps_strategy, float sampling_time)
{
    size_t new_candidates = _number_of_candidates - _previous_candidates;
    size_t cand_position = 0;

    // NOTE: All the values stored in candidate_list have any averaging taken into accout and are multiplied by correct factors
    // NOTE: All we're doing here is converting to physical units
    for (size_t icand = 0; icand < new_candidates; ++icand)
    {
        // DM in pc cm^-3
        cand_position = icand * 4;
        output_candidates[cand_position + 0] =  (float)_candidate_list[icand * 4 + 0] * (sps_strategy.strategy().dm_step())[current_range] + (sps_strategy.strategy().dm_low())[current_range];
        // Start time in seconds
        output_candidates[cand_position + 1] = (float)(_candidate_list[icand * 4 + 1] * (sps_strategy.strategy().in_bin())[current_range]) * sampling_time;
        // snr
        output_candidates[cand_position + 2] = (float)_candidate_list[icand * 4 + 2];
        // Width in seconds
        output_candidates[cand_position + 3] = (float)_candidate_list[icand * 4 + 3] * (sps_strategy.strategy().in_bin())[current_range] * sampling_time;
    }
}

} // namespace astroaccelerate
} // namespace sps
} // namespace modules
} // namespace cheetah
} // namespace ska
