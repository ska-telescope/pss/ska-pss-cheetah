#include "SpsCuda.cuh"
#include "cheetah/cuda_utils/cuda_errorhandling.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sps {
namespace astroaccelerate {

void call_kernel_THR_GPU_WARP(const dim3 &grid_size, const dim3 &block_size,
					float const* __restrict__ d_input, ushort *d_input_taps, float *d_output_list,
					int *gmem_pos, float threshold, int nTimesamples, int offset, int shift,
					int max_list_size, int DIT_value, float sampling_time,
					float in_bin, float start_time)
{
    ::astroaccelerate::THR_GPU_WARP<<<grid_size, block_size>>>(d_input, d_input_taps, d_output_list,
					gmem_pos, threshold, nTimesamples, offset, shift,
					max_list_size, DIT_value, sampling_time,
					in_bin, start_time);
}

void call_kernel_dilate_peak_find(const dim3 &grid_size, const dim3 &block_size
                                , const float *d_input, ushort* d_input_taps
                                , float *d_peak_list, const int width
                                , const int height, const int offset, const float threshold
                                , int max_peak_size, int *gmem_pos, int shift
                                , int DIT_value, float sampling_time, float in_bin, float start_time)
{
    ::astroaccelerate::dilate_peak_find<<<grid_size, block_size>>>(d_input, d_input_taps, d_peak_list, width, height, offset, threshold, max_peak_size, gmem_pos, shift, DIT_value, sampling_time, in_bin, start_time);
}

} // namespace astroaccelerate
} // namespace sps
} // namespace modules
} // namespace cheetah
} // namespace ska
