/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/data/SpCandidate.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "cheetah/modules/ddtr/DedispersionTrialPlan.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <cstdlib>

namespace ska {
namespace cheetah {
namespace modules {
namespace sps {
namespace emulator {

template <class SpsTraits>
Sps<SpsTraits>::Sps(sps::Config const& config)
    : _config(config)
    , _max_delay(0)
{
}

template<class SpsTraits>
Sps<SpsTraits>::Sps(Sps&& other)
    : _dm_trial_metadata(std::move(other._dm_trial_metadata))
    , _config(other._config)
    , _max_delay(other._max_delay)
    , _dm_factors(std::move(other._dm_factors))
{
}

template<class SpsTraits>
std::shared_ptr<typename Sps<SpsTraits>::DmTrialsType> Sps<SpsTraits>::operator()(panda::PoolResource<cheetah::Cpu>&, BufferType& data, SpHandler& sp_handler)
{
    std::shared_ptr<DmTrialsType> dm_trials_ptr;

    if(data.composition().empty())
    {
        return dm_trials_ptr;
    }

    auto const& tf_obj = *(data.composition().front());
    std::size_t nchans = tf_obj.number_of_channels();

    // DmTrials type
    std::size_t nsamples = data.data_size() / nchans;

    try
    {
        _dm_trial_metadata = _config.ddtr_config().generate_dmtrials_metadata(tf_obj.sample_interval(), nsamples, _max_delay);
    }
    catch (const std::exception& e)
    {
        PANDA_LOG_ERROR << "Exception caught: " << e.what();
        return dm_trials_ptr;
    }

    data::DimensionIndex<data::Time> offset_samples(data.offset_first_block()/(nchans * sizeof(NumericalRep)));
    auto const& start_time = tf_obj.start_time(offset_samples);
    dm_trials_ptr = DmTrialsType::make_shared(_dm_trial_metadata, start_time);

    // SpCcl list
    std::shared_ptr<data::SpCcl<NumericalRep>> sp_candidate_list = std::make_shared<data::SpCcl<NumericalRep>>(data.composition(), offset_samples);

    std::srand(time(NULL));
    std::size_t ncands = 0.5 + (_config.emulator_config().candidate_rate() * (nsamples * tf_obj.sample_interval())) / boost::units::si::seconds;

    sp_candidate_list->reserve(ncands);

    for (std::size_t idx=0; idx<ncands; ++idx)
    {
        DmTrialsType& dm_trials = *(dm_trials_ptr);
        std::size_t dmindex = std::rand() % (_config.ddtr_config().dm_trials().size() - 1);
        data::DmTrials<ska::panda::Cpu, float>::DmTrialType current_trial_dm = dm_trials[dmindex];
        data::DmTrialsMetadata::DmType this_dm = current_trial_dm.dm();

        float this_width = rand() % 9 + 1;

        std::size_t block_range = nsamples - offset_samples;
        std::size_t this_sample = rand() % (block_range - 1);

        float sigma = rand() % 90 + 10;

        typename data::SpCcl<NumericalRep>::SpCandidateType::Dm dm(this_dm);
        typename data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType width(this_width * boost::units::si::milli * boost::units::si::seconds);
        typename data::SpCcl<NumericalRep>::SpCandidateType::MsecTimeType start_ms(this_sample * tf_obj.sample_interval());

        typename data::SpCcl<NumericalRep>::SpCandidateType candidate(dm, start_ms, width, sigma, idx);
        sp_candidate_list->emplace_calculate_duration(std::move(candidate));
    }
    sp_handler(sp_candidate_list);

    return dm_trials_ptr;
}


template<class SpsTraits>
void Sps<SpsTraits>::plan(DedispersionPlan const&)
{
    //Do nothing. Method is required to satisfy interface.
}

} // namespace emulator
} // namespace sps
} // namespace modules
} // namespace cheetah
} // namespace ska
