/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/psbc/Psbc.h"
#include "panda/Log.h"
#include "panda/Error.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace psbc {


template<typename Handler>
Psbc<Handler>::Psbc(Config const& config, Handler& h)
    : _duration(0 * data::seconds)
    , _dump_time(config.dump_time())
    , _data(DmTimeType::make_shared())
    , _handler(h)
    , _mutex(std::unique_ptr<std::mutex>(new std::mutex))
    , _handler_called(false)
{
    _data->dump_time(_dump_time);
}

template<typename Handler>
Psbc<Handler>::~Psbc()
{
}

template<typename Handler>
void Psbc<Handler>::operator()(DataType const& data)
{
    PANDA_LOG_DEBUG << "Psbc::operator() invoked";
    std::unique_lock<std::mutex> lk(*_mutex);
    PANDA_LOG_DEBUG << "Psbc::operator() lock acquired";

    auto const& dmtrials = *data;

    if (!_data->blocks().empty())
    {
        auto const& previous_dmtrials = *(_data->blocks().back());
        if (!previous_dmtrials.is_compatible(dmtrials))
        {
            throw panda::Error("Incompatible data block passed to Psbc instance.");
        }
    }

    if (dmtrials.size() == 0)
    {
        PANDA_LOG_WARN << "Psbc was passed an empty dmtrials block.";
        return;
    }
    else
    {
        _duration += dmtrials.duration();
        _data->add(data);
        PANDA_LOG_DEBUG << "Psbc::operator() data added to buffer";
        PANDA_LOG_DEBUG << "Duration: " << _duration
                        << "  Dump time:" << _dump_time;
        if(_duration >= _dump_time) {
            PANDA_LOG_DEBUG << "Psbc::operator() invoking handler";
            if (!this->is_contiguous())
            {
                //throw panda::Error("Not all Psbc buffer blocks are contiguous.");
                PANDA_LOG_DEBUG << "Not contiguous";
            }
            //_handler(_data);
            _data->ready();
            _duration = 0 * data::seconds;
            _data = DmTimeType::make_shared();
        }
    }

    if(_handler_called==false)
    {
        _handler_called = true;
        _handler(_data);
    }

}

template<typename Handler>
bool Psbc<Handler>::is_contiguous() const
{
    auto const& blocks = _data->blocks();
    auto prev = blocks.cbegin();
    auto next = std::next(blocks.cbegin(), 1);
    for (std::size_t idx=0; idx<blocks.size()-1; ++idx)
    {
        if (!((*prev)->is_contiguous(**next)))
        {
            return false;
        }
        ++prev;
        ++next;
    }
    return true;
}

} // namespace psbc
} // namespace modules
} // namespace cheetah
} // namespace ska
