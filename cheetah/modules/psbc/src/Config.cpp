/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/psbc/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace psbc {


Config::Config()
    : cheetah::utils::Config("psbc")
    , _dump_time(600 * data::seconds)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("dump_time", boost::program_options::value<double>()
        ->default_value(0.0)
        ->notifier([this](double value){_dump_time = Seconds(value * data::seconds);})
        , "The accumulation interval of the buffer between calls to its handler.");
}


typename Config::Seconds const& Config::dump_time() const
{
    return _dump_time;
}

void Config::dump_time(Seconds dt)
{
    _dump_time = dt;
}

} // namespace psbc
} // namespace modules
} // namespace cheetah
} // namespace ska
