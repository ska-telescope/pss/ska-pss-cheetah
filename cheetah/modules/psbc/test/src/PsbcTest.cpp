#include "cheetah/data/test_utils/DmTrialsGeneratorUtil.h"
#include "cheetah/modules/psbc/test/PsbcTest.h"
#include "cheetah/modules/psbc/Psbc.h"
#include "cheetah/modules/psbc/Config.h"
#include "cheetah/utils/NullHandler.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/TimeSeries.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace psbc {
namespace test {

PsbcTest::PsbcTest()
    : ::testing::Test()
{
}

PsbcTest::~PsbcTest()
{
}

void PsbcTest::SetUp()
{
}

void PsbcTest::TearDown()
{
}


struct PsbcTestHandler
{
    typedef data::DmTime<ska::cheetah::data::DmTrials<ska::panda::Cpu, float>> DmTimeType;
    typedef data::TimeSeries<ska::panda::Cpu, float> TimeSeriesType;


    PsbcTestHandler()
    {

    }

    ~PsbcTestHandler()
    {
        _t1.join();
    }
    /**
     * @brief perform a Noop for any parameter set
     */
    static void threaded_handler(std::shared_ptr<DmTimeType> data)
    {
        data->wait();
        TimeSeriesType extracted_data(90000);
        auto dm_time_iterator = data->begin(1);
        for(unsigned int dm_index = 0; dm_index < 128; ++dm_index)
        {

            auto dm_trial_slice = *dm_time_iterator;
            auto dm_trial_slice_iterator = dm_trial_slice->cbegin();
            (*dm_trial_slice_iterator).copy_to(extracted_data);
            ASSERT_TRUE(std::all_of(extracted_data.cbegin(), extracted_data.cend(), [dm_index](float i) { return i ==dm_index; }));
            ++dm_time_iterator;
        }
    }

    inline void operator()(std::shared_ptr<DmTimeType>& data)
    {
        _t1 = std::thread(threaded_handler, data);
    }

    private:
        std::thread _t1;

};

TEST_F(PsbcTest, test_contiguity)
{
    typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
    typedef typename DmTrialsType::DmType Dm;
    typedef typename DmTrialsType::TimeType Seconds;

    Config config;
    utils::NullHandler handler;
    Psbc<decltype(handler)> buffer(config,handler);
    data::test::DmTrialsGeneratorUtil<DmTrialsType> trials_generator;
    config.dump_time(Seconds(10.0 * data::seconds));
    for (std::size_t block_idx=0; block_idx<10; ++block_idx)
    {
        auto trial = trials_generator.generate(Seconds(0.000064*data::seconds),10,3);
        buffer(trial);
    }
    ASSERT_TRUE(buffer.is_contiguous());
}

/**
* @brief check the data sanity. Fill the DmTime buffer with the simulated DmTrials
*        and check the sanity after timeseries is extracted.
*/
TEST_F(PsbcTest, test_data_sanity)
{
    typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
    typedef typename DmTrialsType::DmType Dm;
    typedef typename DmTrialsType::TimeType Seconds;

    Config config;
    config.dump_time(Seconds(100.0 * data::seconds));
    PsbcTestHandler handler;
    Psbc<decltype(handler)> buffer(config,handler);
    data::test::DmTrialsGeneratorUtil<DmTrialsType> trials_generator;

    for (std::size_t block_idx=0; block_idx<101; ++block_idx)
    {
        auto trial = trials_generator.generate(Seconds(0.001*data::seconds),10000,128);
        for(unsigned int dm_index = 0; dm_index < 128; ++dm_index)
        {
            auto current_trial = (*trial)[dm_index];
            std::fill(current_trial.begin(), current_trial.end(), dm_index);
        }
        buffer(trial);
    }
    ASSERT_TRUE(buffer.is_contiguous());
}

} // namespace test
} // namespace psbc
} // namespace modules
} // namespace cheetah
} // namespace ska
