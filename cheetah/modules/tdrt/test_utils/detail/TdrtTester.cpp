/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/tdrt/test_utils/TdrtTester.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
TdrtTesterTraits<ArchitectureTag,ArchitectureCapability>::TdrtTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
tdrt::Tdrt& TdrtTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}


template <typename DeviceType, typename DataType>
struct SamplingTimePropagationTest
{
    inline static void test(DeviceType& device, tdrt::Tdrt& api)
    {
        std::size_t size = 1000;
        DataType input(0.001 * data::seconds);
        input.resize(size);
        DataType output;
        api.process(device,input,output,10.0 * data::metres_per_second_squared);
        ASSERT_EQ(output.sampling_interval(),input.sampling_interval());
        ASSERT_EQ(output.size(),size);
    }
};

template <typename DeviceType, typename DataType>
struct ResamplingOffsetTest
{
    inline static void test(DeviceType& device, tdrt::Tdrt& api, double acceleration)
    {
        double const size = 1<<23;
        std::size_t const idx = size/2;
        double const tsamp = 0.000064;
        DataType input(tsamp * data::seconds);
        input.resize(size,0);
        input[idx] = 1;
        DataType output;
        api.process(device, input, output, acceleration * data::metres_per_second_squared);
        double const accel_fact = -1 * acceleration * tsamp / (2.0 * 299792458.0);
        std::size_t new_idx = idx+idx*accel_fact*(idx-size);
        bool pass = false;
        //We know there can be fairly extreme rounding issues for large data sets
        //so we check three samples either side for the pulse.
        for (std::size_t ii = new_idx-3; ii < new_idx+3; ++ii)
        {
            if (output[ii] == 1)
              pass = true;
        }
        ASSERT_TRUE(pass);
    }
};

template <typename TestTraits>
TdrtTester<TestTraits>::TdrtTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
TdrtTester<TestTraits>::~TdrtTester()
{
}

template<typename TestTraits>
void TdrtTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void TdrtTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(TdrtTester, test_sampling_time_propagation)
{
    TypeParam traits;
    SamplingTimePropagationTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,float>>::test(device,traits.api());
    SamplingTimePropagationTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,double>>::test(device,traits.api());
}

ALGORITHM_TYPED_TEST_P(TdrtTester, test_resampling_mapping)
{
    TypeParam traits;
    for (double acceleration=-1000.0; acceleration<=1000.0; acceleration+=300.0)
    {
        ResamplingOffsetTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,float>>::test(device,traits.api(), acceleration);
        ResamplingOffsetTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,double>>::test(device,traits.api(), acceleration);
    }
    ResamplingOffsetTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,float>>::test(device,traits.api(), 0.0);
    ResamplingOffsetTest<typename TypeParam::DeviceType, data::TimeSeries<typename TypeParam::Arch,double>>::test(device,traits.api(), 0.0);
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_SUITE_P(TdrtTester, test_sampling_time_propagation, test_resampling_mapping);

} // namespace test
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska
