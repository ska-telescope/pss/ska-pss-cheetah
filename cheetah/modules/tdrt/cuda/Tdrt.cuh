#ifndef SKA_CHEETAH_MODULES_TDRT_CUDA_TDRT_H
#define SKA_CHEETAH_MODULES_TDRT_CUDA_TDRT_H

#include "cheetah/modules/tdrt/cuda/Config.h"
#include "cheetah/modules/tdrt/Config.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {

/**
 * @brief
 *   CUDA/Thrust implementation of the Tdrt algorithm
 */

class Tdrt: public utils::AlgorithmBase<Config, tdrt::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        Tdrt(Config const& config, tdrt::Config const& algo_config);
        Tdrt(Tdrt const&) = delete;
        Tdrt(Tdrt&&) = default;
        ~Tdrt();

        /**
         * @brief      Resample a time series to a given acceleration
         *
         * @param      gpu           The gpu to be processed on
         * @param      input         The input time series
         * @param      output        The output time series
         * @param[in]  acceleration  The acceleration value to resample to
         *
         * @tparam     T             The value types of input and output
         * @tparam     Alloc         The allocator types of input and output
         */
        template <typename T, typename Alloc>
        void process(ResourceType& gpu,
            data::TimeSeries<Architecture,T,Alloc> const& input,
            data::TimeSeries<Architecture,T,Alloc>& output,
            data::AccelerationType acceleration);
};

} // namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/tdrt/cuda/detail/Tdrt.cu"

#endif //SKA_CHEETAH_MODULES_TDRT_CUDA_TDRT_H