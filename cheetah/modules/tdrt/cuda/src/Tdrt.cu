#include "cheetah/modules/tdrt/cuda/Tdrt.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {

Tdrt::Tdrt(Config const& config, tdrt::Config const& algo_config)
    : utils::AlgorithmBase<Config, tdrt::Config>(config,algo_config)
{
}

Tdrt::~Tdrt()
{
}

} // namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska
