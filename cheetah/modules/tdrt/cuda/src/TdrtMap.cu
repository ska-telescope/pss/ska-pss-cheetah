#include "cheetah/modules/tdrt/cuda/TdrtMap.cuh"
#include "cheetah/data/Units.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {

/**
 * @brief
 *   Functor for remapping timeseries using time domain resampling
 */

TdrtMap::TdrtMap(data::AccelerationType acceleration, std::size_t size, data::TimeType tsamp)
{
    this->accel_fact = (((acceleration * tsamp) / (2.0 * boost::units::si::constants::codata::c))).value();
    this->size = (double) size;
}

} // namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska