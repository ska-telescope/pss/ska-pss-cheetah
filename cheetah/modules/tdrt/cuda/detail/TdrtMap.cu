#include "cheetah/modules/tdrt/cuda/TdrtMap.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {

/**
 * @brief
 *   Functor for remapping timeseries using time domain resampling
 */

inline __host__ __device__
std::size_t TdrtMap::operator()(std::size_t idx) const
{
    return (std::size_t)(idx+idx*accel_fact*(idx-size));
}

} // namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska