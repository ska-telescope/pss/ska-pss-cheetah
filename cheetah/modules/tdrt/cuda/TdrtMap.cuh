#ifndef SKA_CHEETAH_MODULES_TDRT_CUDA_TDRTMAP_H
#define SKA_CHEETAH_MODULES_TDRT_CUDA_TDRTMAP_H

#include "cheetah/data/Units.h"
#include "cheetah/cuda_utils/cuda_thrust.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {

/**
 * @brief
 *   Functor for remapping accelerated time series
 *
 * @details
 *   For use with thrust::gather
 */


struct TdrtMap: public thrust::unary_function<std::size_t,std::size_t>
{
	double accel_fact;
	double size;

	/**
	 * @brief      Create new functor instance
	 *
	 * @param[in]  acceleration  The acceleration to resample to
	 * @param[in]  size          The size if the input time series
	 * @param[in]  tsamp         The sampling interval of the time series
	 */
	TdrtMap(data::AccelerationType acceleration, std::size_t size, data::TimeType tsamp);

	inline __host__ __device__
	std::size_t operator()(std::size_t idx) const;

};

} //namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/tdrt/cuda/detail/TdrtMap.cu"

#endif // SKA_CHEETAH_MODULES_TDRT_CUDA_TDRTMAP_H
