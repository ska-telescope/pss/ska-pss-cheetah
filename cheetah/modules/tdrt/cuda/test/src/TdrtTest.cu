#include "cheetah/modules/tdrt/cuda/Tdrt.cuh"
#include "cheetah/modules/tdrt/test_utils/TdrtTester.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace cuda {
namespace test {

struct CudaTraits : public tdrt::test::TdrtTesterTraits<tdrt::cuda::Tdrt::Architecture,tdrt::cuda::Tdrt::ArchitectureCapability>
{
    typedef tdrt::test::TdrtTesterTraits<tdrt::cuda::Tdrt::Architecture, typename tdrt::cuda::Tdrt::ArchitectureCapability> BaseT;
    typedef Tdrt::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Cuda, TdrtTester, CudaTraitsTypes);

} // namespace test
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska