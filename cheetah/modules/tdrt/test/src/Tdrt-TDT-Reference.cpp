/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "Tdrt-TDT-Reference.h"
#include <iostream>
#include <cmath>
// uncomment to disable assert()
// #define NDEBUG
#include <cassert>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdrt {
namespace test {

TdrtTDTReference::TdrtTDTReference()
{
}

void TdrtTDTReference::process(TimeSeries<float>& h_input,
                               TimeSeries<float>& h_output,
                               float acceleration)
{
    assert(h_input.get_length() == h_output.get_length());
    assert(h_input.get_delta_t() == h_output.get_delta_t());

    long long numTimeSamps= h_input.get_length();;
    double    sampTime=     h_input.get_delta_t();
    double    pulsarTime=   0.0;
    double    timeThru=     0.0;
    double    earthTime=    0.0;

    const double c = 299792458.0; // speed of light

    float obsLen = sampTime * numTimeSamps;
    float tau0 = sampTime/(1 + acceleration * obsLen/(2 * c));
    // float earlyWeight, lateWeight;

    long long nsamp;
    long long samp_out;

    float* input  = h_input.get_data();
    float* output = h_output.get_data();
    for (nsamp=0; nsamp < numTimeSamps; nsamp++)
    {
        // Time interval on Earth
        earthTime = nsamp * sampTime;

        timeThru = pulsarTime;

        // Relate the time interval at Earth to that of the pulsar
        pulsarTime = pulsarTime + tau0 * (1 + acceleration * earthTime/c);

        // samp_out = (int) floor(timeThru/sampTime);
        samp_out = (int) round(timeThru/sampTime);

        //   earlyWeight = (timeThru - samp_out * sampTime)/sampTime;
        ///lateWeight = 1 - lateWeight;

        if (samp_out >= 0 && samp_out < numTimeSamps)
        {
            // output[samp_out] += lateWeight * input[nsamp];
            output[samp_out] = input[nsamp];
        }
        /*
        if (samp_out >= -1 && samp_out < (numTimeSamps - 1))
        {
            output[samp_out+1] = earlyWeight * input[nsamp];
        }
        */
    }
}

void TdrtTDTReference::const_accel_pulsar(double acceleration, float period,
                                          TimeSeries<float>& h_output)
{
    const double c     = 299792458.0; // speed of light
    const double tsamp = h_output.get_delta_t();

    float f0= 1.0 / period;
    float fdot= acceleration / c * f0;
    float Nrot, Nrot_next;

    float* tsout = h_output.get_data();

    //Initialize tdum array, calc Nrot and Nrotint

    // for(size_t i= 0; i < h_output.get_length() - 1; i++)
    for(size_t i= 0; i < h_output.get_length(); i++)
    {
        Nrot = tsamp * i * f0 + 0.5 * fdot * tsamp *tsamp * i * i;
        Nrot_next = tsamp * (i+1) * f0 + 0.5 * fdot * tsamp * tsamp * (i+1) * (i+1);

        // float floc;
        // float binfrac;
        if( ((int) floor(Nrot_next) - (int) floor(Nrot)) == 1)
        {
            // floc = f0 + fdot * tsamp * i;
            tsout[i]= 1.0;
            /*binfrac=fabs(Nrot-lround(Nrot))/floc/tsamp;
              tsout[i]=binfrac;
              tsout[i+1]=1-binfrac; */
        }
        else
        {
            tsout[i]=0.0;
        }
    }
}

void TdrtTDTReference::sine_calibration_signal(float period, TimeSeries<float>& h_output)
{
    const float tsamp = h_output.get_delta_t();
    const float pi =    3.14159265;
    float* tsout =      h_output.get_data();

    for(size_t i= 0; i < h_output.get_length(); i++)
    {
        tsout[i]= sin( 2.0*pi*i * tsamp / period  );
    }
}

double TdrtTDTReference::resampled_period(double acceleration, double period, double tsamp, int length)
{
    double c = 299792458.0; // speed of light (m/s)
    double new_period;

    new_period = period/(1+(acceleration*tsamp*length)/(2*c));

    std::cout << "Period after resampling: " << new_period << std::endl;

    return new_period;
}

float TdrtTDTReference::tdrt_correlation(TimeSeries<float>& h_tsA, TimeSeries<float>& h_tsB)
{
    assert(h_tsA.get_length() == h_tsB.get_length());
    assert(h_tsA.get_delta_t() == h_tsB.get_delta_t());

    size_t length = h_tsA.get_length();
    float* tsA = h_tsA.get_data();
    float* tsB = h_tsB.get_data();
    size_t npul  = 0;
    size_t found = 0;
    double correlation = 0.0;

    for(size_t i=0; i<length; ++i)
    {
        correlation += tsA[i] * tsB[i];

        /*This code is only needed to count pulses*/
        if((tsA[i] != 0) && (found==0))
        {
            npul+=1;
            found=1;
        }
        if((tsA[i] == 0) && (found==1))
        {
            found=0;
        }
    }

    //std::cout << "Correlation: " << correlation << std::endl;
    //std::cout << "Number of pulses: " << npul  << std::endl;
    //std::cout << "Normalized correlation: " << correlation/npul << std::endl;

    return correlation/npul;
}

float TdrtTDTReference::tdrt_bin_diff(TimeSeries<float>& h_tsA, TimeSeries<float>& h_tsB)
{
    assert(h_tsA.get_length() == h_tsB.get_length());
    assert(h_tsA.get_delta_t() == h_tsB.get_delta_t());

    size_t length = h_tsA.get_length();
    float* tsA = h_tsA.get_data();
    float* tsB = h_tsB.get_data();

    size_t i = 0;
    size_t npulA=0;
    size_t npulB=0;
    size_t npul=0;
    size_t counting=0;

    float TOA_A=0.0;
    float TOA_B=0.0;
    float TOA_diff=0.0;

    float amp=0;
    float* Alist = (float*)malloc(sizeof(float)*length/2);
    float* Blist = (float*)malloc(sizeof(float)*length/2);

    for(i=0; i<length; ++i)
    {
        if(tsA[i] > 0)
        {
            TOA_A+=tsA[i]*i;
            amp+=tsA[i];
            counting=1;
        }
        else if((tsA[i] == 0) && (counting == 1))
        {
            Alist[npulA]=TOA_A/amp;
            amp=0;
            TOA_A=0;
            npulA+=1;
            counting=0;
        }
    }

    for(i=0; i<length; ++i)
    {
        if(tsB[i] > 0)
        {
            TOA_B+=tsB[i]*i;
            amp+=tsB[i];
            counting=1;
        }
        else if((tsB[i] == 0) && (counting == 1))
        {
            Blist[npulB]=TOA_B/amp;
            amp=0;
            TOA_B=0;
            npulB+=1;
            counting=0;
        }
    }

    if(npulA<npulB) npul=npulA;
    else npul=npulB;

    for(i=0; i<npul; ++i)
    {
        TOA_diff+=fabs(Alist[i]-Blist[i]);
    }

    //std::cout << "Num pulses: " << npul << std::endl;
    //std::cout << "TOA diff: " << TOA_diff << std::endl;
    //std::cout << "Norm TOA diff: " << TOA_diff/npul << std::endl;

    free(Alist);
    free(Blist);

    return TOA_diff/npul;
}

std::tuple<float, size_t> TdrtTDTReference::tdrt_recover_amp(TimeSeries<float>& h_ts)
{

    size_t length = h_ts.get_length();
    float* ts = h_ts.get_data();

    size_t counting=0;
    size_t npul=1;

    float aTOA = 0.0;

    for(size_t i=0; i<length; ++i)
    {
        if(ts[i] > 0)
        {
            if(i==0)
                npul=0;

            aTOA+=ts[i];
            counting=1;
        }
        else if((ts[i] == 0) && (counting==1))
        {
            npul+=1;
            counting=0;
        }
    }

    aTOA/=npul;

    //std::cout << "Number of pulses: " << npul << std::endl;
    //std::cout << "Amplitude recovered: " << aTOA << std::endl;

    return std::make_tuple(aTOA, npul);
}

void TdrtTDTReference::tdrt_bin_spacing(TimeSeries<float>& h_ts)
{
    size_t length = h_ts.get_length();
    float* ts = h_ts.get_data();

    int* bin_list=(int *) malloc(length);
    int* bin_diff=(int *) malloc(length);
    unsigned int ii=0;
    unsigned int npulses=0;
    int holder=0;

    /*First loop over time series and find locations of pulses*/
    for(ii=0; ii<length; ii++)
    {
        if(ts[ii] != 0.0)
        {
            bin_list[npulses]=ii;
            npulses++;
        }
    }

    /*Calculate differences*/
    for(ii=0; ii<npulses-1; ii++)
        bin_diff[ii]=bin_list[ii+1]-bin_list[ii];

    /*Sort*/
    int noflip=0;
    while(noflip == 0)
    {
        noflip=1;
        for(ii=0; ii<npulses-1; ii++)
        {
            if(bin_diff[ii+1]<bin_diff[ii])
            {
                holder=bin_diff[ii];
                bin_diff[ii]=bin_diff[ii+1];
                bin_diff[ii+1]=holder;
                noflip=0;
            }
        }
    }

    /*Calc mean */
    double mean=0.0;

    for(ii=0; ii<npulses-1; ii++)
    {
        mean+=bin_diff[ii];
    }
    mean/=(npulses-1);
    printf("Mean: %lf\n", mean);

    /* Calc stddev */
    double variance=0.0;
    for(ii=0; ii<npulses-1; ii++)
    {
        variance+=(bin_diff[ii]-mean)*(bin_diff[ii]-mean);
    }
    variance/=(npulses-1);
    variance=sqrt(variance);
    printf("Variance: %lf\n", variance);

    int count=1;
    bin_diff[npulses]=0.0; /*This is a hack to allow loop to go over edge*/
    for(ii=0; ii<npulses-1; ii++)
    {
       if(bin_diff[ii+1] != bin_diff[ii])
       {
           printf("%d: %d\n", bin_diff[ii], count);
           count=1;
       }
       else
       {
           count++;
       }
    }
}

double TdrtTDTReference::fold_pulsar(TimeSeries<float>& h_ts, double period, int adjacent)
{
    int int_period = (int) round(period);
    int bin_to_write = 0;
    int num_pulses = 0;
    int length = h_ts.get_length();
    int ii=0;
    int max_bin=-1;
    double max_val=-1.0;
    double pulse_sum = 0.0;

    float* ts = h_ts.get_data();
    double* profile = (double *) malloc(sizeof(double)*int_period);

    for(ii=0; ii<int_period; ii++) profile[ii]=0.0;

    std::cout << "Folding at a period of " << int_period << " bins" << std::endl;

    /* Fold profile */
    for(ii=0; ii<length; ii++)
    {
        bin_to_write=ii%int_period;
        profile[bin_to_write]+=ts[ii];
        if(bin_to_write == int_period-1) num_pulses++;
    }

    std::cout << "Found " << num_pulses << " pulses" << std::endl;

    for(ii=0; ii<int_period; ii++) printf("%lf ", profile[ii]);
    printf("\n");

    /*Find max bin*/
    max_bin=0; max_val=profile[0];
    for(ii=0; ii<int_period; ii++)
    {
        if(profile[ii]>max_val)
        {
            max_val=profile[ii];
            max_bin=ii;
        }
    }

    /* Sum profile including adjecent bins set by input param */
    pulse_sum=profile[max_bin];

    for(ii=1; ii<adjacent+1; ii++)
    {
        if(max_bin+ii<=int_period-ii) /*Check for wrapping*/
            pulse_sum+=profile[max_bin+ii];
        else
            pulse_sum+=profile[ii-1];

        if(max_bin-ii>=0)
            pulse_sum+=profile[max_bin-ii];
        else
            pulse_sum+=profile[int_period-ii];
    }

    std::cout << "WARNING: This function only works for input periods that are integers" << std::endl;
    std::cout << "Folded pulse sum: " << pulse_sum << std::endl;

    free(profile);

    return pulse_sum/num_pulses;
}

} // namespace test
} // namespace tdrt
} // namespace modules
} // namespace cheetah
} // namespace ska