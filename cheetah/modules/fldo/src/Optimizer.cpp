/*
 * Copyright 2024 SKA Observatory
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "cheetah/modules/fldo/Optimizer.h"
namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {

Optimizer::Optimizer()
: _width(0)
{
}

Optimizer::~Optimizer()
{
}

Optimizer::Profile& Optimizer::profile()
{
    return _profile;
}

float const& Optimizer::mean()
{
    return _mean;
}

float const& Optimizer::stddev()
{
    return _stddev;
}

std::size_t const& Optimizer::width()
{
    return _width;
}

void Optimizer::genstats(Config const& config)
{
    /*
     * @brief we assume here that the peak is ALWAYS the pulse peak and the ON profile is not more than 50%
     * of the array
     */

    _pampindex = std::distance(_profile.folded_profile().begin(), std::max_element(_profile.folded_profile().begin(), _profile.folded_profile().end()));
    if (_pampindex <= config.number_of_phase_bins()/2)
    {
	_mean = std::reduce(_profile.folded_profile().begin() + config.number_of_phase_bins()*3/4,_profile.folded_profile().end())/(config.number_of_phase_bins()/4);
        _stddev = std::reduce(_profile.folded_profile().begin() + config.number_of_phase_bins()*3/4, _profile.folded_profile().end(), 0.0, [](float val, float _mean){return sqrt(powf(val - _mean, 2.0)); } )/config.number_of_phase_bins()/4;
    }
    else
    {
	_mean = std::reduce(_profile.folded_profile().begin(), _profile.folded_profile().begin() + config.number_of_phase_bins()/4)/config.number_of_phase_bins()/4;
        _stddev = std::reduce(_profile.folded_profile().begin(), _profile.folded_profile().begin() + config.number_of_phase_bins()/4, 0.0, [](float val, float _mean){return sqrt(powf(val - _mean, 2.0)); })/config.number_of_phase_bins()/4;
    }

    if (_stddev == 0.0)
    {
	PANDA_LOG_WARN << "std deviation is 0. making it some small number";
	_stddev = 0.5;
    }
}

Optimizer::Profile::Profile()
{
}

Optimizer::Profile::~Profile()
{
}

void Optimizer::Profile::generate_profile(Config const& config, data::OptimisedCandidate<>& candidate)
{
    // resize all arrays
    _prefix_sum_array.assign(config.number_of_phase_bins(), 0.0);
    _circular_conv.assign(config.number_of_phase_bins(), 0.0);
    _folded_profile.assign(config.number_of_phase_bins(), 0.0);
        // adding data over subints and frequency channels
    for (std::size_t ii=0; ii < config.number_of_subints(); ++ii)
    {
	auto subint = candidate.subint(ii);
	for (std::size_t jj=0; jj < config.number_of_phase_bins(); ++jj)
        {
            auto slice = subint.slice(data::DimensionSpan<data::Frequency>(
                                                   data::DimensionIndex<data::Frequency>(0)
                                                   , data::DimensionSize<data::Frequency>(config.number_of_subbands())
                                                  )
                                              , data::DimensionSpan<pss::astrotypes::units::PhaseAngle>(
						      data::DimensionIndex<pss::astrotypes::units::PhaseAngle>(jj)
						      , data::DimensionSize<pss::astrotypes::units::PhaseAngle>(1)
                                                  )
                                             );
	    _folded_profile[jj] = std::reduce(std::execution::par, slice.begin(), slice.end(), _folded_profile[jj]);
	}
    }
}

std::vector<float> const& Optimizer::Profile::folded_profile()
{
    return _folded_profile;
}

std::vector<float>& Optimizer::Profile::prefix_sum_array()
{
    return _prefix_sum_array;
}

std::vector<float>& Optimizer::Profile::circular_conv()
{
    return _circular_conv;
}

float const& Optimizer::snr()
{
    return _snr;
}

data::OptimisedCandidate<> Optimizer::apply_bin_correction(Config const& config, data::OptimisedCandidate<> candidate, MsecTimeType const& period_fdas, MsecTimeType const& period_trial, SecPerSecType const& pdot_fdas, SecPerSecType const& pdot_trial, float tbin, float tsub)
{
    std::vector<std::int32_t> bin_shifts(config.number_of_subints());
    float pulsespersubint = tsub/candidate.metadata().period().value();
    std::size_t index = 0;
    auto compute_shifts = [&](std::int32_t shift )
	                  {
			      shift = index*(((period_trial - period_fdas).value() * pulsespersubint)/tbin  + (pdot_trial - pdot_fdas).value()*tsub);
			      ++index;
			      return shift;
			  };

    // Define the bin shifts
    std::transform(std::execution::par, bin_shifts.begin(), bin_shifts.end(), bin_shifts.begin(), compute_shifts);

    // apply the bin shifts to the data
    for (std::size_t ii=0; ii < config.number_of_subbands(); ++ii)
    {
	for (std::size_t jj=0; jj < config.number_of_subints(); ++jj)
	{
	    auto slice = candidate.subband(ii).slice(data::DimensionSpan<data::Time>(
                                                   data::DimensionIndex<data::Time>(jj)
                                                   , data::DimensionSize<data::Time>(1)
                                                  )
                                              , data::DimensionSpan<pss::astrotypes::units::PhaseAngle>(
                                                      data::DimensionIndex<pss::astrotypes::units::PhaseAngle>(0)
                                                      , data::DimensionSize<pss::astrotypes::units::PhaseAngle>(config.number_of_phase_bins())
						      ));

	    if (bin_shifts[jj] > 0)
                std::rotate(std::execution::par, slice.begin(), slice.begin() += bin_shifts[jj], slice.end());
	    else
	        std::rotate(std::execution::par, slice.begin(), slice.begin() += (config.number_of_phase_bins() + bin_shifts[jj]), slice.end());
	}
    }

    return candidate;
}


void Optimizer::compute_snr()
{
    if (_stddev == 0.0)
    {
	PANDA_LOG_WARN << "std deviation is 0. making it some small number";
	_stddev = 0.5;
    }
    /* Just getting the peak SNR for now*/
    _snr = (_profile.folded_profile()[_pampindex] - _mean)/_stddev; // DEV NOTE: This will change
}

void Optimizer::optimal_filter(Config const& config)
{
    /* fill the prefix array */
    _optimal_snr.assign(config.number_of_phase_bins(), 0.0);
    std::size_t idx = 0;
    float smax = 0.0;
    for (auto it = _profile.folded_profile().begin(); it != _profile.folded_profile().end(); ++it)
    {
        _profile.prefix_sum_array()[idx] = std::reduce(std::execution::par, _profile.folded_profile().begin(), it, *(_profile.folded_profile().begin()));
	++idx;
    }

    /* Compute the circular convolution*/
    for (std::size_t ii = 0; ii < config.number_of_phase_bins(); ++ii )
    {
	for (std::size_t jj = 0 ; jj < config.number_of_phase_bins(); ++jj)
	{
	    if (jj == 0)
                _profile.circular_conv()[jj] = _profile.prefix_sum_array()[ii];
	    else if ( jj + ii < config.number_of_phase_bins() - 1)
	        _profile.circular_conv()[jj] = _profile.prefix_sum_array()[jj + ii] - _profile.prefix_sum_array()[jj -1];
	    else
		_profile.circular_conv()[jj] = _profile.prefix_sum_array()[config.number_of_phase_bins() - 1] - _profile.prefix_sum_array()[jj] + _profile.prefix_sum_array()[ii + jj - config.number_of_phase_bins()];
	    if (_profile.circular_conv()[jj] > smax)
                smax = _profile.circular_conv()[jj];
	}
	/* compute the snr */
	_optimal_snr[ii] = (smax - ((ii+1)*_mean))/(_stddev*sqrt((ii+1)*(1 + (float)(ii+1)/(float)config.number_of_phase_bins())));
	smax = 0.0;
    }
    _width = (std::size_t) std::distance(_optimal_snr.begin(),std::max_element(_optimal_snr.begin(), _optimal_snr.end())) + 1;
}

} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
