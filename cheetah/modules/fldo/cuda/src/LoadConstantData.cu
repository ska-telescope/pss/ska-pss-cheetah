/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/cuda_utils/cuda_errorhandling.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {
namespace util {

/**
 * void load_constant_data(double *delta_freq, double *nu, double *nudot, float *dm,
 *                         int *nbins, int nchannels, int nchan_per_subband,
 *                         size_t nsubints, double tsamp, int ncandidates)
 *
 * @brief Copies r/o informatiom to the device constant memory space.
 *
 * @param delta_freq            array with tabulated k_DM * (1/v_1 ^2 - 1/v_2^2)
 * @param nu                    array with candidates 1/v
 * @param nudot                 array with candidates 1/v^2
 * @param dm                    array with candidates dispersion measure
 * @param nbins                 array with the candidates number of phases
 * @param nchannels             number of frequency channels
 * @param nchan_per_subband     number of channesl/sub-band
 * @param nsubints              number of sub-integrations
 * @param tsamp                 sampling time (in sec)
 * @param ncandidates           number of candidates
 *
 * @return On failure throws a runtime_error exception.
 *
 */
void load_constant_data(double *delta_freq, double *nu, double *nudot, float *dm,
                        int *nbins, int nchannels, int nchan_per_subband,
                        size_t nsubints, double tsamp, int ncandidates)
{
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_delta_freq, delta_freq, nchannels * sizeof(double), 0, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_nu, nu, ncandidates * sizeof(double), 0, cudaMemcpyHostToDevice));
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_nudot, nudot, ncandidates * sizeof(double), 0, cudaMemcpyHostToDevice));
    // candidates DMs
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_dm, dm, ncandidates * sizeof(float), 0, cudaMemcpyHostToDevice));
    // candidates phases
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_nbins, nbins, ncandidates * sizeof(int), 0, cudaMemcpyHostToDevice));
    // subint time mean value
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_nchan_per_band, &nchan_per_subband, sizeof(int), 0, cudaMemcpyHostToDevice));
    // number of sub-integrations
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_nsubints, &nsubints, sizeof(int), 0, cudaMemcpyHostToDevice));
    // sampling time
    // OSS: the prebin value is taken into account inside the kernel function.
    CUDA_ERROR_CHECK(cudaMemcpyToSymbol(cuda::d_tsamp, &tsamp,  sizeof(double), 0, cudaMemcpyHostToDevice));
}

} // utils
} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
