/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @brief
 *  GPU main kernels
 *
 * @details
 *
 * initial assumptions:
 * Data are read in into main CPU memory. They are divided into 64
 * subintegrations (blocks). At least one full block is present at the same
 * time on main memory. Data are in unsigned char format (8 bit)
 * Block dimension is (4096 frequency channels) X (up to 187500 sequential
 * measures).  This lead to up to 3GB. Too much, we will split either in
 * frequency or in time.
 *
 * Each thread reads and process a single frequency num_frequency =
 * blockIdx.x and inside GPU frequency number is threadIdx.x data are read
 * and then converted into streams of float (4bytes) in GPU main memory.
 *
 *
 */

// includes, system

#include <float.h>
#include <stdio.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {
//
// void input_converter(unsigned char *g_in, float *g_out, int local_measures, int nchan)
//
// g_in                 the input data read from file
// g_out                the converted output data as float
// local_measure        the number of sample per channel and for sub-integration
// nchan                the global number of channel
//
__global__ void input_converter(unsigned char *g_in, float *g_out, int pitch_dim,
                                int local_measures, int nchan)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;

    if ((i < local_measures) && (j < nchan)) {
        g_out[i + pitch_dim * j] = (float)g_in[i + pitch_dim *j] - 128;
    }
}

//
// __global__ void normalize_kernel(float *folded, float *weight, int ncand,
//                                  float mean, int nsubband)
//
// folded       global array with folded data
// weight       global array with weights
// ncand        the candidate id
// mean         the input data mean
// nsubband     the number of freq. bands
//
__global__ void normalize_kernel(float *folded, float *weight, int ncand,
                                 float mean, int nsubbands)
{
   int n;       //loop counter
   int idx0 = threadIdx.x + blockIdx.x*blockDim.x * nsubbands + ncand *
                            gridDim.x * blockDim.x * nsubbands;
   int sub_idx = 0;     //subband index (0, 1, .., 63)
   int idx = 0;
   float profile = 0.;
   float counts = 0.;

   /*
    * threadIdx.x       the bin number
    * blockDim.x        max number of bins
    * blockIdx.x        sub-int number (0, 1, ..., nsubints - 1)
    * gridDim.x         number of sub-integrations (nsubints)
    */
   //mean = -0.353104; // Debug: mean value of test_data as commputed by Mike

   // idx0: the index of the bin of the subband 0 of the current subint
   // loop on the band
   for (n = 0; n < nsubbands; n ++) {
        sub_idx = n * blockDim.x;       // the subband index
        idx = idx0 + sub_idx;           // the global bin index for the current candidate
        counts = weight[idx];
        profile = 0.;
        if (counts > FLT_EPSILON) {
            // here we subctract the mean value of data
            profile = folded[idx];
            profile = profile/counts - mean;
        }
        folded[idx] = profile;

    }
}

//
// __global__ void profile_kernel(float *in, float *out, int ncand, int nloop)
//
// Builds intermediate and final profile
// This kernel is called two times: in the first call is created the
// profile in freq (an array with Ncand*subints*max_phases elements
// In the second call is build the final profile (an array with
// max_phases * Ncand elements)
//
// in           the input array
//              first call : d_folded
//              second call: d_outprof
// out          the output array
//              first call : d_outprof
//              second call: d_outprof
// ncand        the candidate id
// nloop        the number of repetition:
//              first call  = nsubbands
//              second call = nsubints
__global__ void profile_kernel(float *in, float *out, int ncand, int nloop)
{
   int idx0 = threadIdx.x + blockIdx.x * blockDim.x * nloop +
                            ncand * blockDim.x * gridDim.x * nloop;
   int sub_idx = 0;     // shift depending on loop counter
   int idx;             // index inside data array
   float profile = 0.;
   float ftscrunch = 0;

    //
    // threadIdx.x       bin
    // blockDim.x        max number of bins
    // blockIdx.x        first call : sub-int number (0, 1, ..., nsubints - 1)
    //                   second call: 0
    // gridDim.x         first call : number of sub-integrations (nsubints)
    //                   second call: 1
    //
    // loop on the subband/subints
    for (int n = 0; n < nloop; n ++) {
        sub_idx = n* blockDim.x;
        idx = idx0 + sub_idx;
        profile = in[idx];
        ftscrunch += profile;
    }
    out[threadIdx.x + blockIdx.x * blockDim.x + ncand * blockDim.x*gridDim.x] =
                                                               ftscrunch/nloop;
}

//
//__global__ void best_profile_kernel(float *in, float *out, int ncand,
//                                    int nloop, int index, int ntrial)
//
// Build the signal profile for all the trials.
//
// in           the profile in freq.
// out          an array with the signal profile for each 3-tuple of trial values
// ncand        the candidate number
// index        the trial index identifying the 3-tuple
// ntrial       the max number of trials
__global__ void best_profile_kernel(float *in, float *out, int ncand,
                                    int nloop, int index, int ntrial)
{
   int idx0 = threadIdx.x + ncand * blockDim.x * nloop; // starting index
   int sub_idx = 0;     // shift depending on loop counter
   int idx;             // index inside data array
   float profile = 0.;
   float ftscrunch = 0;
    //
    // threadIdx.x       bin
    // blockDim.x        max number of bins
    //
    // loop on the subband/subints
    for (int n = 0; n < nloop; n ++) {
        sub_idx = n* blockDim.x;
        idx = idx0 + sub_idx;
        profile = in[idx];
        ftscrunch += profile;
    }
    out[threadIdx.x + index * blockDim.x + ncand * blockDim.x * ntrial] = ftscrunch/nloop;
}

//
//
//__global__ void perturbate_kernel(float *g_in, float *g_out,
//                                 double delta_nu, double delta_dm,
//                                 double delta_nudot, int candidate,
//                                 int max_phases, int index)
// g_in  the input data to be corrected
// g_out the output data pertubed
// ncand the candidate number
// delta_nu     perturbation in freq.
// delta_dm     perturbation in dm     (perturbed = original*delta_dm)
// delta_pdot   perturbation in pdot   (perturbed = original*delta_pdot)
// candidate    the candidate number
// max_phases   maximum number of phases
// index        the global index used to address the set of three parameters used
//              in the optimization phase. This value is only used for
//              debug purpose. To remove later!!
//
//
__global__ void perturbate_kernel(float *g_in, float *g_out,
                                 float*phase_shift, int candidate,
                                 int max_phases, int index)
{
    //
    // threadIdx.x      phase bin
    // blockIdx.x       the sub-band number
    // gridDim.x        the number of subbands
    // blockIdx.y       the sub-integ number
    // gridDim.y        the number of sub-integ
    //
    int subband_idx  = threadIdx.y + blockIdx.x * blockDim.y;  // the sub-band index (0, 1, ...64)

    int nsubbands = gridDim.x * blockDim.y;
    // start phase: the shift to the next profile of max_phases bins.
    int start_phase = max_phases * subband_idx + blockIdx.y * max_phases * gridDim.x * blockDim.y +
                                 + candidate * max_phases *gridDim.x * blockDim.y * gridDim.y;
    int ibin = 0;
    float ph_shift = phase_shift[subband_idx + blockIdx.y * nsubbands + candidate * nsubbands * gridDim.y];
    int nbins = d_nbins[candidate];
    int bin_off = (int)(ph_shift * nbins + 0.5);        // the offset in bins of the perturbed profile
    extern __shared__ float indata[];

    for (int idx = threadIdx.x; idx < max_phases;  idx += blockDim.x) {
        //null the output array
        g_out[idx + start_phase] = 0;
        indata[threadIdx.x] = 0;
    }
    __syncthreads();
    for (int idx = threadIdx.x; idx < nbins; idx += blockDim.x) {
        ibin = (idx + bin_off) % nbins;
        indata[idx + max_phases * threadIdx.y] = g_in[start_phase + ibin];
        g_out[idx + start_phase] = indata[idx + max_phases * threadIdx.y];
    }
}

//
//
//__global__ void compute_sn(float* profile, float* best, int max_phases, int cand,
//                           int nelem, float sigma)
//
//
// profile              the profile array
// best                 the array to store best result during pertubation
// max_phases           the max number of phases
// cand                 the candidate index
// nelem                the total number of pertubation applied
// sigma                the profile standard deviation less a nbins correction (see comment below)
//
//
// From the "Handbbok of Pulsar astronomy" (pag 167): the most commonly-used measure
// of profile significance is the signal to noise ratio, S/N ususally
// defined as:
//
// S/N = 1/(_sigma* (sqrt(Weff))) * sum (p_i - p_mean)  for i = 0,.., nbins
//
// Weff         the equivalent width (in bins) of a top-hat pulse with the
//              same area and peak height as the observad profile
// p_i          the amplitude of the i-th bin of the profile
// p_mean       off pulse mean calculated for on the raw time series
// _sigma = sigma * (sqrt(nbins))
// sigma        is the standard deviation calculated for the raw time series
//              ( sqr( (<X^2> - <X>^2) / (num(X)-1) Where X are input
//              values both in time and in frequency )
// nsamples     the total number of samples in the time series
//
// The calculation of S/N is repetead for each (dm, nu, nudot) combination
// and for each combination is taken the max value.
// The number of combinations (trials) depends on the value of range and
// step choosen for each of the 3 dimensions.
//
// The kernel runs a thread for each trial.
// If the total number of trials is > 1024 (the max number of threads
// programmable for each SM), the threads (= trial) has to be divided in
// block.

// OSS: the last block may contain a number of threads < 1024. So it
// is necessary to compare the global thread id number (= trial index)
// with the max number of trials passed to the kernel as argument.

__global__ void compute_sn(float* profile, float* best, int max_phases, int ncand, int nelem, float sigma)
{
    //
    // threadIdx.x      the trial number inside a thread block
    // blockDimx.x      the number of trials/per block
    // blockIdx.x       the block id:
    // gridDim.x        the number trials block
    //
    int width;          // impulse width in bin
    int nelem_per_block = blockDim.x;   // the number of pertubations per thread block
    float curval = 0;                   // the profile value inside the box
    float sn = 0;                       // the signal-to-noise value
    float b_width = 0;                  // the best pulse width in physical unit
    float b_sn = - FLT_MAX;             // the best signal-to-noise value
    float maxval = 0.;
    float _sigma;                        // standard deviation of the profile
    int tid = threadIdx.x;              // local thread id
    int gid = threadIdx.x + blockIdx.x * blockDim.x;    // global thread id = trial index
    int start = gid * max_phases + ncand * nelem * max_phases;
    extern float __shared__ result[];

    int nbins = d_nbins[ncand];  // the real number of phases for the current candidate
    _sigma = sigma * sqrt((float)nbins);
    //_sigma = sigma;
    // Warning!!! Last arg should be sigma*sqrt((float)  (nbins[ncand] * first_prebin))
    // but  in calling routine we do not have nbins[], so we make computation here
    result[tid] = 0;

    if (gid < nelem) {
        for (width = 1; width < nbins >> 1; width ++) {
            curval = 0;
            for (int ibin = 0; ibin < width; ibin++){       // initial guess
                curval += profile[start + ibin % nbins];
            }

            maxval = curval;
            // To get maximum value we shift the 'active' window on the phase
            // array.  To save computation on each step we start from the value
            // of preceding try and then we get the shift of window by dropping
            // its first value and adding the first bin after its end. The
            // assumed topology is toroidal: last phase bin is contigous to the first
            for (int ibin = 0; ibin < nbins; ibin++){
                int addbin = ibin + width;
                curval -= profile[ start + ibin  % nbins];
                curval += profile[ start + addbin  % nbins];
                if (maxval < curval) {
                    maxval = curval;
                }
            }
            sn = maxval /sqrt((float)width)/_sigma;
            if (sn > b_sn) {
                b_sn = sn;
                b_width =  width;
            }
        }
        // store the best sn and width values in shared memory
        //
        result[tid] = b_sn;
        result[tid + nelem_per_block] = b_width/(d_nu[ncand] * nbins);
        __syncthreads();
        // copy to global memory all the nelem values for sn and width
        best[gid + ncand * nelem * 2] = result[tid];
        best[gid + nelem + ncand * nelem * 2] = result[tid + nelem_per_block];
    }
    return ;
}

//
// __global__ void transposeNoBankConflicts(float *odata, unsigned char *idata,
//                                          int width, int height, int nsamp)
//
// Executes the transpose of input data and convert it from unsigned to float.
//
// odata         the output data matrix
// idata         the input  data matrix
// width         the matrix width (= pitch dimension to get aligned  memory data)
// height        the matrix heigth(= number of freq. channels)
// nsamp         the number of samples per sub-integration and per channel
//
__global__ void transposeNoBankConflicts(float *odata, unsigned char *idata,
                                         int width, int height, int nsamp)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction
    int tile_dimy = blockDim.y;         // number of threads in y direction

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
     __syncthreads();

    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int index_in = 0;
    if ((xIndex < width) && (yIndex < nsamp)) {
        index_in = xIndex + (yIndex)*width;
        // convert from unsigned to signed
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();

    xIndex = blockIdx.y * tile_dimy + threadIdx.x;
    yIndex = blockIdx.x * tile_dimx + threadIdx.y;
    int index_out = xIndex + (yIndex)*height;
    // convert to float
    odata[index_out] = (float)tile[threadIdx.x + blockDim.x * threadIdx.y];
}

//
// __global__ void transposePreBin(float *odata, unsigned char *idata,
//                                          int width, int height,
//                                          int nsamp, int prebin)
//
// Executes the transpose of input data and convert it from unsigned to
// float. If the prebinning value > 1, it executes also the data binning.
//
// odata         the output data matrix
// idata         the input  data matrix
// in_height     the input matrix number of rows (heigth) equal to the
//               number of freq. channels
// out_width     the outout matrix number of cols (width) equal to the
//               pitch dimension to get aligned  memory data
// nsamp         the number of samples per sub-integration and per channel
// prebin        the number of samples averaged as prebinning (1-8 power of 2)
//
__global__ void transposePreBin(float *odata, unsigned char *idata, int in_height,
                                int out_width, int nsamp, int prebin)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction
    int tile_dimy = blockDim.y;         // number of threads in y direction
    //int prebin_index = threadIdx.x % prebin;  // index inside prebinning group
    // OSS: prebin is a power of 2 so modulo operation can be replaced
    // with the next one
    int prebin_index = threadIdx.x  & (prebin - 1);     // index inside prebinning group
    float ftemp = 0.;                                   // temporary float well

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
     __syncthreads();

    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int index_in = 0;
    if ((xIndex < in_height) && (yIndex < nsamp)) {
        index_in = xIndex + (yIndex)*in_height;
        // convert from unsigned to signed
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();

    // we only proceed if first thread of prebinning group

    if (prebin_index == 0) {
        xIndex = blockIdx.y * tile_dimy + threadIdx.x;
        yIndex = blockIdx.x * tile_dimx + threadIdx.y;
        //int index_out = xIndex/prebin + (yIndex)*height/prebin;
        int index_out = xIndex/prebin + (yIndex)*out_width;
        // convert to float and sum up
        for (int i = 0 ; i < prebin ; i++) {
            ftemp += (float)tile[threadIdx.x + i + blockDim.x * (threadIdx.y)];
        }
        odata[index_out] = ftemp/prebin;
    }
    return;
}
} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
