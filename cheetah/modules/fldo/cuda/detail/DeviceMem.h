/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_FLDO_CUDA_DEVICEMEM_H
#define SKA_CHEETAH_MODULES_FLDO_CUDA_DEVICEMEM_H

#include "cheetah/modules/fldo/cuda/CommonDefs.h"

/**
 * @brief
 *   GPU global device memory declarations
 */
namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {

__device__ __constant__ double d_nu[ska::cheetah::modules::fldo::cuda::max_candidates];
__device__ __constant__ double d_nudot[ska::cheetah::modules::fldo::cuda::max_candidates];
__device__ __constant__ double d_delta_freq[ska::cheetah::modules::fldo::cuda::max_frequencies];
__device__ __constant__ float  d_dm[ska::cheetah::modules::fldo::cuda::max_candidates];
__device__ __constant__ int    d_nbins[ska::cheetah::modules::fldo::cuda::max_candidates];
__device__ __constant__ int    d_nchan_per_band;
__device__ __constant__ int    d_nsubints;
__device__ __constant__ double d_tsamp;

} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska


#endif //SKA_CHEETAH_MODULES_FLDO_CUDA_DEVICEMEM_H