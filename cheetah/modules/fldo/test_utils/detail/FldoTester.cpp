/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fldo/test_utils/FldoTester.h"
#include "cheetah/modules/fldo/CommonDefs.h"
#include "cheetah/modules/fldo/Types.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/GaussianNoiseConfig.h"

#include "cheetah/sigproc/SigProcFileStream.h"
#include "cheetah/modules/fldo/cuda/CommonDefs.h"
#include <boost/filesystem.hpp>
#include "panda/test/gtest.h"

//#define DEEP_TESTING 1
// OSS: to define DEEP_TESTING it can be added in the CMakeList.txt file
// (in the cheetah/cheetah/fldo/cuda/test directory) the line:
// add_definitions(-DDEEP_TESTING). cmake files need to be re-generated to
// let this modify take effect.

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace test {

template<class FldoAlgo>
typename FldoTesterTraits<FldoAlgo>::Api& FldoTesterTraits<FldoAlgo>::api(PoolType& pool)
{
    if(!_api) {
        _config.deactivate_all();
        _config.pool(pool);
        _config.template config<typename FldoAlgo::Config>().active(true);
        _api.reset(new Api(_config, _handler));
    }
    return *_api;
}

template<class FldoAlgo>
typename FldoTesterTraits<FldoAlgo>::FldoHandler& FldoTesterTraits<FldoAlgo>::handler()
{
    return _handler;
}

template <typename TestTraits>
FldoTester<TestTraits>::FldoTester()
    : BaseT()
{
}

template <typename TestTraits>
FldoTester<TestTraits>::~FldoTester()
{
}

template<typename TestTraits>
void FldoTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void FldoTester<TestTraits>::TearDown()
{
}

// candidate_number is the number of fake candidates produced during test
constexpr int candidate_number = 5;

void load_candidates(data::Scl &scl_data)
{
    typedef data::Candidate<Cpu, float> CandidateType;

    //TODO: check if float or double is needed. In this case we need to update the Scl definition!!

    // Open the file with candidates
    std::ifstream myfile;
    std::string line;
    double period, pdot, dm;

#ifdef DEEP_TESTING
    // This test checks the correctness of FLDO algorithm. To be used during development
    myfile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        myfile.open(panda::test::test_file("candidates.txt"), std::fstream::in);
        std::size_t ident = 0; // Candidate identifier number
        while (getline(myfile, line)) {
            std::stringstream ss;
            ss << line;
            // Scan line to get pulsar info
            ss >> period >> pdot >> dm;
            PANDA_LOG_DEBUG << "period: " << period << ", pdot: " << pdot << ", dm: " << dm;
            CandidateType::MsecTimeType period_val(period * boost::units::si::seconds);
            CandidateType::Dm dm_val(dm * data::parsecs_per_cube_cm);
            CandidateType::SecPerSecType pdot_val(pdot);
            // Store values inside a candidate structure
            CandidateType candidate(period_val, pdot_val, dm_val, ident);
            ident++;
            // Add the candidate to the scl vector
            scl_data.push_back(candidate);
        }
        myfile.close();
    }
    catch(std::ifstream::failure e) {
        PANDA_LOG_ERROR << "Exception opening/reading/closing candidates file:"
                        << e.what();
    }
#else // !DEEP_TESTING
    // Produce a few fake candidates
    //double fake_period[] = {5.94685039e-04, 7.88370079e-04, 9.82055118e-04, 1.17574016e-03, 1.12340000e-03 };
    double fake_period[] = {1.17574016
                            , 5.94675039
                            , 17.88370079
                            , 0.0112340000
                            , 9.82055118
                           };
    double fake_pdot[]   = {0.0
                            , 0.0
                            , 0.0
                            , 0.0
                            , 0.0
                           };
    double fake_dm[]     = {1.01000000e+01
                            , 1.01000000e+01
                            , 1.01000000e+01
                            , 1.01000000e+01
                            , 1.01000000e+01
                           };
    for(size_t i = 0; i < candidate_number; ++i)
    {
        period = fake_period[i];
        pdot   = fake_pdot[i];
        dm     = fake_dm[i];
        PANDA_LOG_DEBUG << "period: " << period << ", pdot: " << pdot << ", dm: " << dm;
        CandidateType::MsecTimeType period_val(period * boost::units::si::seconds);
        CandidateType::Dm dm_val(dm * data::parsecs_per_cube_cm);
        CandidateType::SecPerSecType pdot_val(pdot);
        // Store values inside a candidate structure
        CandidateType candidate(period_val, pdot_val, dm_val, i);
        // Add the candidate to the scl vector
        scl_data.push_back(candidate);
    }
#endif // DEEP_TESTING

    // Print for debug the value inside the data::Scl vector
    for(data::VectorLike<std::vector<data::Scl::CandidateType>>::ConstIterator it = scl_data.begin();
        it != scl_data.end();
        ++it
       )
    {
        PANDA_LOG_DEBUG << " period = " << it->period()
                        << ", pdot = " << it->pdot()
                        << ", dm = " << it->dm()
                        << ", ident = " << it->ident();
    }

    // Sort the candidates by period in ascending order using a lambda function
    std::sort(scl_data.begin()
              , scl_data.end()
              , [](CandidateType first, CandidateType second)
                {
                    return (first.period() < second.period());
                }
             );

}

// This test runs Fldo with no data, i.e. a zero-sized TimeFrequency
// block. Fldo should return an empty Ocld in this case.
POOL_ALGORITHM_TYPED_TEST_P(FldoTester, test_empty_data)
{
    typedef typename TypeParam::TimeFrequencyType DataType;
    typedef typename DataType::value_type NumericalT;
    TypeParam traits;
    auto& api = traits.api(pool);

    // Create an empty chunk of TimeFrequency data
    std::vector<std::shared_ptr<DataType>> tf_data;
    // Create an empty (zero-length) candidate list
    data::Scl scl_data;
    // Run Fldo with empty data and an empty candidate list
    auto job = api(tf_data, scl_data);
    job->wait();

    // The Ocld returned from Fldo should be empty, so check that the size is zero
    std::shared_ptr<data::Ocld> results = traits.handler().data();
    ASSERT_EQ(0U, results->size());
}

// This test executes the folding operator on noisy data generated at runtime.
// This test adds information to the data about the sampling time and frequecies.
POOL_ALGORITHM_TYPED_TEST_P(FldoTester, test_gaussian_noise_data)
{
    typedef typename TypeParam::TimeFrequencyType DataType;
    typedef typename DataType::value_type NumericalT;
    TypeParam traits;
    auto& api = traits.api(pool);

    typename DataType::FrequencyType delta(-0.2 * boost::units::si::mega * boost::units::si::hertz);
    typename DataType::FrequencyType start(1400.0 * boost::units::si::mega * boost::units::si::hertz);

    //fldo::Config fldo_config;
    generators::GaussianNoiseConfig config;
    generators::GaussianNoise<DataType> noise(config);

    std::vector<std::shared_ptr<DataType>> tf_data;

    // Generate some Gaussian noise
    for(size_t i = 0; i < 64; ++i)
    {
        // Create a TimeFrequency chunk of a certain size
        std::shared_ptr<DataType>
            time_frequency_data(new DataType(data::DimensionSize<data::Time>(512)
                                             , data::DimensionSize<data::Frequency>(1024)
                                            )
                               );
        time_frequency_data->set_channel_frequencies_const_width(start, delta);
        // Set the sample interval for generated data
        boost::units::quantity<boost::units::si::time, double> dt(64 * boost::units::si::micro * boost::units::si::seconds);
        time_frequency_data->sample_interval(dt);
        // Add Gaussian noise to the TimeFrequency chunk
        noise.next(*time_frequency_data);
        // Append this new chunk of noise to the end of the TimeFrequency data vector
        tf_data.emplace_back(time_frequency_data);
    }

    data::Scl scl_data;

    // Load a list of pulsar candidates into the candidate list
    load_candidates(scl_data);

    // init of config variables
    //fldo_config.phases(fldo::o_phases);

    // Assert that Fldo has not been called yet
    ASSERT_FALSE(traits.handler().executed());

    // Call Fldo with the noise data and list of candidates
    auto job = api(tf_data, scl_data);
    job->wait();

    ASSERT_TRUE(traits.handler().executed());

    std::shared_ptr<data::Ocld> results = traits.handler().data();

}

#ifdef DEEP_TESTING
// This test checks the correctness of FLDO algorithm. To be used during development
//
// This test executes the folding operator on data read from a file in sigproc format.
// This tests gets the number of samples stored into the file and then calculates the
// optimal number of samples taking into account the number of channels, sub-integration
// and max rebin value. It then organizes the data in a number of chunks equal to the
// number of sub-integrations and call the folding algorithm.
POOL_ALGORITHM_TYPED_TEST_P(FldoTester, test_sigproc_file)
{
    typedef typename TypeParam::TimeFrequencyType DataType;
    typedef typename DataType::value_type NumericalT;
    TypeParam traits;
    auto& api = traits.api(pool);

    int nsubints = 64;
    std::ifstream file_stream;
    std::stringstream ss;
    ss << "ska.dat";
    file_stream.exceptions (std::ifstream::failbit | std::ifstream::badbit );
    try
    {
        file_stream.open(panda::test::test_file(ss.str()), std::ios::binary);
        sigproc::SigProcHeader r;
        r.read(file_stream);
        file_stream.close();
        PANDA_LOG_DEBUG << "number of bits: " << r.number_of_bits();
        PANDA_LOG_DEBUG << "header size: " << r.size();
        //PANDA_LOG << "number of bits: " << r.number_of_bits();
        //PANDA_LOG << "header size: " << r.size();
        size_t nchannels = r.number_of_channels();
        size_t file_size = boost::filesystem::file_size(panda::test::test_file(ss.str()));
        uint64_t nsamples = (file_size - r.size()) * (8/r.number_of_bits());
        PANDA_LOG_DEBUG << "number of channels: " << nchannels;
        PANDA_LOG_DEBUG << "number of samples: " << nsamples;
        //-data::TimeType sampling_time =  r.sample_interval();
        //-double tobs = (nsamples/nchannels) * sampling_time.value();
        // We want the data to be a multiple also of the fldo::max_prebin value
        if((nsamples % (nsubints * nchannels * fldo::cuda::max_prebin)) != 0)
        {
            nsamples = (uint64_t) (
                                      nsamples
                                      /
                                      (
                                          ((float) nsubints)
                                          *
                                          nchannels
                                          *
                                          fldo::cuda::max_prebin
                                      )
                                  )
                                  *
                                  (
                                      nsubints
                                      *
                                      nchannels
                                      *
                                      fldo::cuda::max_prebin
                                  );
            //-tobs = (nsamples/nchannels) * sampling_time.value();
        }
        std::vector<std::shared_ptr<DataType>> tf_data;
        sigproc::Config config;
        // Set the data file name
        config.set_sigproc_files(panda::test::test_file(ss.str()));
        // Configure the chunk data dimension. The number it specifies has to be calculated as:
        // chunk_number_of_samples = number_of_sample_in_a_chunk/numer_of_channels
        // because the routine that allocates the chunk dimension (resize()) multiplies the value
        // of chunk_number_of_samples for the number of channels specified into the header
        config.set_chunk_samples(nsamples/nsubints/nchannels);
        sigproc::SigProcFileStream stream(config);
        panda::DataManager<sigproc::SigProcFileStream> chunk_manager(stream);
        stream.process();
        for(size_t i = 0; i < nsubints; ++i)
        {
            PANDA_LOG_DEBUG <<"Loading subint: " << i;
            std::tuple<std::shared_ptr<DataType>> data = chunk_manager.next();
            std::shared_ptr<DataType> time_frequency_data = std::get<0>(data);
            tf_data.emplace_back(time_frequency_data);
        }
        PANDA_LOG << "Loaded data from file!";
        data::Scl scl_data;
        // Load the list of pulsar candidates from a file
        load_candidates(scl_data);

        auto job = api(tf_data, scl_data);
        job->wait();
    }
    catch(std::ifstream::failure e)
    {
        std::stringstream s;
        s << "Exception accessing data file:"
          << e.what();
        PANDA_LOG_ERROR << s.str();
    }
}
#endif // DEEP_TESTING

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
#ifdef DEEP_TESTING
REGISTER_TYPED_TEST_SUITE_P(FldoTester, test_empty_data, test_gaussian_noise_data, test_sigproc_file);
#else
REGISTER_TYPED_TEST_SUITE_P(FldoTester, test_empty_data, test_gaussian_noise_data);
#endif

} // namespace test
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
