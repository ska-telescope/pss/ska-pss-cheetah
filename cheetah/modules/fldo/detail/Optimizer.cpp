#include "cheetah/modules/fldo/Optimizer.h"
#include <execution>

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {

template<typename TimeFrequencyType>
void Optimizer::operator()(data::OptimisedCandidate<>& candidate , Config const& config, std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data, data::Scl& scl_data, std::size_t number)
{
    // observation length
    _observation_length = tf_data[tf_data.size() - 1]->end_time()
                                                                + tf_data[0]->sample_interval()
                                                                - tf_data[0]->start_time();
    // get the period range
    float tsub = _observation_length.count()*86400000/config.number_of_subints();
    float tbin = scl_data[number].period().value()/config.number_of_phase_bins();

    // check that the period is smaller than tsub
    if (scl_data[number].period().value() >= tsub)
	    return;

    // assumes that the period does not deviate beyond 1% of the true value and goes over 512 values
    // DEV NOTE: computation of the step size and range for p and p-dot needs to change. This is a placeholder for now
    MsecTimeType halfprange = scl_data[number].period() * 0.01;
    MsecTimeType pstep = 2* halfprange * 0.001953125; // ms conversion factor
    auto period = scl_data[number].period();
    std::size_t npsteps = 2*halfprange/pstep;
    _prange.resize(npsteps);
    std::size_t index = 0;
    std::generate(_prange.begin(), _prange.end(), [&]() {++index; return (period - halfprange) + index*pstep;});

    // for now assume that p-dot never beyond 5% of the nominal value and we go over 32 values of p-dot
    // get the pdrange
    index = 0;
    _pdrange.resize(32);
    SecPerSecType halfpdrange = 0.05 * scl_data[number].pdot().value();
    SecPerSecType pdstep = 2*(halfpdrange)/2;
    auto pdot = scl_data[number].pdot();
    std::generate(_pdrange.begin(), _pdrange.end(), [&]() {++index; return pdot - halfpdrange + index*pdstep;});

    // compute the SNR for each P and P-dot
    auto snrs = std::vector<float>(_prange.size()* _pdrange.size());

    _profile.generate_profile(config, candidate);
    genstats(config);

    // generate stats for the nominal detected period
    for (std::size_t ii=0; ii < _prange.size(); ++ii)
    {
	for (std::size_t jj=0; jj < _pdrange.size(); ++jj)
	{
	     // calculate and apply bin shift based on p and p-dot
             auto modified_candidate = apply_bin_correction(config, candidate, scl_data[number].period(), _prange[ii], scl_data[number].pdot(), _pdrange[jj], tbin, tsub);

             // generate a profile for the folded data
	     _profile.generate_profile(config, modified_candidate);

	     // get snr
	     _pampindex = std::distance(_profile.folded_profile().begin(), std::max_element(_profile.folded_profile().begin(), _profile.folded_profile().end()));
	     compute_snr();
	     snrs[ii*_pdrange.size() + jj] = _snr;
	}
    }

    /* get optimal values */
    _profile.generate_profile(config, candidate);
    auto max_snr_idx = std::max_element(std::execution::par,snrs.begin(), snrs.end());

    /* get the period and p-dot corresponding to the max_snr*/
    std::size_t pindex = std::distance(snrs.begin(), max_snr_idx)/_pdrange.size();
    std::size_t pdindex = std::distance(snrs.begin(), max_snr_idx) - pindex*_pdrange.size();


    /* get the optimal snr and width */
    /* one would have to apply the bin correction once more here to get the correct SNR and width*/
    auto optimized_candidate = apply_bin_correction(config, candidate, scl_data[number].period(), _prange[pindex], scl_data[number].pdot(), _pdrange[pdindex], tbin, tsub);
    _profile.generate_profile(config, optimized_candidate);
    optimal_filter(config);

    scl_data[number].period(_prange[pindex]);
    scl_data[number].pdot(_pdrange[pdindex]);
    scl_data[number].sigma(_optimal_snr[_width - 1]);
    scl_data[number].width(MsecTimeType::from_value(_width * tbin));
}


} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
