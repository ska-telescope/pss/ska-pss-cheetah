/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_CPU_CONFIG_H
#define SKA_CHEETAH_MODULES_FLDO_CPU_CONFIG_H


#include "panda/ConfigActive.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {

/**
 * @brief
 *     Configuration options specific to the FLDO CPU algorithm
 */

class Config : public panda::ConfigActive
{
        typedef panda::ConfigActive BaseT;

    public:
        Config();
        /**
         *  @brief  Return the number of subints in a folded data cube
         *
         *  @return The number of subints in a folded data cube
         */
        std::size_t const& number_of_subints() const;

        /**
         *  @brief  Set the number of subints in a folded data cube
         *
         *  @param  num_subints  The new number of subints
         */
        void number_of_subints(std::size_t const& num_subints);

        /**
         *  @brief  Return the number of subbands in a folded data cube
         *
         *  @return The number of subbands in a folded data cube
         */
        std::size_t const& number_of_subbands() const;

        /**
         *  @brief  Set the number of subbands in a folded data cube
         *
         *  @param  num_subbands  The new number of subbands
         */
        void number_of_subbands(std::size_t const& num_subbands);

        /**
         *  @brief  Return the number of phase bins in a folded data cube
         *
         *  @return The number of phase bins in a folded data cube
         */
        std::size_t const& number_of_phase_bins() const;

        /**
         *  @brief  Set the number of phase bins in a folded data cube
         *
         *  @param  num_phase_bins  The new number of phase bins
         */
        void number_of_phase_bins(std::size_t const& num_phase_bins);

       /**
         *  @brief  Return the number of channels in a folded data cube
         *
         *  @return The number of channels in a folded data cube
         */
        std::size_t const& number_of_channels() const;

        /**
         *  @brief  Set the number of channels in a folded data cube
         *
         *  @param  num_channels  The new number of channels
         */
        void number_of_channels(std::size_t const& num_channels);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private: // members
        size_t _number_of_subints;
        size_t _number_of_subbands;
        size_t _number_of_phase_bins;
        size_t _number_of_channels;

};


} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FLDO_CPU_CONFIG_H
