/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {
namespace test {



template<typename FldoTraits>
FldoTestHelper<FldoTraits>::FldoTestHelper(fldo::Config const& config)
: BaseT(config)
,_config(config)
{
}

/* helper to fold one candidate */
template<typename FldoTraits>
std::shared_ptr<data::Ocld> FldoTestHelper<FldoTraits>::fold_helper(panda::PoolResource<Cpu>& device
                                                        , std::vector<std::shared_ptr<typename
                                                              cpu::Fldo<FldoTraits>::TimeFrequencyType>> const& tf_data
                                                        , data::Scl const& scl_data
                                                        , size_t const& candidate_number
                                                       )
{
    this->_optimised_candidate = std::make_shared<data::OptimisedCandidate<>>(
			data::DimensionSize<data::Time>(_config.cpu_algo_config().number_of_subints())
			, data::DimensionSize<data::Frequency>(_config.cpu_algo_config().number_of_subbands())
			, data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.cpu_algo_config().number_of_phase_bins())
			);
    //set all the metadata
    this->_optimised_candidate->metadata().period(scl_data[candidate_number].period());
    this->_optimised_candidate->metadata().dm(scl_data[candidate_number].dm());
    this->_optimised_candidate->metadata().pdot(scl_data[candidate_number].pdot());
    this->_optimised_candidate->metadata().harmonic(scl_data[candidate_number].harmonic());
    this->_optimised_candidate->metadata().width(scl_data[candidate_number].width());


    this->accumulate_data(device, tf_data, scl_data, candidate_number);
    auto ocld = std::make_shared<data::Ocld>();
    ocld->add(*(this->_optimised_candidate));
    return ocld;
}

template<typename FldoTraits>
void  FldoTestHelper<FldoTraits>::accumulate_data_helper(panda::PoolResource<Cpu>& device
                                                        , std::vector<std::shared_ptr<typename
                                                              cpu::Fldo<FldoTraits>::TimeFrequencyType>> const& tf_data
                                                        , data::Scl const& scl_data
                                                        , size_t const& candidate_number
                                                       )
{
    this->accumulate_data(device, tf_data, scl_data, candidate_number);
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::setup()
{
    // Create a buffer to keep track of how many samples
    // have been accumulated in the _temporary_sums_buffer
    this->_temporary_counts_buffer.resize(this->cpu_config().number_of_phase_bins(), 0);

    // Create a buffer in which to accumulate folded data
    this->_temporary_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<typename BaseT::TemporarySumsBufferType>>(
                                 data::DimensionSize<pss::astrotypes::units::PhaseAngle>(this->cpu_config().number_of_phase_bins())
                                 , data::DimensionSize<data::Frequency>(this->cpu_config().number_of_channels())
                             );

    // Create a buffer in which to store the final folded observation
    this->_accumulated_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<typename BaseT::AccumulatedSumsBufferType>>(
                                   data::DimensionSize<pss::astrotypes::units::PhaseAngle>(this->cpu_config().number_of_phase_bins())
                                   , data::DimensionSize<data::Frequency>(this->cpu_config().number_of_channels())
                               );

    // Create a buffer to keep track of how many samples
    // have been accumulated in the _accumulated_sums_buffer
    this->_accumulated_counts_buffer.resize(this->cpu_config().number_of_phase_bins(), 0);
}

template<typename FldoTraits>
fldo::Config const& FldoTestHelper<FldoTraits>::config() const
{
    return _config;
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::flush_buffers_helper()
{
    this->flush_buffers();
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::export_subint_helper(typename cpu::Fldo<FldoTraits>::Dm const& dm
                                                      , typename FldoTraits::FrequencyType const&
                                                            frequency_of_first_channel
						      , std::shared_ptr<typename FldoTraits::TimeFrequencyType> const& tf_block
                                                      , typename FldoTraits::TimePointType::duration const&
                                                            subint_length
                                                     )
{
    this->export_subint(dm
                        , frequency_of_first_channel
			, tf_block
                        , subint_length
                       );
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::create_optimised_candidate(uint16_t const& number_of_subints
                                                            , uint16_t const& number_of_subbands
                                                            , uint16_t const& number_of_phase_bins
                                                           )
{
    this->_optimised_candidate = std::make_shared<
                                     data::OptimisedCandidate<
                                     >
                                 >
                                 (
                                  data::DimensionSize<data::Time>(number_of_subints)
                                  , data::DimensionSize<data::Frequency>(number_of_subbands)
                                  , data::DimensionSize<pss::astrotypes::units::PhaseAngle>(number_of_phase_bins)
                                 );
}

template<typename FldoTraits>
std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<typename cpu::Fldo<FldoTraits>::TemporarySumsBufferType>>
        FldoTestHelper<FldoTraits>::temporary_sums_buffer()
{
    return this->_temporary_sums_buffer;
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::temporary_sums_buffer_create(data::DimensionSize<data::Frequency> num_channels)
{
    this->_temporary_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<
                                       typename cpu::Fldo<FldoTraits>::TemporarySumsBufferType>>
                                   (data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.cpu_algo_config().number_of_phase_bins())
                                    , data::DimensionSize<data::Frequency>(num_channels)
                                   );
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::temporary_sums_buffer_fill(size_t phase_bin_number
                                                        , std::vector<typename FldoTraits::NumericalRep> spectrum
                                                       )
{
    std::transform(spectrum.begin()
                   , spectrum.end()
                   , this->_temporary_sums_buffer->phase_bin(phase_bin_number).begin()
                   , this->_temporary_sums_buffer->phase_bin(phase_bin_number).begin()
                   , std::plus<typename cpu::Fldo<FldoTraits>::TemporarySumsBufferType>{}
                  );
}

template<typename FldoTraits>
std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<typename cpu::Fldo<FldoTraits>::AccumulatedSumsBufferType>>
        FldoTestHelper<FldoTraits>::accumulated_sums_buffer()
{
    return this->_accumulated_sums_buffer;
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::accumulated_sums_buffer_create(data::DimensionSize<data::Frequency> num_channels)
{
    this->_accumulated_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<
                                         typename cpu::Fldo<FldoTraits>::AccumulatedSumsBufferType>>
                                     (data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.cpu_algo_config().number_of_phase_bins())
                                      , data::DimensionSize<data::Frequency>(num_channels)
                                     );
}

template<typename FldoTraits>
std::vector<typename cpu::Fldo<FldoTraits>::TemporaryCountsBufferType>
        FldoTestHelper<FldoTraits>::temporary_counts_buffer()
{
    return this->_temporary_counts_buffer;
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::temporary_counts_buffer_create()
{
    this->_temporary_counts_buffer.resize(_config.cpu_algo_config().number_of_phase_bins(), 0);
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::temporary_counts_buffer_fill(std::vector<typename
                                                              cpu::Fldo<FldoTraits>::TemporaryCountsBufferType> counts
                                                         )
{
    this->_temporary_counts_buffer = counts;
}

template<typename FldoTraits>
std::vector<typename cpu::Fldo<FldoTraits>::AccumulatedCountsBufferType>
        FldoTestHelper<FldoTraits>::accumulated_counts_buffer()
{
    return this->_accumulated_counts_buffer;
}

template<typename FldoTraits>
void FldoTestHelper<FldoTraits>::accumulated_counts_buffer_create()
{
    this->_accumulated_counts_buffer.resize(_config.cpu_algo_config().number_of_phase_bins(), 0);
}

} // namespace test
} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
