/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_CPU_TEST_UTILS_FLDOTESTHELPER_H
#define SKA_CHEETAH_MODULES_FLDO_CPU_TEST_UTILS_FLDOTESTHELPER_H

#include "cheetah/modules/fldo/cpu/Fldo.h"
#include "cheetah/modules/fldo/Config.h"
#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {
namespace test {

/**
 * @brief Helper methods for testing CPU Fldo integration
 *
 * @details Methods to access private data members for unit tests. These data
 *          members will not need to be accessed externally during normal Fldo
 *          operation, so publicly-accessible getters are not wanted.
 */

template<typename FldoTraits>
class FldoTestHelper : public cpu::Fldo<FldoTraits>
{

    public:

	typedef cpu::Fldo<FldoTraits> BaseT;

	FldoTestHelper(fldo::Config const&);

	/**
	 * Setup the buffers
	 */
	void setup();

	/**
	 * return to config object
	 */
	fldo::Config const& config() const;

        /**
         * Define public functions to call private functions from Fldo for testing
         */

        // Call the accumulate_data() function
	void accumulate_data_helper(panda::PoolResource<Cpu>& // device
                                    , std::vector<std::shared_ptr<typename cpu::Fldo<FldoTraits>::TimeFrequencyType>>
                                          const& // tf_data
                                    , data::Scl const& // scl_data
                                    , size_t const& // candidate_number
                                   );

	std::shared_ptr<data::Ocld> fold_helper(panda::PoolResource<Cpu>& // device
                                    , std::vector<std::shared_ptr<typename cpu::Fldo<FldoTraits>::TimeFrequencyType>>
                                          const& // tf_data
                                    , data::Scl const& // scl_data
                                    , size_t const& // candidate_number
                                   );



        // Call the flush_buffers() function
        void flush_buffers_helper();

        // Call the export_subint() function
        void export_subint_helper(typename cpu::Fldo<FldoTraits>::Dm const& // dm
                                  , typename FldoTraits::FrequencyType const& // frequency_of_first_channel
			          , std::shared_ptr<typename FldoTraits::TimeFrequencyType> const& // tf_block
                                  , typename FldoTraits::TimePointType::duration const& // subint_length
                                 );

        /**
         * Define public functions to access private data members from Fldo for testing
         */

        // Create the _optimised_candidate
        void create_optimised_candidate(uint16_t const& number_of_subints
                                        , uint16_t const& number_of_subbands
                                        , uint16_t const& number_of_phase_bins
                                       );

        // Return the _temporary_sums_buffer
        std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<typename cpu::Fldo<FldoTraits>::TemporarySumsBufferType>>
                temporary_sums_buffer();
        // Create an empty _temporary_sums_buffer
        void temporary_sums_buffer_create(data::DimensionSize<data::Frequency>);
        // Copy data to a phase bin in the _temporary_sums_buffer
        void temporary_sums_buffer_fill(size_t
                                        , std::vector<typename FldoTraits::NumericalRep>
                                       );

        // Return the _accumulated_sums_buffer
        std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<typename cpu::Fldo<FldoTraits>::AccumulatedSumsBufferType>>
                accumulated_sums_buffer();
        // Create an empty _accumulated_sums_buffer
        void accumulated_sums_buffer_create(data::DimensionSize<data::Frequency>);

        // Return the _temporary_counts_buffer
        std::vector<typename cpu::Fldo<FldoTraits>::TemporaryCountsBufferType>
                temporary_counts_buffer();
        // Create an empty _temporary_counts_buffer
        void temporary_counts_buffer_create();
        // Copy data to the _temporary_counts_buffer
        void temporary_counts_buffer_fill(std::vector<typename cpu::Fldo<FldoTraits>::TemporaryCountsBufferType>);

        // Return the _accumulated_counts_buffer
        std::vector<typename cpu::Fldo<FldoTraits>::AccumulatedCountsBufferType>
                accumulated_counts_buffer();
        // Create an empty _accumulated_counts_buffer
        void accumulated_counts_buffer_create();

    private:
	fldo::Config const& _config;

};

} // namespace test
} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fldo/cpu/test_utils/detail/FldoTestHelper.cpp"

#endif // SKA_CHEETAH_MODULES_FLDO_CPU_TEST_UTILS_FLDOTESTHELPER_H
