@page skald_algorithm_guide SKALD algorithm guide
# Notes for developers
## SKALD CPU folding algorithm
The SKALD folding algorithm is a CPU-based folding algorithm. It works by folding each time sample, then transposing, dedispersing, and downsampling the data (scrunching) in frequency; we refer to this method as fold-then-dedisperse (FtD)

This differs from the standard folding algorithm which transposes data from time/frequency (TF) order to frequency/time (FT) order, folds each frequency channel, then dedisperses and scrunches the data; we refer to this as dedisperse-then-fold (DtF).

Omitting the tranpose from TF to FT is required to make the algorithm efficient enough for a CPU (here we mean any general TF->FT tranpose). Also, the standard method of on-the-fly dedispersion during the folding process (common in the DtF method) does not make optimal use of the CPU cache.

There is one caveat to the above use of FtD as opposed to DtF: the DtF algorithm is the mathematically correct one. For a pulsar signal present in the data with a ~low acceleration, the FtD method is equivalent to the DtF method. However, for a highly-accelerated pulsar, where the pulsar signal noticeably varies over one subintegration, the FtD method will cause pulse smearing. However, for the current SKA pulsar search specifications, and the currently-known highest-acceleration pulsar binary systems, this smearing is negligible. At the most extreme, the pulse smearing introduced by using the FtD method is on the order of 0.04 of a pulse period (assuming a subintegration length of 8.4 seconds, which is what is currently planned in the design).
