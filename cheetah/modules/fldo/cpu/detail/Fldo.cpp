/* The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/data/Units.h"
#include "cheetah/modules/fldo/CommonDefs.h"
#include "cheetah/modules/fldo/Optimizer.h"
#include "cheetah/modules/fldo/cpu/Fldo.h"
#include <execution>

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {

template<typename FldoTraits>
Fldo<FldoTraits>::Fldo(fldo::Config const& algo_config)
    : _config(algo_config.cpu_algo_config())
    , _current_subint(0)
    , _reference_time(utils::julian_days(0))
    , _nu(0)
    , _nudot(0)
{
    // Create a buffer to keep track of how many samples
    // have been accumulated in the _temporary_sums_buffer
    _temporary_counts_buffer.resize(algo_config.cpu_algo_config().number_of_phase_bins(), 0);

    // Create a buffer to keep track of how many samples
    // have been accumulated in the _accumulated_sums_buffer
    _accumulated_counts_buffer.resize(algo_config.cpu_algo_config().number_of_phase_bins(), 0);

}

template<typename FldoTraits>
Config& Fldo<FldoTraits>::cpu_config()
{
    return const_cast<Config&>(_config);
}

template<typename FldoTraits>
std::shared_ptr<data::Ocld>
    Fldo<FldoTraits>::operator()(panda::PoolResource<Cpu>& device
                                 , std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data
                                 , data::Scl const& scl_data
                                )
{
    // Create an empty Ocld to hold each folded candidate
    _ocld = std::make_shared<data::Ocld>();

    /** Fold each candidate present in the Scl. Each optimised candidate then gets added
     *  as an element to the Ocld, which is output once all of the folding is complete.
     *  If there are no candidates in the Scl, an empty Ocld will be returned.
     */
    if((tf_data.size() > 0) && (scl_data.size() > 0))
    {

        for(size_t candidate_number = 0; candidate_number < scl_data.size(); ++candidate_number)
        {

            // Create a new OptimisedCandidate object to hold this folded candidate
            _optimised_candidate = std::make_shared<data::OptimisedCandidate<>>(
                                       data::DimensionSize<data::Time>(_config.number_of_subints())
                                       , data::DimensionSize<data::Frequency>(_config.number_of_subbands())
                                       , data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.number_of_phase_bins())
                                   );
	    //set all the metadata
	    _optimised_candidate->metadata().dm(scl_data[candidate_number].dm());
	    _optimised_candidate->metadata().harmonic(scl_data[candidate_number].harmonic());
	    _optimised_candidate->metadata().period(scl_data[candidate_number].period());
            _optimised_candidate->metadata().pdot(scl_data[candidate_number].pdot());
            _optimised_candidate->metadata().width(scl_data[candidate_number].width());

            // Fold the input TimeFrequency data using the candidate parameters from the Scl
            Fldo<FldoTraits>::accumulate_data(device, tf_data, scl_data, candidate_number);


	    // This is where the optimizer class is constructed
	    fldo::Optimizer optimizer;

	    // run the optimization
	    optimizer.operator()<TimeFrequencyType>(*_optimised_candidate, _config, tf_data, const_cast<data::Scl&>(scl_data), candidate_number);


	    // memset optimized candidate to 0
	    std::fill(_optimised_candidate->begin(), _optimised_candidate->end(), 0.0);

	    // run the fold again with the optimized values
	    Fldo<FldoTraits>::accumulate_data(device, tf_data, scl_data, candidate_number);

	    // update metadata
	    _optimised_candidate->metadata().period(scl_data[candidate_number].period());
            _optimised_candidate->metadata().pdot(scl_data[candidate_number].pdot());
            _optimised_candidate->metadata().width(scl_data[candidate_number].width());

            // Add this folded candidate to the Ocld
            _ocld->add(*_optimised_candidate);
        }
    }

    return _ocld;

}

// Accumulate data from TimeFrequency block into PhaseFrequencyArray
template<typename FldoTraits>
void Fldo<FldoTraits>::accumulate_data(panda::PoolResource<Cpu>&
                                       , std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data
                                       , data::Scl const& scl_data
                                       , size_t const& candidate_number
                                      )
{
    // reset the current subint
    _current_subint = 0;

    // Verify that we've been passed a valid candidate number
    // before attempting to accumulate data for a candidate
    if(candidate_number >= scl_data.size())
    {
        panda::Error e("trying to fold candidate ");
        e << (candidate_number + 1) << ", only " << scl_data.size() << " candidate(s) in the candidate list";
        throw e;
    }

    // Create a buffer in which to accumulate folded data
    _temporary_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<TemporarySumsBufferType>>(
                                 data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.number_of_phase_bins())
                                 , data::DimensionSize<data::Frequency>(_config.number_of_channels())
                             );

    // Create a buffer in which to store the final folded observation
    _accumulated_sums_buffer = std::make_shared<pss::astrotypes::types::PhaseFrequencyArray<AccumulatedSumsBufferType>>(
                                   data::DimensionSize<pss::astrotypes::units::PhaseAngle>(_config.number_of_phase_bins())
                                   , data::DimensionSize<data::Frequency>(_config.number_of_channels())
                               );


    // Initialise the reference epoch of the observation, _reference_time, to
    // the middle of the observation (the entire observation across all subints)
    _reference_time = tf_data[0]->start_time()
                      + (tf_data[tf_data.size() - 1]->end_time()
                            + tf_data[0]->sample_interval()
                            - tf_data[0]->start_time()
                        )/2;

    // Set the frequency and frequency derivative of this candidate
    // based on the period and period derivative in the Scl
    _nu = 1.0/boost::units::quantity<data::Seconds, double>(scl_data[candidate_number].period());
    _nudot = -1.0 * scl_data[candidate_number].pdot()
                    /(boost::units::quantity<data::Seconds, double>(scl_data[candidate_number].period())
                        * boost::units::quantity<data::Seconds, double>(scl_data[candidate_number].period())
                     );

    float sample_subint = 0;
    // Calculate the length of the entire observation.
    // Note, tf_data[tf_data.size() - 1]->end_time() points to the
    // *start time* of the last sample (see end_time() definition in
    // TimeFrequencyCommon.cpp), so to get the end time, we need to add
    // the sampling time to get the true end time of the last sample.
    const typename TimePointType::duration observation_length = tf_data[tf_data.size() - 1]->end_time()
                                                                + tf_data[0]->sample_interval()
                                                                - tf_data[0]->start_time();

    // Calculate the length of one subint based on the observation length and the number of subints we requested
    const typename TimePointType::duration subint_length = observation_length/_config.number_of_subints();

    // Fold each time sample into its corresponding subint
    for(size_t data_chunk = 0; data_chunk < tf_data.size(); ++data_chunk)
    {
        for(size_t time_sample = 0; time_sample < tf_data[data_chunk]->number_of_spectra(); ++time_sample)
        {
            // Check in which subint this sample falls
            sample_subint =
                _config.number_of_subints()
                *
                (tf_data[data_chunk]->start_time(time_sample)
                 - tf_data[0]->start_time()
                )
                /
                observation_length;


            // If the sample falls outside of the subint being accumulated, copy the
            // contents of the PhaseFrequencyArray to a SubInt in in the output OptimisedCandidate
            if(floor(sample_subint) > _current_subint)
            {
                export_subint(scl_data[candidate_number].dm()                         // The DM of the candidate
                              , tf_data[data_chunk]->low_high_frequencies().second    // The frequency of the first
                                                                                      // (highest-frequency) channel
                              , tf_data[data_chunk]   // reference to the shared_ptr of the TF block
			      , subint_length                                             // Length of each subint
                             );
            }

            const boost::units::quantity<data::Seconds, double> ref_time_offset =
                pss::astrotypes::units::duration_cast<boost::units::quantity<data::Seconds, double>>(
                    tf_data[data_chunk]->start_time(time_sample)
- tf_data[0]->start_time()
//                                                                  - _reference_time // TODO, probably didn't work because I'd not set the start times of any TimeFrequency chunk
                );

            // DEV NOTE: INSTEAD OF CALCULATING PHASE (WHICH GETS MODULO'D) AND
            // THEN PHASE BIN (WHICH HAS ISSUES CURRENTLY BECAUSE OF A BUG IN
            // THE ModuloOne CLASS IN ASTROTYPES), CALCULATE PHASE BIN DIRECTLY
            // (WHICH IS AN int AND THUS HAS NO RELIANCE ON ASTROTYPES) AND
            // THEN MODULO THAT)
            const uint16_t phase_bin =
                std::fmod(
                    (
                        (float)(_nu * ref_time_offset
                                   + 0.5 * _nudot
                                   * ref_time_offset * ref_time_offset
                               ).value()
                               * _config.number_of_phase_bins()
                    )
                    , _config.number_of_phase_bins()
                );

            if(_temporary_counts_buffer[phase_bin] >= _accumulated_bin_capacity)
            {
                flush_buffers();
            }
            ++_temporary_counts_buffer[phase_bin];

            // Add all channels for this sample to a sample in a temporary buffer
            std::transform(std::execution::par, tf_data[data_chunk]->spectrum(time_sample).begin()
                           , tf_data[data_chunk]->spectrum(time_sample).end()
                           , _temporary_sums_buffer->phase_bin(phase_bin).begin()
                           , _temporary_sums_buffer->phase_bin(phase_bin).begin()
                           , std::plus<TemporarySumsBufferType>{}
                          );

        }
    }

    /**
     *  Check if we are at the final subint. If we are
     *  then export the data since the last subint will
     *  never get exported based on the logic above.
     *  Note that this does not take into account whether
     *  the entire subint is filled.
     */
    if (floor(sample_subint) + 1  == _config.number_of_subints())
    {
        export_subint(scl_data[candidate_number].dm()
                      , tf_data[tf_data.size() - 1]->low_high_frequencies().second
		      ,  tf_data[0]
                      , subint_length
                     );
    }
}

// Export a finished sub-integration to the attached Candidate object
template<typename FldoTraits>
void Fldo<FldoTraits>::export_subint(Dm const& dm
                                     , FrequencyType const& frequency_of_first_channel
				     , std::shared_ptr<TimeFrequencyType> const& tf_block
                                     , typename TimePointType::duration const& subint_length
                                    )
{

    // Flush data from temporary data-size-limited PhaseFrequencyArray into final PhaseFrequencyArray
    flush_buffers();

    // Normalise data in each channel by the number of counts in each phase bin
    for(size_t phase_bin = 0; phase_bin < _config.number_of_phase_bins(); ++phase_bin)
    {
        const AccumulatedCountsBufferType bin_count = _accumulated_counts_buffer[phase_bin];
        const float normalisation_factor = bin_count > 0 ? 1.0/bin_count : 1.0;
        for(auto channel_iter = _accumulated_sums_buffer->phase_bin(phase_bin).begin();
                  channel_iter != _accumulated_sums_buffer->phase_bin(phase_bin).end();
                  ++channel_iter
            )
        {
            *channel_iter *= normalisation_factor;
        }
    }

    // Send the normalised data for dedispersion and frequency-scrunching
    ingest_phase_frequency_array_data(_accumulated_sums_buffer
                            , dm
                            , frequency_of_first_channel
                            , _current_subint
			    , tf_block
                            , subint_length
                           );

    // Zero the accumulated sums and counts buffers and move to the next subint
    std::fill(std::execution::par_unseq, _accumulated_sums_buffer->begin(), _accumulated_sums_buffer->end(), 0);
    std::fill(std::execution::par_unseq, _accumulated_counts_buffer.begin(), _accumulated_counts_buffer.end(), 0);
    ++_current_subint;

}

// Add sums buffer into final sums, reset sums buffer to zero
// Add counts buffer into final counts, reset counts buffer to zero
template<typename FldoTraits>
void Fldo<FldoTraits>::flush_buffers()
{

    // Add the data from the temporary sums buffer to the accumulated sums buffer
    std::transform(std::execution::par, _temporary_sums_buffer->begin()
                   , _temporary_sums_buffer->end()
                   , _accumulated_sums_buffer->begin()
                   , _accumulated_sums_buffer->begin()
                   , std::plus<AccumulatedSumsBufferType>{}
    );

    // Clear the temporary sums buffer
    std::fill(std::execution::par_unseq, _temporary_sums_buffer->begin(), _temporary_sums_buffer->end(), 0);

    // Add the counts from the temporary counts buffer to the accumulated counts buffer
    std::transform(std::execution::par, _temporary_counts_buffer.begin()
                   , _temporary_counts_buffer.end()
                   , _accumulated_counts_buffer.begin()
                   , _accumulated_counts_buffer.begin()
                   , std::plus<AccumulatedCountsBufferType>{}
    );
    // Clear the temporary counts buffer
    std::fill(std::execution::par_unseq, _temporary_counts_buffer.begin(), _temporary_counts_buffer.end(), 0);

}

// Ingest phase_frequency_array data (in PF order) into subint number current_subint
template<typename FldoTraits>
void Fldo<FldoTraits>::ingest_phase_frequency_array_data(std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<AccumulatedSumsBufferType>>
                                                   const& normalised_sums_buffer
                                               , Dm const& dm
                                               , FrequencyType const& frequency_of_first_channel
                                               , uint16_t const& current_subint
					       , std::shared_ptr<TimeFrequencyType> const& tf_data
                                               , typename TimePointType::duration const& subint_length
                                              )
{
    // Verify the number frequency channels is equal to the data
    if (_config.number_of_channels() != tf_data->number_of_channels())
    {
	throw std::invalid_argument("The number of channels must match the data");
    }

    // Verify the number of subbands is set so that each
    // subband has an equal number of frequency channels
    if(_config.number_of_channels() % _config.number_of_subbands())
    {
        throw std::invalid_argument("The number of channels must divide evenly into the number of subbands");
    }

    // Define the dispersion constant, i.e. e^2/2*pi*m(e)*c
    const auto dispersion_constant = fldo::k_dm
                                     * data::megahertz
                                     * data::megahertz
                                     / data::parsecs_per_cube_cm
                                     * boost::units::si::seconds;

    // Define start time and end time of a subint based on the current subint and the length of a subint
    const typename TimePointType::duration subint_start_time = current_subint * subint_length;
    const typename TimePointType::duration subint_end_time = subint_start_time + subint_length;

    // Calculate the average spin frequency across the subint
    const boost::units::quantity<data::Hertz, double>
        average_spin_frequency =
            0.5 * (2 * _nu
                   + _nudot
                   * (
                       -1 * boost::units::quantity<data::Seconds, double>::from_value(
                           ((_reference_time - subint_start_time).time_since_epoch()).count()
                       )
                       -
                       boost::units::quantity<data::Seconds, double>::from_value(
                           ((_reference_time - subint_end_time).time_since_epoch()).count()
                       )
                     )
                  );

    // Total number of channels divided by the total number
    // of subbands, i.e. the number of channels per subband
    const uint16_t channels_per_subband = _config.number_of_channels()/_config.number_of_subbands();

    for(size_t subband_index = 0; subband_index < _config.number_of_subbands(); ++subband_index)
    {

        // Calculate the indices of the first and last
        // frequency channels in the current frequency subband
        const uint16_t first_channel_index =
                         subband_index * channels_per_subband;
        const uint16_t last_channel_index =
                         first_channel_index + channels_per_subband;

        for(size_t channel_index = first_channel_index; channel_index < last_channel_index; ++channel_index)
        {
            // Calculate the dispersion delay between this channel and the first channel
            const boost::units::quantity<data::Seconds, double>
                dispersion_delay =
                    dispersion_constant
                    * dm
                    * (
                          ((1/tf_data->channel_frequencies()[channel_index])
                              * (1/tf_data->channel_frequencies()[channel_index])
                          )
                          -
                          ((1/frequency_of_first_channel)
                              * (1/frequency_of_first_channel)
                          )
                      );

            // Calculate the dispersion delay as a number of phase bins
            const double dispersion_delay_in_phase_bins =
                round((dispersion_delay
                       * average_spin_frequency
                       * (double) _config.number_of_phase_bins() // Dev note: double is
                                                         // required here for the
                                                         // multiply_typeof_helper
                      ).value()
		     );

            fused_rollback_accumulate(normalised_sums_buffer->channel(channel_index)
                                      , dispersion_delay_in_phase_bins
                                      , current_subint
                                      , subband_index
                                     );

        }

    }

}

template<typename FldoTraits>
void Fldo<FldoTraits>::fused_rollback_accumulate(pss::astrotypes::types::PhaseFrequencyArray<
                                                           AccumulatedSumsBufferType
                                                       >::Channel
                                                       const& profile
                                                 , uint16_t const& channel_shift
                                                 , uint16_t const& subint_number
                                                 , uint16_t const& subband_number
                                                )
{

    // Calculate an index, based on how many phase bins this channel
    // will be shifted, that will be where the phase bins in the
    // unshifted (dispersed) channel reach the end of the output
    // subband to which they are being added (after dedispersion)
    const uint16_t profile_wrap_index = channel_shift % _config.number_of_phase_bins();
    // Calculate an index in the output subband where the start
    // of the unshifted channel will fall after dedispersion
    const uint16_t profile_start_in_subband = _config.number_of_phase_bins() - profile_wrap_index;

    // Get the subint to which we want to add these data from the output candidate
    auto output_subint = _optimised_candidate->subint(subint_number);
    // Get the subband to which we want to add these data from this subint
    auto output_subband = output_subint.slice(data::DimensionSpan<data::Frequency>(
                                                   data::DimensionIndex<data::Frequency>(subband_number)
                                                   , data::DimensionSize<data::Frequency>(1)
                                                  )
                                              , data::DimensionSpan<pss::astrotypes::units::PhaseAngle>(
                                                   output_subint.template dimension<pss::astrotypes::units::PhaseAngle>()
                                                  )
                                             );

    // Create an iterator that points to the beginning of this
    // "profile" (all phase bins across this frequency channel)
    auto profile_wrap = profile.begin();
    // Move the iterator so that it points to what will be the end of
    // the input profile in the output subband (after dedispersion)
    std::advance(profile_wrap, profile_wrap_index);

    // Add the input profile (from it's beginning to where we've calculated
    // it will reach the end of the output subband) to the output subband,
    // starting at the index we've calculated based on how many phase bins
    // the data need to be shifted to correct for dedispersion
    std::transform(profile.begin()
                   , profile_wrap
                   , output_subband.begin() += profile_start_in_subband
                   , output_subband.begin() += profile_start_in_subband
                   , std::plus<float>{}
                  );

    // Add the input profile (starting from where we left off above
    // and adding the rest of the profile to its end) to the beginning
    // of the output subband
    std::transform(profile_wrap
                   , profile.end()
                   , output_subband.begin()
                   , output_subband.begin()
                   , std::plus<float>{}
                  );
}

} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
