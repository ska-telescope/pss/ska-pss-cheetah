/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fldo/cpu/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {


Config::Config()
    : BaseT("cpu", "use cpu algorithm for folding")
    , _number_of_subints(64) // default of 64 subints chosen based on the
			     // expected ~536-second observation length
    , _number_of_subbands(64) // default of 64 subbands chosen based on the
			      // expected 4096 input frequency channels
    , _number_of_phase_bins(128) // default of 128 phase bins chosen based
				 // on the expected sampling time of 64 us
    , _number_of_channels(1024)
{
}

std::size_t const& Config::number_of_subints() const
{
    return _number_of_subints;
}

void Config::number_of_subints(std::size_t const& num_subints)
{
    _number_of_subints = num_subints;
}

std::size_t const& Config::number_of_subbands() const
{
    return _number_of_subbands;
}

void Config::number_of_subbands(std::size_t const& num_subbands)
{
    _number_of_subbands = num_subbands;
}

std::size_t const& Config::number_of_phase_bins() const
{
    return _number_of_phase_bins;
}

void Config::number_of_phase_bins(std::size_t const& num_phase_bins)
{
    _number_of_phase_bins = num_phase_bins;
}

std::size_t const& Config::number_of_channels() const
{
    return _number_of_channels;
}

void Config::number_of_channels(std::size_t const& num_channels)
{
    _number_of_channels = num_channels;
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    BaseT::add_options(add_options);
    add_options("number_of_subints", boost::program_options::value<std::size_t>(&_number_of_subints)->default_value(_number_of_subints), "The number of subints to use for folding.");
    add_options("number_of_subbands", boost::program_options::value<std::size_t>(&_number_of_subbands)->default_value(_number_of_subbands), "The number of frequency subbands to use for folding.");
    add_options("number_of_phase_bins", boost::program_options::value<std::size_t>(&_number_of_phase_bins)->default_value(_number_of_phase_bins), "The number of phase bins for the folded profile.");
    add_options("number_of_frequency_channels", boost::program_options::value<std::size_t>(&_number_of_channels)->default_value(_number_of_channels), "Total number of frequency channels.");
}

} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
