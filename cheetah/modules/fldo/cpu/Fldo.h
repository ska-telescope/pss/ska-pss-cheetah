/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_CPU_FLDO_H
#define SKA_CHEETAH_MODULES_FLDO_CPU_FLDO_H

#include "cheetah/data/NuDot.h"
#include "cheetah/data/Units.h"
#include "cheetah/modules/fldo/Config.h"
#include "cheetah/modules/fldo/cpu/Config.h"
#include "cheetah/modules/fldo/Types.h"
#include "pss/astrotypes/units/Time.h"
#include "pss/astrotypes/types/PhaseFrequencyArray.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {

/**
 * @brief CPU-based pulsar folding algoithm
 * @details A CPU-based pulsar folding algorithm that is specifically tailored
 *          to be fast enough to meet the timing constraints of the Fldo module.
 *          It does this by:
 *
 *              1) using a temporary buffer to store additions that has a numeric
 *                 type that is smaller than that of the final storage buffer;
 *
 *              2) transposing the phase/frequency data (the array stored in
 *                 each time subintegration of the output candidate) just
 *                 before dedispersing the data, as opposed to transposing the
 *                 time/frequency when the data are initially read in.
 */

template<typename FldoTraits>
class Fldo
{

    public:
        typedef cheetah::Cpu Architecture;
        typedef cpu::Config Config;
        typedef FldoTraits Traits;
        typedef data::Candidate<Cpu, float> CandidateType;

        typedef typename CandidateType::Dm Dm;
        typedef typename Traits::TimeFrequencyType TimeFrequencyType;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimePointType TimePointType;
        typedef typename TimeFrequencyType::value_type NumericalT;

        /**
         * NOTE: The numeric type used for the "counts" and "sums" buffers
         *       impacts performance measurably. Using the shortest possible
         *       type (uint16_t) gives the best results.
         */

        // The numeric type of the temporary buffer into which folded time samples
        // are added. This may not have enough numeric precision to hold an entire
        // subintegration's worth of folded data, but is constrained for performance
        // purposes.
        typedef uint16_t TemporarySumsBufferType;
        // The numeric type of the final buffer for storing folded time samples
        // for an entire subintegration.
        typedef float AccumulatedSumsBufferType;
        // The numeric type of the temporary buffer into which the number of time
        // samples added to a particular phase bin are stored. This may not have
        // enough numeric precision to hold an entire subintegration's worth of
        // phase-bin counts, but is constrained for performance purposes.
        typedef uint16_t TemporaryCountsBufferType;
        // The numeric type of the final buffer for storing the number of time
        // samples added to a particular phase bin for an entire subintegration.
        typedef uint32_t AccumulatedCountsBufferType;

    public:
        Fldo(fldo::Config const&);
        Fldo(Fldo const&) = delete;
        Fldo(Fldo&&) = default;

        Config& cpu_config();
        /**
         * @brief Performs the folding operation on the provided data for the given candidate
         */
        std::shared_ptr<data::Ocld>
            operator()(panda::PoolResource<Cpu>& device
                       , std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data
                       , data::Scl const& scl_data
                      );

    protected:

        /**
         *  DEV NOTE: The methods and variables below in this protected stanza
         *            are protected because they need to be accessed as part of
         *            testing, which is done via the FldoTestHelper class, which
         *            inherits from Fldo
         */

        void accumulate_data(panda::PoolResource<Cpu>& // device
                             , std::vector<std::shared_ptr<TimeFrequencyType>> const& // tf_data
                             , data::Scl const& // scl_data
                             , size_t const& // candidate_number
                            );
        void export_subint(Dm const&                        // dm
                           , FrequencyType const&           // frequency_of_first_channel
	                   , std::shared_ptr<TimeFrequencyType> const& tf_data
                           , typename TimePointType::duration const& // subint_length
                          );
        void flush_buffers();
        void ingest_phase_frequency_array_data(std::shared_ptr<
                                             pss::astrotypes::types::PhaseFrequencyArray<AccumulatedSumsBufferType>
                                         > const& // normalised_sums_buffer
                                     , Dm const& // dm
                                     , FrequencyType const& // frequency_of_first_channel
                                     , uint16_t const& // current_subint
				     , std::shared_ptr<TimeFrequencyType> const& tf_data
                                     , typename TimePointType::duration const& // subint_length
                                    );
        void fused_rollback_accumulate(pss::astrotypes::types::PhaseFrequencyArray<
                                               AccumulatedSumsBufferType
                                           >::Channel const& // profile
                                       , uint16_t const& // channel_shift
                                       , uint16_t const& // subint_number
                                       , uint16_t const& // subband_number
                                      );

        std::vector<TemporaryCountsBufferType> _temporary_counts_buffer;
        std::vector<AccumulatedCountsBufferType> _accumulated_counts_buffer;
        std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<TemporarySumsBufferType>> _temporary_sums_buffer;
        std::shared_ptr<pss::astrotypes::types::PhaseFrequencyArray<AccumulatedSumsBufferType>> _accumulated_sums_buffer;
        std::shared_ptr<data::OptimisedCandidate<>> _optimised_candidate;

    private:

        // The maximum number of samples that can go into a single "sums" buffer
        // phase bin before there is a risk of numeric overflow.
	Config const& _config;
        static constexpr size_t _accumulated_bin_capacity =
            std::numeric_limits<TemporarySumsBufferType>::max()
            /
            std::numeric_limits<uint8_t>::max();

	std::shared_ptr<data::Ocld> _ocld;

	size_t _current_subint;
        // The reference time (in seconds since the beginning of
        // an observation) at which the reported candidate period
        // is valid (since the actual period will change over the
        // observation due to the candidate's period derivative).
        TimePointType _reference_time;
        boost::units::quantity<data::Hertz, double> _nu;
        data::NuDotType<double> _nudot;

};

} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fldo/cpu/detail/Fldo.cpp"

#endif // SKA_CHEETAH_MODULES_FLDO_CPU_FLDO_H
