/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_CPU_TEST_CPUTEST_H
#define SKA_CHEETAH_MODULES_FLDO_CPU_TEST_CPUTEST_H

#include "cheetah/data/Units.h"
#include "cheetah/modules/fldo/cpu/Fldo.h"
#include "cheetah/modules/fldo/cpu/test_utils/FldoTestHelper.h"
#include "cheetah/modules/fldo/test_utils/FldoTester.h"
#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {
namespace test {

struct Dummy;

struct CpuTraits : public fldo::test::FldoTesterTraits<
                              fldo::cpu::Fldo<
                                  fldo::FldoTraits<Dummy, Dummy, uint16_t>
                              >
                          >
{
};

/**
 * @brief Class to test the CPU version of Fldo
 *
 * @details
 *
 */
class CpuTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        CpuTest();
        ~CpuTest();
};

} // namespace test
} // namespace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FLDO_CPU_TEST_CPUTEST_H
