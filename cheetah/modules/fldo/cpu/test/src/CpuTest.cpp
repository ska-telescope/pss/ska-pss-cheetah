/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/fldo/cpu/test/CpuTest.h"
#include "cheetah/data/Units.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cpu {
namespace test {

CpuTest::CpuTest()
    : ::testing::Test()
{
}

CpuTest::~CpuTest()
{
}

void CpuTest::SetUp()
{
}

void CpuTest::TearDown()
{
}

namespace {

    data::Scl create_scl(double period, float dm)
    {
        data::Scl::CandidateType::MsecTimeType  candidate_period(period * boost::units::si::seconds);
        data::Scl::CandidateType::SecPerSecType candidate_pdot = 0;
        data::Scl::CandidateType::Dm            candidate_dm = dm * data::parsecs_per_cube_cm;
        uint8_t                                 candidate_harmonic = 1;
        data::Scl::CandidateType::MsecTimeType  candidate_width(0.1 * boost::units::si::seconds);
        data::Scl::CandidateType candidate(candidate_period
                                           , candidate_pdot
                                           , candidate_dm
                                           , candidate_harmonic
                                           , candidate_width
                                           , 10.0
                                           , 1
                                          );

        data::Scl scl;
        scl.push_back(candidate);
        return scl;
    }

    template<typename TimeFrequencyType>
    std::shared_ptr<TimeFrequencyType> create_tf_data(uint16_t time_dim, uint16_t freq_dim, float samp_time)
    {
        std::shared_ptr<TimeFrequencyType> tf_data = TimeFrequencyType::make_shared(
                                                         data::DimensionSize<data::Time>(time_dim),
                                                         data::DimensionSize<data::Frequency>(freq_dim)
                                                     );

        tf_data->sample_interval(samp_time * data::second);

        return tf_data;
    }

    template<typename Traits>
    cpu::test::FldoTestHelper<Traits> create_fldo_test_helper(uint16_t num_subints
                                                              , uint16_t num_subbands
                                                              , uint16_t num_phase_bins
							      , uint16_t num_channels
							      , fldo::Config& config
                                                             )
    {
        // Create a Fldo helper object so we can access private members of the class for testing
        cpu::test::FldoTestHelper<Traits> test_helper(config);

       // Set the output dimensions of the folded data cube
	test_helper.cpu_config().number_of_subints(num_subints);
        test_helper.cpu_config().number_of_subbands(num_subbands);
        test_helper.cpu_config().number_of_phase_bins(num_phase_bins);
	test_helper.cpu_config().number_of_channels(num_channels);

       // set up the helper with new parameters
        test_helper.setup();

        return test_helper;
    }

} // namespace

/*TEST(CpuTest, test_accumulation)
{

    // Set candidate metadata so that the first 5 time samples fall in the 5 phase
    // bins consecutively, and the next 5 time samples fall in those same phase bins
    data::Scl scl = create_scl(1, 1);

    // Create a TimeFrequency data chunk of a certain size (twice
    // the number of time samples as there are phase bins (5)).
    // Set the sampling time of the TimeFrequency chunk (must match
    // the period so that the folding happens as expected below).
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data =
        create_tf_data<CpuTraits::TimeFrequencyType>(10, 10, 0.2);

    for(size_t time_sample = 0; time_sample < time_frequency_data->number_of_spectra(); ++time_sample)
    {
        // Create a vector with a length that matches the number of channels in the data
        std::vector<CpuTraits::NumericalRep> reference_data(time_frequency_data->number_of_channels());

        // Fill this new vector with value increasing from 1 (11, 21, etc...)
        std::iota(reference_data.begin(), reference_data.end(), time_sample * time_frequency_data->number_of_channels() + 1);

        // Use the TimeFrequency iterator interface to generate data with a known pattern
        std::copy(reference_data.begin(), reference_data.end(), time_frequency_data->spectrum(time_sample).begin());
    }

    // Verify that each channel of a random spectrum
    // has been assigned the increasing numbers we expect
    std::vector<CpuTraits::NumericalRep> test_vector(time_frequency_data->number_of_channels());
    std::iota(test_vector.begin(), test_vector.end(), 21);
    ASSERT_TRUE(std::equal(time_frequency_data->spectrum(2).begin()
                           , time_frequency_data->spectrum(2).end()
                           , test_vector.begin()
                          )
               );



    std::vector<std::shared_ptr<CpuTraits::TimeFrequencyType>> tf_data;

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.emplace_back(time_frequency_data);

    panda::PoolResource<Cpu> device(1);

    // Create a Fldo helper object so we can access private members of the class for testing

    fldo::Config config;
    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(1, 1, 5, 10, config);*/

    /**
     *  Call Fldo::accumulate_data() given the data we created above
     */
    /*test_helper.accumulate_data_helper(device, tf_data, scl, 0);

    // Get the _temporary_sums_buffer
    auto temporary_sums_buffer = test_helper.temporary_sums_buffer();

    // Create a vector that holds data which we know the
    // PhaseFrequencyArray should hold in the first phase bin
    std::vector<CpuTraits::NumericalRep> results_reference_0 {
                                             static_cast<CpuTraits::NumericalRep>(52),
                                             static_cast<CpuTraits::NumericalRep>(54),
                                             static_cast<CpuTraits::NumericalRep>(56),
                                             static_cast<CpuTraits::NumericalRep>(58),
                                             static_cast<CpuTraits::NumericalRep>(60),
                                             static_cast<CpuTraits::NumericalRep>(62),
                                             static_cast<CpuTraits::NumericalRep>(64),
                                             static_cast<CpuTraits::NumericalRep>(66),
                                             static_cast<CpuTraits::NumericalRep>(68),
                                             static_cast<CpuTraits::NumericalRep>(70)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the result PhaseFrequencyArray
    ASSERT_TRUE(results_reference_0.size() == temporary_sums_buffer->phase_bin(0).data_size());

    // Check the results with what we know the PhaseFrequencyArray
    // should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(0).begin()
                           , temporary_sums_buffer->phase_bin(0).end()
                           , results_reference_0.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // PhaseFrequencyArray should hold in the second phase bin
    std::vector<CpuTraits::NumericalRep> results_reference_1 {
                                             static_cast<CpuTraits::NumericalRep>(72),
                                             static_cast<CpuTraits::NumericalRep>(74),
                                             static_cast<CpuTraits::NumericalRep>(76),
                                             static_cast<CpuTraits::NumericalRep>(78),
                                             static_cast<CpuTraits::NumericalRep>(80),
                                             static_cast<CpuTraits::NumericalRep>(82),
                                             static_cast<CpuTraits::NumericalRep>(84),
                                             static_cast<CpuTraits::NumericalRep>(86),
                                             static_cast<CpuTraits::NumericalRep>(88),
                                             static_cast<CpuTraits::NumericalRep>(90)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the result PhaseFrequencyArray
    ASSERT_TRUE(results_reference_1.size() == temporary_sums_buffer->phase_bin(1).data_size());

    // Check the results with what we know the PhaseFrequencyArray
    // should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(1).begin()
                           , temporary_sums_buffer->phase_bin(1).end()
                           , results_reference_1.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // PhaseFrequencyArray should hold in the third phase bin
    std::vector<CpuTraits::NumericalRep> results_reference_2 {
                                             static_cast<CpuTraits::NumericalRep>(92),
                                             static_cast<CpuTraits::NumericalRep>(94),
                                             static_cast<CpuTraits::NumericalRep>(96),
                                             static_cast<CpuTraits::NumericalRep>(98),
                                             static_cast<CpuTraits::NumericalRep>(100),
                                             static_cast<CpuTraits::NumericalRep>(102),
                                             static_cast<CpuTraits::NumericalRep>(104),
                                             static_cast<CpuTraits::NumericalRep>(106),
                                             static_cast<CpuTraits::NumericalRep>(108),
                                             static_cast<CpuTraits::NumericalRep>(110)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the result PhaseFrequencyArray
    ASSERT_TRUE(results_reference_2.size() == temporary_sums_buffer->phase_bin(2).data_size());

    // Check the results with what we know the PhaseFrequencyArray
    // should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(2).begin()
                           , temporary_sums_buffer->phase_bin(2).end()
                           , results_reference_2.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // PhaseFrequencyArray should hold in the fourth phase bin
    std::vector<CpuTraits::NumericalRep> results_reference_3 {
                                             static_cast<CpuTraits::NumericalRep>(112),
                                             static_cast<CpuTraits::NumericalRep>(114),
                                             static_cast<CpuTraits::NumericalRep>(116),
                                             static_cast<CpuTraits::NumericalRep>(118),
                                             static_cast<CpuTraits::NumericalRep>(120),
                                             static_cast<CpuTraits::NumericalRep>(122),
                                             static_cast<CpuTraits::NumericalRep>(124),
                                             static_cast<CpuTraits::NumericalRep>(126),
                                             static_cast<CpuTraits::NumericalRep>(128),
                                             static_cast<CpuTraits::NumericalRep>(130)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the result PhaseFrequencyArray
    ASSERT_TRUE(results_reference_3.size() == temporary_sums_buffer->phase_bin(3).data_size());

    // Check the results with what we know the PhaseFrequencyArray
    // should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(3).begin()
                           , temporary_sums_buffer->phase_bin(3).end()
                           , results_reference_3.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // PhaseFrequencyArray should hold in the fifth phase bin
    std::vector<CpuTraits::NumericalRep> results_reference_4 {
                                             static_cast<CpuTraits::NumericalRep>(132),
                                             static_cast<CpuTraits::NumericalRep>(134),
                                             static_cast<CpuTraits::NumericalRep>(136),
                                             static_cast<CpuTraits::NumericalRep>(138),
                                             static_cast<CpuTraits::NumericalRep>(140),
                                             static_cast<CpuTraits::NumericalRep>(142),
                                             static_cast<CpuTraits::NumericalRep>(144),
                                             static_cast<CpuTraits::NumericalRep>(146),
                                             static_cast<CpuTraits::NumericalRep>(148),
                                             static_cast<CpuTraits::NumericalRep>(150)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the result PhaseFrequencyArray
    ASSERT_TRUE(results_reference_4.size() == temporary_sums_buffer->phase_bin(4).data_size());

    // Check the results with what we know the PhaseFrequencyArray
    // should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(4).begin()
                           , temporary_sums_buffer->phase_bin(4).end()
                           , results_reference_4.begin()
                          )
               );

}*/

/**
 * This will test flush_buffers() by running accumulate_data() until flush_buffers() is naturally called
 */


/*TEST(CpuTest, test_flush_buffers_via_accumulate_data)
{

    // Set candidate metadata so that the first 5 time samples fall in the 5 phase
    // bins consecutively, and the next 5 time samples fall in those same phase bins
    data::Scl scl = create_scl(1, 1);

    // Create a TimeFrequency data chunk with a large number of time samples.
    // To force a call to flush_buffers(), we will need to add many time samples
    // to the same phase bin. Set the sampling time of the TimeFrequency chunk
    // to match the period so that the folding happens as expected below.
    // NB: the numerical pattern is set so that the buffers will fill after
    // 1286 time samples, but we add some empty time samples after that to
    // keep export_subint()from running immediately.
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data =
//        create_tf_data<CpuTraits::TimeFrequencyType>(1286, 10, 0.2);
        create_tf_data<CpuTraits::TimeFrequencyType>(1287, 10, 0.2);

    for(size_t time_sample = 0; time_sample < 1286; ++time_sample)
    {
        // Create a vector with a length that matches the number of channels in the data
        std::vector<CpuTraits::NumericalRep> reference_data(time_frequency_data->number_of_channels());

        // Fill this new vector with value increasing from 1 (11, 21, etc...)
        std::iota(reference_data.begin(), reference_data.end(), time_sample * time_frequency_data->number_of_channels() + 1);

        // Use the TimeFrequency iterator interface to generate data with a known pattern
        std::copy(reference_data.begin(), reference_data.end(), time_frequency_data->spectrum(time_sample).begin());
    }
    // Fill the rest of the TimeFrequency block with zeros
    for(size_t time_sample = 1286; time_sample < time_frequency_data->number_of_spectra(); ++time_sample)
    {
        // Create a vector with a length that matches the number of channels in the data
        std::vector<CpuTraits::NumericalRep> reference_data(time_frequency_data->number_of_channels());
        // Fill this new vector with zeros
        std::fill(reference_data.begin(), reference_data.end(), 0);
        // Use the TimeFrequency iterator interface to insert these zeros into the TimeFrequendcy object
        std::copy(reference_data.begin(), reference_data.end(), time_frequency_data->spectrum(time_sample).begin());
    }

    std::vector<std::shared_ptr<CpuTraits::TimeFrequencyType>> tf_data;

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.emplace_back(time_frequency_data);

    panda::PoolResource<Cpu> device(1);

    // Create a Fldo helper object so we can access private members of the class for testing
    fldo::Config config;

    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(1, 1, 5, 10, config);

    // Call Fldo::accumulate_data() given the data we created above
    test_helper.accumulate_data_helper(device, tf_data, scl, 0);

    // Get the _temporary_sums_buffer
    auto temporary_sums_buffer = test_helper.temporary_sums_buffer();

    // After Fldo::flush_buffers() is called, there is a call to std::transform(),
    // so the _temporary_sums_buffer will not be completely empty when we check,
    // but will have a known pattern of numbers in the time sample that was being
    // accumulated when the buffers were flushed (which will time sample 1285
    // in this test).
    std::vector<CpuTraits::NumericalRep> temporary_sums_results_reference {
                                             static_cast<CpuTraits::NumericalRep>(12851),
                                             static_cast<CpuTraits::NumericalRep>(12852),
                                             static_cast<CpuTraits::NumericalRep>(12853),
                                             static_cast<CpuTraits::NumericalRep>(12854),
                                             static_cast<CpuTraits::NumericalRep>(12855),
                                             static_cast<CpuTraits::NumericalRep>(12856),
                                             static_cast<CpuTraits::NumericalRep>(12857),
                                             static_cast<CpuTraits::NumericalRep>(12858),
                                             static_cast<CpuTraits::NumericalRep>(12859),
                                             static_cast<CpuTraits::NumericalRep>(12860)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the temporary sums buffer
    ASSERT_TRUE(temporary_sums_results_reference.size() == temporary_sums_buffer->phase_bin(0).data_size());

    // Check the results with what we know the temporary
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(temporary_sums_buffer->phase_bin(0).begin()
                           , temporary_sums_buffer->phase_bin(0).end()
                           , temporary_sums_results_reference.begin()
                          )
               );

    // Check that the rest of the temporary sums buffers have been flushed and are empty
    for(size_t phase_bin_iter = 1; phase_bin_iter != test_helper.cpu_config().number_of_phase_bins(); ++phase_bin_iter)
    {
        for(auto channel_iter = temporary_sums_buffer->phase_bin(phase_bin_iter).begin();
                 channel_iter != temporary_sums_buffer->phase_bin(phase_bin_iter).end();
                 ++channel_iter
           )
        {
            ASSERT_EQ(*channel_iter, 0);
        }
    }

    // Get the _accumulated_sums_buffer
    auto accumulated_sums_buffer = test_helper.accumulated_sums_buffer();

    // Create a vector that holds data which we know the
    // accumulated sums buffer should hold the first phase bin
    std::vector<CpuTraits::NumericalRep> accumulated_sums_results_reference_0 {
                                             static_cast<CpuTraits::NumericalRep>(1645057),
                                             static_cast<CpuTraits::NumericalRep>(1645314),
                                             static_cast<CpuTraits::NumericalRep>(1645571),
                                             static_cast<CpuTraits::NumericalRep>(1645828),
                                             static_cast<CpuTraits::NumericalRep>(1646085),
                                             static_cast<CpuTraits::NumericalRep>(1646342),
                                             static_cast<CpuTraits::NumericalRep>(1646599),
                                             static_cast<CpuTraits::NumericalRep>(1646856),
                                             static_cast<CpuTraits::NumericalRep>(1647113),
                                             static_cast<CpuTraits::NumericalRep>(1647370)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the accumulated sums buffer
    ASSERT_TRUE(accumulated_sums_results_reference_0.size() == accumulated_sums_buffer->phase_bin(0).data_size());

    // Check the results with what we know the accumulated
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(accumulated_sums_buffer->phase_bin(0).begin()
                           , accumulated_sums_buffer->phase_bin(0).end()
                           , accumulated_sums_results_reference_0.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // accumulated sums buffer should hold the second phase bin
    std::vector<CpuTraits::NumericalRep> accumulated_sums_results_reference_1 {
                                             static_cast<CpuTraits::NumericalRep>(1647627),
                                             static_cast<CpuTraits::NumericalRep>(1647884),
                                             static_cast<CpuTraits::NumericalRep>(1648141),
                                             static_cast<CpuTraits::NumericalRep>(1648398),
                                             static_cast<CpuTraits::NumericalRep>(1648655),
                                             static_cast<CpuTraits::NumericalRep>(1648912),
                                             static_cast<CpuTraits::NumericalRep>(1649169),
                                             static_cast<CpuTraits::NumericalRep>(1649426),
                                             static_cast<CpuTraits::NumericalRep>(1649683),
                                             static_cast<CpuTraits::NumericalRep>(1649940)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the accumulated sums buffer
    ASSERT_TRUE(accumulated_sums_results_reference_1.size() == accumulated_sums_buffer->phase_bin(1).data_size());

    // Check the results with what we know the accumulated
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(accumulated_sums_buffer->phase_bin(1).begin()
                           , accumulated_sums_buffer->phase_bin(1).end()
                           , accumulated_sums_results_reference_1.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // accumulated sums buffer should hold the third phase bin
    std::vector<CpuTraits::NumericalRep> accumulated_sums_results_reference_2 {
                                             static_cast<CpuTraits::NumericalRep>(1650197),
                                             static_cast<CpuTraits::NumericalRep>(1650454),
                                             static_cast<CpuTraits::NumericalRep>(1650711),
                                             static_cast<CpuTraits::NumericalRep>(1650968),
                                             static_cast<CpuTraits::NumericalRep>(1651225),
                                             static_cast<CpuTraits::NumericalRep>(1651482),
                                             static_cast<CpuTraits::NumericalRep>(1651739),
                                             static_cast<CpuTraits::NumericalRep>(1651996),
                                             static_cast<CpuTraits::NumericalRep>(1652253),
                                             static_cast<CpuTraits::NumericalRep>(1652510)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the accumulated sums buffer
    ASSERT_TRUE(accumulated_sums_results_reference_2.size() == accumulated_sums_buffer->phase_bin(2).data_size());

    // Check the results with what we know the accumulated
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(accumulated_sums_buffer->phase_bin(2).begin()
                           , accumulated_sums_buffer->phase_bin(2).end()
                           , accumulated_sums_results_reference_2.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // accumulated sums buffer should hold the fourth phase bin
    std::vector<CpuTraits::NumericalRep> accumulated_sums_results_reference_3 {
                                             static_cast<CpuTraits::NumericalRep>(1652767),
                                             static_cast<CpuTraits::NumericalRep>(1653024),
                                             static_cast<CpuTraits::NumericalRep>(1653281),
                                             static_cast<CpuTraits::NumericalRep>(1653538),
                                             static_cast<CpuTraits::NumericalRep>(1653795),
                                             static_cast<CpuTraits::NumericalRep>(1654052),
                                             static_cast<CpuTraits::NumericalRep>(1654309),
                                             static_cast<CpuTraits::NumericalRep>(1654566),
                                             static_cast<CpuTraits::NumericalRep>(1654823),
                                             static_cast<CpuTraits::NumericalRep>(1655080)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the accumulated sums buffer
    ASSERT_TRUE(accumulated_sums_results_reference_3.size() == accumulated_sums_buffer->phase_bin(3).data_size());

    // Check the results with what we know the accumulated
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(accumulated_sums_buffer->phase_bin(3).begin()
                           , accumulated_sums_buffer->phase_bin(3).end()
                           , accumulated_sums_results_reference_3.begin()
                          )
               );

    // Create a vector that holds data which we know the
    // accumulated sums buffer should hold the fifth phase bin
    std::vector<CpuTraits::NumericalRep> accumulated_sums_results_reference_4 {
                                             static_cast<CpuTraits::NumericalRep>(1655337),
                                             static_cast<CpuTraits::NumericalRep>(1655594),
                                             static_cast<CpuTraits::NumericalRep>(1655851),
                                             static_cast<CpuTraits::NumericalRep>(1656108),
                                             static_cast<CpuTraits::NumericalRep>(1656365),
                                             static_cast<CpuTraits::NumericalRep>(1656622),
                                             static_cast<CpuTraits::NumericalRep>(1656879),
                                             static_cast<CpuTraits::NumericalRep>(1657136),
                                             static_cast<CpuTraits::NumericalRep>(1657393),
                                             static_cast<CpuTraits::NumericalRep>(1657650)
                                         };

    // Verify our reference vector is the same length
    // as a phase bin in the accumulated sums buffer
    ASSERT_TRUE(accumulated_sums_results_reference_4.size() == accumulated_sums_buffer->phase_bin(4).data_size());

    // Check the results with what we know the accumulated
    // sums buffer should hold in this specific phase bin
    ASSERT_TRUE(std::equal(accumulated_sums_buffer->phase_bin(4).begin()
                           , accumulated_sums_buffer->phase_bin(4).end()
                           , accumulated_sums_results_reference_4.begin()
                          )
               );

    // Get the _temporary_counts_buffer
    auto temporary_counts_buffer = test_helper.temporary_counts_buffer();

    // After Fldo::flush_buffers() is called, the bin in _temporary_counts_buffer
    // that is being added to (that caused flush_buffers() to be called) is
    // incremented, so that bin will have a count of one. Also, since we added
    // one extra time sample, that will also cause the count of the next bin to
    // increment to one, but all other bins should have their counts reset to
    // zero.
    ASSERT_EQ(temporary_counts_buffer[0], 1);
    ASSERT_EQ(temporary_counts_buffer[1], 1);

    // Check that the rest of the temporary counts buffers have been flushed and are empty
    for(size_t phase_bin_iter = 2; phase_bin_iter != test_helper.cpu_config().number_of_phase_bins(); ++phase_bin_iter)
    {
        ASSERT_EQ(temporary_counts_buffer[phase_bin_iter], 0);
    }

    // Get the _accumulated_counts_buffer
    auto accumulated_counts_buffer = test_helper.accumulated_counts_buffer();

    // For flush_buffers() to be called, we need to exceed the _accumulated_bin_capacity,
    // which in these tests is 257, so every phase bin should have this value of counts
    for(size_t phase_bin_iter = 0; phase_bin_iter != test_helper.cpu_config().number_of_phase_bins(); ++phase_bin_iter)
    {
        ASSERT_EQ(accumulated_counts_buffer[phase_bin_iter], 257);
    }

}*/

/**
 * This will test flush_buffers() by calling it directly, which
 * allows for a test with more minimal data than the "public" test
 */
TEST(CpuTest, test_flush_buffers)
{

    int num_channels = 10;

    // Create a Fldo helper object so we can access private members of the class for testing

    fldo::Config config;
    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(1, 1, 5, 10, config);

    /**
     *  Create a _temporary_sums_buffer
     */
    test_helper.temporary_sums_buffer_create(data::DimensionSize<data::Frequency>(num_channels));

    // Create a spectrum of zeros to insert into a phase bin of the _temporary_sums_buffer
    std::vector<typename CpuTraits::NumericalRep> spectrum(num_channels, 0);

    // Fill every other channel (starting at channel 1) in this spectrum with a value of one
    for(int channel = 1; channel < num_channels; channel+=2)
    {
        spectrum[channel] = 1;
    }

    // Add this spectrum to every other sample in the _temporary_sums_buffer
    for(size_t phase_bin = 0; phase_bin < test_helper.cpu_config().number_of_phase_bins(); phase_bin+=2)
    {
        test_helper.temporary_sums_buffer_fill(phase_bin, spectrum);
    }

    // Reset the reference spectrum to all zeros
    std::fill(spectrum.begin(), spectrum.end(), 0);

    // Fill every other channel (starting at channel 0) in this spectrum with a value of one
    for(int channel = 0; channel < num_channels; channel+=2)
    {
        spectrum[channel] = 1;
    }

    // Add this spectrum to every other sample in the _temporary_sums_buffer
    for(size_t phase_bin = 1; phase_bin < test_helper.cpu_config().number_of_phase_bins(); phase_bin+=2)
    {
        test_helper.temporary_sums_buffer_fill(phase_bin, spectrum);
    }

    /**
     *  The _temporary_sums_buffer should now have
     *  the following pattern of numbers in it:
     *
     *                  phase bin
     *                  ---------
     *                0  1  0  1  0
     *           c |  1  0  1  0  1
     *           h |  0  1  0  1  0
     *           a |  1  0  1  0  1
     *           n |  0  1  0  1  0
     *           n |  1  0  1  0  1
     *           e |  0  1  0  1  0
     *           l |  1  0  1  0  1
     *                0  1  0  1  0
     *                1  0  1  0  1
     */

    /**
     *  Create an _accumulated_sums_buffer
     */
    test_helper.accumulated_sums_buffer_create(data::DimensionSize<data::Frequency>(num_channels));

    // Get the _accumulated_sums_buffer
    auto accumulated_sums_buffer = test_helper.accumulated_sums_buffer();

    // Verify the _accumulated_sums_buffer is empty
    for(auto iter = accumulated_sums_buffer->begin(); iter != accumulated_sums_buffer->end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Create a _temporary_counts_buffer
     */
    test_helper.temporary_counts_buffer_create();

    // Create a vector of unique numbers to insert into the _temporary_counts_buffer
    std::vector<typename CpuTraits::NumericalRep> counts = {4, 3, 2, 5, 1};

    // Add this vector to the _temporary_counts_buffer
    test_helper.temporary_counts_buffer_fill(counts);

    /**
     *  Create an _accumulated_counts_buffer
     */
    test_helper.accumulated_counts_buffer_create();

    // Get the _accumulatd_counts_buffer
    auto accumulated_counts_buffer = test_helper.accumulated_counts_buffer();

    // Verify the _accumulated_counts_buffer is empty
    for(auto iter = accumulated_counts_buffer.begin(); iter != accumulated_counts_buffer.end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Call flush_buffers()
     */
    test_helper.flush_buffers_helper();

    /**
     *  Verify _temporary_sums_buffer is now empty
     */
    auto temporary_sums_buffer = test_helper.temporary_sums_buffer();
    for(auto iter = temporary_sums_buffer->begin(); iter != temporary_sums_buffer->end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Verify _temporary_counts_buffer is now empty
     */
    auto temporary_counts_buffer = test_helper.temporary_counts_buffer();
    for(auto iter = temporary_counts_buffer.begin(); iter != temporary_counts_buffer.end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Verify _accumulated_sums_buffer has the correct pattern
     */
    std::vector<CpuTraits::NumericalRep>
        accumulated_sums_reference = {  0, 1, 0, 1, 0, 1, 0, 1, 0, 1
                                      , 1, 0, 1, 0, 1, 0, 1, 0, 1, 0
                                      , 0, 1, 0, 1, 0, 1, 0, 1, 0, 1
                                      , 1, 0, 1, 0, 1, 0, 1, 0, 1, 0
                                      , 0, 1, 0, 1, 0, 1, 0, 1, 0, 1
                                     };
    auto accumulated_sums_buffer_new = test_helper.accumulated_sums_buffer();
    ASSERT_TRUE(std::equal(accumulated_sums_buffer_new->begin()
                           , accumulated_sums_buffer_new->end()
                           , accumulated_sums_reference.begin()
                          )
               );

    /**
     *  Verify _accumulated_counts_buffer has the correct pattern
     */
    auto accumulated_counts_buffer_new = test_helper.accumulated_counts_buffer();
    ASSERT_TRUE(std::equal(accumulated_counts_buffer_new.begin(), accumulated_counts_buffer_new.end(), counts.begin()));

}

TEST(CpuTest, test_folding_nonexistent_candidate)
{

    // Set candidate metadata for a single dummy candidate
    data::Scl scl = create_scl(1, 1);

    // Create a TimeFrequency data chunk (the size does not matter for this test).
    // Set the sampling time (the value does not matter for this test).
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data =
        create_tf_data<CpuTraits::TimeFrequencyType>(10, 10, 0.2);

    std::vector<std::shared_ptr<CpuTraits::TimeFrequencyType>> tf_data;

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.emplace_back(time_frequency_data);

    panda::PoolResource<Cpu> device(1);

    // Create a Fldo helper object so we can access private members of the class for testing
    fldo::Config config;
    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(1, 1, 5, 10, config);

    // Call Fldo::accumulate_data() given the data we created above, but
    // attempt to fold the second candidate, when there is only one
    // candidate present in the candidate list
    ASSERT_THROW(test_helper.accumulate_data_helper(device, tf_data, scl, 1), panda::Error);

}

TEST(CpuTest, test_export_subint)
{
    int num_channels = 10;

    // Create a Fldo helper object so we can access private members of the class for testing
    fldo::Config config;
    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(1, 1, 5, 10, config);

    // Since we're calling export_subint() directly in this test and bypassing
    // the main operator(), we need to create an OptimisedCandidate ourselves
    test_helper.create_optimised_candidate(test_helper.cpu_config().number_of_subints()
                                           , test_helper.cpu_config().number_of_subbands()
                                           , test_helper.cpu_config().number_of_phase_bins()
                                          );

    /**
     *  Create a _temporary_sums_buffer
     */
    test_helper.temporary_sums_buffer_create(data::DimensionSize<data::Frequency>(num_channels));

    // Create a spectrum of ones to insert into the _temporary_sums_buffer
    std::vector<typename CpuTraits::NumericalRep> spectrum(num_channels, 1);

    // Add this spectrum to every sample in the _temporary_sums_buffer
    for(size_t phase_bin = 0; phase_bin < test_helper.cpu_config().number_of_phase_bins(); ++phase_bin)
    {
        test_helper.temporary_sums_buffer_fill(phase_bin, spectrum);
    }

    /**
     *  Create an _accumulated_sums_buffer
     */
    test_helper.accumulated_sums_buffer_create(data::DimensionSize<data::Frequency>(num_channels));

    // Get the _accumulated_sums_buffer
    auto accumulated_sums_buffer = test_helper.accumulated_sums_buffer();

    // Verify the _accumulated_sums_buffer is empty
    for(auto iter = accumulated_sums_buffer->begin(); iter != accumulated_sums_buffer->end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Create a _temporary_counts_buffer
     */
    test_helper.temporary_counts_buffer_create();

    // Create a vector of unique numbers to insert into the _temporary_counts_buffer
    std::vector<typename CpuTraits::NumericalRep> counts = {4, 0, 2, 5, 1};

    // Add this vector to the _temporary_counts_buffer
    test_helper.temporary_counts_buffer_fill(counts);

    /**
     *  Create an _accumulated_counts_buffer
     */
    test_helper.accumulated_counts_buffer_create();

    // Get the _accumulated_counts_buffer
    auto accumulated_counts_buffer = test_helper.accumulated_counts_buffer();

    // Verify the _accumulated_counts_buffer is empty
    for(auto iter = accumulated_counts_buffer.begin(); iter != accumulated_counts_buffer.end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    /**
     *  Call flush_buffers() to populate the accumulated counts and sums buffers
     */
    test_helper.flush_buffers_helper();

    // Get the _accumulated_sums_buffer
    auto accumulated_sums_buffer_2 = test_helper.accumulated_sums_buffer();

    // Verify the _accumulated_sums_buffer is filled with ones
    for(size_t phase_bin = 0; phase_bin < test_helper.cpu_config().number_of_phase_bins(); ++phase_bin)
    {
        ASSERT_TRUE(std::equal(accumulated_sums_buffer_2->phase_bin(phase_bin).begin()
                               , accumulated_sums_buffer_2->phase_bin(phase_bin).end()
                               , spectrum.begin()
                              )
                   );
    }

    // Get the _accumulated_counts_buffer
    auto accumulated_counts_buffer_2 = test_helper.accumulated_counts_buffer();

    // Verify the _accumulated_counts_buffer has the correct pattern
    ASSERT_TRUE(std::equal(accumulated_counts_buffer_2.begin()
                           , accumulated_counts_buffer_2.end()
                           , counts.begin()
                          )
               );

    /**
     *  Repopulate the (now-cleared) temporary sums and counts buffers
     */
    for(size_t phase_bin = 0; phase_bin < test_helper.cpu_config().number_of_phase_bins(); ++phase_bin)
    {
        test_helper.temporary_sums_buffer_fill(phase_bin, spectrum);
    }
    test_helper.temporary_counts_buffer_fill(counts);

    // Create a TimeFrequency data chunk (the size does not matter for this test).
    // Set the sampling time (the value does not matter for this test).
    auto f1 =  1410.0 * boost::units::si::mega * boost::units::si::hertz;
    auto delta =  -1.0 * boost::units::si::mega * boost::units::si::hertz;

    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data =
        create_tf_data<CpuTraits::TimeFrequencyType>(10, 10, 0.2);

    time_frequency_data->set_channel_frequencies_const_width( f1, delta );
    std::vector<std::shared_ptr<CpuTraits::TimeFrequencyType>> tf_data;

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.emplace_back(time_frequency_data);

    // Set a subint duration; the value doesn't matter
    // for this test, as it's passed though the function.
    const typename CpuTraits::TimePointType::duration subint_length(utils::julian_days(0));

    /**
     *  Call export_subint(), which should send data for dedispersion
     *  and frequency-scrunching, and clear all of the buffers
     */
    test_helper.export_subint_helper(1 * data::parsec_per_cube_cm
                                     , 1410.0 * data::megahertz
				     , tf_data[0]
                                     , subint_length
                                    );

    // Verify _temporary_sums_buffer is now empty
    auto temporary_sums_buffer = test_helper.temporary_sums_buffer();
    for(auto iter = temporary_sums_buffer->begin(); iter != temporary_sums_buffer->end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    // Verify _temporary_counts_buffer is now empty
    auto temporary_counts_buffer_2 = test_helper.temporary_counts_buffer();
    for(auto iter = temporary_counts_buffer_2.begin(); iter != temporary_counts_buffer_2.end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    // Verify _accumulated_sums_buffer is now empty
    auto accumulated_sums_buffer_3 = test_helper.accumulated_sums_buffer();
    for(auto iter = accumulated_sums_buffer_3->begin(); iter != accumulated_sums_buffer_3->end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

    // Verify _accumulated_counts_buffer is now empty
    auto accumulated_counts_buffer_3 = test_helper.accumulated_counts_buffer();
    for(auto iter = accumulated_counts_buffer_3.begin(); iter != accumulated_counts_buffer_3.end(); ++iter)
    {
        ASSERT_EQ(*iter, 0);
    }

}

/**
 *  Create a signal in time/frequency data that mimics a very simple pulsar signal.
 *  Test that, when given the correct folding period and DM, the folding algorithm
 *  folds the data so that the signal ends up aligned in frequency (proving the
 *  dedispersion works correctly) and all of the signal is in one phase bin in the
 *  folded profile.
 */
TEST(CpuTest, test_fldo_algo)
{

    // Parameters for constructing TimeFrequency chunk
    uint8_t time_samples_per_tf_chunk = 100;
    uint8_t number_of_channels = 64;
    float sampling_time = 2.0; // DEV NOTE: this sampling time is unusually long to deal with precision issues currently in astrotypes with storing the timestamps of time samples as MJDs, so currently the time between two samples is stored in decimal days

    // Parameters for dispersion/dedispersion
    CpuTraits::FrequencyType frequency_of_first_channel = 1699.75 * data::megahertz; // Centre frequency of first channel
    CpuTraits::FrequencyType frequency_channel_width = -0.5 * data::megahertz; // Channel width is negative as we set the frequency of the highest channel
    cpu::Fldo<CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm; // DEV NOTE: DM is very high so there is some sweep across the small BW, given the large sampling time set above

    // Create TimeFrequency data chunks to hold pulsar-like data. These chunks
    // will have a length such that the 'injected' pulsar will occupy different
    // time samples in each chunk.
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data_one =
        create_tf_data<CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data_two =
        create_tf_data<CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );
    std::shared_ptr<CpuTraits::TimeFrequencyType> time_frequency_data_three =
        create_tf_data<CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );

    // Calculate the start times (in MJD) of each TimeFrequency chunk
    cpu::Fldo<CpuTraits>::TimePointType start_time_one(utils::julian_days(60000.0));
    cpu::Fldo<CpuTraits>::TimePointType start_time_two(utils::julian_days(60000.0 + (200.0/86400.0)));
    cpu::Fldo<CpuTraits>::TimePointType start_time_three(utils::julian_days(60000.0 + (400.0/86400.0)));
    cpu::Fldo<CpuTraits>::TimePointType start_time_four(utils::julian_days(60000.0 + (600.0/86400.0)));

    // Set the start times (in MJD) of each TimeFrequency chunk
    time_frequency_data_one->start_time(start_time_one);
    time_frequency_data_two->start_time(start_time_two);
    time_frequency_data_three->start_time(start_time_three);

    // Set channel frequencies in the TimeFrequency chunks
    time_frequency_data_one->set_channel_frequencies_const_width(frequency_of_first_channel
                                                                 , frequency_channel_width
                                                                );
    time_frequency_data_two->set_channel_frequencies_const_width(frequency_of_first_channel
                                                                 , frequency_channel_width
                                                                );
    time_frequency_data_three->set_channel_frequencies_const_width(frequency_of_first_channel
                                                                   , frequency_channel_width
                                                                  );

    // Get a vector containing the frequencies of each channel
    std::vector<typename CpuTraits::FrequencyType> channel_frequencies =
        time_frequency_data_one->channel_frequencies();

    // Fill the TimeFrequency chunks with zeros
    for(size_t time_sample = 0; time_sample < time_samples_per_tf_chunk; ++time_sample)
    {
        std::fill(time_frequency_data_one->spectrum(time_sample).begin()
                  , time_frequency_data_one->spectrum(time_sample).end()
                  , 0
                 );
        std::fill(time_frequency_data_two->spectrum(time_sample).begin()
                  , time_frequency_data_two->spectrum(time_sample).end()
                  , 0
                 );
        std::fill(time_frequency_data_three->spectrum(time_sample).begin()
                  , time_frequency_data_three->spectrum(time_sample).end()
                  , 0
                 );
    }

    // Create a vector to hold the dispersion delay in time samples
    // for each frequency channel. Initialise each element to zero.
    std::vector<uint16_t> dispersion_delay_in_time_samples(number_of_channels, 0);

    // Calculate dispersion delays (compared to the
    // highest-frequency channel) for each channel
    for(size_t channel_index = 1; channel_index < number_of_channels; ++channel_index)
    {

        // Calculate the dispersion delay between this channel and the first channel
        const boost::units::quantity<data::Seconds, double>
            dispersion_delay =
                (4149.377593360996
                    * data::megahertz
                    * data::megahertz
                    / data::parsecs_per_cube_cm
                    * boost::units::si::seconds
                )
                * dm
                * (
                      ((1/channel_frequencies[channel_index])
                          * (1/channel_frequencies[channel_index])
                      )
                      -
                      ((1/frequency_of_first_channel)
                          * (1/frequency_of_first_channel)
                      )
                  );

        // Calculate the dispersion delay as a number of time samples
        dispersion_delay_in_time_samples[channel_index] =
            round((dispersion_delay/(sampling_time * data::seconds)).value());

    }

    // Inject a pulsar-like signal with a period of 0.6 seconds (gives a pulse
    // every 30 time samples given the sampling time of 0.02 seconds) and a DM
    // of 2000. Start at the 15th time sample.
    for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
    {
        // Set each time/frequency bin that will hold the pulsar-like signal to a value
        time_frequency_data_one->spectrum(14 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 5;
        time_frequency_data_one->spectrum(44 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 6;
        time_frequency_data_one->spectrum(74 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 7;
    }

    // Create a vector to hold all TimeFrequency data chunks in the observation
    std::vector<std::shared_ptr<CpuTraits::TimeFrequencyType>> tf_data;

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.push_back(time_frequency_data_one);

    // Continue to inject the pulsar-like signal every 30 time samples. Next
    // starting time sample (based on where we left off above) is 104, which
    // is time sample 4 in this new data chunk.
    for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
    {
        // Set each time/frequency bin that will hold the pulsar-like signal to a value
        time_frequency_data_two->spectrum(4 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 8;
        time_frequency_data_two->spectrum(34 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 9;
        time_frequency_data_two->spectrum(64 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 6;
        time_frequency_data_two->spectrum(94 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 5;
    }

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.push_back(time_frequency_data_two);

    // Continue to inject the pulsar-like signal every 30 time samples. Next
    // starting time sample (based on where we left off above) is 124, which
    // is time sample 24 in this new data chunk.
    for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
    {
        // Set each time/frequency bin that will hold the pulsar-like signal to a value
        time_frequency_data_three->spectrum(24 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 4;
        time_frequency_data_three->spectrum(54 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 3;
        time_frequency_data_three->spectrum(84 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 2;
    }

    // Append this data chunk to the end of the TimeFrequency data vector
    tf_data.push_back(time_frequency_data_three);

    // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // Create a candidate list with parameters that
    // match the pulsar-like signal we generated
    data::Scl scl = create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;
        // Create an Fldo object
    cpu::test::FldoTestHelper<CpuTraits> test_helper = create_fldo_test_helper<CpuTraits>(2, 16, 30, 64, config);


    /**  Set parameters of output data cube.
      *  The input signal has 30 time samples, with a pulse injected every 30
      *  time samples starting at sample 14 (pulses at time sample 14, 44, 74,
      *  104, 134, 164, 194, 224, 254, 284). Using two output subints means the
      *  data will be split between subints at time sample 150.
      */

    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube =
        test_helper.fold_helper(device, tf_data, scl, 0);

    /** Inspect the data cube to verify the injected signal has been folded properly.
     *  All signal should appear in phase bin 14
     */

    // Verify that the Ocld has one candidate
    ASSERT_EQ(ocld_data_cube->size(), 1);

    // Pull the candidate out of the Ocld and store it in its own candidate object
    data::OptimisedCandidate folded_candidate = ocld_data_cube->front();

    // Verify the candidate dimensions are correct after Fldo has run
    ASSERT_EQ(folded_candidate.number_of_subints(), 2);
    ASSERT_EQ(folded_candidate.number_of_subbands(), 16);
    ASSERT_EQ(folded_candidate.number_of_phase_bins(), 30);

    // Extract the phase bin into which the signal should have been folded
    auto phase_bin = folded_candidate.phase_bin(14);

    // Verify the candidate dimensions are correct
    ASSERT_EQ(phase_bin.number_of_subints(), 2);
    ASSERT_EQ(phase_bin.number_of_subbands(), 16);
    ASSERT_EQ(phase_bin.number_of_phase_bins(), 1);

    // Extract the first subint of the phase bin
    auto subband_array = phase_bin.slice(data::DimensionSpan<data::Time>(data::DimensionIndex<data::Time>(0)
                                                                         , data::DimensionSize<data::Time>(1)
                                                                        )
                                         , data::DimensionSpan<data::Frequency>(
                                               phase_bin.template dimension<data::Frequency>()
                                                                               )
                                        );

    // Verify the candidate dimensions are correct
    ASSERT_EQ(subband_array.number_of_subints(), 1);
    ASSERT_EQ(subband_array.number_of_subbands(), 16);
    ASSERT_EQ(subband_array.number_of_phase_bins(), 1);

    // Five pulses were added together in the first subint, with values
    // of 5, 6, 7, 8, and 9, so the resultant folded pulse should have
    // a value of 35, which will get normalised to 7. Then, after four
    // channels are added into each subband, the pulse in each subband
    // will have a value of 28.
    for(auto iter = subband_array.begin(); iter != subband_array.end(); ++iter)
    {
        ASSERT_DOUBLE_EQ(*iter, 28);
    }

    // Extract the second subint of the phase bin
    subband_array = phase_bin.slice(data::DimensionSpan<data::Time>(data::DimensionIndex<data::Time>(1)
                                                                         , data::DimensionSize<data::Time>(1)
                                                                        )
                                         , data::DimensionSpan<data::Frequency>(
                                               phase_bin.template dimension<data::Frequency>()
                                                                               )
                                        );

    // Verify the candidate dimensions are correct
    ASSERT_EQ(subband_array.number_of_subints(), 1);
    ASSERT_EQ(subband_array.number_of_subbands(), 16);
    ASSERT_EQ(subband_array.number_of_phase_bins(), 1);

    // Five pulses were added together in the second subint, with values
    // of 6, 5, 4, 3, and 2, so the resultant folded pulse should have
    // a value of 20, which will get normalised to 4. Then, after four
    // channels are added into each subband, the pulse in each subband
    // will have a value of 17 after rounding up as expected from floating point addition.
    for(auto iter = subband_array.begin(); iter != subband_array.end(); ++iter)
    {
        EXPECT_NEAR(*iter, 17, 1);
    }

}

} // namespace test
} // namaspace cpu
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace test {

typedef ::testing::Types<cpu::test::CpuTraits> CpuTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(Cpu, FldoTester, CpuTraitsTypes);

} // namespace test
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
