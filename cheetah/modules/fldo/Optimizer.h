/*
 * Copyright 2024 SKA Observatory
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_OPTIMIZER_H
#define SKA_CHEETAH_MODULES_FLDO_OPTIMIZER_H
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/make_scaled_unit.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/make_scaled_unit.hpp>
#include "cheetah/data/Units.h"
#include "cheetah/modules/fldo/Types.h"
#include "pss/astrotypes/units/Time.h"
#include "cheetah/modules/fldo/cpu/Config.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {

/**
 * @brief
 * @details
 */

class Optimizer
{
    public:
        typedef data::DedispersionMeasureType<double> Dm;
	typedef boost::units::quantity<data::MilliSeconds, double> MsecTimeType;
        typedef boost::units::quantity<boost::units::si::dimensionless, float> SecPerSecType;
	typedef pss::astrotypes::units::ModifiedJulianClock::time_point TimePointType;
	typedef typename cpu::Config Config;

	/*
	 * @brief a sub-class that saves the folded profile for optimization steps
	 */


    public:
	class Profile
	{
	    public:
		Profile();
		~Profile();

		void generate_profile(Config const&, data::OptimisedCandidate<>& candidate);
		std::vector<float> const& folded_profile();
                std::vector<float>& prefix_sum_array();
                std::vector<float>& circular_conv();

	    private:
		std::vector<float> _folded_profile;
		std::vector<float> _prefix_sum_array;
		std::vector<float> _circular_conv;
	};

        /**
         * @brief optimize the paramters of the folded candidate. Takes in the optimized_candidate object
         */
        Optimizer();

        /**
         * @brief copy constructor not available
         * @internal if you add a copy constructor please remember "rule of 5" and unit tests
         */
        Optimizer(Optimizer const&) = delete;

        ~Optimizer();

	/*
	 * @brief the operator that runs the optimization
	 */
	template<typename TimeFrequencyType>
	void operator()(data::OptimisedCandidate<>& candidate , Config const& config, std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data, data::Scl& scl_data, std::size_t number);

	/*
	 * @brief generate the stats for SNR calculation for the profile
	 */
	void genstats(Config const& config);

	/*
	 * @brief run a matched filter on the optimal paramters to get the final width
	 */
	void optimal_filter(Config const& config);

	/*
	 * @brief calculate and apply bin shift based on the p and p-dot
	 */
	data::OptimisedCandidate<> apply_bin_correction(Config const& config, data::OptimisedCandidate<> candidate, MsecTimeType const& period_fdas, MsecTimeType const& period_trial, SecPerSecType const& pdot_fdas, SecPerSecType const& pdot_trial, float tbin, float tsub);

	/*
	 * @brief compute the S/N of the resulting folded profile
	 */
        void compute_snr();

	/*
	 * @brief return the Profile member
	 */
        Profile& profile();

	/*
	 * @brief getters for the mean and std dev
	 */
	float const& mean();

	float const& stddev();

	/*
	 * @brief getter for the optimal width
	 */
	std::size_t const& width();

	/*
	 * @brief getter for the peak SNR
	 */
	float const& snr();



    private:
	std::vector<MsecTimeType> _prange;
	std::vector<SecPerSecType> _pdrange;
	float _snr;
	std::vector<float> _optimal_snr;
        std::size_t _width;
	std::size_t _pampindex;
	Profile _profile;
	float _mean;
	float _stddev;
	typename TimePointType::duration _observation_length;
};


} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "cheetah/modules/fldo/detail/Optimizer.cpp"
#endif // SKA_CHEETAH_MODULES_FLDO_OPTIMIZER_H
