/*
 * Copyright 2024 SKA Observatory
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "cheetah/modules/fldo/test/OptimizerTest.h"
#include "cheetah/modules/fldo/cpu/test/CpuTest.h"
#include "cheetah/modules/fldo/cpu/test/src/CpuTest.cpp"
#include "cheetah/modules/fldo/cpu/test_utils/FldoTestHelper.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace test {

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(FldoTester);

namespace {

    std::vector<std::shared_ptr<cpu::test::CpuTraits::TimeFrequencyType>> create_data(cpu::Fldo<cpu::test::CpuTraits>::Dm dm)
    {
        // Parameters for constructing TimeFrequency chunk
	uint8_t time_samples_per_tf_chunk = 100;
	uint8_t number_of_channels = 64;
	float sampling_time = 2.0; // DEV NOTE: this sampling time is unusually long to deal with precision issues currently in astrotypes with storing the timestamps of time samples as MJDs, so currently the time between two samples is stored in decimal days

	// Parameters for dispersion/dedispersion
	cpu::test::CpuTraits::FrequencyType frequency_of_first_channel = 1699.75 * data::megahertz; // Centre frequency of first channel
	cpu::test::CpuTraits::FrequencyType frequency_channel_width = -0.5 * data::megahertz; // Channel width is negative as we set the frequency of the highest channel

	// Create TimeFrequency data chunks to hold pulsar-like data. These chunks
	// will have a length such that the 'injected' pulsar will occupy different
	// time samples in each chunk.
	std::shared_ptr<cpu::test::CpuTraits::TimeFrequencyType> time_frequency_data_one =
	cpu::test::create_tf_data<cpu::test::CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );
	std::shared_ptr<cpu::test::CpuTraits::TimeFrequencyType> time_frequency_data_two =
        cpu::test::create_tf_data<cpu::test::CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );
	std::shared_ptr<cpu::test::CpuTraits::TimeFrequencyType> time_frequency_data_three =
        cpu::test::create_tf_data<cpu::test::CpuTraits::TimeFrequencyType>(time_samples_per_tf_chunk
                                                     , number_of_channels
                                                     , sampling_time
                                                    );

	// Calculate the start times (in MJD) of each TimeFrequency chunk
	cpu::Fldo<cpu::test::CpuTraits>::TimePointType start_time_one(utils::julian_days(60000.0));
	cpu::Fldo<cpu::test::CpuTraits>::TimePointType start_time_two(utils::julian_days(60000.0 + (200.0/86400.0)));
	cpu::Fldo<cpu::test::CpuTraits>::TimePointType start_time_three(utils::julian_days(60000.0 + (400.0/86400.0)));
	cpu::Fldo<cpu::test::CpuTraits>::TimePointType start_time_four(utils::julian_days(60000.0 + (600.0/86400.0)));

	// Set the start times (in MJD) of each TimeFrequency chunk
	time_frequency_data_one->start_time(start_time_one);
	time_frequency_data_two->start_time(start_time_two);
	time_frequency_data_three->start_time(start_time_three);

	// Set channel frequencies in the TimeFrequency chunks
	time_frequency_data_one->set_channel_frequencies_const_width(frequency_of_first_channel
			, frequency_channel_width
			);
	time_frequency_data_two->set_channel_frequencies_const_width(frequency_of_first_channel
			, frequency_channel_width
			);
	time_frequency_data_three->set_channel_frequencies_const_width(frequency_of_first_channel
			, frequency_channel_width
			);

	// Get a vector containing the frequencies of each channel
	std::vector<typename cpu::test::CpuTraits::FrequencyType> channel_frequencies =
		time_frequency_data_one->channel_frequencies();

	// Fill the TimeFrequency chunks with zeros
	for(size_t time_sample = 0; time_sample < time_samples_per_tf_chunk; ++time_sample)
	{
	    std::fill(time_frequency_data_one->spectrum(time_sample).begin()
                  , time_frequency_data_one->spectrum(time_sample).end()
                  , 1
                     );
            std::fill(time_frequency_data_two->spectrum(time_sample).begin()
                  , time_frequency_data_two->spectrum(time_sample).end()
                  , 1
                     );
            std::fill(time_frequency_data_three->spectrum(time_sample).begin()
                  , time_frequency_data_three->spectrum(time_sample).end()
                  , 1
                     );
	}


	// Create a vector to hold the dispersion delay in time samples
	// for each frequency channel. Initialise each element to zero.
	std::vector<uint16_t> dispersion_delay_in_time_samples(number_of_channels, 0);

	// Calculate dispersion delays (compared to the
	// highest-frequency channel) for each channel
	for(size_t channel_index = 1; channel_index < number_of_channels; ++channel_index)
	{

	    // Calculate the dispersion delay between this channel and the first channel
	    const boost::units::quantity<data::Seconds, double>
                dispersion_delay =
                (4149.377593360996
                    * data::megahertz
                    * data::megahertz
                    / data::parsecs_per_cube_cm
                    * boost::units::si::seconds
                )
                * dm
                * (
                      ((1/channel_frequencies[channel_index])
                          * (1/channel_frequencies[channel_index])
                      )
                      -
                      ((1/frequency_of_first_channel)
                          * (1/frequency_of_first_channel)
                      )
                  );

            // Calculate the dispersion delay as a number of time samples
            dispersion_delay_in_time_samples[channel_index] =
                round((dispersion_delay/(sampling_time * data::seconds)).value());

	}

	// Inject a pulsar-like signal with a period of 60 seconds (gives a pulse
	// every 30 time samples given the sampling time of 2 seconds) and a DM
	// of 2000. Start at the 15th time sample.
	for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
        {
	    // Set each time/frequency bin that will hold the pulsar-like signal to a value
            time_frequency_data_one->spectrum(14 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 5;
            time_frequency_data_one->spectrum(44 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 6;
            time_frequency_data_one->spectrum(74 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 7;
	}

	// Create a vector to hold all TimeFrequency data chunks in the observation
	std::vector<std::shared_ptr<cpu::test::CpuTraits::TimeFrequencyType>> tf_data;

	// Append this data chunk to the end of the TimeFrequency data vector
	tf_data.push_back(time_frequency_data_one);

	// Continue to inject the pulsar-like signal every 30 time samples. Next
	// starting time sample (based on where we left off above) is 104, which
	// is time sample 4 in this new data chunk.
	for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
	{
            // Set each time/frequency bin that will hold the pulsar-like signal to a value
            time_frequency_data_two->spectrum(4 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 8;
            time_frequency_data_two->spectrum(34 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 9;
            time_frequency_data_two->spectrum(64 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 6;
            time_frequency_data_two->spectrum(94 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 5;
	}

	// Append this data chunk to the end of the TimeFrequency data vector
	tf_data.push_back(time_frequency_data_two);

	// Continue to inject the pulsar-like signal every 30 time samples. Next
	// starting time sample (based on where we left off above) is 124, which
	// is time sample 24 in this new data chunk.
	for(size_t channel_index = 0; channel_index < number_of_channels; ++channel_index)
	{
            // Set each time/frequency bin that will hold the pulsar-like signal to a value
            time_frequency_data_three->spectrum(24 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 4;
            time_frequency_data_three->spectrum(54 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 3;
            time_frequency_data_three->spectrum(84 + dispersion_delay_in_time_samples[channel_index])[channel_index] = 2;
        }

	// Append this data chunk to the end of the TimeFrequency data vector
	tf_data.push_back(time_frequency_data_three);

	return tf_data;
    }
} // namespace

OptimizerTest::OptimizerTest()
    : ::testing::Test()
{
}

OptimizerTest::~OptimizerTest()
{
}

void OptimizerTest::SetUp()
{
}

void OptimizerTest::TearDown()
{
}

/**
 * @test testing something
 * @given some initial state
 * @when some action
 * @then some expected state
 */
TEST_F(OptimizerTest, test_constructor)
{
    /* make sure the object is created without throwing any errors*/
    ASSERT_NO_THROW(Optimizer optimizer);
    ASSERT_NO_THROW(Optimizer::Profile profile);
}

TEST_F(OptimizerTest, test_snr_computation)
{
    /* first compute the stats and check if the values make sense */

    // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // match the pulsar-like signal we generated
    data::Scl scl = cpu::test::create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(2, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);


    // get a shared ptr to the optimized candidate

    auto candidate = ocld_data_cube->front();

    // construct the profile object
    Optimizer optimizer;

    // generate the profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(),  candidate));

    // compute the stats
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // Make sure we are getting the right stats
    ASSERT_NEAR(optimizer.mean(), 128.0, 50.0);
    ASSERT_NEAR(optimizer.stddev(), 0.5,0.1);

    // compute the snr
    ASSERT_NO_THROW(optimizer.compute_snr());

}

TEST_F(OptimizerTest, test_optimal_filter)
{
    // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // match the pulsar-like signal we generated
    data::Scl scl = cpu::test::create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(2, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);


    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // construct the optimizer class
    Optimizer optimizer;

    // generate profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), candidate));

    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // Run the optimal filter
    ASSERT_NO_THROW(optimizer.optimal_filter(config.cpu_algo_config()));

    // check that the optimal width is equal to 1 samples
    ASSERT_EQ(optimizer.width(), 1U);

}

TEST_F(OptimizerTest, test_optimal_filter_different_width)
{
    // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // match the pulsar-like signal we generated
    data::Scl scl = cpu::test::create_scl(62.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(4, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);


    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // construct the optimizer class
    Optimizer optimizer;

    // generate profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), candidate));

    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // Run the optimal filter
    ASSERT_NO_THROW(optimizer.optimal_filter(config.cpu_algo_config()));

    // check that the optimal width is equal to 7 samples
    ASSERT_EQ(optimizer.width(), 7U);

}


TEST_F(OptimizerTest, test_profile_generation)
{
   // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // match the pulsar-like signal we generated
    data::Scl scl = cpu::test::create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(2, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);


    // get a shared ptr to the optimized candidate

    auto candidate = ocld_data_cube->front();

    // construct the profile object
    Optimizer::Profile profile;

    // generate the profile
    ASSERT_NO_THROW(profile.generate_profile(config.cpu_algo_config(),  candidate));

    //expect the max_value in phase bin 14
    auto peak = std::max_element(profile.folded_profile().begin(), profile.folded_profile().end());
    ASSERT_EQ(std::distance(profile.folded_profile().begin(),peak), 14);
}


TEST_F(OptimizerTest, test_bin_shift_wrong_period)
{
   /* test a scenario where FDAS has reported a wrong period and when one folds at the right period, you get a boost in S/N */

   // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // are slightly offset from the pulsar parameters
    data::Scl scl = cpu::test::create_scl(62.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(4, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);

    float tsub = 600000.0/4;
    float tbin = 62000.0/30;

    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // generate optimizer and run a bin shift correction
    Optimizer optimizer;

    //generate the profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), candidate));

    //generate stats and snr
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr1 = optimizer.snr();

    // test bin shift (for now with P-dot of 0) This needs to change.
    data::Scl::CandidateType::MsecTimeType trial_period(60.0 * boost::units::si::seconds);
    data::Scl::CandidateType::MsecTimeType period_fdas(62.0 * boost::units::si::seconds);
    data::Scl::CandidateType::SecPerSecType trial_pdot = 0.0;
    data::Scl::CandidateType::SecPerSecType pdot_fdas = 0.0;

    auto modified_candidate = optimizer.apply_bin_correction(config.cpu_algo_config(), candidate, period_fdas, trial_period, pdot_fdas, trial_pdot, tbin, tsub);

    // generate the profile again
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), modified_candidate));

    // compute the stats
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // generate snr
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr2 = optimizer.snr();

    ASSERT_GT(snr2, snr1);
}

TEST_F(OptimizerTest, test_bin_shift_wrong_trial_period_large)
{
   /* test a scenario where FDAS has reported the right period and when one folds at the wrong trial period, you get a reduction in S/N */

   // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // are slightly offset from the pulsar parameters
    data::Scl scl = cpu::test::create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(4, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);

    float tsub = 600000.0/4;
    float tbin = 60000.0/30;

    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // generate optimizer and run a bin shift correction
    Optimizer optimizer;

    //generate the profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), candidate));

    //generate stats and snr
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr1 = optimizer.snr();

    // trial period > true period
    data::Scl::CandidateType::MsecTimeType trial_period(62.0 * boost::units::si::seconds);
    data::Scl::CandidateType::MsecTimeType period_fdas(60.0 * boost::units::si::seconds);
    data::Scl::CandidateType::SecPerSecType trial_pdot = 0.0;
    data::Scl::CandidateType::SecPerSecType pdot_fdas = 0.0;

    auto modified_candidate = optimizer.apply_bin_correction(config.cpu_algo_config(), candidate, period_fdas, trial_period, pdot_fdas, trial_pdot, tbin, tsub);

    // generate the profile again
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), modified_candidate));

    // compute the stats
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // generate snr
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr2 = optimizer.snr();

    ASSERT_GT(snr1, snr2);
}

TEST_F(OptimizerTest, test_bin_shift_wrong_trial_period_small)
{
    /* test a scenario where FDAS has reported the right period and when one folds at the wrong trial period, you get a reduction in S/N */

   // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // are slightly offset from the pulsar parameters
    data::Scl scl = cpu::test::create_scl(60.0, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(4, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);

    float tsub = 600000.0/4;
    float tbin = 60000.0/30;

    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // generate optimizer and run a bin shift correction
    Optimizer optimizer;

    //generate the profile
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), candidate));

    //generate stats and snr
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr1 = optimizer.snr();

    // trial period  < true period
    data::Scl::CandidateType::MsecTimeType trial_period(58.0 * boost::units::si::seconds);
    data::Scl::CandidateType::MsecTimeType period_fdas(60.0 * boost::units::si::seconds);
    data::Scl::CandidateType::SecPerSecType trial_pdot = 0.0;
    data::Scl::CandidateType::SecPerSecType pdot_fdas = 0.0;

    auto modified_candidate = optimizer.apply_bin_correction(config.cpu_algo_config(), candidate, period_fdas, trial_period, pdot_fdas, trial_pdot, tbin, tsub);

    // generate the profile again
    ASSERT_NO_THROW(optimizer.profile().generate_profile(config.cpu_algo_config(), modified_candidate));

    // compute the stats
    ASSERT_NO_THROW(optimizer.genstats(config.cpu_algo_config()));

    // generate snr
    ASSERT_NO_THROW(optimizer.compute_snr());

    auto snr3 = optimizer.snr();

    ASSERT_GT(snr1, snr3);
}

TEST_F(OptimizerTest, test_optimizer)
{
    // Get a CPU on which to run
    panda::PoolResource<Cpu> device(1);

    // set the dm
    cpu::Fldo<cpu::test::CpuTraits>::Dm dm = 200000 * data::parsecs_per_cube_cm;

    // create data
    auto tf_data = create_data(dm);

   // Create a candidate list with parameters that
    // are slightly offset from the pulsar parameters
    data::Scl scl = cpu::test::create_scl(60.5, dm.value());

    // Create an empty config to pass to the Fldo constructor
    fldo::Config config;

    // Create an Fldo object
    cpu::test::FldoTestHelper<cpu::test::CpuTraits> test_helper = cpu::test::create_fldo_test_helper<cpu::test::CpuTraits>(4, 16, 30, 64, config);


    // Pass these data through Fldo
    std::shared_ptr<data::Ocld> ocld_data_cube = test_helper.fold_helper(device, tf_data, scl, 0);


    // get a shared ptr to the optimized candidate
    auto candidate = ocld_data_cube->front();

    // generate optimizer and run a bin shift correction
    Optimizer optimizer;

    // run the full optimizer
    ASSERT_NO_THROW(optimizer.operator()<cpu::test::CpuTraits::TimeFrequencyType>(candidate, config.cpu_algo_config(), tf_data, scl, 0));

    // check all values
    ASSERT_EQ(optimizer.width(), 1U);
}



} // namespace test
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
