/*
 * Copyright 2024 SKA Observatory
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_TEST_OPTIMIZERTEST_H
#define SKA_CHEETAH_MODULES_FLDO_TEST_OPTIMIZERTEST_H

#include <gtest/gtest.h>
#include "cheetah/modules/fldo/Optimizer.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace test {

/**
 * @brief
 * @details
 */

class OptimizerTest : public ::testing::Test
{
    protected:
        /**
         * @brief setup the common test fixtures
         */
        void SetUp() override;

        /**
         * @brief tear down the common test fixtures
         */
        void TearDown() override;

    public:
        /**
         * @brief constructor
         */
        OptimizerTest();

        /**
         * @brief copy constructor not available
         * @internal if you add a copy constructor please remember "rule of 5" and unit tests
         */
        OptimizerTest(OptimizerTest const&) = delete;

        ~OptimizerTest();

    private:
};


} // namespace test
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FLDO_TEST_OPTIMIZERTEST_H
