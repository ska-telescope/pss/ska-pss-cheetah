/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_SIFT_CONFIG_H
#define SKA_CHEETAH_MODULES_SIFT_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/modules/sift/simple_sift/Config.h"
#include "panda/PoolSelector.h"
#include "panda/MultipleConfigModule.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sift {

/**
 * @brief	Configuration for the sift module
 */
class Config : public panda::MultipleConfigModule<utils::Config
                                                 ,simple_sift::Config
                                                 >
{
        typedef panda::MultipleConfigModule<utils::Config
                                           ,simple_sift::Config
                                           > BaseT;

    public:
        Config();

        /**
         * @brief Configuration details for the simple_sift algorithm
         */
        simple_sift::Config const& simple_sift_algo_config() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace sift
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_SIFT_CONFIG_H
