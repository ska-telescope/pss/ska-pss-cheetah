/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_SIFT_SIMPLESIFT_CONFIG_H
#define SKA_CHEETAH_MODULES_SIFT_SIMPLESIFT_CONFIG_H

#include "panda/ConfigActive.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace sift {
namespace simple_sift {

/**
 * @brief	Configuration for the simple_sift module
 *
 */
class Config : public panda::ConfigActive
{
        typedef panda::ConfigActive BaseT;

    public:
        Config();

        /**
         * @brief The max number of harmonics to search
         */
        std::size_t num_candidate_harmonics() const;
        void num_candidate_harmonics(std::size_t const& num_candidate_harmonics);

        /**
         * @brief
         */
        double match_factor() const;
        void match_factor(double const& match_factor);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        std::size_t _num_candidate_harmonics;
        double _match_factor;
};

} // namespace simple_sift
} // namespace sift
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_SIFT_SIMPLESIFT_CONFIG_H
