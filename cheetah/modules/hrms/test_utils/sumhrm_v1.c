
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void main(int argc, char *argv[])
{

  int i=0;
  /* the data file contains the power spectrum of dedispersed timeseries*/
  /* The first few points (nf1 below) are set to zero as defined by */
  /*  corresponding to a period 9.9999 */
  FILE *fp;
  char line[256];

  float *sp; /* spectrum - a 1D array of floats*/
  int nfolds = 5, fold; /* number of harmonic sums*/
  int foldvals[5] = {1,2,4,8,16}; /* successive array-stretch by a factor of 2*/
  int n,m,ii, fval;
  float x, xdiv;
  int lstidx;
  int NFS;
  int nf = 0; /* number of frequnecy channels in the spectrum*/
  int nf1 =4; /* defined by 2*tsamp*nf/Pmax, where tsamp is the original sampling time*/
  /* and Pmax is the largest period pulsar that we'd like to detect. */
  /* For the test data in here, pmax=9.9999 seconds, and tsamp=54.36666 us*/

  /* The 2D array with 5 rows and nf columns. The rows contain the harmonic sums. */
  /* first row with strech=1, or same as the original spectra, second*/
  /*  row contains 2nd harmonic sum, ie spectra streched by 2 and added to the original - */
  /* Third row contains the 3rd harmonic sum etc. */

  /* only five harmonics - so declare the number of rows we need */
  float **power = malloc(nfolds * sizeof(*power));

  /* following lines read the file with input spectra. Int he processes, we find out the */
  /* number of columns  and use that to dynamically allocate space*/
  fp = fopen(argv[1], "r");

  while ( fgets(line, 256, fp) != NULL ) nf++;

  fprintf(stderr,"read %d lines of data\n",nf);

  /* we got the number of columns - declare second dimension of the array*/
  for(fold = 0; fold < nfolds; fold++){
	power[fold] = malloc(nf * sizeof(**power));
	assert(power[fold] != NULL);
  }

  /* and this array contains the original spectra*/
  sp = (float *) malloc(nf*4);
  assert(sp != NULL);

  /* rewind file, so that we can read from the start*/
  fseek(fp, 0, SEEK_SET);

  i=0;

  while ( fgets(line, 250, fp) != NULL ) {
    sp[i] = strtof(line, NULL);
    i++;
  };
  fprintf(stderr,"converted %d lines of data\n",i);

  /* Harmonic sum - a direct translation of sigproc's sumhrm.f */
  /* Fortran's array conventions are inverted as is normal in C*/
  for (fold=0; fold< nfolds; fold++){
    fval = foldvals[fold];
	/* the first harmonic sum is just the copy of original array */
    if (fval == 1) for (n=0;n<nf;n++) power[0][n] = sp[n];
    else {
      NFS = fval*nf1 - fval/2;
      xdiv = 1.0/fval;
      for (n=NFS; n< nf; n++){
        power[fold][n] = 0.0;
        lstidx = -1;
        for (m=0;m<foldvals[fold];m++){
          x = n * m * xdiv + 0.5; /* stretch by this factor before adding*/
          ii = (int) x;
		  if ( ii > 1 && ii != lstidx) power[fold][n] = power[fold][n] + sp[ii];
          lstidx = ii;
        }
      }
    }
  }

  /* write out the first 20000 values*/
  for (ii=0;ii<20000;ii++){
	fprintf(stdout, "%07d %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f",
			ii, sp[ii],power[0][ii],power[1][ii],power[2][ii],power[3][ii],power[4][ii]);
	printf("\n");
  }

  /* release the memory area and close data file*/
  for(fold = 0; fold < nfolds; fold++) free(power[fold]);

  free(power);

  fclose(fp);
}