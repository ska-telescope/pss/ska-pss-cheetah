/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/hrms/test_utils/HrmsTester.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/utils/Architectures.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace modules {
namespace hrms {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
HrmsTesterTraits<ArchitectureTag,ArchitectureCapability>::HrmsTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
hrms::Hrms& HrmsTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}


template <typename DeviceType, typename DataType>
struct ExectionTest
{
    inline static void test(DeviceType& device, hrms::Hrms& api)
    {
        DataType input(0.001 * data::hz);
        std::vector<DataType> output(4);
        input.resize(20);
        api.process(device,input,output);
    }
};

template <typename TestTraits>
HrmsTester<TestTraits>::HrmsTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
HrmsTester<TestTraits>::~HrmsTester()
{
}

template<typename TestTraits>
void HrmsTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void HrmsTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(HrmsTester, test_execution)
{
    TypeParam traits;
    ExectionTest<typename TypeParam::DeviceType, data::PowerSeries<typename TypeParam::Arch,float>>::test(device,traits.api());
    ExectionTest<typename TypeParam::DeviceType, data::PowerSeries<typename TypeParam::Arch,double>>::test(device,traits.api());
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_SUITE_P(HrmsTester, test_execution);

} // namespace test
} // namespace hrms
} // namespace modules
} // namespace cheetah
} // namespace ska
