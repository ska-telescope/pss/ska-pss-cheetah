#include "cheetah/modules/hrms/cuda/Hrms.cuh"
#include "cheetah/data/Units.h"
#include "cheetah/cuda_utils/cuda_thrust.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace hrms {
namespace cuda {
namespace detail {

/**
 * @brief      A thrust functor for harmonic summing
 *
 * @details    This is a private class that is only used by the
 *             cuda::Hrms object to implement harmonic summing
 *             using the for_each capabilites of Thrust.
 *
 * @tparam     T          The value type of the series to be summed
 * @tparam     max_harms  The max number of sums to be performed
 *                        used for compile-time loop unrolling.
 */
template <typename T, unsigned max_harms>
struct HarmonicSumFunctor
{
    public:
        /**
         * @brief      Construct a new instance
         *
         * @param      input   Pointer to the data to be summed.
         * @param      output  Array of pointers to the output data buffers
         */
        HarmonicSumFunctor(T const* input, T** output);

        /**
         * For a given index this method will calculate the relevant
         * summed value for that index in each output array.
         */
        inline __host__ __device__
        void operator() (unsigned idx) const;

    private:
        T const* _input;
        T** _output;
};

template <typename T, unsigned max_harms>
HarmonicSumFunctor<T, max_harms>::HarmonicSumFunctor(T const* input, T** output)
    : _input(input)
    , _output(output)
{
}

/**
 * This is not an optimised implementation of the harmonic summing, but it should
 * be within ~5% of the speed of a texture + tricks implementation (assuming enough
 * information is available at compile time to unroll).
 */
template <typename T, unsigned max_harms>
inline __host__ __device__ void HarmonicSumFunctor<T, max_harms>::operator() (unsigned idx) const
{
    unsigned int ii, jj, harm;
    float fharm;
    T val = _input[idx];

    //#pragma unroll -- these want to be unrolled
    for (jj=0; jj<max_harms; jj++)
    {
        harm = 1<<(jj+1);
        fharm = 1.0f/harm;

        //#pragma unroll -- these want to be unrolled
        for (ii=1;ii<harm;ii+=2)
        {
            float factor = ii*fharm;
            val += _input[(unsigned int) (idx*factor + 0.5f)];
        }
        _output[jj][idx] = val;
    }
}

} // namespace detail


template <typename T, typename Alloc>
void Hrms::process(ResourceType& gpu,
    data::PowerSeries<cheetah::Cuda,T,1,Alloc> const& input,
    std::vector<data::PowerSeries<cheetah::Cuda,T,1,Alloc>>& output)
{
    /**
     * To keep the cleanness of just using the PowerSeries class
     * rather than implementing a PowerSeries2D-like class with a
     * single contiguous storage buffer, we make the decision here to
     * use std::vectors of PowerSeries objects for the output of
     * the method. To use these on the device, it is necessary to first
     * create thrust::device_vector of device pointers that can itself
     * be passed as a raw pointer to any function. Here this is done by
     * first creating a host_vector, filling it with raw pointer casts of
     * each output series and finally copying that host_vector back to
     * the device where it can be used.
     */
    PUSH_NVTX_RANGE("cuda_Hrms_process",0);
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();

    PUSH_NVTX_RANGE("cuda_Hrms_process_prepare",1);
    //container for device pointers
    thrust::host_vector<T*> output_ptrs_host(output.size());

    for (int idx=0; idx<output.size(); ++idx)
    {
        auto& series = output[idx];
        double hnum = (double)(1<<(idx+1));
        //resize the outputs to the correct size
        series.resize(input.size());

        //recalculate the dof
        series.degrees_of_freedom(input.degrees_of_freedom()*hnum);

        //set the outputs metadata
        series.frequency_step((input.frequency_step().value()/hnum) * data::hz);

        //store device pointer in host_vector
        output_ptrs_host[idx] = thrust::raw_pointer_cast(series.data());
    }

    //copy the host array of device pointers to the device
    thrust::device_vector<T*> output_ptrs_device = output_ptrs_host;
    auto input_ptr = thrust::raw_pointer_cast(input.data());
    auto output_ptrs = thrust::raw_pointer_cast(output_ptrs_device.data());
    thrust::counting_iterator<unsigned> begin(0);
    thrust::counting_iterator<unsigned> end = begin + input.size();

    POP_NVTX_RANGE; // cuda_Hrms_process_prepare
    PUSH_NVTX_RANGE("cuda_Hrms_process_execute_kernels",2);
    //Here we use a switch case based on the number of PowerSeries objects in
    //the output vector. This determines the number of harmonic sums to be performed.
    //The switch case then allows us to dispatch to a partial specialisation of the
    //harmonic summing functor.
    switch (output.size())
    {
    case 1:
        thrust::for_each(thrust::cuda::par, begin, end, detail::HarmonicSumFunctor<T,1>(input_ptr,output_ptrs));
        break;
    case 2:
        thrust::for_each(thrust::cuda::par, begin, end, detail::HarmonicSumFunctor<T,2>(input_ptr,output_ptrs));
        break;
    case 3:
        thrust::for_each(thrust::cuda::par, begin, end, detail::HarmonicSumFunctor<T,3>(input_ptr,output_ptrs));
        break;
    case 4:
        thrust::for_each(thrust::cuda::par, begin, end, detail::HarmonicSumFunctor<T,4>(input_ptr,output_ptrs));
        break;
    case 5:
        thrust::for_each(thrust::cuda::par, begin, end, detail::HarmonicSumFunctor<T,5>(input_ptr,output_ptrs));
        break;
    default:
        panda::Error("Invalid number of sums requested of Hrms.");
    }
    POP_NVTX_RANGE; // cuda_Hrms_process_execute_kernels
    POP_NVTX_RANGE; // cuda_Hrms_process
}

} // namespace cuda
} // namespace hrms
} // namespace modules
} // namespace cheetah
} // namespace ska