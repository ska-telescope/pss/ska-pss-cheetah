#include "cheetah/modules/dred/cuda/detail/MedianScrunch.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace dred {
namespace cuda {
namespace detail {

template <typename T>
inline __host__ __device__ T median3(T a, T b, T c)
{
    return a < b ? b < c ? b
    : a < c ? c : a
    : a < c ? a
    : b < c ? c : b;
}

template <typename T>
inline __host__ __device__  T median4(T a, T b, T c, T d)
{
    return a < c ? b < d ? a < b ? c < d ? 0.5f*(b+c) : 0.5f*(b+d)
    : c < d ? 0.5f*(a+c) : 0.5f*(a+d)
    : a < d ? c < b ? 0.5f*(d+c) : 0.5f*(b+d)
    : c < b ? 0.5f*(a+c) : 0.5f*(a+b)
    : b < d ? c < b ? a < d ? 0.5f*(b+a) : 0.5f*(b+d)
    : a < d ? 0.5f*(a+c) : 0.5f*(c+d)
    : c < d ? a < b ? 0.5f*(d+a) : 0.5f*(b+d)
    : a < b ? 0.5f*(a+c) : 0.5f*(c+b);
}

template <typename T>
inline __host__ __device__ T median5(T a, T b, T c, T d, T e)
{
    return b < a ? d < c ? b < d ? a < e ? a < d ? e < d ? e : d
    : c < a ? c : a
    : e < d ? a < d ? a : d
    : c < e ? c : e
    : c < e ? b < c ? a < c ? a : c
    : e < b ? e : b
    : b < e ? a < e ? a : e
    : c < b ? c : b
    : b < c ? a < e ? a < c ? e < c ? e : c
    : d < a ? d : a
    : e < c ? a < c ? a : c
    : d < e ? d : e
    : d < e ? b < d ? a < d ? a : d
    : e < b ? e : b
    : b < e ? a < e ? a : e
    : d < b ? d : b
    : d < c ? a < d ? b < e ? b < d ? e < d ? e : d
    : c < b ? c : b
    : e < d ? b < d ? b : d
    : c < e ? c : e
    : c < e ? a < c ? b < c ? b : c
    : e < a ? e : a
    : a < e ? b < e ? b : e
    : c < a ? c : a
    : a < c ? b < e ? b < c ? e < c ? e : c
    : d < b ? d : b
    : e < c ? b < c ? b : c
    : d < e ? d : e
    : d < e ? a < d ? b < d ? b : d
    : e < a ? e : a
    : a < e ? b < e ? b : e
    : d < a ? d : a;
}

template <typename T>
Median5Functor<T>::Median5Functor(const T* in)
    :_in(in)
{
}

template <typename T>
inline __host__ __device__ T Median5Functor<T>::operator()(unsigned int i) const
{
    T a = _in[5*i+0];
    T b = _in[5*i+1];
    T c = _in[5*i+2];
    T d = _in[5*i+3];
    T e = _in[5*i+4];
    return median5<T>(a, b, c, d, e);
}

template <typename T>
LinearStretchFunctor<T>::LinearStretchFunctor(const T* in, unsigned in_size, float step)
    : _in(in)
    , _in_size(in_size)
    , _step(step)
    , _correction(((int)(_step/2))/_step)
{
}

template <typename T>
T LinearStretchFunctor<T>::operator()(unsigned out_idx) const
{
    float fidx = ((float)out_idx) / _step - _correction;
    unsigned idx = (unsigned) fidx;
    if (fidx<0)
        idx = 0;
    else if (idx + 1 >= _in_size)
        idx = _in_size-2;
    return _in[idx] + ((_in[idx+1] - _in[idx]) * (fidx-idx));
}

} // namespace detail
} // namespace cuda
} // namespace dred
} // namespace modules
} // namespace cheetah
} // namespace ska
