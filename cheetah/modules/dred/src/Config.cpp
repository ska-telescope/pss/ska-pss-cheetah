/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/dred/Config.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace dred {


Config::Config()
    : utils::Config("dred")
    , _oversmoothing_factor(5)
{
    add(_cuda_config);
    add(_pwft_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("oversmoothing_factor", boost::program_options::value<std::size_t>(&_oversmoothing_factor)->default_value(5L),
        "This parameter determines a scaling factor for the width of the median filter window in the dereddening algorithm.");
}

cuda::Config const& Config::cuda_config() const
{
    return _cuda_config;
}

modules::pwft::Config const& Config::pwft_config() const
{
    return _pwft_config;
}

std::size_t Config::oversmoothing_factor() const
{
    return _oversmoothing_factor;
}

void Config::oversmoothing_factor(std::size_t val)
{
    _oversmoothing_factor = val;
}

} // namespace dred
} // namespace modules
} // namespace cheetah
} // namespace ska
