/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <vector>
#include "cheetah/modules/tdas/Config.h"
#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {
namespace test {

TEST(AccListGenerator, GenList) {

    AccListGenConfig config;

    config.acc_lo(-250.0);
    config.acc_hi( 250.0);
    config.tolerance(1.10);
    config.pulse_width(0.000064);
    config.cfreq(1400.0);
    config.bw(0.3);
    data::DedispersionMeasureType<float> dm(1000.0 * data::parsecs_per_cube_cm);
    auto accel_list = config.acceleration_list(dm,1<<23,0.000064);
    std::cout << "acceleration trials generated: " << accel_list.size() << std::endl;
    ASSERT_EQ(accel_list.size(),(std::size_t)243);
    for (auto const& acc: accel_list)
        std::cout << acc << "\t";
    std::cout << "\n";
}

} // namespace test
} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska