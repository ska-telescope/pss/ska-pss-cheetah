/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_TDAS_CUDA_CONFIG_H
#define SKA_CHEETAH_MODULES_TDAS_CUDA_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/modules/tdrt/Config.h"
#include "cheetah/modules/tdao/Config.h"
#include "cheetah/modules/pwft/Config.h"
#include "cheetah/modules/hrms/Config.h"
#include "cheetah/modules/fft/Config.h"
#include "cheetah/modules/brdz/Config.h"
#include "cheetah/modules/dred/Config.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {
namespace cuda {

/**
 * @brief
 *
 * @details
 *
 */

class Config : public utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief indicate if the algorithm is to be used
         */
        bool active() const;

        /**
         * @brief      Get the algorithm configuration for the Dred module
         */
        modules::dred::Config const& dred_config() const;
        modules::dred::Config& dred_config();

        /**
         * @brief      Get the algorithm configuration for the Brdz module
         */
        modules::brdz::Config const& brdz_config() const;
        modules::brdz::Config& brdz_config();

        /**
         * @brief      Get the algorithm configuration for the Tdrt module
         */
        modules::tdrt::Config const& tdrt_config() const;
        modules::tdrt::Config& tdrt_config();

        /**
         * @brief      Get the algorithm configuration for the Tdao module
         */
        modules::tdao::Config const& tdao_config() const;
        modules::tdao::Config& tdao_config();

        /**
         * @brief      Get the algorithm configuration for the Pwft module
         */
        modules::pwft::Config const& pwft_config() const;
        modules::pwft::Config& pwft_config();

        /**
         * @brief      Get the algorithm configuration for the Hrms module
         */
        modules::hrms::Config const& hrms_config() const;
        modules::hrms::Config& hrms_config();

        /**
         * @brief      Get the algorithm configuration for the Fft module
         */
        modules::fft::Config const& fft_config() const;
        modules::fft::Config& fft_config();

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        modules::dred::Config _dred_config;
        modules::brdz::Config _brdz_config;
        modules::tdrt::Config _tdrt_config;
        modules::tdao::Config _tdao_config;
        modules::pwft::Config _pwft_config;
        modules::hrms::Config _hrms_config;
        modules::fft::Config _fft_config;

};


} // namespace cuda
} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_TDAS_CUDA_CONFIG_H
