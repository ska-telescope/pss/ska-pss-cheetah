/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_TDAS_TEST_TDASTESTER_H
#define SKA_CHEETAH_MODULES_TDAS_TEST_TDASTESTER_H

#include "cheetah/modules/tdas/Config.h"
#include "cheetah/modules/tdas/Tdas.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/Units.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {
namespace test {

/**
 * @brief
 * Generic functional test for the TdasTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requiremnst it needs to run.
 *
 * e.g.
 * @code
 * template <typename T>
 * struct CudaTraits
 *   : public tdas::test::TdasTesterTraits<typename tdas::cuda::Tdas<T>::Architecture, typename tdas::cuda::Tdas<T>::ArchitectureCapability, T>
 * {
 *   typedef tdas::cuda::Tdas<T> ApiType;
 *   typedef typename ApiType::Architecture Arch;
 *   typedef typename tdas::test::TdasTesterTraits< Arch, typename ApiType::ArchitectureCapability, T> BaseT;
 *   typedef typename BaseT::DeviceType DeviceType;
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_SUITE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<CudaTraits<float>, OpenClTraits<double>> MyTypes;
 * INSTANTIATE_TYPED_TEST_SUITE_P(MyAlgo, TdasTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_SUITE_P must be in the same namespace as this class
 *
 */


template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
struct TdasTesterTraits : public utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability>
{
    public:
        typedef utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::DeviceType DeviceType;
        typedef T ValueType;

    public:
        TdasTesterTraits();
        tdas::Tdas<T>& api();
        tdas::Config& config();

    private:
        tdas::Config _config;
        tdas::Tdas<T> _api;
};


template <typename TestTraits>
class TdasTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        TdasTester();
        ~TdasTester();

    private:
};

TYPED_TEST_SUITE_P(TdasTester);

} // namespace test
} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/tdas/test_utils/detail/TdasTester.cpp"


#endif // SKA_CHEETAH_MODULES_TDAS_TEST_TDASTESTER_H
