subpackage(cuda)

set(MODULE_TDAS_LIB_SRC_CPU
    src/Config.cpp
    src/AccListGenConfig.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

set(MODULE_TDAS_LIB_SRC_CUDA
    ${LIB_SRC_CUDA}
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
