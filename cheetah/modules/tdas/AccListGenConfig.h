/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_TDRT_ACCELERATIONLISTGENCONFIG_H
#define SKA_CHEETAH_MODULES_TDRT_ACCELERATIONLISTGENCONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include <cmath>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {

/**
 * @brief
 *   AccelerationListGen configuration parameters
 *
 */
class AccListGenConfig : public cheetah::utils::Config
{
    public:
        AccListGenConfig();
        ~AccListGenConfig();

        /**
         * @brief return/set the acceleration start m/s/s
         */
        double acc_lo() const;
        void acc_lo(double);

        /**
         * @brief return/set the acceleration end m/s/s/ m/s/s
         */
        double acc_hi() const;
        void acc_hi(double);

        /**
         * @brief return/set the acceleration smearing tolerance (1.11=10%)
         */
        double tolerance() const;
        void tolerance(double);

        /**
         * @brief return/set the minimum pulse width for which acc_tol is valid (in us)
         */
        double pulse_width() const;
        void pulse_width(double);

        /**
         * @brief return/set the centre frequency of the data block (in MHz)
         */
        double cfreq() const;
        void cfreq(double);

        /**
         * @brief return the channel bandwidth (in MHz)
         */
        double bw() const;
        void bw(double);

        /**
         * @brief  return the maximum absolute acceleration magnitude
         */
        data::AccelerationType magnitude() const;

        /**
         * @brief      Generate a list of accelerations
         *
         * @param[in]  dm                 The current dispersion measure
         * @param[in]  number_of_samples  The number of samples
         * @param[in]  sampling_interval  The sampling interval
         *
         * @return     A vector of accelerations
         */
        std::vector<data::AccelerationType> acceleration_list(data::DedispersionMeasureType<float> dm,
            std::size_t number_of_samples,
            double sampling_interval) const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        double _acc_lo;
        double _acc_hi;
        double _tol;
        double _pulse_width;
        double _cfreq;
        double _bw;
};

} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_TDRT_ACCELERATIONLISTGENCONFIG_H
