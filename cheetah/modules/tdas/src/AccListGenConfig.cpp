/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/tdas/AccListGenConfig.h"
#include <cmath>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {

static const double  default_acc_lo = -5.0;
static const double  default_acc_hi = 5.0;
static const double  default_tol= 1.10;
static const double  default_pulse_width = 0.000064;
static const double  default_cfreq = 1400.0;
static const double  default_bw = 700.0/4960.0;

AccListGenConfig::AccListGenConfig()
    : cheetah::utils::Config("acceleration_list_generator")
    , _acc_lo(default_acc_lo)
    , _acc_hi(default_acc_hi)
    , _tol(default_tol)
    , _pulse_width(default_pulse_width)
    , _cfreq(default_cfreq)
    , _bw(default_bw)
{
}

AccListGenConfig::~AccListGenConfig()
{
}

void AccListGenConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("acc_lo", boost::program_options::value<double>(&_acc_lo)->default_value(default_acc_lo),
    "acceleration start m/s/s")

    ("acc_hi", boost::program_options::value<double>(&_acc_hi)->default_value(default_acc_hi),
     "acceleration end m/s/s/")

    ("tol", boost::program_options::value<double>(&_tol)->default_value(default_tol),
     "acceleration smearing tolerance (1.11=10%)")

    ("pulse_width", boost::program_options::value<double>(&_pulse_width)->default_value(default_pulse_width),
     "minimum pulse width for which acc_tol is valid (in seconds)")

    ("cfreq", boost::program_options::value<double>(&_cfreq)->default_value(default_cfreq),
     "centre frequency of the data block (in MHz)")

    ("bw", boost::program_options::value<double>(&_bw)->default_value(default_bw),
     "channel bandwidth (in MHz)");
}

double AccListGenConfig::acc_lo() const
{
    return _acc_lo;
}

void AccListGenConfig::acc_lo(double acc_lo)
{
    _acc_lo = acc_lo;
}

double AccListGenConfig::acc_hi() const
{
    return _acc_hi;
}

void AccListGenConfig::acc_hi(double acc_hi)
{
    _acc_hi = acc_hi;
}

double AccListGenConfig::tolerance() const
{
    return _tol;
}

void AccListGenConfig::tolerance(double tol)
{
    _tol = tol;
}

double AccListGenConfig::pulse_width() const
{
    return _pulse_width;
}

void AccListGenConfig::pulse_width(double pulse_width)
{
    _pulse_width = pulse_width;
}

double AccListGenConfig::cfreq() const
{
    return _cfreq;
}

void AccListGenConfig::cfreq(double cfreq)
{
    _cfreq = cfreq;
}

double AccListGenConfig::bw() const
{
    return _bw;
}

void AccListGenConfig::bw(double bw)
{
    _bw = bw;
}

data::AccelerationType AccListGenConfig::magnitude() const
{
    return std::max(std::abs(_acc_lo),std::abs(_acc_hi)) * data::meters_per_second_squared;
}

std::vector<data::AccelerationType> AccListGenConfig::acceleration_list(
    data::DedispersionMeasureType<float> dm,
    std::size_t number_of_samples,
    double sampling_interval) const
{
    std::vector<data::AccelerationType> acc_list;
    double _tobs = number_of_samples*sampling_interval;

    if (_acc_hi == _acc_lo) {
        acc_list.push_back(_acc_lo * data::meters_per_second_squared);
        return acc_list;
    }
    double tdm = (8.3 * std::abs(_bw) / std::pow(_cfreq,3.0) * dm.value());
    double c = boost::units::si::constants::codata::c.value().value();
    double ttdm = tdm*tdm;
    double tpulse = _pulse_width*_pulse_width;
    double ttsamp = sampling_interval*sampling_interval;
    double ttobs = _tobs * _tobs;
    double width = std::sqrt(ttdm+tpulse+ttsamp);
    double alt_a = 2.0 * width * 24.0 * c/ttobs * std::sqrt((_tol*_tol)-1.0);
    std::size_t naccels = std::size_t(((_acc_hi-_acc_lo)/alt_a));
    acc_list.reserve(naccels+3);
    acc_list.push_back(0.0 * data::meters_per_second_squared);
    for (auto acc=_acc_lo; acc <= _acc_hi; acc+=alt_a)
    {
        acc_list.push_back(acc * data::meters_per_second_squared);
    }
    return acc_list;
}


} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska
