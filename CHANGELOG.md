# Changelog

## 0.5.0 (2024-02-19)

### New

* AT4-1570 First connection of Acceleration Search with FLDO
* AT4-1701 Make a skeleton for Z-dot impl interface in Rfim module
* AT4-1703 Make an implementation of Z-dot filter in cheetah

### Change

* AT4-1535 Expose FLDO config parameters
* AT4-1651 Convert Ocld to save floating point data
* AT4-1659 Adding optimization step to FLDO
* AT4-1680 added log message class relevant to PSS control layer
* AT4-1725 Add a metric of spectral moment to IQRM RFIM algorithm in cheetah
* AT4-1728 Optimise flagged channel replacement in IQRM RFIM algorithm


### Fix

* AT4-1631 Fix FLDO implementation