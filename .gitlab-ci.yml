variables:
  GIT_STRATEGY: clone
  CUDA_PATH: -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-12.2/
  CURRENT_CHEETAH_VERSION: 0.5.0

default:
  interruptible: true

stages:
  - set-up
  - build-panda
  - build-astrotypes
  - lint
  - build
  - test
  - product-test
  - package
  - publish
  - publish-docs
  - clean-up

.common:
  rules:
      # Execute pipeline automatically if commit is made to dev or main
      # (this should only happen when code is merged to dev or main)
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"

      # Create but do not execute pipeline for feature branches on commit,
      # or if created via the gitlab GUI
    - if: ($CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "api")
      when: manual
      allow_failure: false

      # Execute pipeline if triggered by a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"

      # Execute pipeline automatically if triggered by a MR
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

# This job sets up a symbolic link to from the root repository to build/ska-pss-cheetah/source. This is done because
# we need the following filesystem structure in order to keep the panda source code, astrotypes source code and cheetah source code separate
# or else this caused issues for our cmake build system:
# cheetah/build
# cheetah/source
# panda/build
# panda/install
# panda/source
# astrotypes/build
# astrotypes/install
# astrotypes/source
# We put the above structure inside an additional 'build' directory to comply with SKAO CICD Pipeline conventions.
set-up:
  stage: set-up
  extends: .common
  before_script:
    - required_space=20000000
    - available_space=`df -k $HOME | tail -1 | awk '{ print $4 }'`
    - if (( available_space < required_space )); then echo “Insufficient disk space to store required cache. Available” >&2 ; exit 1 ; fi
  script:
    - mkdir build && cd build
    - mkdir cheetah && cd cheetah
    - cd ../../
    - ln -s $CI_PROJECT_DIR/ build/cheetah/source
  tags:
    - ubuntu-22.04
  artifacts:
    name: "$CI_COMMIT_REF_NAME-cheetah"
    paths:
      - build/cheetah/source/*
    expire_in: 1 d

.build-panda:
  stage: build-panda
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - cd build
    - mkdir panda && cd panda
    - git clone https://gitlab.com/ska-telescope/pss/ska-pss-panda.git source
    - if [ "$CI_COMMIT_BRANCH" == "main" ]; then cd source && git checkout main && cd .. ; fi
    - >
        if [ "$FPGA_BUILD" == "true" ]; then
          cd source
          git submodule init
          git config submodule.thirdparty/intel_agilex_fpga.url https://pss_cheetah:$FPGA_SUBMODULE_DEPLOY_TOKEN@gitlab.com/ska-telescope/pss/ska-pss-fdas-fpga-interface.git
          git submodule update --remote --merge --recursive && /usr/bin/make -C thirdparty/intel_agilex_fpga/intel_lib/ CFLAGS='-O0 -g' all
          cd .. ;
        fi
    - mkdir build install
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX=../install $CMAKE_FLAGS ../source
    - make -j 4
    - make install
  artifacts:
    name: "$CI_COMMIT_REF_NAME-panda-install"
    paths:
      - build/panda/install/*
    expire_in: 1 d

.build-astrotypes:
  stage: build-astrotypes
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - cd build
    - mkdir astrotypes && cd astrotypes
    - git clone https://gitlab.com/ska-telescope/pss/ska-pss-astrotypes.git source
    - mkdir build install
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX=../install $CMAKE_FLAGS ../source
    - make -j 4
    - make install
  artifacts:
    name: "$CI_COMMIT_REF_NAME-astrotypes-install"
    paths:
      - build/astrotypes/install/*
    expire_in: 1 d

lint:
  stage: lint
  needs:
    - set-up
    - build-panda-release
    - build-astrotypes-release
  image: ubuntu:22.04
  rules:
      # Execute job automatically if commit is made to dev or main
      # (this should only happen when code is merged to dev or main).
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
      allow_failure: true

      # Execute job if triggered by a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"
      allow_failure: true

      # Execute job automatically if triggered by a MR
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      allow_failure: true
  tags:
    - ska-default-xlarge
  script:
    - apt-get -y update
    - apt install -y cmake
    - cmake --version
    - apt install -y python3 gcc g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libfftw3-dev git
    - apt install -y clang-tidy
    - clang-tidy --version
    - cd build/cheetah
    - mkdir build && cd build
    - 'cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
      -DPANDA_INSTALL_DIR=$CI_PROJECT_DIR/build/panda/install -DASTROTYPES_INSTALL_DIR=$CI_PROJECT_DIR/build/astrotypes/install
      -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false ../source'
    - 'run-clang-tidy -checks=''cppcoreguidelines-*,performance-*,readibility-*,modernize-*,misc-*,clang-analyzer-*,google-*,
      -cppcoreguidelines-pro-type-reinterpret-cast,-cppcoreguidelines-pro-bounds-array-to-pointer-decay,-modernize-use-nodiscard''
      -quiet 2>&1 | tee clang-tidy.log'
    - mkdir reports
    - python3 ../source/tools/clang-tidy-to-junit/clang-tidy-to-junit.py clang-tidy.log ../source/cheetah clang-tidy > reports/lint-results.xml
  artifacts:
    name: "$CI_COMMIT_REF_NAME-lint"
    paths:
      - build/cheetah/build/clang-tidy.log
      - build/cheetah/build/reports/lint-results.xml
    expire_in: 1 d

.build:
  stage: build
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - cd build/cheetah
    - mkdir build && cd build
    - cmake -DPANDA_INSTALL_DIR=$CI_PROJECT_DIR/build/panda/install -DASTROTYPES_INSTALL_DIR=$CI_PROJECT_DIR/build/astrotypes/install $CMAKE_FLAGS ../source
    - make -j 4
    - cd ../.. && tar -czvf cheetahBuild.tar.gz cheetah
  cache:
    key:
      files:
        - cheetahBuild.tar.gz
      prefix: $CI_PIPELINE_ID-$SPIN
    paths:
      - build
    policy: push

.test:
  stage: test
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - cd build/ && tar -xzvf cheetahBuild.tar.gz
    - cd cheetah/build/
    - >
        if [ "$FPGA_TEST" == "true" ]; then
          sudo setcap cap_sys_admin+ep cheetah/modules/fdas/intel_fpga/test/gtest_fdas_intel_fpga
          sudo setcap cap_sys_admin+ep cheetah/pipelines/search_pipeline/test/gtest_pipeline
        fi
    - mkdir reports
    # The CMake documentation states we do not have to use CMake in order to use CTest. We are using CTest
    # directly here so we can use the 'output-on-failure' flag to give verbose details of a failed test.
    - ctest --output-on-failure --force-new-ctest-process --output-junit reports/test-results-$SPIN.xml
  retry:
    max: 2
  cache:
    key:
      files:
        - cheetahBuild.tar.gz
      prefix: $CI_PIPELINE_ID-$SPIN
    paths:
      - build
    policy: pull
  artifacts:
    reports:
      junit: build/cheetah/build/reports/test-results-$SPIN.xml

.product-test:
  stage: product-test
  extends: .common
  before_script:
   - mkdir venv
   - python3 -m venv venv/$CI_JOB_ID
   - source venv/$CI_JOB_ID/bin/activate
   - cd build/ && tar -xzvf cheetahBuild.tar.gz
   - cd cheetah/build/
   - pip install --upgrade pip
   - pip install --index-url https://artefact.skao.int/repository/pypi-internal/simple --extra-index-url https://pypi.org/simple ska-pss-protest
   - mkdir $CI_PROJECT_DIR/protest_results
  tags:
    - ubuntu-22.04
  cache:
    key:
      files:
        - cheetahBuild.tar.gz
      prefix: $CI_PIPELINE_ID-$SPIN
    paths:
      - build
    policy: pull

.package:
  stage: package
  rules:
  #   # Execute job only on main branch
    - if: $CI_COMMIT_BRANCH == "main"
  extends: .common
  script:
    - cd build/ && tar -xzvf cheetahBuild.tar.gz
    - cd cheetah/build/
    # In order to bypass the check cmake performs, that checks the source directory in the cache is the same as the current target directory for
    # packaging, we need to clear the cache. This is an issue that occurs only when running 'make package' in a gitlab ci pipeline because each
    # job is entirely independent and may end up running using a different directory due to the gitlab-runner concurrency. This unfortunatly
    # means make builds cheetah again which slows things down.
    - rm CMakeCache.txt
    - >
        if [ -f "$GTEST_CMAKE_CACHE" ]; then
          rm "$GTEST_CMAKE_CACHE"
        fi
    - cmake -DPANDA_INSTALL_DIR=$CI_PROJECT_DIR/build/panda/install -DASTROTYPES_INSTALL_DIR=$CI_PROJECT_DIR/build/astrotypes/install $CMAKE_FLAGS ../source
    - make -j package
  tags:
    - ubuntu-22.04
  cache:
    key:
      files:
        - cheetahBuild.tar.gz
      prefix: $CI_PIPELINE_ID-$SPIN
    paths:
      - build
    policy: pull
  artifacts:
    name: "$CI_COMMIT_REF_NAME-package"
    paths:
      - build/cheetah/build/$CHEETAH_PACKAGE_NAME.deb
      - build/cheetah/build/CPackConfig.cmake
    expire_in: 1 d

# As part of the publishing stage we capture the output from curl and fail when the server does not return the requested artefact/data. This is
# in place to ensure the gitlab job fails if the curl is unsuccessful
.publish:
  stage: publish
  rules:
    # Execute job only on main branch
    - if: $CI_COMMIT_BRANCH == "main"
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - cd build/cheetah/build/
    - "curl -i -u ${CAR_APT_USERNAME}:${CAR_APT_PASSWORD} -H \"Content-Type: multipart/form-data\" --data-binary \"@${CHEETAH_PACKAGE_NAME}.deb\"
      \"https://artefact.skao.int/repository/apt-bionic-internal/\" | tee curl.out 2>&1"
    - reply=`cat curl.out | grep HTTP | head -1 | awk '{ print $2 }'`
    - if [ $reply -ne 201 ] ; then exit 1 ; fi

build-panda-release:
  extends: .build-panda
  needs:
    - set-up
  variables:
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false"

build-panda-release-cuda:
  extends: .build-panda
  needs:
    - set-up
  variables:
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=true"

build-panda-release-fpga:
  extends: .build-panda
  tags:
    - pss-kitsune
  needs:
    - set-up
  variables:
    FPGA_BUILD: true
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_INTEL_FPGA=true"
  after_script:
    - sudo setcap cap_sys_admin+ep build/panda/build/panda/arch/intel_agilex_fpga/test/gtest_panda_intel_fpga

build-astrotypes-release:
  extends: .build-astrotypes
  needs:
    - set-up
  variables:
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false"

# For this and other cheetah build job specifications we require both <platform>-set-up and
# <platform>-build-panda-<build_type> to have completed. The file structure generated by "set-up"
# must be made available to the cheetah build jobs, as well as to the panda build jobs. For this
# reason, both are listed as "needs" in all cheetah build jobs.
build-debug:
  extends: .build
  needs:
    - set-up
    - build-panda-release
    - build-astrotypes-release
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=debug -DENABLE_CUDA=false -DENABLE_NASM=false"

build-debug-nasm:
  extends: .build
  needs:
    - set-up
    - build-panda-release
    - build-astrotypes-release
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-nasm
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=debug-DENABLE_CUDA=false -DENABLE_NASM=true"

build-release:
  extends: .build
  needs:
    - set-up
    - build-panda-release
    - build-astrotypes-release
  variables:
    SPIN: release
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=false"

build-release-nasm:
  extends: .build
  needs:
    - set-up
    - build-panda-release
    - build-astrotypes-release
  variables:
    SPIN: release-nasm
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=true"

build-debug-cuda:
  extends: .build
  needs:
    - set-up
    - build-panda-release-cuda
    - build-astrotypes-release
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-cuda
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=debug -DENABLE_CUDA=true -DENABLE_NASM=false"

build-debug-cuda-nasm:
  extends: .build
  needs:
    - set-up
    - build-panda-release-cuda
    - build-astrotypes-release
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-cuda-nasm
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=debug -DENABLE_CUDA=true -DENABLE_NASM=true"

build-release-cuda:
  extends: .build
  needs:
    - set-up
    - build-panda-release-cuda
    - build-astrotypes-release
  variables:
    SPIN: release-cuda
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=true -DENABLE_NASM=false"

build-release-cuda-nasm:
  extends: .build
  needs:
    - set-up
    - build-panda-release-cuda
    - build-astrotypes-release
  variables:
    SPIN: release-cuda-nasm
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=true -DENABLE_NASM=true"

build-debug-fpga:
  extends: .build
  needs:
    - set-up
    - build-panda-release-fpga
    - build-astrotypes-release
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  tags:
    - pss-kitsune
  variables:
    SPIN: debug-fpga
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=debug -DENABLE_CUDA=false -DENABLE_NASM=false -DENABLE_INTEL_FPGA=true"

build-release-fpga:
  extends: .build
  needs:
    - set-up
    - build-panda-release-fpga
    - build-astrotypes-release
  tags:
    - pss-kitsune
  variables:
    SPIN: release-fpga
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=false -DENABLE_INTEL_FPGA=true"

test-debug:
  extends: .test
  needs:
    - build-debug
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug

test-debug-nasm:
  extends: .test
  needs:
    - build-debug-nasm
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-nasm

test-debug-cuda:
  extends: .test
  needs:
    - build-debug-cuda
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-cuda

test-debug-cuda-nasm:
  extends: .test
  needs:
    - build-debug-cuda-nasm
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  variables:
    SPIN: debug-cuda-nasm

test-debug-fpga:
  extends: .test
  needs:
    - build-debug-fpga
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  tags:
    - pss-kitsune
  variables:
    FPGA_TEST: true
    SPIN: debug-fpga

test-release:
  extends: .test
  needs:
    - build-release
  variables:
    SPIN: release

test-release-nasm:
  extends: .test
  needs:
    - build-release-nasm
  variables:
    SPIN: release-nasm

test-release-cuda:
  extends: .test
  needs:
    - build-release-cuda
  variables:
    SPIN: release-cuda

test-release-cuda-nasm:
  extends: .test
  needs:
    - build-release-cuda-nasm
  variables:
    SPIN: release-cuda-nasm

test-release-fpga:
  extends: .test
  needs:
    - build-release-fpga
  tags:
    - pss-kitsune
  variables:
    FPGA_TEST: true
    SPIN: release-fpga

product-test-release:
  extends: .product-test
  needs:
    - build-release
  script:
    - protest -i partial --path cheetah --outdir $CI_PROJECT_DIR/protest_results --keep --reduce --cache /raid/tvcache/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/protest_results
    expire_in: 1 week
  variables:
    SPIN: release

product-test-release-nasm:
  extends: .product-test
  needs:
    - build-release-nasm
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
  script:
    - protest -i nasm --path cheetah --outdir $CI_PROJECT_DIR/protest_results --keep --reduce --cache /raid/tvcache/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/protest_results
    expire_in: 1 week
  variables:
    SPIN: release-nasm

product-test-release-nasm-subset:
  extends: .product-test
  needs:
    - build-release-nasm
  rules:
    - if: $CI_COMMIT_BRANCH != "dev" && $CI_COMMIT_BRANCH != "main"

      # Create but do not execute pipeline for feature branches on commit,
      # or if created via the gitlab GUI
    - if: ($CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "api")
      when: manual
      allow_failure: false

      # Execute pipeline if triggered by a schedule
    - if: $CI_PIPELINE_SOURCE != "schedule"

      # Execute pipeline automatically if triggered by a MR
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - protest -i nasm subset --path cheetah --outdir $CI_PROJECT_DIR/protest_results --keep --reduce --cache /raid/tvcache/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/protest_results
    expire_in: 1 week
  variables:
    SPIN: release-nasm

package-release:
  extends: .package
  needs:
    - build-panda-release
    - build-astrotypes-release
    - build-release
    - test-release
    - product-test-release
  variables:
    SPIN: release
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=false"

package-release-cuda:
  extends: .package
  needs:
    - build-panda-release-cuda
    - build-astrotypes-release
    - build-release-cuda
    - test-release-cuda
    # When we introduce cuda product tests we should change this job dependency to
    # product-test-release-cuda
    - product-test-release
  variables:
    SPIN: release-cuda
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-cuda
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=true -DENABLE_NASM=false"

package-release-cuda-nasm:
  extends: .package
  needs:
    - build-panda-release-cuda
    - build-astrotypes-release
    - build-release-cuda-nasm
    - test-release-cuda-nasm
    # When we introduce cuda product tests we should change this job dependency to
    # product-test-release-cuda-nasm
    - product-test-release-nasm
  variables:
    SPIN: release-cuda-nasm
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-cuda-nasm
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=true -DENABLE_NASM=true"

package-release-nasm:
  extends: .package
  needs:
    - build-panda-release
    - build-astrotypes-release
    - build-release-nasm
    - test-release-nasm
    - product-test-release-nasm
  variables:
    SPIN: release-nasm
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-nasm
    CMAKE_FLAGS: "$CUDA_PATH -DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=true"

package-release-fpga:
  extends: .package
  needs:
    - build-panda-release-fpga
    - build-astrotypes-release
    - build-release-fpga
    - test-release-fpga
    # When we introduce intel_fpga product tests we should change this job dependency to
    # product-test-release-fpga
    - product-test-release
  tags:
    - pss-kitsune
  variables:
    SPIN: release-fpga
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-intelfpga
    CMAKE_FLAGS: "-DCMAKE_BUILD_TYPE=release -DENABLE_CUDA=false -DENABLE_NASM=false -DENABLE_INTEL_FPGA=true"

publish-release:
  extends: .publish
  needs:
    - package-release
  variables:
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION

publish-release-cuda:
  extends: .publish
  needs:
    - package-release-cuda
  variables:
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-cuda

publish-release-cuda-nasm:
  extends: .publish
  needs:
    - package-release-cuda-nasm
  variables:
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-cuda-nasm

publish-release-nasm:
  extends: .publish
  needs:
    - package-release-nasm
  variables:
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-nasm

publish-release-fpga:
  extends: .publish
  needs:
    - package-release-fpga
  tags:
    - pss-kitsune
  variables:
    CHEETAH_PACKAGE_NAME: cheetah-$CURRENT_CHEETAH_VERSION-intelfpga

doc-build:
  stage: build
  image: alpine
  script:
    - apk update && apk add git build-base cmake doxygen graphviz ttf-freefont
    - git clone https://gitlab.com/ska-telescope/pss/ska-pss-panda.git
    - git clone https://gitlab.com/ska-telescope/pss/ska-pss-astrotypes.git
    - mkdir build-docs
    - cd build-docs
    - cmake -DCMAKE_BUILD_TYPE=documentation -DPANDA_SOURCE_DIR=../panda/ -DASTROTYPES_SOURCE_DIR=../astrotypes ../
    - make doc
  artifacts:
    paths:
      - build-docs/
  only:
    - main

pages:
  stage: publish-docs
  image: alpine
  needs:
    - doc-build
  script:
    - mv build-docs/doc/html/ public/
  artifacts:
    paths:
      - public/
  only:
    - main

clean-up:
  stage: clean-up
  extends: .common
  tags:
    - ubuntu-22.04
  script:
    - if [ ! -z $CACHE_PATH ] && [ ! -z $CI_PIPELINE_ID ]; then rm -rf $CACHE_PATH/$CI_PIPELINE_ID*; fi
